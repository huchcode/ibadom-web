<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index()
    {
        // Some example data
        $data['title'] = "GPAC Enterprise - Profile Page";
        $data['base_url'] = base_url();

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }

        $user_info = $this->User_model->get_user_info($user_id);
        $data['user_info'] = $user_info;

        $user_detail = $this->User_model->get_user_detail($user_id,$user_info->type);
        $data['user_detail'] = $user_detail;

        if ($user_info->status == 'active') {
            $data['status_ko'] = '정상';
        } elseif ($user_info->status == 'pause' ){
            $data['status_ko'] = '일시정지';
        } elseif ($user_info->status == 'stop')  {
            $data['status_ko'] = '정지';
        } elseif ($user_info->status == 'temporary')  {
            $data['status_ko'] = '가입';
        } elseif ($user_info->status == 'termination')  {
            $data['status_ko'] = '해지';
        }
        
        // Load the template from the views directory
        if ($user_info->type == 'master') {
            $this->parser->parse("home/profile_master.tpl", $data);
        } else {
            $this->parser->parse("home/profile_person.tpl", $data);
        }      
        
    }

    public function ajax_update_profile($field=null) {

        $uid = $parameter_1 = $this->uri->segment(4);
        $user_info = $this->User_model->get_user_info($uid);

        $tableref = $this->input->post('tableref');

        if ($tableref == 'user_detail') {
            if ($user_info->type == 'master') {
                $table = 'companyInformation';
            } else {
                $table = 'employee';
            }
        } else {
            $table = 'user';
        }


        $value = $this->input->post('value');

        if (empty($value)) {
            if ($field=='admin_position' || $field=='admin_dept') {
                $updated_value = $this->User_model->update_user_per_field($field,$value,$user_info->id,$table);
            } else {
                $updated_value = $value;
            }
        } else {
            if ($field=='password') {
                $value = md5($value);
            }
            $updated_value = $this->User_model->update_user_per_field($field,$value,$user_info->id,$table);
        }

        echo $updated_value;
    }

    public function upload($uid) {

        $user_info = $this->User_model->get_user_info($uid);
        //print_r($_FILES['profile_image']);exit;
        
        if ($_FILES) {
            if ( 0 < $_FILES['profile_image']['error'] ) {
                echo 'Error: ' . $_FILES['profile_image']['error'] . '<br>';
            } else {
                $response = $this->User_model->upload_profile_pic($_FILES['profile_image'],$user_info->id);
                echo $response;
            }
        } else {
            echo 'Error: missing image file';
        }
    }

    public function company_cert($uid) {

        $user_info = $this->User_model->get_user_info($uid);
        //print_r($_FILES['company_image']);exit;
        
        if ( 0 < $_FILES['company_image']['error'] ) {
            echo 'Error: ' . $_FILES['company_image']['error'] . '<br>';
        }
        else {
            $response = $this->User_model->upload_companycert_pic($_FILES['company_image'],$user_info->id);
            echo $response;
        }
    }
}
