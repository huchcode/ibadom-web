<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Mypage extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
    }

    public function index()
    {
        // Some example data
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();
        $session = $this->session->get_userdata();
        //$data['body']  = "This is body text to show that the Smarty Parser works!";

        print_r($session);

        // Load the template from the views directory
        $this->parser->parse("home/mypage.tpl", $data);
    }
}
