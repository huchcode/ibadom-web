<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Registration extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->model('User_model');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();        

        $this->parser->parse("intro/registration_terms.tpl", $data);
    }

    public function registration_form()
    {
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();

        //Registration
        $msg = "";
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $username = $this->input->post('username'); // user.username
            $password = $this->input->post('password'); // user.password
            $confirm_password = $this->input->post('confirm_password'); // user.password
            $admin_name = $this->input->post('name');     // companyInformation.admin_name
            $admin_phone_number = $this->input->post('phone');    // companyInformation.admin_phone_number
            $admin_email = $this->input->post('user_email').'@'.$this->input->post('host_email'); // companyInformation.admin_email
            $admin_dept = $this->input->post('department'); // companyInformation.admin_dept
            $admin_position = $this->input->post('position');   // companyInformation.admin_position
            $profile_image = $this->input->post('profile_image');  // user.profile_picture
            $company_number = $this->input->post('company_number'); // companyInformation.business_registration_number
            $company_name = $this->input->post('company_name');   // user.name
            $representative_name = $this->input->post('representative_name');  // companyInformation.owner_name
            $initials = $this->input->post('initials');      // companyInformation.company_initial
            $company_image = $this->input->post('company_image');  // companyInformation.location_business_registration

            if (strcmp($password, $confirm_password) !== 0) {
                $msg = '비밀번호와 비밀번호 확인이 다릅니다.';
            }
            else
            {
                if(empty($_FILES["company_image"]["name"])) {
                    $msg = '사업자등록증사본을 첨부하세요';
                }

                if(!empty($msg))
                {
                    $data['error_msg'] = $msg;
                    $this->parser->parse("intro/registration_form.tpl", $data);
                    return;
                }

                $msg = $this->User_model->register($username, $password, $company_name, $_FILES["profile_image"]["name"], $admin_name, $admin_phone_number, $admin_email, $admin_dept, $admin_position, $company_number, $representative_name, $initials, $_FILES["company_image"]["name"]);
                if(isset($msg['error'])) {
                    $data['error_msg'] = $msg['error'];
                    $this->parser->parse("intro/registration_form.tpl", $data);
                    return;
                }
            }
        }
        //End Registration

        if(isset($msg['upload_data'])) {
            $data['title'] = 'Registration Message';
            $data['common_msg_header'] = '회원가입(마스터계정)이 접수되었습니다';
            $data['common_msg_content'] = '빠른 시일내에 요청사항을 확인한 후 이메일로 결과를 알려드리겠습니다';
            $this->parser->parse("intro/common_msg_registration.tpl", $data);           
        } else {
             $this->parser->parse("intro/registration_form.tpl", $data);
        }
    }

    public function check_username()
    {
        $data = null;
        if (isset($_POST['check_username']) && !empty($_POST['check_username'])) {
            $username = $_POST['check_username'];
            $data = $this->User_model->check_availability($username);
        }       
        return print_r($data);
    }

    public function check_initial()
    {
        $data = null;
        if (isset($_POST['check_initial']) && !empty($_POST['check_initial'])) {
            $initial = $_POST['check_initial'];
            $data = $this->User_model->check_initial($initial);
        }
        return print_r($data);
    }
}
