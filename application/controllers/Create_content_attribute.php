<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Create_content_attribute extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
    }

    public function contentsAlbum($parent_content_id="")
    {
    	$this->load->model('ContentsAlbum_model');
    	$book_id = $this->input->post('book_id');
    	$album = $this->input->post('book_id');

        $data = array(
            'content_id' => $parent_content_id
        );
        $this->session->set_userdata($data);

    	$contents = $this->ContentsAlbum_model->insert_contentsAlbum($parent_content_id, $album);

    	redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function contentsSound($parent_content_id="")
    {
    	$this->load->model('ContentsSound_model');
    	$book_id = $this->input->post('book_id');
    	$sound = $this->input->post('book_id');

        $data = array(
            'content_id' => $parent_content_id
        );
        $this->session->set_userdata($data);

    	$contents = $this->ContentsSound_model->insert_contentsSound($parent_content_id, $sound);

    	redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function contentsSubtitle($parent_content_id="")
    {
    	$this->load->model('ContentsText_model');
    	$book_id = $this->input->post('book_id');
    	$subtitle = $this->input->post('subtitle');

        $data = array(
            'content_id' => $parent_content_id
        );
        $this->session->set_userdata($data);

    	$contents = $this->ContentsText_model->insert_contentsSubtitle($parent_content_id, $subtitle);

    	redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function contentsDetail($parent_content_id="")
    {
    	$this->load->model('ContentsText_model');
    	$book_id = $this->input->post('book_id');
    	$description = $this->input->post('editor1');

        $data = array(
            'content_id' => $parent_content_id
        );
        $this->session->set_userdata($data);

    	$contents = $this->ContentsText_model->insert_contentsDetail($parent_content_id, $description);

    	redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function contentsVideo($parent_content_id="")
    {
    	$this->load->model('ContentsVideo_model');
    	$book_id = $this->input->post('book_id');
    	$video = $this->input->post('book_id');

        $data = array(
            'content_id' => $parent_content_id
        );
        $this->session->set_userdata($data);

    	$contents = $this->ContentsVideo_model->insert_contentsVideo($parent_content_id, $video);

    	redirect('/create_content/index/'.$book_id, 'refresh');
    }
}