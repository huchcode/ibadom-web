<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class My_created_contents extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->model('Book_model');
        $this->load->model('User_model');
    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        $username = $this->session->userdata('username');        
        $master_user_id = $this->session->userdata('master_user_id');
        $profile_picture = $this->session->userdata('profile_picture');
        $item_id = $this->session->userdata('item_id');
        $this->load->model('Book_model');
        $contents = $this->Book_model->created_contents_list($user_id);
        $iconContents = $this->get_icon_contents();
        $bgMindmap = $this->get_background_mindmap();

        $data['title'] = "GPAC Enterprises";
        $data['base_url'] = base_url();
        $data['item_id'] = $item_id;
        $data['user_id'] = $user_id;
        $data['master_id'] = $master_user_id;
        $data['profile_picture'] = $profile_picture;
        $data['contents'] = array($contents);
        $data['iconContents'] = $iconContents;
        $data['bgMindmap'] = $bgMindmap;

        $this->parser->parse("educationcontents/my_content_list.tpl", $data);
    }

    public function change_status($book_id="")
    {
        $user_id = $this->session->userdata('user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }
        
        $contents = $this->Book_model->change_status_book($book_id);

        redirect('/my_created_contents', 'refresh');
    }

    public function get_icon_contents()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['icons_name'])) {
            $result = $this->Book_model->get_icon_contents($master_user_id, $_POST['icons_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_icon_contents($master_user_id);
        }

        return $result;
    }

    public function get_background_mindmap()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['bgmindmap_name'])) {
            $result = $this->Book_model->get_background_mindmap($master_user_id, $_POST['bgmindmap_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_background_mindmap($master_user_id);
        }

        return $result;
    }

    public function get_pictogram_contents()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['pictogram_name'])) {
            $result = $this->Book_model->get_pictogram_contents($master_user_id, $_POST['pictogram_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_pictogram_contents($master_user_id);
        }

        return $result;
    }
}
