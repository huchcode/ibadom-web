<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Dept_management extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index()
    {
        // Some example data
        $data['title'] = "GPAC Enterprise - Department Manegement";
        $data['base_url'] = base_url();

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }

        $user_info = $this->User_model->get_user_info($user_id);
        if ($user_info->role!='master' && $user_info->role!='admin') {
            redirect('/my_created_contents', 'refresh');
        }

        $data['department_list'] = $this->User_model->get_department_list($user_info->master_user_id);

        //print_r($data);

        $this->parser->parse("toregisterstaffuser/dept_list.tpl", $data);
    }

    public function ajax_get_dept($dept_id) {
        $dept = $this->User_model->get_department($dept_id);
        echo json_encode($dept);
    }

    public function ajax_delete_dept($dept_id) {
        $dept = $this->User_model->delete_department($dept_id);
        echo $dept_id;
    }

    public function add()
    {

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }
        $user_info = $this->User_model->get_user_info($user_id);

        if(count($this->input->post())>0) {

            if (!empty($this->input->post('dept_name')) && !empty($this->input->post('dept_code'))) {
                $this->User_model->add_department($user_info->master_user_id, $this->input->post());
            }
        }

        redirect('/dept_management', 'refresh');

    }

    public function update()
    {

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }
        $user_info = $this->User_model->get_user_info($user_id);

        if( count($this->input->post()) > 0 ) {

            $this->User_model->update_department($this->input->post());
        }

        redirect('/dept_management', 'refresh');

    }

    public function import()
    {

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }
        $user_info = $this->User_model->get_user_info($user_id);

        if( count($this->input->post()) > 0 ) {
            $this->User_model->add_department_batch($user_info->master_user_id, $this->input->post());
        }

        redirect('/dept_management', 'refresh');

    }

}
