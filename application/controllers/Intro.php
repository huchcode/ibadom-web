<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Intro extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->helper('file');
    }

    public function index()
    {
        // Some example data
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();
        //$data['body']  = "This is body text to show that the Smarty Parser works!";

        $username = $this->session->userdata('username');
        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');
        $profile_picture = $this->session->userdata('profile_picture');

        $data['username'] = $username;
        $data['user_id'] = $user_id;
        $data['master_id'] = $master_user_id;
        $data['profile_picture'] = $profile_picture;

        // Load the template from the views directory
        $this->parser->parse("intro/intro.tpl", $data);
    }
}
