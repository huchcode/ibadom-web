<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Customercenter extends CI_Controller {

    public function __construct() {
        parent::__construct();

        // Ideally you would autoload the parser
        $this->load->model('Book_model');
        $this->load->model('LearningAssign_model');
        $this->load->library('mongo_db');
    }

    public function index() {

        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        $arr = array('master_user_id' => intval($master_user_id));
        $collection = $this->mongo_db->db->selectCollection('announcement');
        $find_result = $collection->find($arr);
        $find_result->sort(array('created' => -1));

        $data['announcement'] = $find_result;
        $data['title'] = "Content Assign Content";
        $data['base_url'] = base_url();
        
        $this->parser->parse("customercenter/cs_home.tpl", $data);
    }

    public function announcement() {

        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $datetime = $this->input->post('datetime');
            $datetime = strtotime($datetime);
            $mdate = date("Y-m-d H:i:s");

            $collection = $this->mongo_db->db->selectCollection('announcement');
            $collection->insert(array('master_user_id'=>intval($master_user_id),'title'=>$title,'description'=>$description,'start_timestamp'=>$datetime, 'created'=>strtotime($mdate)));
        }

        $arr = array('master_user_id' => intval($master_user_id));
        $collection = $this->mongo_db->db->selectCollection('announcement');
        $find_result = $collection->find($arr);
        $find_result->sort(array('created' => -1));

        //print_r($find_result); exit;

        $data['announcement'] = $find_result;
        $data['title'] = "Content Assign Content";
        $data['base_url'] = base_url();
        
        $this->parser->parse("customercenter/announcement.tpl", $data);
    }

    public function announcement_detail($_id="") {

        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        if(empty($_id))
        {
            redirect('/', 'refresh');
        }

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $id = $this->input->post('id');
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $datetime = $this->input->post('datetime');
            $datetime = strtotime($datetime);

            $collection = $this->mongo_db->db->selectCollection('announcement');
            $collection->update(
                array('_id' => new MongoId($id)),
                array('$set' => array('title' => $title, 'description' => $description, 'start_timestamp' => $datetime))
            );
        }

        $collection = $this->mongo_db->db->selectCollection('announcement');
        $find_result = $collection->find(array('_id' => new MongoId($_id), 'master_user_id'=>intval($master_user_id)));

        $data['announcement'] = $find_result;
        $data['title'] = "Content Assign Content";
        $data['base_url'] = base_url();
        
        $this->parser->parse("customercenter/announcement_detail.tpl", $data);
    }

    public function delete_announcement() {
        $id = $this->input->post('id');

        //print_r($id); exit;

        $collection = $this->mongo_db->db->selectCollection('announcement');
        $collection->remove(array('_id' => new MongoId($id)));

        redirect('/customercenter/announcement', 'refresh');
    }
}