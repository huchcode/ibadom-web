<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Forgot_password extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index()
    {
        $password_reset_success = $this->session->userdata('password_reset_success');
        if ($password_reset_success) {
            redirect('/login', 'refresh');
            return;
        }
        // Some example data
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();
        $data['errormessage'] = null;

        $username = $this->input->post('username');
        $email = $this->input->post('email');
        $name = $this->input->post('name');

        $data['username'] = $username;
        $data['email'] = $email;
        $data['name'] = $name;

        if( count($this->input->post()) > 0 ) {

            if (empty($this->input->post('username'))) {

                $data['errormessage'] = "아이디를 입력하세요";
                $this->parser->parse("intro/forgot_password.tpl", $data);

            } elseif (empty($this->input->post('email'))) {

                $data['errormessage'] = "이메일을 입력하세요";
                $this->parser->parse("intro/forgot_password.tpl", $data);

            } elseif (empty($this->input->post('name'))) {

                $data['errormessage'] = "성명을 입력하세요";
                $this->parser->parse("intro/forgot_password.tpl", $data);

            } else {

                $user_info = $this->User_model->get_user_info_using_username($username);
                
                if (!empty($user_info)) {

                    $user_detail = $this->User_model->get_user_detail($user_info->id,$user_info->type);

                    if ($email != $user_detail->admin_email) {
                        $data['errormessage'] = "입력 정보와 일치하는 계정이 없습니다.";
                        $this->parser->parse("intro/forgot_password.tpl", $data);
                    } elseif ($name != $user_info->name) {
                        $data['errormessage'] = "입력 정보와 일치하는 계정이 없습니다.";
                        $this->parser->parse("intro/forgot_password.tpl", $data);
                    } else {

                        $temp_pw = $this->User_model->get_random_password(8);
                        $new_password = $this->User_model->reset_user_password($user_info->id,$temp_pw);
                        
                        //print_r($new_password);

                        $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'mail.huchcode.com',
                            'smtp_port' => 25,
                            'smtp_user' => 'noreply@huchcode.com',
                            'smtp_pass' => 'smith2k15',
                            'mailtype'  => 'html'
                        );
                        $this->load->library('email', $config);

                        $this->email->set_newline("\r\n");

                        $this->email->from('noreply@huchcode.com', 'Huchcode');
                        $this->email->to($user_detail->admin_email, $user_info->username); 
                        //$this->email->cc(''); 
                        $this->email->bcc('bryan615@gmail.com'); 

                        $this->email->subject('GPAC Enterprise password has been reset');
                        $this->email->message('Your new password is '.$new_password);  

                        $this->email->send();

                        //echo $this->email->print_debugger();

                        $this->session->set_userdata('password_reset_success', true);
                        $data['common_msg_header'] = "암호 재설정";
                        $data['common_msg_content'] = "이 메일로 새로운 비밀번호를 보내드렸습니다. 확인하시고 다시 접속 바랍니다.";
                        $this->parser->parse("intro/forgot_password_success.tpl", $data);
                    }

                } else {
                    $data['errormessage'] = "입력 정보와 일치하는 계정이 없습니다.";
                    $this->parser->parse("intro/forgot_password.tpl", $data);
                }
            }

        } else {
            
            $this->parser->parse("intro/forgot_password.tpl", $data);

        }
    }
}
