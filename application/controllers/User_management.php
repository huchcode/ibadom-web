<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class User_management extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index($msg="")
    {
        // Some example data
        $data['title'] = "GPAC Enterprise - User Manegement";
        $data['base_url'] = base_url();
        if ($msg=='upload_error') {
            $data['msg'] = 'There was an error during CSV file import. Please check your CSV file for duplicates and missing fields.';
        } else {
            $data['msg'] = '';
        }

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }

        $user_info = $this->User_model->get_user_info($user_id);
        // if ($user_info->role!='master' && $user_info->role!='admin') {
        //     redirect('/my_created_contents', 'refresh');
        // }

        if( count($this->input->post()) > 0 ) {

            $data['search_name'] = $this->input->post('search_name');
            $data['search_department_id'] = $this->input->post('search_department_id');

            $data['employee_list'] = $this->User_model->get_employee_list($user_info->master_user_id,$data['search_name'],$data['search_department_id']);
        } else {
            $data['search_name'] = null;
            $data['search_department_id'] = 0;
            $data['employee_list'] = $this->User_model->get_employee_list($user_info->master_user_id);
        }
        $data['department_list'] = $this->User_model->get_department_list($user_info->master_user_id);

        //print_r($data);

        $this->parser->parse("toregisterstaffuser/user_list.tpl", $data);
    }


    public function search_user()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['search_name']) && !empty($_POST['search_name'])) {
            $result = $this->User_model->search_user_name($master_user_id, $_POST['search_name']);
        }

        return $result;
    }

    public function ajax_get_user($user_id) {
        $userinfo['user'] = $this->User_model->get_user_info($user_id);
        $userinfo['employee'] = $this->User_model->get_user_detail($user_id,'general');
        echo json_encode($userinfo);
    }

    public function ajax_delete_user($user_id) {
        $this->User_model->delete_employee($user_id);
        echo $user_id;
    }

    public function ajax_check_duplicate_user($field,$value) {

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->User_model->get_user_info($user_id);

        $userinfo = $this->User_model->check_duplicate_user($field,$value,$user_info->master_user_id);
        echo json_encode($userinfo);
    }

    public function add()
    {

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }
        $user_info = $this->User_model->get_user_info($user_id);

        if( count($this->input->post()) > 0 ) {

            $response = $this->User_model->add_employee($user_info->master_user_id, $this->input->post());
        }

        redirect('/user_management', 'refresh');

    }

    public function update()
    {

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }
        $user_info = $this->User_model->get_user_info($user_id);

        if( count($this->input->post()) > 0 ) {
            $this->User_model->update_employee($this->input->post());
        }

        redirect('/user_management', 'refresh');

    }

    public function import()
    {

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }
        $user_info = $this->User_model->get_user_info($user_id);

        if( count($this->input->post()) > 0 ) {
            $response = $this->User_model->add_employee_batch($user_info->master_user_id, $this->input->post());

            if ($response) {
                redirect('/user_management', 'refresh');
            } else {
                redirect('/user_management/index/upload_error', 'refresh');
            }
        }

        

    }
}
