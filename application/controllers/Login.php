<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Login extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        //$this->load->controller('Created_contents');
        $this->load->library('user_agent');
        $this->load->helper('cookie');
    }

    public function index()
    {
        $login_username = null;
        $login_password = null;
        //Login
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();
        $this->session->set_userdata('password_reset_success', false);

        $remember_me = get_cookie('remember_me');

        //print_r($remember_me); exit;

        if($remember_me)
        {
            $login_username = get_cookie('login_username');
            $login_password = get_cookie('login_password');
        }

        $msg = "";
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {   

            $username = $this->input->post('username'); // user.username
            $password = $this->input->post('password'); // user.password         

            $remember = $this->input->post('remember_me');
            if($remember)
            {
                $this->input->set_cookie('remember_me', true, 86400*30);
                $login_username = $this->input->set_cookie('login_username', $username, 86400*30);
                $login_password = $this->input->set_cookie('login_password', $password, 86400*30);
            }
            else
            {
                delete_cookie("login_username");
                delete_cookie("login_password");
                delete_cookie("remember_me");
            }

            $username = $this->input->post('username'); // user.username
            $password = $this->input->post('password'); // user.password            

            if ($password == '' || $username == '') {
                //print_r('empty'); exit;
                $msg = '사용자 이름과 암호를 모두 입력하십시오.';
            }
            else
            {
                $this->load->model('User_model');
                $msg = $this->User_model->login($username, $password);
                //print_r('ss'); exit;
            }
        }

        $username = $this->session->userdata('username');

        if(!empty($username))
        {
            // print_r($_SESSION); exit;
            $role = $this->session->userdata('role');
            if($role == 'master') {
                redirect('/user_management', 'refresh');
            }

            redirect('/my_created_contents', 'refresh');

            return;
        }
        else
        {
            $data['errormsg'] = $msg;
            $data['username'] = $login_username;
            $data['password'] = $login_password;
            $data['remember_me'] = $remember_me;
            $this->parser->parse("intro/login.tpl", $data);
            return;
        }
        
        //End Login

        if(!empty($msg))
        {
            redirect('/my_created_contents', 'refresh');
            return;
        }
        else
        {
            $data['errormsg'] = $msg;
            $this->parser->parse("intro/login.tpl", $data);
        }
    }
}
