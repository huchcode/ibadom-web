<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Examination_list extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Contents_model');
        $this->load->model('User_model');
    }

    public function index($book_id=null,$listsort='asc')
    {
        if(empty($book_id))
        {
            redirect('/my_created_contents', 'refresh');
        }

        // Some example data
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();
        $data['list_sort'] = $listsort;
        $data['book_id'] = $book_id;

        $user_id = $this->session->userdata('user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        if( count($this->input->post()) > 0 ) {

            $post['book_id'] = $book_id;
            $post['content_id'] = $this->input->post('content_id');
            $examination_type = $this->input->post('question_type');
            $post['input_type'] = $examination_type;
            $post['question'] = $this->input->post('question');
            $post['picture'] = $this->input->post('attached_photo_url');

            if ($examination_type == 'multiple') {
                $post['examination_answer_text'] = $this->input->post('answermulti');
                $post['examination_answer_picture'] = $this->input->post('answerimagemulti');
                $post['examination_answer_correct_flag'] = $this->input->post('answermultitype');
            } else {
                $post['examination_answer_text'] = $this->input->post('answertext');
                $post['examination_answer_picture'] = $this->input->post('answerimagetext');
                $post['examination_answer_correct_flag'] = $this->input->post('answertexttype');

            }
            $examination_list = $this->Contents_model->get_examination_list($book_id,'asc');
            $sort = (count($examination_list)/10);
            $data['contents'] = $this->Contents_model->add_examination_list($post,$sort);

            redirect('/examination_list/index/'.$book_id, 'refresh');
        }


        $data['contents'] = $this->Contents_model->get_book_contents($book_id);
        $data['examination_list'] = $this->Contents_model->get_examination_list($book_id,$listsort);

        // Load the template from the views directory
        $this->parser->parse("educationcontents/examination_list.tpl", $data);
    }

    public function update() {

        $user_id = $this->session->userdata('user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        if( count($this->input->post()) > 0 ) {
            //print_r($this->input->post());exit;

            $post['examination_id'] = $this->input->post('examination_id');
            $post['book_id'] = $this->input->post('book_id');
            $post['content_id'] = $this->input->post('content_id');
            $examination_type = $this->input->post('question_type');
            $post['input_type'] = $examination_type;
            $post['question'] = $this->input->post('question');
            $post['picture'] = $this->input->post('attached_photo_url');

            if ($examination_type == 'multiple') {
                $post['examination_answer_text'] = $this->input->post('answermulti');
                $post['examination_answer_picture'] = $this->input->post('answerimagemulti');
                $post['examination_answer_correct_flag'] = $this->input->post('answermultitype');
            } else {
                $post['examination_answer_text'] = $this->input->post('answertext');
                $post['examination_answer_picture'] = $this->input->post('answerimagetext');
                $post['examination_answer_correct_flag'] = $this->input->post('answertexttype');
            }
            $data['contents'] = $this->Contents_model->update_examination_list($post);

            redirect('/examination_list/index/'.$post['book_id'], 'refresh');
        }

    }

    public function ajax_get_questions($examination_id) {
        $exam_answers = $this->Contents_model->get_exam_questions($examination_id);
        echo json_encode($exam_answers);
    }


    public function ajax_delete_exam($examination_id) {
        $response = $this->Contents_model->delete_examination($examination_id);
        echo $response;
    }

    public function upload($examination_id) {
        
        if ($_FILES) {
            if ( 0 < $_FILES['attached_image']['error'] ) {
                echo 'Error: ' . $_FILES['attached_image']['error'] . '<br>';
            } else {
                $response = $this->User_model->upload_exam_attach_image($_FILES['attached_image'],$examination_id);
                echo $response;
            }
        } else {
            echo 'Error: missing attached_image file';
        }
    }
}
