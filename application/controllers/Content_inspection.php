<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

class Content_inspection extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        $this->load->model('Book_model');
    }

    public function index()
    {

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        } else {
            //redirect('/content_inspection/search/requested', 'refresh');
        }
        
        // Some example data
        $data['title'] = "Content Inspection";
        $data['base_url'] = base_url();
        $data['book_status'] = 'requested';
        $data['department_id'] = 0;

        $data['department_options'] =$this->Book_model->get_department_info();
        $data['content_inspection_list'] = $this->Book_model->get_content_inspection_list('requested');

        // Load the template from the views directory
        $this->parser->parse("educationcontents/content_inspection_list.tpl", $data);
    }

    public function search($book_status=null,$department_id=null)
    {

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }
        
        // Some example data
        $data['title'] = "Content Inspection";
        $data['base_url'] = base_url();
        $data['book_status'] = $book_status;
        $data['department_id'] = $department_id;

        $data['department_options'] =$this->Book_model->get_department_info();
        $data['content_inspection_list'] = $this->Book_model->get_content_inspection_list($book_status,$department_id);

        // Load the template from the views directory
        $this->parser->parse("educationcontents/content_inspection_list.tpl", $data);
    }

    public function update_status() {

        $book_id = $this->input->post('book_id');
        $status = $this->input->post('status');

        $data = $this->Book_model->change_book_status($book_id,$status);

        if ($data) {
            return $status;
        } else {
            return 'Error';
        }

    }
}
