<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Service_change extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
    }

    public function index($uid)
    {

        if (empty($uid)) {
            redirect('/login', 'refresh');
        }
        $uid_info = $this->User_model->get_user_info($uid);

        // Some example data
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();

        $user_id = $this->session->userdata('user_id');
        $user_info = $this->User_model->get_user_info($user_id);
        $data['user_info'] = $user_info;

        if ($user_info->type != 'master') {
            redirect('/profile', 'refresh');
        }

        //print_r($user_info);exit;

        $post_state = $this->input->post('state');
        $post_reason = $this->input->post('reason');

        if ($post_state == 'pause') {
            
            $this->User_model->update_user_status($user_id,$post_state,$post_reason);
            $this->parser->parse("home/success_stop.tpl", $data);
            
        } elseif ($post_state == 'termination') {

            $this->User_model->update_user_status($user_id,$post_state,$post_reason);
            $this->parser->parse("home/success_stop.tpl", $data);
            
        } elseif ($post_state == 'active') {

            $this->User_model->update_user_status($user_id,$post_state,"");
            $this->parser->parse("home/success_normal.tpl", $data);

        } else {
            
            // Load the template from the views directory
            if ($uid_info->status == 'active') {
                $data['status_ko'] = '정상';
                $this->parser->parse("home/service_change_stop.tpl", $data);
            } elseif ($uid_info->status == 'pause' ){
                $data['status_ko'] = '일시정지';
                $this->parser->parse("home/service_change_normal.tpl", $data);
            } elseif ($uid_info->status == 'termination')  {
                $data['status_ko'] = '해지';
                $this->parser->parse("home/service_change_normal.tpl", $data);
            } else {
                redirect('/', 'refresh');
            }

        }
        
    }
}
