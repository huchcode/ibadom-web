<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Create_content extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->model('Book_model');
        $this->load->model('Contents_model');
        $this->load->model('Contentsalbum_model');
        $this->load->model('Contentsvideo_model');
        $this->load->model('Contentssound_model');
    }

    public function index($book_id="") {
        
        if(!empty($_FILES))
        {
            print_r($_FILES);
            exit;
        }

        $user_id = $this->session->userdata('user_id');        
        $company_initial = $this->session->userdata('company_initial');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }
        
        if(empty($book_id)) {
            print_r('error book_id'); exit;
            redirect('/', 'refresh');
        }

        $isValid = $this->Book_model->check_valid_book($book_id);

        //print_r($isValid);exit;

        if(!$isValid) {
            redirect('/', 'refresh');
        }

        $isDeletable = $this->session->userdata('isDeletable');
        if($isDeletable ==  null) {
            $data = array(
                'isDeletable' => ''
            );
            $this->session->set_userdata($data);
        }

        $username = $this->session->userdata('username');
        //$user_id = $this->session->userdata('user_id');
        $book_info = $this->Book_model->get_book_info($book_id);
        $contents = $this->Contents_model->get_contents($book_id);

        $parent_content_id = $this->Contents_model->get_parent_content_id($book_id);
        $children = $this->Contents_model->child_contents($book_id);
        $tree = $this->Contents_model->parseTree($children, $parent_content_id);
        $author_id = $this->Book_model->get_book_author_id($book_id);

        //$pictogram = $this->get_pictogram_contents();
        $pictogramBackground = $this->get_pictogramBackground_contents();
        $iconContents = $this->get_icon_contents();
        $bgMindmap = $this->get_background_mindmap();
        //$data['pictogram'] = $pictogram;
        $data['pictogramBackground'] = $pictogramBackground;
        $data['iconContents'] = $iconContents;
        $data['bgMindmap'] = $bgMindmap;

        //print_r(json_encode($tree)); exit;
        // var_dump($tree);exit;

        $data['title'] = "Developing Contents";
        $data['base_url'] = base_url();
        $data['book_info'] = $book_info;
        $data['contents'] = $contents;
        $data['children'] = $tree;
        $data['book_id'] = $book_id;
        $data['author_id'] = $author_id;
        $data['path'] = strtoupper($company_initial);
        //var_dump($contents);
        $data['error'] = '';

        $this->session->set_userdata('book_id',$book_id);
        
        $this->parser->parse("educationcontents/create_content.tpl", $data);
    }

    public function set_id() {
        $this->session->set_userdata('book_id',$_POST[item_id]);
    }

    public function parent_id() {
        $this->session->set_userdata('parent_content_id',$_POST[item_id]);
    }

    public function edit_title() {
        $edit_title = $this->input->post('edit_title');
        $book_id = $this->input->post('book_id');
        $book_info = $this->Book_model->edit_title($edit_title, $book_id);

        $data = array(
            'content_id' => 0
        );
        $this->session->set_userdata($data);

        redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function edit_description() {
        $edit_description = $this->input->post('edit_description');
        $book_id = $this->input->post('book_id');
        $book_info = $this->Book_model->edit_description($edit_description, $book_id);

        $data = array(
            'content_id' => 0
        );
        $this->session->set_userdata($data);

        redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function insert_content($parent_content_id="") {
        $title = $this->input->post('title');
        $subtitle = $this->input->post('subtitle');
        $book_id = $this->input->post('book_id');

        $data = array(
            'content_id' => $parent_content_id
        );
        $this->session->set_userdata($data);

        if( ! empty($title)) {
            $contents = $this->Contents_model->insert_contents($title, $subtitle, $book_id, $parent_content_id);
        }

        redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function insert_contentsAlbum($content_id = 0) {
        $book_id = $this->input->post('book_id');

        $data = array(
            'content_id' => $content_id
        );
        $this->session->set_userdata($data);

        if(empty($_FILES['img_content1']['name']) && empty($_FILES['img_content2']['name']) && empty($_FILES['img_content3']['name']))
        {
            if(empty($_POST['img_delete1']) && empty($_POST['img_delete2']) && empty($_POST['img_delete3']))
            {
                redirect('/create_content/index/'.$book_id, 'refresh');
            }
        }

        $contents = $this->Contentsalbum_model->insert_contentsAlbum($content_id);

        if(isset($contents['error'])) {
            $error = $contents['error'];
            $company_initial = $this->session->userdata('company_initial');
            $book_info = $this->Book_model->get_book_info($book_id);
            $contents = $this->Contents_model->get_contents($book_id);
            $parent_content_id = $this->Contents_model->get_parent_content_id($book_id);
            $children = $this->Contents_model->child_contents($book_id);
            $tree = $this->Contents_model->parseTree($children, $parent_content_id);
            $pictogram = $this->get_pictogram_contents();
            $pictogramBackground = $this->get_pictogramBackground_contents();
            $iconContents = $this->get_icon_contents();
            $bgMindmap = $this->get_background_mindmap();
            $data['pictogram'] = $pictogram;
            $data['pictogramBackground'] = $pictogramBackground;
            $data['iconContents'] = $iconContents;
            $data['bgMindmap'] = $bgMindmap;
            $data['title'] = "Developing Contents";
            $data['base_url'] = base_url();
            $data['book_info'] = $book_info;
            $data['contents'] = $contents;
            $data['children'] = $tree;
            $data['book_id'] = $book_id;
            $data['path'] = strtoupper($company_initial);
            $data['error'] = $error;

            $this->parser->parse("educationcontents/create_content.tpl", $data);
        } else {
            redirect('/create_content/index/'.$book_id, 'refresh');
        }
    }

    public function insert_contentsVideo($content_id = 0) {
        $book_id = $this->input->post('book_id');

        $data = array(
            'content_id' => $content_id
        );
        $this->session->set_userdata($data);

        if(empty($_FILES['vid_content1']['name']) && empty($_FILES['vid_content2']['name']) && empty($_FILES['vid_content3']['name']))
        {
            if(empty($_POST['vid_delete1']) && empty($_POST['vid_delete2']) && empty($_POST['vid_delete3']))
            {
                redirect('/create_content/index/'.$book_id, 'refresh');
            }
        }

        $contents = $this->Contentsvideo_model->insert_contentsVideo($content_id);

        if(isset($contents['error'])) {
            $error = $contents['error'];
            $company_initial = $this->session->userdata('company_initial');
            $book_info = $this->Book_model->get_book_info($book_id);
            $contents = $this->Contents_model->get_contents($book_id);
            $parent_content_id = $this->Contents_model->get_parent_content_id($book_id);
            $children = $this->Contents_model->child_contents($book_id);
            $tree = $this->Contents_model->parseTree($children, $parent_content_id);
            $pictogram = $this->get_pictogram_contents();
            $pictogramBackground = $this->get_pictogramBackground_contents();
            $iconContents = $this->get_icon_contents();
            $bgMindmap = $this->get_background_mindmap();
            $data['pictogram'] = $pictogram;
            $data['pictogramBackground'] = $pictogramBackground;
            $data['iconContents'] = $iconContents;
            $data['bgMindmap'] = $bgMindmap;
            $data['title'] = "Developing Contents";
            $data['base_url'] = base_url();
            $data['book_info'] = $book_info;
            $data['contents'] = $contents;
            $data['children'] = $tree;
            $data['book_id'] = $book_id;
            $data['path'] = strtoupper($company_initial);
            $data['error'] = $error;

            $this->parser->parse("educationcontents/create_content.tpl", $data);
        } else {
            redirect('/create_content/index/'.$book_id, 'refresh');
        }
    }

    public function insert_contentsSound($content_id = 0) {
        //print_r($_POST); exit;
        
        $book_id = $this->input->post('book_id');

        $data = array(
            'content_id' => $content_id
        );
        $this->session->set_userdata($data);

        if(empty($_FILES['sound_content1']['name']))// && empty($_FILES['img_content2']['name']) && empty($_FILES['img_content3']['name']))
        {
            if(empty($_POST['sound_delete1']))
            {
                redirect('/create_content/index/'.$book_id, 'refresh');
            }
        }

        $contents = $this->Contentssound_model->insert_contentsSound($content_id);

        if(isset($contents['error'])) {
            $error = $contents['error'];
            $company_initial = $this->session->userdata('company_initial');
            $book_info = $this->Book_model->get_book_info($book_id);
            $contents = $this->Contents_model->get_contents($book_id);
            $parent_content_id = $this->Contents_model->get_parent_content_id($book_id);
            $children = $this->Contents_model->child_contents($book_id);
            $tree = $this->Contents_model->parseTree($children, $parent_content_id);
            $pictogram = $this->get_pictogram_contents();
            $pictogramBackground = $this->get_pictogramBackground_contents();
            $iconContents = $this->get_icon_contents();
            $bgMindmap = $this->get_background_mindmap();
            $data['pictogram'] = $pictogram;
            $data['pictogramBackground'] = $pictogramBackground;
            $data['iconContents'] = $iconContents;
            $data['bgMindmap'] = $bgMindmap;
            $data['title'] = "Developing Contents";
            $data['base_url'] = base_url();
            $data['book_info'] = $book_info;
            $data['contents'] = $contents;
            $data['children'] = $tree;
            $data['book_id'] = $book_id;
            $data['path'] = strtoupper($company_initial);
            $data['error'] = $error;

            $this->parser->parse("educationcontents/create_content.tpl", $data);
        } else {
            redirect('/create_content/index/'.$book_id, 'refresh');
        }
    }

    public function get_contentsAlbum($content_id = 0) {
        return $this->Contentsalbum_model->get_contentsAlbum($content_id);
    }

    public function get_contentsVideo($content_id = 0) {
        return $this->Contentsvideo_model->get_contentsVideo($content_id);
    }

    public function get_contentsSound($content_id = 0) {
        return $this->Contentssound_model->get_contentsSound($content_id);
    }

    public function get_contentsTitle($content_id = 0) {
        return $this->Contents_model->get_contentsTitle($content_id);
    }

    public function get_contentsDescription($content_id = 0) {
        return $this->Contents_model->get_contentsDescription($content_id);
    }

    public function get_contentsSubtitle($content_id = 0) {
        return $this->Contents_model->get_contentsSubtitle($content_id);
    }

    public function get_contentsPictogram($content_id = 0) {
        return $this->Contents_model->get_contentsPictogram($content_id);
    }

    public function get_contentsPictogramBackground($content_id = 0) {
        return $this->Contents_model->get_contentsPictogramBackground($content_id);
    }

    public function update_content($parent_content_id="") {
        $title_attribute = $this->input->post('title_attribute');
        $book_id = $this->input->post('book_id');

        $data = array(
            'content_id' => $parent_content_id
        );
        $this->session->set_userdata($data);

        if( ! empty($title_attribute)) {
            $contents = $this->Contents_model->update_content($parent_content_id, $title_attribute);
        }

        redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function insert_child_content($parent_content_id="") {
        //echo $parent_content_id;
        exit;
        $title = $this->input->post('title');
        $subtitle = $this->input->post('subtitle');
        $book_id = $this->input->post('book_id');

        $data = array(
            'content_id' => $parent_content_id
        );
        $this->session->set_userdata($data);

        if( !empty($title)) {
            
            $contents = $this->Contents_model->insert_child_contents($title, $subtitle, $book_id, $parent_content_id);
        }



        //redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function delete_book() {
        //print_r($_POST); exit;
        $book_id = $this->input->post('book_id');

        if(empty($book_id)) {
            redirect('/');
        }
        
        $isDeletable = $this->Book_model->isDeletable($book_id);

        if($isDeletable) {
            $this->Contents_model->delete_book($book_id);
        }

        redirect('/my_created_contents/', 'refresh');
    }

    public function delete_content($id="") {
        if(empty($id)) {
            redirect('/');
        }

        $book_id = $this->input->post('book_id');

        $isDeletable = $this->Book_model->isDeletable($book_id);

        if($isDeletable) {
            $this->Contents_model->delete_content($id);
            $data = array(
                'isDeletable' => ''
            );
            $this->session->set_userdata($data);
        } else {
            $data = array(
                'isDeletable' => '출시완료된 컨텐츠는 삭제할 수 없습니다.'
            );
            $this->session->set_userdata($data);
        }

        redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function delete_child($id="") {
        //var_dump($_POST); exit;
        //print_r($id); exit;
        if(empty($id)) {
            redirect('/');
        }

        $book_id = $this->input->post('book_id');

        $isDeletable = $this->Book_model->isDeletable($book_id);        

        if($isDeletable) {
            $this->Contents_model->delete_child($id);
        }

        redirect('/create_content/index/'.$book_id, 'refresh');
    }

    public function get_pictogram_contents() {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['icons_name'])) {
            $result = $this->Book_model->get_pictogram_contents($master_user_id, $_POST['icons_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_pictogram_contents($master_user_id);
        }

        return $result;
    }

    public function get_pictogram_contents1() {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['icons_name'])) {
            $result = $this->Book_model->get_pictogram_contents($master_user_id, $_POST['icons_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_pictogram_contents($master_user_id);
            print_r(json_encode($result));
        }

        return $result;
    }

    public function get_pictogramBackground_contents() {
        $result = null;
        if (isset($_POST['icons_name'])) {
            $result = $this->Book_model->get_pictogramBackground_contents();
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_pictogramBackground_contents();
        }

        return $result;
    }

    public function get_icon_contents() {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['icons_name'])) {
            $result = $this->Book_model->get_icon_contents($master_user_id, $_POST['icons_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_icon_contents($master_user_id);
        }

        return $result;
    }

    public function get_background_mindmap() {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['bgmindmap_name'])) {
            $result = $this->Book_model->get_background_mindmap($master_user_id, $_POST['bgmindmap_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_background_mindmap($master_user_id);
        }

        return $result;
    }

    public function update_content_pictogram() {
        $result = null;
        if (isset($_POST['content_id']) && isset($_POST['icon_id'])) {
            $result = $this->Book_model->update_content_pictogram($_POST['content_id'], $_POST['icon_id']);
        }

        return $result;
    }

    public function update_content_pictogrambg() {
        $result = null;
        if (isset($_POST['content_id']) && isset($_POST['icon_id'])) {
            $result = $this->Book_model->update_content_pictogrambg($_POST['content_id'], $_POST['icon_id']);
        }

        return $result;
    }

    public function update_pictogram_bg() {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['book_id']) && isset($_POST['icon_id'])) {
            $result = $this->Book_model->update_pictogram_bg($_POST['book_id'], $_POST['icon_id']);
        }

        return $result;
    }

    public function update_icon() {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['book_id']) && isset($_POST['icon_id'])) {
            $result = $this->Book_model->update_icon($_POST['book_id'], $_POST['icon_id']);
        }

        return $result;
    }

    public function update_bgmindmap() {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['book_id']) && isset($_POST['bgmindmap_id'])) {
            $result = $this->Book_model->update_bgmindmap($_POST['book_id'], $_POST['bgmindmap_id']);
        }

        return $result;
    }

    public function update_content_sort()
    {
        $data = array(
            'content_id' => $_POST['content_id']
        );
        $this->session->set_userdata($data);

        //$ret = false;
        
        if (isset($_POST['position']) && isset($_POST['content_id'])) {
            $this->Contents_model->change_contents_sort($_POST['position'], $_POST['content_id']);
        }

        //return print_r($ret);
    }

    public function attribute_preview()
    {
        $data = null;

        if (isset($_POST['position']) && isset($_POST['content_id'])) {
            $data = $this->Contents_model->get_attribute_preview($_POST['position'], $_POST['content_id']);
        }

        return print_r(json_encode($data));
    }

    public function clear_session() {
        $data = array(
            'isDeletable' => ''
        );
        $this->session->set_userdata($data);
    }
}