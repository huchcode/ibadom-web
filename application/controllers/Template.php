<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Template extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->model('Book_model');
        $this->load->model('Contents_model');
        $this->load->model('Contentsalbum_model');
        $this->load->model('Contentsvideo_model');
        $this->load->model('Contentssound_model');
    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');
        $company_initial = $this->session->userdata('company_initial');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        $iconContents = $this->get_icon_contents();
        $data['iconContents'] = $iconContents;
        
        $data['title'] = "Icon for contents";
        $data['base_url'] = base_url();
        //var_dump($contents);
        
        $this->parser->parse("template/template_book_icon_list.tpl", $data);
    }

    public function get_icon_contents()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['icons_name'])) {
            $result = $this->Book_model->get_icon_contents($master_user_id, $_POST['icons_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_icon_contents($master_user_id);
        }

        return $result;
    }
}