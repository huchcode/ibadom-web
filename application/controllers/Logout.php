<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Logout extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        //$this->load->controller('Created_contents');
    }

    public function index()
    {
    	$this->session->sess_destroy();

        // $this->session->unset_userdata('user_id');
        // $this->session->unset_userdata('username');
        // $this->session->unset_userdata('name');
        // $this->session->unset_userdata('type');
        // $this->session->unset_userdata('profile_picture');
        // $this->session->unset_userdata('logged_in');
        // $this->session->unset_userdata('master_user_id');
        // $this->session->unset_userdata('role');
        // $this->session->unset_userdata('company_initial');
        // $this->session->unset_userdata('content_id');

        redirect('/', 'refresh');
    }
}