<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter("must-revalidate");

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Learning_progress_examination extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->model('Examination_model');
        $this->load->model('Contents_model');
    }

    public function index($book_id=0)
    {
        if($book_id == 0) {
            redirect('/', 'refresh');
        }

        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }
        
        $result = $this->Examination_model->examination_list($book_id);

        $data['title'] = "Progress Tracking for contents";
        $data['base_url'] = base_url();
        $data['examinationlist'] = $result;
        $data['contents'] = $this->Contents_model->get_book_contents($book_id);
        
        $this->parser->parse("progresstracking/learning_progress_examination.tpl", $data);
    }
}