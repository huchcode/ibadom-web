<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Content_assign extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        $this->load->model('Book_model');
        $this->load->model('LearningAssign_model');
    }

    public function index($id=0, $book_id=0)
    {
        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        $data['title'] = "Content Assign Content";
        $data['base_url'] = base_url();
        $data['book_id'] = 0;
        $data['enable_add'] = false;

        $result = null;
        if ($id != 0) {
            $delete = $this->LearningAssign_model->delete_assignees($id);
            $result = $this->LearningAssign_model->assignees_list_contents($book_id);
            $data['book_id'] = $book_id;
            $data['enable_add'] = true;
            $data['book_name'] = $_POST['book_name'];
        }

        if (isset($_POST['book_id']) && !empty($_POST['book_id'])) {
            $result = $this->LearningAssign_model->assignees_list_contents($_POST['book_id']);
            $data['book_id'] = $_POST['book_id'];
            $data['enable_add'] = true;
        }

        if (isset($_POST['emp_book_id']) && !empty($_POST['emp_book_id'])) {
            $this->LearningAssign_model->add_assignees($master_user_id, $_POST['emp_book_id'], $_POST['target_type'], $_POST['target_id']);
            $result = $this->LearningAssign_model->assignees_list_contents($_POST['emp_book_id']);
            $data['book_id'] = $_POST['emp_book_id'];
            $data['book_name'] = $_POST['emp_book_name'];
            $data['enable_add'] = true;
        }

        $data['assignees'] = $result;

        
        $this->parser->parse("educationplacement/content_assign_content.tpl", $data);
    }

    public function people($id=0, $userid=0)
    {
        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        $data['title'] = "Content Assign People";
        $data['base_url'] = base_url();
        $data['userid'] = 0;
        $data['target_type'] = "";
        $data['enable_add'] = false;

        $result = null;
        if (isset($_POST['id']) && !empty($_POST['id'])) {
            $result = $this->LearningAssign_model->assignees_list_people($master_user_id, $_POST['id'], $_POST['usertype']);
            $data['userid'] = $_POST['id'];
            $data['target_type'] = $_POST['usertype'];
            $data['enable_add'] = true;
        }

        if ($id != 0) {
            $delete = $this->LearningAssign_model->delete_assignees($id);
            $result = $this->LearningAssign_model->assignees_list_people($master_user_id, $userid, $_POST['target_type']);
            $data['target_type'] = $_POST['target_type'];
            $data['enable_add'] = true;
            $data['emp_name'] = $_POST['emp_name'];
            $data['userid'] = $userid;
        }

        if (isset($_POST['emp_book_id']) && !empty($_POST['emp_book_id'])) {
            $this->LearningAssign_model->add_assignees($master_user_id, $_POST['emp_book_id'], $_POST['target_type'], $_POST['target_id']);
            $result = $this->LearningAssign_model->assignees_list_people($master_user_id, $_POST['target_id'], $_POST['target_type']);
            $data['target_type'] = $_POST['target_type'];
            $data['target_id'] = $_POST['target_id'];
            $data['emp_name'] = $_POST['emp_book_name'];
            $data['enable_add'] = true;
            $data['userid'] = $_POST['target_id'];

            //print_r($_POST); exit;
        }

        $data['assignees'] = $result;

        
        $this->parser->parse("educationplacement/content_assign_people.tpl", $data);
    }

    public function search_book()
    {
        $result = null;
        if (isset($_POST['search']) && !empty($_POST['search'])) {
            $result = $this->Book_model->search_book($_POST['search']);
            //print_r($result);
        }

        return $result;
    }

    public function search_book_people()
    {
        $result = null;
        if (isset($_POST['search']) && !empty($_POST['search'])) {
            $result = $this->Book_model->search_book_people($_POST['search'], $_POST['targetid']);
            //print_r($result);            
        }

        return $result;
    }

    public function search_employee()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        $result = $this->LearningAssign_model->search_employee_contents($master_user_id, $_POST['name'], $_POST['book_id']);

        return $result;
    }

    public function search_employee_people()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        $result = $this->LearningAssign_model->search_employee_people($master_user_id, $_POST['name']);

        return $result;
    }

    public function search_content_people()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $result = $this->LearningAssign_model->search_content_people($master_user_id, $_POST['name']);
        }

        return $result;
    }

    public function delete_assignees($id=0, $book_id=0)
    {
        $data['title'] = "Education Placement";
        $data['base_url'] = base_url();

        $result = null;
        if ($book_id != 0) {
            $delete = $this->LearningAssign_model->delete_assignees($id);
            $result = $this->LearningAssign_model->assignees_list($book_id);
        }

        $data['assignees'] = $result;        

        $this->parser->parse("educationplacement/content_assign_content.tpl", $data);
    }

    public function delete_assignees_people($id=0, $book_id=0, $userid=0)
    {
        $data['title'] = "Education Placement";
        $data['base_url'] = base_url();

        $result = null;
        if ($book_id != 0) {
            $delete = $this->LearningAssign_model->delete_assignees($id);
            $result = $this->LearningAssign_model->assignees_list($book_id);
        }

        $data['assignees'] = $result;        

        $this->parser->parse("educationplacement/content_assign_people.tpl", $data);
    }
}