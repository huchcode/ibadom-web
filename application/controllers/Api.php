<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Api extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('User_model');
        $this->load->model('Book_model');
        $this->load->model('Contents_model');
        $this->load->model('Api_model');
        $this->load->library('mongo_db');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        redirect('/login', 'refresh');
    }

    public function get_contents($book_id=0,$version=false)
    {
        if($book_id==0) {
            $response = array("Error"=>"Missing book_id parameter");
        } else {
            $tree = array();
            $book_info = $this->Book_model->get_book_info($book_id);
            $contents = $this->Contents_model->get_contents($book_id);
            $parent_content_id = $this->Contents_model->get_parent_content_id($book_id);
            $children = $this->Contents_model->child_contents($book_id);

            if (empty($parent_content_id)) {
                $response = array("Error"=>"Invalid book_id parameter");
            } else {
                if($version) {
                    $response = $children;
                    // $response = $this->Contents_model->parseTree_chejae($children, $parent_content_id);
                } else {
                    $response = $this->Contents_model->parseTree($children, $parent_content_id);
                }
                
            }
        }

        if (!empty($parent_content_id)) {
            echo json_encode(array_merge(array($book_info), $response));
        } else {
            echo json_encode($response);
        }
    }

    public function mindmapbg($book_id) {
        $book_info =  $this->Book_model->get_background_mindmap_id($book_id);
        $id = $book_info->background_mindmap;
        $background_mindmap =  $this->Book_model->get_background_mindmap_picture($id);

        // print_r($background_mindmap->picture); exit;

        $dirname = "/assets";
        $file = $background_mindmap->picture;
        $file_display = array('jpg', 'jpeg', 'png', 'gif');

        $mimes = array
        (
            'jpg' => 'image/jpg',
            'jpeg' => 'image/jpg',
            'gif' => 'image/gif',
            'png' => 'image/png'
        );

        $pcs = explode('.', $file);
        $pcs2 = explode('/', $pcs[count($pcs)-2]);
        $name = $pcs2[count($pcs2)-1].'.'.$pcs[count($pcs)-1];

        $ext = strtolower($pcs[count($pcs)-1]);
        header('content-type: '. $mimes[$ext]);
        header('content-disposition: inline; filename="'.$name.'";');
        readfile('.'.$file);
        exit();
    }

    public function update_position()
    {
        $id = $this->session->userdata('book_id');
        // echo getcwd();
        // exit;
        if (isset($_POST['json'])) {
            $data = $_POST['minimap'];
            $data = str_replace('data:image/png;base64,', '', $data);
            $data = str_replace(' ', '+', $data);
            $data = base64_decode($data);
            $tmp = $_SERVER['DOCUMENT_ROOT'] .'/assets/uploads/minimap/tmp'. $id . '.png';
            $file = $_SERVER['DOCUMENT_ROOT'] .'/assets/uploads/minimap/minimap_'. $id . '.png';

            $im = imagecreatefromstring($data);

            header('Content-Type: image/png');

            $thumb_width = 200;
            $thumb_height = 200;
            $width = imagesx($im);
            $height = imagesy($im);

            $original_aspect = $width / $height;
            $thumb_aspect = $thumb_width / $thumb_height;

            // echo $original_aspect . ',' . $thumb_aspect;
            // exit;

            //Figure out the dimensions we need.
            if ($original_aspect >= $thumb_aspect)
            {
                // If image is wider than thumbnail (in aspect ratio sense)
                $new_height = $thumb_height;
                $new_width = $width / ($height / $thumb_height);

                // echo $new_height . ',' . $new_width;
                // exit;
            }
            else
            {
                // If the thumbnail is wider than the image
                $new_width = $thumb_width;
                $new_height = $height / ($width / $thumb_width);
            }

            if(imagepng($im, $tmp)){
                $thumb = new Imagick();
                $thumb->setBackgroundColor('white');
                $thumb->readImage($tmp);
                $thumb->setImageFormat("png32");
                // $thumb->resizeImage($new_width,$new_height,Imagick::FILTER_LANCZOS,1);
                $thumb->thumbnailImage(200,null);
                // $thumb->setBackgroundColor(new ImagickPixel('#FFFFFF'));
                $thumb->writeImage($file);
                $thumb->clear();
                $thumb->destroy();

                imagedestroy($im);
                unlink($tmp);
            }

            // $success = file_put_contents($file, $im);

            $array = json_decode($_POST['json'], true);
            $this->Api_model->update_position($array);
            echo $_POST['json'];
        }
    }

    public function send_account()
    {
        $users = $this->User_model->get_pending_users();
        //print_r($users);exit;
        $response = array();

            if (count($users) > 0) {

            foreach ($users as $key => $value) {

                $temp_pw = $this->User_model->get_random_password(8);
                $new_password = $this->User_model->reset_user_password($value->user_id,$temp_pw);

                $config = Array(
                    'protocol' => 'smtp',
                    'smtp_host' => 'mail.huchcode.com',
                    'smtp_port' => 25,
                    'smtp_user' => 'noreply@huchcode.com',
                    'smtp_pass' => 'smith2k15',
                    'mailtype'  => 'html'
                );
                $this->load->library('email', $config);

                $this->email->set_newline("\r\n");

                $this->email->from('noreply@huchcode.com', 'Huchcode');
                $this->email->to($value->email, $value->name);
                //$this->email->cc('');
                $this->email->bcc('bryan615@gmail.com');

                $this->email->subject('GPAC Enterprise login details');
                $this->email->message("Your Username is ".$value->username." <br />Your new password is ".$new_password);

                $this->email->send();

                $this->User_model->update_employee_email_sent($value->user_id);

                $response[] = "updated ".$value->name." (".$value->user_id."), ".$value->email.". new password is ".$temp_pw;
            }

            echo json_encode($response);

        } else {

            $response = array("Message"=>"Zero users found");
        }
    }

    public function testform() {
        $this->parser->parse("testform.tpl");
    }

    public function login()
    {
        if ($this->input->post()) {

            $username = $this->input->post('username'); // user.username
            $password = $this->input->post('password'); // user.password

            if ($password == '' || $username == '') {
                $response = array(
                    "errorCode"=>"10003",
                    "errorMessage"=>"missing input parameters.",
                    "data"=>array()
                    );
            } else {
                $login_response = $this->User_model->login($username, $password);

                if (is_array($login_response)) {

                    $user = $login_response[0];

                    $data['user_id'] = $user->id;
                    $data['username'] = $user->username;
                    $data['name'] = $user->name;
                    $data['profile_picture'] = 'http://'.$_SERVER['SERVER_NAME'].'/'.$user->profile_picture;
                    $data['master_user_id'] = $user->master_user_id;
                    $currenttime = time();
                    $userToken = md5($user->id.$currenttime);
                    $data['token'] = $userToken;

                    $userCollection = $this->mongo_db->db->selectCollection('userToken');
                    $find_result = $userCollection->find(array('user_id'=>$user->id));

                    foreach($find_result as $document) {
                        //display the records
                        $data['token'] = $document['token'];

                        //already exists in userToken Collection in mongoDB
                        $response = array(
                            "errorCode"=>"0",
                            "errorMessage"=>"Success",
                            "data"=>$data
                            );
                        echo json_encode($response);
                        exit;
                    }

                    //insert in userToken Collection in mongoDB
                    $userCollection->insert(array('user_id'=>$user->id,'token'=>$userToken,'modified'=>$currenttime));
                    $response = array(
                        "errorCode"=>"0",
                        "errorMessage"=>"Success",
                        "data"=>$data
                        );
                } else {
                    $response = array(
                        "errorCode"=>"10002",
                        "errorMessage"=>"Incorrect username and password.",
                        "data"=>array()
                        );

                }
            }

        } else {
            $response = array(
                "errorCode"=>"10001",
                "errorMessage"=>"Required post parameters missing",
                "data"=>$this->input->post()
                );
        }

        echo json_encode($response);
    }

    public function sync2mobile() {

        if ($this->input->post()) {

            $p_token = $this->input->post('token');
            $user_id = $this->input->post('user_id');
            $sync_id = $this->input->post('sync_id');
            $device_info = $this->input->post('device_information');

            if ($p_token == '') {
                $response = array(
                    "errorCode"=>"20003",
                    "errorMessage"=>"missing required token parameters.",
                    "data"=>array()
                    );

            } elseif ($user_id == '') {
                $response = array(
                    "errorCode"=>"20004",
                    "errorMessage"=>"missing required user_id parameters.",
                    "data"=>array()
                    );

            } elseif ($sync_id == '') {
                $response = array(
                    "errorCode"=>"20005",
                    "errorMessage"=>"missing required sync_id parameters.",
                    "data"=>array()
                    );

            } elseif ($device_info == '') {
                $response = array(
                    "errorCode"=>"20006",
                    "errorMessage"=>"missing required device_information parameters.",
                    "data"=>array()
                    );

            } else {

                //select token from userToken
                $userCollection = $this->mongo_db->db->selectCollection('userToken');
                $find_result = $userCollection->find(array('user_id'=>$user_id));

                foreach($find_result as $document) {
                    //if token <> p_token return error
                    if ($document['token'] != $p_token) {

                        //already exists in userToken Collection in mongoDB
                        $response = array(
                            "errorCode"=>"20002",
                            "errorMessage"=>"Invalid token.",
                            "data"=>array()
                            );
                        echo json_encode($response);
                        exit;
                    }
                }

                //if sync_id = 0  synced = '2015/01/01' and insert syncDevice
                //else  select synced where id = :sync_id
                if ($sync_id == 0) {
                    $sync_id = $this->Api_model->insert_sync_device($user_id,$device_info);
                }

                //select employee_id, department_id, master_user_id
                $user_info = $this->User_model->get_user_info($user_id);

                if (empty($user_info)) {

                    //company can't use mobile app
                    $response = array(
                        "errorCode"=>"20009",
                        "errorMessage"=>"Empty user information. Please contact administrator",
                        "data"=>array("user"=>$user_info)
                        );
                    echo json_encode($response);
                    exit;

                }

                $user_detail = $this->User_model->get_user_detail($user_id,$user_info->type);

                if (empty($user_detail)) {

                    //company can't use mobile app
                    $response = array(
                        "errorCode"=>"20008",
                        "errorMessage"=>"Missing user details. Please contact administrator",
                        "data"=>array("user"=>$user_info)
                        );
                    echo json_encode($response);
                    exit;

                }

                if ($user_info->type=='master') {

                    //company can't use mobile app
                    $response = array(
                        "errorCode"=>"20007",
                        "errorMessage"=>"Not allowed to use the mobile app.",
                        "data"=>array("user"=>$user_info)
                        );
                    echo json_encode($response);
                    exit;

                }

                $employee_info['user_id'] = $user_detail->user_id;
                $employee_info['employee_id'] = $user_detail->id;
                $employee_info['department_id'] = $user_detail->department_id;
                $employee_info['master_user_id'] = $user_detail->master_user_id;

                $sync_info = $this->Api_model->get_sync_device($sync_id);

                //get book_ids[], books[], contents[], examination[]
                $data['book_ids'] = $this->Api_model->get_book_id_for_mobile($employee_info);
                $new_books_count = $this->Api_model->count_newly_added_books($employee_info,$sync_info->synced);

                if ($new_books_count > 0) {
                    $sync_id = $this->Api_model->reset_sync_device($sync_id);
                    $sync_info = $this->Api_model->get_sync_device($sync_id);
                }


                if (count($data['book_ids'])>0) {
                    $data['books'] = $this->Api_model->get_book_for_mobile($data['book_ids'],$sync_info->synced);
                    $data['contents'] = $this->Api_model->get_contents_for_mobile($data['book_ids'],$sync_info->synced);
                    $data['examination'] = $this->Api_model->get_examination_for_mobile($data['book_ids'],$sync_info->synced);
                }

                //generate zipfile
                $filelist = $this->Api_model->get_file_list($employee_info['master_user_id'],$sync_info->synced);
                $shellpath = $_SERVER['DOCUMENT_ROOT']."/shell/";
                $filename = $shellpath.$user_id.".txt";
                $myfile = fopen($filename,"w");
                foreach ($filelist as $key => $value) {
                    fwrite($myfile, $_SERVER['DOCUMENT_ROOT'].$value."\n");
                }
                fclose($myfile);

                //execute shell script
                shell_exec($shellpath."make_file.sh ".$user_id);

                $data['zipfile'] = "/download/".$user_id.".zip";

                //sync2mobilestamp
                $this->Api_model->update_sync_device($sync_id);
                $data['sync_id'] = $sync_id;

                $response = array(
                    "errorCode"=>"0",
                    "errorMessage"=>"Success",
                    "data"=>$data
                    );
            }

        } else {
            $response = array(
                "errorCode"=>"20001",
                "errorMessage"=>"Required post parameters missing",
                "data"=>array()
                );
        }

        echo json_encode($response);
    }

    public function synclearningactual() {

        if ($this->input->post()) {

            $p_token = $this->input->post('token');
            $user_id = $this->input->post('user_id');
            $sync_id = $this->input->post('sync_id');
            $data = $this->input->post('data');

            if ($p_token == '') {
                $response = array(
                    "errorCode"=>"20003",
                    "errorMessage"=>"missing required token parameters.",
                    "data"=>array()
                    );
            } elseif (empty($sync_id)) {
                $response = array(
                    "errorCode"=>"20010",
                    "errorMessage"=>"Invalid sync_id.",
                    "data"=>array()
                    );
            } else {

                //select token from userToken
                $userCollection = $this->mongo_db->db->selectCollection('userToken');
                $find_result = $userCollection->find(array('user_id'=>$user_id));

                foreach($find_result as $document) {
                    //if token <> p_token return error
                    if ($document['token'] != $p_token) {

                        //already exists in userToken Collection in mongoDB
                        $response = array(
                            "errorCode"=>"20002",
                            "errorMessage"=>"Invalid token.",
                            "data"=>array()
                            );
                        echo json_encode($response);
                        exit;
                    }
                }

                $response = $this->Api_model->sync_learning_actual($user_id,$sync_id,$data);

            }

        } else {
            $response = array(
                "errorCode"=>"10001",
                "errorMessage"=>"Missing input parameters",
                "data"=>array()
                );
        }

        echo json_encode($response);
    }

    public function updateuserphoto() {

        if ($this->input->post()) {

            $p_token = $this->input->post('token');
            $user_id = $this->input->post('user_id');

            if ($p_token == '') {
                $response = array(
                    "errorCode"=>"20003",
                    "errorMessage"=>"missing required token parameters.",
                    "data"=>array()
                    );

            } elseif ($user_id == '') {
                $response = array(
                    "errorCode"=>"20004",
                    "errorMessage"=>"missing required user_id parameters.",
                    "data"=>array()
                    );

            } else {

                //select token from userToken
                $userCollection = $this->mongo_db->db->selectCollection('userToken');
                $find_result = $userCollection->find(array('user_id'=>$user_id));

                foreach($find_result as $document) {
                    //if token <> p_token return error
                    if ($document['token'] != $p_token) {

                        //already exists in userToken Collection in mongoDB
                        $response = array(
                            "errorCode"=>"20002",
                            "errorMessage"=>"Invalid token.",
                            "data"=>array()
                            );
                        echo json_encode($response);
                        exit;
                    }
                }

                $upPath = "assets/uploads/profilePics/".$user_id;
                if(!file_exists($upPath)) {
                    mkdir($upPath, 0777, true);
                }
                $config = array(
                    'upload_path' => $upPath,
                    'allowed_types' => "gif|jpg|png|jpeg",
                    'file_name' => time(),
                    'overwrite' => TRUE,
                    //'max_size' => "5120000",
                    //'max_height' => "4028",
                    //'max_width' => "4028"
                );
                $this->load->library('upload', $config);
                if(!$this->upload->do_upload('userpic'))
                {

                    $response = array(
                        "errorCode"=>"80001",
                        "errorMessage"=>$this->upload->display_errors(),
                        "data"=>array()
                        );

                }
                else
                {
                    $imageDetailArray = $this->upload->data();

                    $image_url = '/'.$upPath.'/'.$imageDetailArray['file_name'];
                    $this->User_model->update_user_photo($user_id,$image_url);

                    $response = array(
                        "errorCode"=>"0",
                        "errorMessage"=>"Success",
                        "data"=>array('profile_picture'=>$image_url)
                        );
                }

            }

        } else {
            $response = array(
                "errorCode"=>"10001",
                "errorMessage"=>"Missing input parameters",
                "data"=>array()
                );
        }

        echo json_encode($response);

    }

    public function changepassword() {

        if ($this->input->post()) {

            $p_token = $this->input->post('token');
            $user_id = $this->input->post('user_id');
            $old_password = $this->input->post('old_password');
            $new_password = $this->input->post('new_password');

            if ($p_token == '') {
                $response = array(
                    "errorCode"=>"20003",
                    "errorMessage"=>"missing required token parameters.",
                    "data"=>array()
                    );

            } elseif ($user_id == '') {
                $response = array(
                    "errorCode"=>"20004",
                    "errorMessage"=>"missing required user_id parameters.",
                    "data"=>array()
                    );

            } else {

                //select token from userToken
                $userCollection = $this->mongo_db->db->selectCollection('userToken');
                $find_result = $userCollection->find(array('user_id'=>$user_id));

                foreach($find_result as $document) {
                    //if token <> p_token return error
                    if ($document['token'] != $p_token) {

                        //already exists in userToken Collection in mongoDB
                        $response = array(
                            "errorCode"=>"20002",
                            "errorMessage"=>"Invalid token.",
                            "data"=>array()
                            );
                        echo json_encode($response);
                        exit;
                    }
                }


                $validate_response = $this->User_model->validate_user_password($user_id, $old_password);

                if (is_array($validate_response)) {

                    $newpass = $this->User_model->reset_user_password($user_id,$new_password);
                    $response = array(
                        "errorCode"=>"0",
                        "errorMessage"=>"Success",
                        "data"=>array("new_password"=>$newpass)
                        );

                } else {
                    $response = array(
                        "errorCode"=>"10002",
                        "errorMessage"=>"Incorrect username and password.",
                        "data"=>array()
                        );

                }

            }

        } else {
            $response = array(
                "errorCode"=>"10001",
                "errorMessage"=>"Missing input parameters",
                "data"=>array()
                );
        }

        echo json_encode($response);
    }

    public function forgetpassword() {

        if ($this->input->post()) {

            $username = $this->input->post('username');
            $name = $this->input->post('name');
            $email = $this->input->post('email');

            if ($username == '') {
                $response = array(
                    "errorCode"=>"20020",
                    "errorMessage"=>"missing required username parameter.",
                    "data"=>array()
                    );

            } else {

                $userdetails = $this->User_model->get_user_employee_via_username($username);

                if (is_object($userdetails)) {

                    if ($userdetails->email == '') {
                        $response = array(
                            "errorCode"=>"20021",
                            "errorMessage"=>"User [".$username."] has no Email address.",
                            "data"=>array()
                            );

                    } elseif ($userdetails->name_from_user != $userdetails->name_from_employee) {
                        $response = array(
                            "errorCode"=>"20022",
                            "errorMessage"=>"User's nickname is different for user and employee table.",
                            "data"=>array()
                            );

                    } elseif ($userdetails->name_from_user != $name) {
                        $response = array(
                            "errorCode"=>"20024",
                            "errorMessage"=>"Invalid name parameter.",
                            "data"=>array()
                            );

                    } elseif ($userdetails->email != $email) {
                        $response = array(
                            "errorCode"=>"20025",
                            "errorMessage"=>"Invalid email parameter.",
                            "data"=>array()
                            );

                    } else {
                        $new_password = $this->User_model->get_random_password();
                        $newpass = $this->User_model->reset_user_password($userdetails->user_id,$new_password);


                        $config = Array(
                            'protocol' => 'smtp',
                            'smtp_host' => 'mail.huchcode.com',
                            'smtp_port' => 25,
                            'smtp_user' => 'noreply@huchcode.com',
                            'smtp_pass' => 'smith2k15',
                            'mailtype'  => 'html'
                        );
                        $this->load->library('email', $config);

                        $this->email->set_newline("\r\n");

                        $this->email->from('noreply@huchcode.com', 'Huchcode');
                        $this->email->to($userdetails->email, $name);
                        //$this->email->cc('');
                        $this->email->bcc('bryan615@gmail.com');

                        $this->email->subject('GPAC Enterprise login details');
                        $this->email->message("Your Username is ".$username." <br />Your new password is ".$new_password);

                        $this->email->send();

                        $response = array(
                            "errorCode"=>"0",
                            "errorMessage"=>"Success",
                            "data"=>array("new_password"=>$newpass)
                            );
                    }

                } else {

                    $response = array(
                        "errorCode"=>"20023",
                        "errorMessage"=>"Incorrect username.",
                        "data"=>array()
                        );

                }

            }

        } else {
            $response = array(
                "errorCode"=>"10001",
                "errorMessage"=>"Missing input parameters",
                "data"=>array()
                );
        }

        echo json_encode($response);
    }
}
