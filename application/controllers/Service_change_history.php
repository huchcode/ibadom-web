<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Service_change_history extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->model('User_status_history_model');
    }

    public function index($uid=null)
    {
        // Some example data
        $data['title'] = "GPAC Enterprise";
        $data['base_url'] = base_url();

        $user_id = $this->session->userdata('user_id');
        if (empty($user_id)) {
            redirect('/login', 'refresh');
        }

        if (empty($uid)) {
            redirect('/profile', 'refresh');
        }

        $user_status_history = $this->User_status_history_model->get_user_status_history($uid);
        $data['user_status_history'] = $user_status_history;
        
        $this->parser->parse("home/user_status_history.tpl", $data);
        
        
    }
}
