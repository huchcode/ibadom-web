<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Template_book_icon extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->model('Book_model');
        $this->load->model('Iconcontents_model');
        $this->load->model('User_model');
        $this->load->model('Upload_model');
    }

    public function index()
    {
        $data['title'] = "Icon for contents";
        $data['base_url'] = base_url();
        $data['error'] = '';

        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if(!empty($_FILES['img_content']['name']))
            {
                $contents = $this->Iconcontents_model->upload_icon($_POST['name'], $user_id, $master_user_id);
            } else {
                $data['error'] = 'Image required.';
            }
        }

        $iconContents = $this->get_icon_contents();
        $data['iconContents'] = $iconContents;

        if(isset($contents['error'])) {
            $error = $contents['error'];
            $data['error'] = $error;
        }
        //var_dump($contents);
        
        $this->parser->parse("template/template_book_icon_list.tpl", $data);
    }

    public function get_icon_contents()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['icons_name'])) {
            $result = $this->Book_model->get_icon_contents($master_user_id, $_POST['icons_name']);
            print_r(json_encode($result));
        } else {
            $result = $this->Book_model->get_icon_contents($master_user_id);
        }

        return $result;
    }

    public function delete_icon_contents()
    {
        $user_id = $this->session->userdata('user_id');
        $role = $this->session->userdata('role');

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $contents = $this->Iconcontents_model->delete_icon_contents($_POST['id'], $user_id, $role);
        }

        redirect('/template_book_icon', 'refresh');
    }

    public function edit_icon_contents()
    {
        $user_id = $this->session->userdata('user_id');
        $role = $this->session->userdata('role');

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $contents = $this->Iconcontents_model->edit_icon_contents($_POST['id'], $_POST['name'], $user_id, $role);
        }

        redirect('/template_book_icon', 'refresh');
    }
}