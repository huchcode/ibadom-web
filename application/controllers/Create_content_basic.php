<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter(false);

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Create_content_basic extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
    }

    public function create_content_basic()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $author_id = $this->session->userdata('user_id');
            $master_user_id = $this->session->userdata('master_user_id');
            $title = $this->input->post('title');
            $description = $this->input->post('description');
            $icon_id = $this->input->post('icon_id');
            $background_mindmap_id = $this->input->post('background_mindmap_id');

            if(empty($icon_id)) {
                $icon_id = 1;
            }

            if(empty($background_mindmap_id)) {
                $background_mindmap_id = 1;
            }

            //print_r($_POST); exit;

            $this->load->model('Book_model');
            $contents = $this->Book_model->create_content_basic($author_id, $title, $description, $master_user_id, $icon_id, $background_mindmap_id);

            redirect('/login', 'refresh');
        }
    }
}