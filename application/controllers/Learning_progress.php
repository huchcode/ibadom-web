<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

ini_set('session.cache_limiter','public');
session_cache_limiter("must-revalidate");

/**
 * CI Smarty
 *
 * Smarty templating for Codeigniter
 *
 * @package   CI Smarty
 * @author    Dwayne Charrington
 * @copyright 2015 Dwayne Charrington and Github contributors
 * @link      http://ilikekillnerds.com
 * @license   MIT
 * @version   3.0
 */

class Learning_progress extends CI_Controller {

    public function __construct()
    {
        parent::__construct();

        // Ideally you would autoload the parser
        //$this->load->library('parser');
        $this->load->model('Book_model');
        $this->load->model('Employee_model');
        $this->load->model('Department_model');
        $this->load->model('Learningprogress_model');
    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');
        $master_user_id = $this->session->userdata('master_user_id');

        if(empty($user_id))
        {
            redirect('/login', 'refresh');
        }

        $data['type'] = '';

        if (isset($_POST['type']) && !empty($_POST['type'])) {
            $result = $this->Learningprogress_model->progress_list($master_user_id, $_POST['type'], $_POST['id']);
            //print_r($result); exit;
            $data['userid'] = $_POST['id'];
            $data['type'] = $_POST['type'];
            $data['enable_add'] = true;

            $data['progresslist'] = $result;
        }

        $data['title'] = "Progress Tracking";
        $data['base_url'] = base_url();
        
        $this->parser->parse("progresstracking/learning_progress.tpl", $data);
    }

    public function search_employee()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $result = $this->Employee_model->search_employee($master_user_id, $_POST['name']);
            //print_r($result);            
        }

        return $result;
    }

    public function search_department()
    {
        $master_user_id = $this->session->userdata('master_user_id');

        $result = null;
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $result = $this->Department_model->search_department($master_user_id, $_POST['name']);
            //print_r($result);            
        }

        return $result;
    }

    public function search_book()
    {
        $result = null;
        if (isset($_POST['name']) && !empty($_POST['name'])) {
            $result = $this->Book_model->search_book($_POST['name']);
            //print_r($result);            
        }

        return $result;
    }
}