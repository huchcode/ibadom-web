{include file="common/header.tpl"}

		<link rel="stylesheet" href="{$base_url}assets/css/customer_center.css">
		<link rel="stylesheet" type="text/css" href="{$base_url}assets/css/jquery.datetimepicker.css"/>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/lib/ckeditor/ckeditor.js"></script>

		<!-- customer center notice detail -->
		<section id="content">
			<nav id="content__nav">
				home > 고객센터 > 공지사항 > 상세내용
			</nav>
			<section id="cs-home" class="content-container">
				<h1 id="content__heading" class="blue">공지사항</h1>
				<div class="content-options--right">
					<button class="activate activate--orange activate--white" id="btn-delete-announcement">삭제</button>
					<button class="activate activate--dark-blue activate--white" id="btn-edit-announcement">수정</button>
					</div>
				<div class="viewer">
					<div class="viewer__inline">
						<div class="viewer__inline__heading">
							<p class="blue">제목</p>
						</div>
						<div class="viewer__inline__spacer"></div>
						<div class="viewer__inline__text viewer__inline__text--bold viewer__inline__text--border">
						{foreach from=$announcement item=items}
							<p id="title">{$items.title}</p>
						{/foreach}
						</div>
					</div>
					<div class="viewer__inline">
						<div class="viewer__inline__text viewer__inline__text--full">
							<span id="_description">
								{foreach from=$announcement item=items}
									{$items.description}
								{/foreach}
							</span>
						</div>
					</div>
					<div class="viewer__button-container">
						<a href="/customercenter/announcement">
							<button class="confirm">목록보기</button>
						</a>
					</div>
				</div>
			</section>
		</section>
		<!-- end customer center notice detail -->
		<!-- edit announcement -->
		<section id="edit-announcement" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container popup__container--wide">
			<form id="description_attribute" method="POST" action="">
					<div class="popup__heading inline-middle">
						<h3>공지사항</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('edit-announcement');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>					
					<div class="popup__content">
						<div class="popup__content__section">
							{foreach from=$announcement item=items}
							<p class="popup__black" id="create-content-popup__selected">
							제목: <input type="text" required="" autocomplete="off" tabindex="1" style="display: inline-block; width: 231px;" name="title" id="title" value="{$items.title}">

							공지일자: <input type="text" required="" autocomplete="off" tabindex="1" style="display: inline-block; width: 145px;" name="datetime" id="datetime" value="{$items.start_timestamp}">
							</p>
							{/foreach}
						</div>
						<div class="popup__content__section">
							<textarea id="description" name="description"></textarea>
							<script>
				                // Replace the <textarea id="editor1"> with a CKEditor
				                // instance, using default configuration.
				                CKEDITOR.config.height = 100;
				                CKEDITOR.replace('description', {
									"filebrowserImageUploadUrl": "/assets/lib/ckeditor/plugins/imgupload.php?path=announcement"
								});
				            </script>
						</div>
						<div class="popup__content__section popup__content__section--center">
							{foreach from=$announcement item=items}
							<input type="hidden" id="id" name="id" value="{$items._id}">
							{/foreach}
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end edit announcement -->
		<!-- confirmation -->
		<section id="confirmation" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-pictogram" onclick="close_section('confirmation');">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form id="change_status" method="POST" action="/customercenter/delete_announcement">
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div id="delete_message" class="popup__content__section--center">
							
						</div>
						<div class="popup__content__section popup__content__section--center">
							{foreach from=$announcement item=items}
							<input type="hidden" id="id" name="id" value="{$items._id}">
							{/foreach}
							<button class="activate activate--dark-blue activate--white" type="submit">확인</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end confirmation -->

		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery.datetimepicker.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {

				// $('button#btn-add-announcement').on('click', function() {
				// 	{literal}
				// 	$('input#datetime').datetimepicker({value: new Date(), step:10});
				// 	{/literal}					
				// 	$('section#create-announcement').show();
				// 	$("html").getNiceScroll().hide();
				// });

				$('input#datetime').datetimepicker();

				$('button#btn-delete-announcement').on('click', function(){
					var text = $('p#title').text();
					$('div#delete_message').text(text+' 을(를) 삭제하시겠습니까?');
					$('#confirmation').show();
					$("html").getNiceScroll().hide();
				});

				CKEDITOR.instances.description.setData($('span#_description').html().trim());

				$('button#btn-edit-announcement').on('click', function(){
					$('section#edit-announcement').show();
					$("html").getNiceScroll().hide();
				});
			});

			function close_section(id) {	     	
		     	$('section#'+id).hide();	     	
		     	$("html").getNiceScroll().show();
		    }
		</script>		

{include file="common/footer.tpl"}