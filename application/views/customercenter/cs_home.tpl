{include file="common/header.tpl"}

		<link rel="stylesheet" href="{$base_url}assets/css/customer_center.css">

		<!-- customer service home -->
		<section id="content">
			<nav id="content__nav">
				home > 고객센터
			</nav>
			<section id="cs-home" class="content-container">
				<h1 id="content__heading" class="blue">고객센터</h1>				
				<div class="list-container">
					<div class="list__heading">
						<h3 class="inline-middle">
							<img src="{$base_url}assets/img/notification_dark_blue_icon.png" alt="Notifications">
							<span>공지사항</span>
							<a class="blue" href="/customercenter/announcement" style="float: right;">더보기</a>
						</h3>							
					</div>
					<ul class="list__value">
						{foreach from=$announcement key=i item=items name=announce}
						  {if $smarty.foreach.announce.index == 3}
						    {break}
						  {/if}
						  	<li>
								<a href="/customercenter/announcement_detail/{{$items._id}}">
									<span class="list__value__date">{$items.start_timestamp}</span>
									<span>{$items.title}</span>
								</a>
							</li>
						{/foreach}
					</ul>
				</div>
				<!-- <div class="list-container">
					<div class="list__heading">
						<h3 class="inline-middle">
							<img src="{$base_url}assets/img/envelop_dark_blue_icon.png" alt="My email">
							<span>이메일 문의 내역</span>
						</h3>
					</div>
					<ul class="list__value">
						<li>
							<span>이메일문의 하신 것이 없습니다.</span>
						</li>
					</ul>
				</div> -->
				<div class="list-container">
					<div class="list__heading">
						<h3 class="inline-middle">
							<img src="{$base_url}assets/img/bubble_dark_blue_icon.png" alt="FAQ">
							<span>FAQ 자주묻는 질문</span>
							<a class="blue" href="/customercenter/announcement" style="float: right;">더보기</a>
						</h3>
					</div>
					<ul class="list__value">
						<li>
							<span>회원가입에러가 날때는 어떻게?</span>
						</li>
						<li>
							<span>정산금에 대한 세금계산서 발행</span>
						</li>
						<li>
							<span>컨텐츠 발행승인이 났는데도 에러가 나는 경우</span>
						</li>
					</ul>
				</div>
				<hr>
				<div id="cs-home__help" class="list-container">
					<a href="">
						<div class="inline-middle">
							<img src="{$base_url}assets/img/envelop_dark_blue_icon.png" alt="Email">
							<span>이메일 문의하기</span>
						</div>
					</a>
					<a href="">
						<div class="inline-middle">
							<img src="{$base_url}assets/img/book_dark_blue_icon.png" alt="How to use guide">
							<span>이용가이드 보기</span>
						</div>
					</a>
					<div class="inline-middle">
						<img src="{$base_url}assets/img/phone_dark_blue_icon.png" alt="Email">
						<span>고객상담전화</span>
						<span class="dark-blue">070-4367-4725</span>
					</div>
				</div>
			</section>
		</section>
		<!-- end customer service home -->

{include file="common/footer.tpl"}