{include file="common/header.tpl"}

		<link rel="stylesheet" href="{$base_url}assets/css/customer_center.css">
		<link rel="stylesheet" type="text/css" href="{$base_url}assets/css/jquery.datetimepicker.css"/>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/lib/ckeditor/ckeditor.js"></script>

		<!-- customer center notice -->
		<section id="content">
			<nav id="content__nav">
				home > 고객센터 > 공지사항
			</nav>
			<section id="cs-notice" class="content-container">
				<h1 id="content__heading" class="blue">공지사항</h1>
				<div id="cs-notice__options" class="content-options clearfix">
					<div class="content-options--right">
						<button class="activate activate--dark-blue activate--white activate--icon" id="btn-add-announcement">
						<img src="{$base_url}assets/img/notification_dark_blue_icon.png">
						컨텐츠 생성
					</button>
					</div>
				</div>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<div id="cs-notice__title" class="table__cell">
							<p>제목</p>
						</div>
						<div id="cs-notice__date" class="table__cell">
							<p>공지일자</p>
						</div>
					</div>
					{foreach from=$announcement item=items}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p><a href="/customercenter/announcement_detail/{{$items._id}}">{$items.title}</a></p>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$items.start_timestamp}</p>
						</div>
					</div>
					{/foreach}
				</div>
			</section>
		</section>
		<!-- end customer center notice -->
		<!-- add announcement -->
		<section id="create-announcement" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container popup__container--wide">
			<form id="description_attribute" method="POST" action="">
					<div class="popup__heading inline-middle">
						<h3>공지사항</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-announcement');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>					
					<div class="popup__content">
						<div class="popup__content__section">
							<p class="popup__black" id="create-content-popup__selected">
							제목: <input type="text" required="" autocomplete="off" tabindex="1" style="display: inline-block; width: 231px;" name="title" id="title">

							공지일자: <input type="text" required="" autocomplete="off" tabindex="1" style="display: inline-block; width: 145px;" name="datetime" id="datetime">
							</p>
						</div>
						<div class="popup__content__section">
							<textarea id="description" name="description"></textarea>
							<script>
				                // Replace the <textarea id="editor1"> with a CKEditor
				                // instance, using default configuration.
				                CKEDITOR.config.height = 100;
				                CKEDITOR.replace('description', {
									"filebrowserImageUploadUrl": "/assets/lib/ckeditor/plugins/imgupload.php?path=announcement"
								});
				            </script>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end announcement -->

		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery.datetimepicker.js"></script>

		<script type="text/javascript">
			$(document).ready(function() {

				$('button#btn-add-announcement').on('click', function() {
					{literal}
					$('input#datetime').datetimepicker({value: new Date(), step:10});
					{/literal}					
					$('section#create-announcement').show();
					$("html").getNiceScroll().hide();
				});

				$('input#datetime').datetimepicker();
			});

			function close_section(id) {	     	
		     	$('section#'+id).hide();	     	
		     	$("html").getNiceScroll().show();
		    }
		</script>

{include file="common/footer.tpl"}