{include file="common/header.tpl"}
<link rel="stylesheet" href="../../assets/css/to_register_staff_user.css">
<link rel="stylesheet" href="../../assets/css/module.css">
<script type="text/javascript" src="../../assets/js/jquery.csv-0.71.js"></script>

<!-- dept list -->
<section id="content">
	<nav id="content__nav">
		home > 직원(부서)등록 > 부서등록
	</nav>
	<section id="dept-list" class="content-container">
		<h1 id="content__heading" class="blue">부서등록</h1>
		{if {$smarty.session.role} != 'admin' and {$smarty.session.role} != 'master'}
			{include file="common/permission.tpl"}

			<script type="text/javascript">
			$(document).ready(function() {
				$('div#dept-list__options').empty();
				$('div.table-container').empty();
			});
			</script>
		{/if}
		<a id="content__sub-heading" class="grey inline-middle" href="/user_management">
			<img src="../../assets/img/user_plus_grey_icon.png" alt="Register department">
			<span class="no-padding">직원등록하기</span>
		</a>
		<div id="dept-list__options" class="content-options clearfix">
			<button id="dept-list__add" class="activate activate--dark-blue activate--white activate--icon content-options--right" onclick="deptaddopen()">
				<img src="../../assets/img/stack_white_icon.png" alt="Add">
				추가
			</button>
		</div>
		<div class="table-container">
			<div class="table__row table__row--heading">
				<div id="dept-list__num" class="table__cell">
					<p>No.</p>
				</div>
				<div id="dept-list__department-name" class="table__cell">
					<p>부서명</p>
				</div>
				<div id="dept-list__department-num" class="table__cell">
					<p>부서코드</p>
				</div>
			</div>
			{foreach from=$department_list key=i item=dept}
			<div class="table__row" id="dept_row_{$dept->id}" onclick="edit('{$dept->id}');" style="cursor:pointer">
				<div class="table__cell table__cell--center">
					<p>{$i+1}</p>
				</div>
				<div class="table__cell table__cell--center">
					<p>{$dept->name}</p>
				</div>
				<div class="table__cell table__cell--center">
					<p>{$dept->dept_code}</p>
				</div>
			</div>
			{/foreach}
		</div>
		<!--div class="paging">
			<a class="paging__first" href="">&#9668;&#9668;</a>
			<a class="paging__prev" href="">&#9668;</a>
			<a class="paging__to blue" href="">1</a>
			<a class="paging__to" href="">2</a>
			<a class="paging__to" href="">3</a>
			<a class="paging__to" href="">4</a>
			<a class="paging__next" href="">&#9658;</a>
			<a class="paging__last" href="">&#9658;&#9658;</a>
		</div-->
	</section>
</section>
<!-- end dept list -->
<script>
$('body').on('click', '.popup__bg', function(){
	$("#dept-excel-popup").hide();
	$("#dept-add-popup").hide();
	$('#dept-edit-popup').hide();
	$("#delete-popup").hide();
	$("#upload_filename").html('엑셀파일을 선택하세요');

	$('#dept_excel_num').val('');
});
$(document).ready(function() {
	if(isAPIAvailable()) {
		$('#files').bind('change', handleFileSelect);
	}
});

function isAPIAvailable() {
	// Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
	  // Great success! All the File APIs are supported.
	  return true;
	} else {
	  // source: File API availability - http://caniuse.com/#feat=fileapi
	  // source: <output> availability - http://html5doctor.com/the-output-element/
	  document.writeln('The HTML5 APIs used in this form are only available in the following browsers:<br />');
	  // 6.0 File API & 13.0 <output>
	  document.writeln(' - Google Chrome: 13.0 or later<br />');
	  // 3.6 File API & 6.0 <output>
	  document.writeln(' - Mozilla Firefox: 6.0 or later<br />');
	  // 10.0 File API & 10.0 <output>
	  document.writeln(' - Internet Explorer: Not supported (partial support expected in 10.0)<br />');
	  // ? File API & 5.1 <output>
	  document.writeln(' - Safari: Not supported<br />');
	  // ? File API & 9.2 <output>
	  document.writeln(' - Opera: Not supported');
	  return false;
	}
}
  function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object
    var file = files[0];

    // read the file metadata
    var output = ''
        output += '<span style="font-weight:bold;">' + escape(file.name) + '</span><br />\n';
        output += ' - FileType: ' + (file.type || 'n/a') + '<br />\n';
        output += ' - FileSize: ' + file.size + ' bytes<br />\n';
        output += ' - LastModified: ' + (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br />\n';

    // read the file contents
    analyseCsvFile(file);

	$("#upload_filename").html(escape(file.name));

    // post the results
    //document.writeln(output);
  }

function analyseCsvFile(file) {
	var reader = new FileReader();
	reader.readAsText(file);

	reader.onload = function(event){
		var csv = event.target.result;
		var data = $.csv.toArrays(csv);
		$('#dyanamic_input_datas').html();

		for(var row in data) {
			if (row > 0) {
				$('#dyanamic_input_datas').append('<input type="hidden" name="dept_name[]" value="'+data[row][0]+'" />');
				$('#dyanamic_input_datas').append('<input type="hidden" name="dept_code[]" value="'+data[row][1]+'" />');
			}
		}

		var import_cnt = data.length - 1;
		$('#dept_excel_num').val(import_cnt);
		
	};
	reader.onerror = function(){ alert('Unable to read ' + file.fileName); };
}
function excelpopupopen() {
	$("#dept-excel-popup").show();
	$('#dyanamic_input_datas').html();
	$("#dept-add-popup").hide();
}
function excelpopupclose(){
	$("#dept-excel-popup").hide();
	$("#upload_filename").html('엑셀파일을 선택하세요');
	$('#dept_excel_num').val('');
};

function deptaddclose(){
	$("#dept-add-popup").hide();
};
function deptaddopen() {
	$("#dept-excel-popup").hide();
	$("#dept-add-popup").show();
};
function edit(department_id) {
	$( "#dept_id" ).val('');
	$( "#dept_name" ).val('');
	$( "#dept_code" ).val('');

    $.ajax({
    	url: '/dept_management/ajax_get_dept/'+department_id,
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,                   
		type: 'post',
		success: function(res){
			$( "#dept_id" ).val(res.dept_id);
			$( "#dept_name" ).val(res.dept_name);
			$( "#dept_code" ).val(res.dept_code);
			$("#dept-edit-popup").show();
		}
	});
}
function editclose() {
	$("#dept-edit-popup").hide();

	$( "#dept_id" ).val('');
	$( "#dept_name" ).val('');
	$( "#dept_code" ).val('');
};
function delete_dept() {
	var department_id = $( "#dept_id" ).val();

    $.ajax({
    	url: '/dept_management/ajax_delete_dept/'+department_id,
		dataType: 'text',
		cache: false,
		contentType: false,
		processData: false,                   
		type: 'post',
		success: function(res){
			$('#dept_row_'+department_id).hide();
			$("#dept-edit-popup").hide();
			$("#delete-popup").hide();

			$( "#dept_id" ).val('');
			$( "#dept_name" ).val('');
			$( "#dept_code" ).val('');
		}
	});
}
function confirm_delete_dept() {
	$("#delete-popup").show();
}
function deleteclose() {
	$("#delete-popup").hide();
}
</script>

<!-- dept excel -->
<section id="dept-excel-popup" class="popup" style="display:none;">
	<div class="popup__bg"></div>
	<div class="popup__container">
		<div class="popup__exit" onclick="excelpopupclose()">
			<img src="../../assets/img/popup_exit.png">
		</div>
		<form method="POST" action="/dept_management/import/">
			<div class="popup__heading clearfix">
				<h3 class="popup__heading__sub">부서추가(엑셀)</h3>
				<div class="popup__heading__sub-options" style="cursor:pointer;" onclick="deptaddopen();">
					<img src="../../assets/img/user_plus_grey_icon.png" alt="Add one">
					<p class="grey">개별추가</p>
				</div>
			</div>
			<div class="popup__content">
				<div class="popup__content__section">
					<div class="popup__content__section__inline">
						<label class="popup__black">파일</label>
						<p class="blue" id="upload_filename">엑셀파일을 선택하세요</p>
						<button class="activate activate--blue file-upload" type="button">불러오기</button>
						<input type="file" id="files" name="files">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">총건수</label>
						<input type="text" id="dept_excel_num" name="dept_excel_num" disabled>
					</div>
					<div id="dyanamic_input_datas"></div>
				</div>
				<div class="popup__content__section popup__content__section--center">
					<div id="download-template" class="inline-middle" onclick="location.href='{$base_url}/assets/csv/sample_dept.csv'" style="cursor:pointer;">
						<img src="../../assets/img/document_blue_icon.png">
						<span class="blue">템플릿 다운받기</span>
						<span>※ 파일 포맷은 CSV (Comma delimited) 입니다.</span>
					</div>
				</div>
				<div class="popup__content__section popup__content__section--center">
					<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end dept excel -->

<!-- dept add -->
<section id="dept-add-popup" class="popup" style="display:none;">
	<div class="popup__bg"></div>
	<div class="popup__container">
		<div class="popup__exit" onclick="deptaddclose();">
			<img src="../../assets/img/popup_exit.png">
		</div>
		<form method="POST" action="/dept_management/add/">
			<div class="popup__heading clearfix">
				<h3 class="popup__heading__sub">부서추가</h3>
				<div class="popup__heading__sub-options" style="cursor:pointer;" onclick="excelpopupopen();">
					<img src="../../assets/img/excel_icon.png" alt="Add excel file">
					<p class="grey">엑셀추가</p>
				</div>
			</div>
			<div class="popup__content">
				<div class="popup__content__section popup__content__section--center">
					<div class="popup__content__section__inline">
						<label class="popup__black">부서명</label>
						<input type="text" name="dept_name">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">부서코드</label>
						<input type="text" name="dept_code">
					</div>
				</div>
				<div class="popup__content__section popup__content__section--center">
					<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end dept add -->

<!-- dept edit -->
<section id="dept-edit-popup" class="popup" style="display:none;">
	<div class="popup__bg"></div>
	<div class="popup__container">
		<div class="popup__exit" onclick="editclose();">
			<img src="../../assets/img/popup_exit.png">
		</div>
		<form method="POST" action="dept_management/update/">
			<input type="hidden" id="dept_id" name="dept_id">
			<div class="popup__heading">
				<h3>부서수정(삭제)</h3>
			</div>
			<div class="popup__content">
				<div class="popup__content__section popup__content__section--center">
					<div class="popup__content__section__inline">
						<label class="popup__black">부서명</label>
						<input type="text" id="dept_name" name="dept_name">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">부서코드</label>
						<input type="text" id="dept_code" name="dept_code">
					</div>
				</div>
				<div class="popup__content__section popup__content__section--center">
					<button class="activate activate--dark-orange activate--white" type="button" onclick="confirm_delete_dept();">삭제</button>
					<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end dept edit -->

<!-- confirm delete -->
<section id="delete-popup" class="popup" style="display:none;">
	<div class="popup__bg"></div>
	<div class="popup__container">
		<div class="popup__exit" onclick="deleteclose()">
			<img src="{$base_url}assets/img/popup_exit.png">
		</div>
		<form method="POST" action="">
			<div class="popup__heading">
				<h3>부서삭제확인?</h3>
			</div>
			<div class="popup__content">
				<div class="popup__content__section popup__content__section--center">
					<input type="hidden" name="delete_exam_id" id="delete_exam_id" value="" />
				</div>
				<div class="popup__content__section popup__content__section--center">
					<button class="activate activate--dark-blue activate--white" type="button" onclick="delete_dept()">확인</button>
					<button class="activate activate--red" type="button" onclick="deleteclose()">취소</button>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end confirm delete -->
{include file="common/footer.tpl"}