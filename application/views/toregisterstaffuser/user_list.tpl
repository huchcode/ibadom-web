{include file="common/header.tpl"}
<link rel="stylesheet" href="../../assets/css/to_register_staff_user.css">
<link rel="stylesheet" href="../../assets/css/module.css">
<script type="text/javascript" src="../../assets/js/jquery.csv-0.71.js"></script>

<!-- user list -->
<section id="content">
	<nav id="content__nav">
		home > 직원(부서)등록 > 직원등록
	</nav>
	<section id="user-list" class="content-container">
		<h1 id="content__heading" class="blue">직원등록</h1>
		{if {$smarty.session.role} != 'admin' and {$smarty.session.role} != 'master'}
			{include file="common/permission.tpl"}

			<script type="text/javascript">
			$(document).ready(function() {
				$('div#user-list__options').empty();
				$('div.table-container').empty();
			});
			</script>
		{/if}
		<a id="content__sub-heading" class="grey inline-middle" href="/dept_management">
			<img src="../../assets/img/users_grey_icon.png" alt="Register department">
			<span class="no-padding">부서등록하기</span>
		</a>
		<div id="user-list__options" class="content-options clearfix">
			<div class="content-options--left">
				<form id="searchform" method="POST" action="/user_management/">
					<p class="grey">성명</p>
					<!--<div class="search-bar">
						<input id="search" type="text" name="search_name" placeholder="이름을 입력하세요" value="{$search_name}">
						<img src="../../assets/img/magnifier_grey_icon.png" alt="Search">
					</div>-->

					<div class="search-bar">
						<input type="text" id="search" name="search_name" placeholder="검색어를 입력하세요" value="{if isset($search_name)}{$search_name}{/if}" autocomplete="off">
						<img src="{$base_url}assets/img/magnifier_grey_icon.png" alt="Search">
						<div id="search_result" class="search__results" style="max-height:290px; overflow-y: scroll">
						</div>
					</div>

					<p class="grey">부서명</p>
					<select class="custom-select--" onchange="submit();" name="search_department_id" data-width="150" data-placeholder="전부서">
						<option value="">전부서</option>
						{foreach from=$department_list key=i item=dept}
						<option value="{$dept->id}" {if $search_department_id==$dept->id}selected{/if}>{$dept->name}</option>
						{/foreach}
					</select>
				</form>
			</div>
			<button id="user-list__add" class="activate activate--dark-blue activate--white activate--icon content-options--right" onclick="useraddopen()">
				<img src="../../assets/img/stack_white_icon.png" alt="Add">
				추가
			</button>
		</div>
		<div class="table-container">
			<div class="table__row table__row--heading">
				<div id="user-list__num" class="table__cell">
					<p>No.</p>
				</div>
				<div id="user-list__name" class="table__cell">
					<p>이름</p>
				</div>
				<div id="user-list__email" class="table__cell">
					<p>이메일</p>
				</div>
				<div id="user-list__member-num" class="table__cell">
					<p>사원번호</p>
				</div>
				<div id="user-list__department" class="table__cell">
					<p>부서</p>
				</div>
				<div id="user-list__authority" class="table__cell">
					<p>권한</p>
				</div>
			</div>
			{foreach from=$employee_list key=i item=employee}
			<div class="table__row" id="user_row_{$employee->user_id}" onclick="edit('{$employee->user_id}');" style="cursor:pointer">
				<div class="table__cell table__cell--center">
					<p>{$i+1}</p>
				</div>
				<div class="table__cell table__cell--center">
					<p>{$employee->name}</p>
				</div>
				<div class="table__cell table__cell--center">
					<p>{$employee->email}</p>
				</div>
				<div class="table__cell table__cell--center">
					<p>{$employee->employee_number}</p>
				</div>
				<div class="table__cell table__cell--center">
					<p>{$employee->dept_name}</p>
				</div>
				<div class="table__cell table__cell--center">
					<p class="blue">{if ($employee->role=='admin')}관리자{elseif ($employee->role=='author')}저자{else}교육생{/if}</p>
				</div>
			</div>
			{/foreach}
		</div>
		<!--div class="paging">
			<a class="paging__first" href="">&#9668;&#9668;</a>
			<a class="paging__prev" href="">&#9668;</a>
			<a class="paging__to blue" href="">1</a>
			<a class="paging__to" href="">2</a>
			<a class="paging__to" href="">3</a>
			<a class="paging__to" href="">4</a>
			<a class="paging__next" href="">&#9658;</a>
			<a class="paging__last" href="">&#9658;&#9658;</a>
		</div-->
	</section>
</section>
<!-- end user list -->

<script>
{if ($msg)}
	alert('{$msg}');
	location.href="/user_management";
{/if}
$('body').on('click', '.popup__bg', function(){
	$("#user-excel-popup").hide();
	$("#user-add-popup").hide();
	$('#user-edit-popup').hide();
	$("#delete-popup").hide();
	$("#upload_filename").html('엑셀파일을 선택하세요');

	$('#user_excel_num').val('');
	$('#user_excel_author').val('');
	$('#user_excel_admin').val('');
	$('#user_excel_person').val('');
});
$(document).ready(function() {

	if(isAPIAvailable()) {
		$('#files').bind('change', handleFileSelect);
	}

	$("input#search").keyup(function() {
		$("#search_result").empty();
		var name = $("#search").val();
		if(name.length > 0)
		{
			$("#search_result").empty();
			//Search Users
			$.ajax({
		      url: "/user_management/search_user",
		      type: "POST",
		      data: { search_name: name },
		      dataType: "json",
		      success: function(response) {
		      	$("#search_result").empty();
		      	console.log(response);
		      	var total = response.length;
		      	if(total > 0)
		      	{
			      	$.each(response, function(i, item) {
					    $("#search_result").append('<div id="search_result_list" class="search__results__item" onclick="searchthisname(\'' + response[i].name + '\')">' + response[i].name + '</div>');
					});
		      	}
		      },
		      error: function(xhr, status, error) {
		      	console.log(xhr);
		      	//alert(xhr.responseText);
				}
		    });
		}
	});

});
function searchthisname(searchname) {
	$("input#search").val(searchname);
	$("#searchform").submit();
}
function validateForm(formid) {
  var un = document.forms[formid]["user_name"].value;
  var ue = document.forms[formid]["user_email"].value;
  var ur = document.forms[formid]["user_role"].value;
  var en = document.forms[formid]["user_employee_number"].value;
  var di = document.forms[formid]["department_id"].value;

  if (formid=='adduserform') {
		var uename = ue.split("@");
	    $.ajax({
	    	url: '/user_management/ajax_check_duplicate_user/email/'+uename[0],
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			type: 'post',
			success: function(result){
				if (result.length > 0) {
					for (var key in result) {
						if (result.hasOwnProperty(key)) {
							alert("Username " + result[key].username + " already exists. Nothing was inserted.");
							return false;
						}
					}
				}
			}
		});
  }

  if (un == null || un == "") {
    alert("Missing 이름");
    return false;
  } else if (ue == null || ue == "") {
    alert("Missing 메일");
    return false;
  } else if (ur == null || ur == "") {
    alert("Missing 권한");
    return false;
  } 
  //else if (en == null || en == "") {
    //alert("Missing 사원번호");
    //return false;
  /*} else if (di == null || di == "") {
    alert("Missing 부서");
    return false;*/
  //} 
  else if (!validateEmail(ue)) {
    alert("Invalid 메일");
    return false;
  } else {
  	return true
  }
}

function isAPIAvailable() {
	// Check for the various File API support.
	if (window.File && window.FileReader && window.FileList && window.Blob) {
	  // Great success! All the File APIs are supported.
	  return true;
	} else {
	  // source: File API availability - http://caniuse.com/#feat=fileapi
	  // source: <output> availability - http://html5doctor.com/the-output-element/
	  document.writeln('The HTML5 APIs used in this form are only available in the following browsers:<br />');
	  // 6.0 File API & 13.0 <output>
	  document.writeln(' - Google Chrome: 13.0 or later<br />');
	  // 3.6 File API & 6.0 <output>
	  document.writeln(' - Mozilla Firefox: 6.0 or later<br />');
	  // 10.0 File API & 10.0 <output>
	  document.writeln(' - Internet Explorer: Not supported (partial support expected in 10.0)<br />');
	  // ? File API & 5.1 <output>
	  document.writeln(' - Safari: Not supported<br />');
	  // ? File API & 9.2 <output>
	  document.writeln(' - Opera: Not supported');
	  return false;
	}
}
function handleFileSelect(evt) {
	var files = evt.target.files; // FileList object
	var file = files[0];

	// read the file metadata
	var output = ''
	    output += '<span style="font-weight:bold;">' + escape(file.name) + '</span><br />\n';
	    output += ' - FileType: ' + (file.type || 'n/a') + '<br />\n';
	    output += ' - FileSize: ' + file.size + ' bytes<br />\n';
	    output += ' - LastModified: ' + (file.lastModifiedDate ? file.lastModifiedDate.toLocaleDateString() : 'n/a') + '<br />\n';

	// read the file contents
	analyseCsvFile(file);

	$("#upload_filename").html(escape(file.name));

	// post the results
	//document.writeln(output);
}

function analyseCsvFile(file) {
	var reader = new FileReader();
	reader.readAsText(file);

	reader.onload = function(event){
		var csv = event.target.result;
		var data = $.csv.toArrays(csv);
		$('#dyanamic_input_datas').html();
		var user_excel_person = 0;
		var user_excel_author = 0;
		var user_excel_admin = 0;

		for(var row in data) {
			if (row > 0) {
				$('#dyanamic_input_datas').append('<input type="hidden" name="user_name[]" value="'+data[row][0]+'" />');
				$('#dyanamic_input_datas').append('<input type="hidden" name="user_email[]" value="'+data[row][1]+'" />');
				$('#dyanamic_input_datas').append('<input type="hidden" name="user_role[]" value="'+data[row][2].toLowerCase()+'" />');
				$('#dyanamic_input_datas').append('<input type="hidden" name="user_employee_number[]" value="'+data[row][3]+'" />');
				$('#dyanamic_input_datas').append('<input type="hidden" name="department_code[]" value="'+data[row][4]+'" />');
				if (data[row][2].toLowerCase() == 'author') {
					user_excel_author++;
				} else if (data[row][2].toLowerCase() == 'admin') {
					user_excel_admin++;
				} else { //if (data[row][2] == 'trainee')
					user_excel_person++;
				}
			}
		}

		var import_cnt = data.length - 1;
		$('#user_excel_num').val(import_cnt);
		$('#user_excel_author').val(user_excel_author);
		$('#user_excel_admin').val(user_excel_admin);
		$('#user_excel_person').val(user_excel_person);

	};
	reader.onerror = function(){ alert('Unable to read ' + file.fileName); };
}
function excelpopupopen() {
	$("#user-excel-popup").show();
	$('#dyanamic_input_datas').html();
	$("#user-add-popup").hide();
}
function excelpopupclose(){
	$("#user-excel-popup").hide();
	$("#upload_filename").html('엑셀파일을 선택하세요');
};

function useraddopen() {
	$("#user-excel-popup").hide();
	$("#user-add-popup").show();
};
function useraddclose(){
	$("#user-add-popup").hide();
};
function edit(user_id) {
	$( "#user_id" ).val('');
	$( "#user_email" ).val('');
	$( "#user_employee_number" ).val('');

    $.ajax({
    	url: '/user_management/ajax_get_user/'+user_id,
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,
		type: 'post',
		success: function(res){
			$( "#user_id" ).val(res.employee.user_id);
			$( "#user_name" ).val(res.employee.name);
			$( "#user_email" ).val(res.employee.email);
			$( "#user_employee_number" ).val(res.employee.employee_number);
			$( "#user_role_edit" ).val(res.user.role);
			$( "#department_id" ).val(res.employee.department_id);
			$( "#user-edit-popup" ).show();
		}
	});
}
function usereditclose() {
	$("#user-edit-popup").hide();

	$( "#user_id" ).val('');
	$( "#user_email" ).val('');
	$( "#user_employee_number" ).val('');
};
function confirm_delete_user() {
	$("#delete-popup").show();
}
function deleteclose() {
	$("#delete-popup").hide();
}
function delete_user() {
	var user_id = $( "#user_id" ).val();

    $.ajax({
    	url: '/user_management/ajax_delete_user/'+user_id,
		dataType: 'text',
		cache: false,
		contentType: false,
		processData: false,
		type: 'post',
		success: function(res){
			$('#user_row_'+user_id).hide();
			$("#user-edit-popup").hide();
			$("#delete-popup").hide();

			$( "#user_id" ).val('');
			$( "#user_email" ).val('');
			$( "#user_employee_number" ).val('');
			window.reload();
		}
	});
}

</script>

<!-- user excel -->
<section id="user-excel-popup" class="popup" style="display:none;">
	<div class="popup__bg"></div>
	<div class="popup__container">
		<div class="popup__exit" onclick="excelpopupclose();">
			<img src="../../assets/img/popup_exit.png">
		</div>
		<form method="POST" action="/user_management/import/">
			<div class="popup__heading clearfix">
				<h3 class="popup__heading__sub">직원추가(엑셀)</h3>
				<div class="popup__heading__sub-options" style="cursor:pointer;" onclick="useraddopen();">
					<img src="../../assets/img/user_plus_grey_icon.png" alt="Add one">
					<p class="grey">개별추가</p>
				</div>
			</div>
			<div class="popup__content">
				<div class="popup__content__section">
					<div class="popup__content__section__inline">
						<label class="popup__black">파일</label>
						<p class="blue" id="upload_filename">엑셀파일을 선택하세요</p>
						<button class="activate activate--blue file-upload" type="button">불러오기</button>
						<input type="file" id="files" name="files">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">총건수</label>
						<input type="text" id="user_excel_num" name="user_excel_num" disabled>
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">일반</label>
						<input type="text" id="user_excel_person" name="user_excel_person" disabled>
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">저자</label>
						<input type="text" id="user_excel_author" name="user_excel_author" disabled>
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">관리자</label>
						<input type="text" id="user_excel_admin" name="user_excel_admin" disabled>
					</div>
				</div>
				<div class="popup__content__section popup__content__section--center">
					<div id="download-template" class="inline-middle" onclick="location.href='{$base_url}/assets/csv/sample_user.csv'" style="cursor:pointer;">
						<img src="../../assets/img/document_blue_icon.png">
						<span class="blue">템플릿 다운받기</span>
						<span>※ 파일 포맷은 CSV (Comma delimited) 입니다.</span>
					</div>
					<div id="dyanamic_input_datas"></div>
				</div>
				<div class="popup__content__section popup__content__section--center">
					<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end user excel -->

<!-- user add -->
<section id="user-add-popup" class="popup" style="display:none;">
	<div class="popup__bg"></div>
	<div class="popup__container">
		<div class="popup__exit" onclick="useraddclose();">
			<img src="../../assets/img/popup_exit.png">
		</div>
		<form method="POST" id="adduserform" action="/user_management/add" onsubmit="return validateForm('adduserform')">
			<div class="popup__heading clearfix">
				<h3 class="popup__heading__sub">직원추가</h3>
				<div class="popup__heading__sub-options" style="cursor:pointer;" onclick="excelpopupopen();">
					<img src="../../assets/img/excel_icon.png" alt="Add excel file">
					<p class="grey">엑셀추가</p>
				</div>
			</div>
			<div class="popup__content">
				<div class="popup__content__section popup__content__section--center">
					<div class="popup__content__section__inline">
						<label class="popup__black">이름</label>
						<input type="text" name="user_name">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">메일</label>
						<input type="text" name="user_email">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black" style="float:left;width:75px;padding-left:72px;">권한</label>
						<select class="custom-select-" id="user_role" name="user_role" data-width="200" data-placeholder="기술1팀">
							<option value=""></option>
							<option value="author">저자</option>
							<option value="trainee">교육생</option>
							<option value="admin">관리자</option>
							<!-- <option value="account">계정</option> -->
						</select>
						<div style="clear:both;"></div>
					</div>
					<div class="popup__content__section__inline" style="margin-top:8px;">
						<label class="popup__black">사원번호</label>
						<input type="text" name="user_employee_number">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black" style="float:left;width:75px;padding-left:72px;">부서</label>
						<select class="custom-select-" id="department_id" name="department_id" data-width="200" data-placeholder="기술1팀">
							<option value=""></option>
							{foreach from=$department_list key=i item=dept}
							<option value="{$dept->id}">{$dept->name}</option>
							{/foreach}
						</select>
						<div style="clear:both"></div>
					</div>
				</div>
				<div class="popup__content__section popup__content__section--center">
					<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end user add -->

<!-- user edit -->
<section id="user-edit-popup" class="popup" style="display:none;">
	<div class="popup__bg"></div>
	<div class="popup__container">
		<div class="popup__exit" onclick="usereditclose();">
			<img src="../../assets/img/popup_exit.png">
		</div>
		<form method="POST" id="edituserform" action="/user_management/update" onsubmit="return validateForm('edituserform')">
			<input type="hidden" id="user_id" name="user_id">
			<div class="popup__heading clearfix">
				<h3>직원수정(삭제)</h3>
			</div>
			<div class="popup__content">
				<div class="popup__content__section popup__content__section--center">
					<div class="popup__content__section__inline">
						<label class="popup__black">이름</label>
						<input type="text" id="user_name" name="user_name">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">메일</label>
						<input type="text" id="user_email" name="user_email">
					</div>
					<div class="popup__content__section__inline" style="margin-bottom:7px;">
						<label class="popup__black" style="float:left;width:75px;padding-left:72px;">권한</label>
						<select class="custom-select-" id="user_role_edit" name="user_role" data-width="200" data-placeholder="일반">
							<option value="author">저자</option>
							<option value="trainee">교육생</option>
							<option value="admin">관리자</option>
							<!-- <option value="account">계정</option> -->
						</select>
						<div style="clear:both"></div>
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black">사원번호</label>
						<input type="text" id="user_employee_number" name="user_employee_number">
					</div>
					<div class="popup__content__section__inline">
						<label class="popup__black" style="float:left;width:75px;padding-left:72px;">부서</label>
						<select class="custom-select-" id="department_id" name="department_id" data-width="200" data-placeholder="기술1팀">
							<option value=""></option>
							{foreach from=$department_list key=i item=dept}
							<option value="{$dept->id}">{$dept->name}</option>
							{/foreach}
						</select>
						<div style="clear:both"></div>
					</div>
				</div>
				<div class="popup__content__section popup__content__section--center">
					<button class="activate activate--dark-orange activate--white" type="button" onclick="confirm_delete_user()">삭제</button>
					<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end user edit -->

<!-- confirm delete -->
<section id="delete-popup" class="popup" style="display:none;">
	<div class="popup__bg"></div>
	<div class="popup__container">
		<div class="popup__exit" onclick="deleteclose()">
			<img src="{$base_url}assets/img/popup_exit.png">
		</div>
		<form method="POST" action="">
			<div class="popup__heading">
				<h3>사용자삭제를확인?</h3>
			</div>
			<div class="popup__content">
				<div class="popup__content__section popup__content__section--center">
					<input type="hidden" name="delete_exam_id" id="delete_exam_id" value="" />
				</div>
				<div class="popup__content__section popup__content__section--center">
					<button class="activate activate--dark-blue activate--white" type="button" onclick="delete_user()">확인</button>
					<button class="activate activate--red" type="button" onclick="deleteclose()">취소</button>
				</div>
			</div>
		</form>
	</div>
</section>
<!-- end confirm delete -->

<style>
.custom-select- {
	display: block;
	width: 200px;
	padding: 5px 8px;
	background-color: #ebebeb;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	font-size: 14px;
	border: 1px solid #cfcfcf;
	float:left;
}
.custom-select-- {
	display: block;
	width: 150px;
	padding: 4px 8px;
	background-color: #ebebeb;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	font-size: 14px;
	border: 1px solid #cfcfcf;
	float:left;
	margin-right:10px;
}
</style>
{include file="common/footer.tpl"}