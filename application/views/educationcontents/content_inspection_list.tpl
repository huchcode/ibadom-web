{include file="common/header.tpl"}
<style>
.custom-select-g1itch {
	width:120px;
	line-height:25px;
	padding:5px 7px;
	background-color: #EEE;
}
</style>

		<!-- content publishing -->
		<section id="content">
			<nav id="content__nav">
				home > 교육컨텐츠심사
			</nav>
			<section id="content-publishing" class="content-container">
				<h1 id="content__heading" class="blue">교육 컨텐츠 심사</h1>
				{if {$smarty.session.role} != 'admin'}
					{include file="common/permission.tpl"}

					<script type="text/javascript">
					$(document).ready(function() {
						$('div#content-publishing__options').empty();
						$('div.table-container').empty();
					});
					</script>
				{/if}
				<div id="content-publishing__options" class="table-options clearfix">
					<div class="table-options--left">
						<p class="grey">부서명</p>
						<select class="custom-select-g1itch" id="custom-select-department" data-width="150" data-placeholder="전부서">
							<option value="">전부서</option>{foreach from=$department_options item=deptmnt}
							<option value="{$deptmnt->id}" {if $department_id==$deptmnt->id}selected{/if}>{$deptmnt->name}</option>{/foreach}
						</select>
						<p class="grey">상태</p>
						<select class="custom-select-g1itch" id="custom-select-status" data-width="150" data-placeholder="심사대상">
							<option value="requested" {if $book_status=='requested'}selected{/if}>출시요청</option>
							<option value="released" {if $book_status=='released'}selected{/if}>출시</option>
							<option value="rejected" {if $book_status=='rejected'}selected{/if}>거절</option>
							<option value="closed" {if $book_status=='closed'}selected{/if}>출시중지</option>
						</select>
					</div>
					<button onclick="location.href='/my_created_contents'" id="content-publishing__my-content" class="activate activate--extend activate--dark-blue activate--white activate--icon table-options--right" style="width:162px;">
						<img src="{$base_url}assets/img/tablet_white_icon.png" alt="My education contents">
						나의 교육 컨텐츠
					</button>
				</div>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<div id="content-publishing__num" class="table__cell">
							<p>No.</p>
						</div>
						<div id="content-publishing__content-name" class="table__cell">
							<p>컨텐츠명</p>
						</div>
						<div id="content-publishing__auther" class="table__cell">
							<p>저자</p>
						</div>
						<div id="content-publishing__descrption" class="table__cell">
							<p>설명</p>
						</div>
						<div id="content-publishing__preview" class="table__cell">
							<p>미리보기</p>
						</div>
						<div id="content-publishing__handle" class="table__cell">
							<p>처리</p>
						</div>
					</div>

					{foreach from=$content_inspection_list key=i item=content}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>{$i+1}</p>
						</div>
						<div class="table__cell">
							<p>{$content->title}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$content->username}({$content->deptname})</p>
						</div>
						<div class="table__cell">
							<button class="activate activate--blue" type="button" onclick="$('.contentdescription').html('<p>{$content->description}</p>');showdetails();">설명</button>

						</div>
						<div class="table__cell table__cell--center">
							<a href="{$base_url}create_content/index/{$content->book_id}">
							<button class="activate activate--green activate--icon" type="button">
								<img src="{$base_url}assets/img/monitor_black_icon.png" alt="Preview">
								미리보기
							</button>
							</a>
						</div>
						<div class="content-publishing__handle table__cell table__cell--center" id="contentdoaction">
							{if $content->current_status == 'requested'}
								<p class="blue" onclick="$('.contentdescription').html('<p>{$content->description|replace:"'":"&#39;"}</p>');$('h3#title').text('{$content->title}');approvecontent({$content->book_id})" style="cursor:pointer;">출시</p>
								<p class="red" onclick="$('.contentdescription').html('<p>{$content->description|replace:"'":"&#39;"}</p>');$('h3#title').text('{$content->title}');rejectcontent({$content->book_id})" style="cursor:pointer;">거절</p>
							{elseif $content->current_status == 'released'}
								<p class="blue" onclick="$('.contentdescription').html('<p>{$content->description|replace:"'":"&#39;"}</p>');$('h3#title').text('{$content->title}');closedcontent({$content->book_id})" style="cursor:pointer;">출시중지</p>
							{elseif $content->current_status == 'rejected'}
								<p class="blue" onclick="$('.contentdescription').html('<p>{$content->description|replace:"'":"&#39;"}</p>');$('h3#title').text('{$content->title}');approvecontent({$content->book_id})" style="cursor:pointer;">출시</p>
							{elseif $content->current_status == 'closed'}
								<p class="blue" onclick="$('.contentdescription').html('<p>{$content->description|replace:"'":"&#39;"}</p>');$('h3#title').text('{$content->title}');approvecontent({$content->book_id})" style="cursor:pointer;">출시</p>
							{/if}

						</div>
					</div>
					{/foreach}

				</div>
				<!--<div class="table__paging">
					<a class="table__paging__first" href="">&#9668;&#9668;</a>
					<a class="table__paging__prev" href="">&#9668;</a>
					<a class="table__paging__to blue" href="">1</a>
					<a class="table__paging__to" href="">2</a>
					<a class="table__paging__to" href="">3</a>
					<a class="table__paging__to" href="">4</a>
					<a class="table__paging__next" href="">&#9658;</a>
					<a class="table__paging__last" href="">&#9658;&#9658;</a>
				</div>-->
			</section>
		</section>
		<!-- end content publishing -->

		<script>
		$("#custom-select-department").change(function() {
			var book_status = $('#custom-select-status').val();
			var dept_id = $('#custom-select-department').val();

			var redirect_url = '{$base_url}content_inspection/search/'+book_status;
			if (dept_id) { redirect_url += '/'+dept_id; }
			window.location = redirect_url;
		});
		$("#custom-select-status").change(function() {
			var book_status = $('#custom-select-status').val();
			var dept_id = $('#custom-select-department').val();

			var redirect_url = '{$base_url}content_inspection/search/'+book_status;
			if (dept_id) { redirect_url += '/'+dept_id; }
			window.location = redirect_url;
		});
		function showdetails() {
			$('#content-publishing-description-popup').show();
		}
		function hidedetails() {
			$('#content-description').html('');
			$('#content-publishing-description-popup').hide();
		}
		function approvecontent(content_id) {
			$('#content-publishing-approve-popup').show();
			$('#input_bookid').val(content_id);
			$('#input_status').val('released');
		}
		function rejectcontent(content_id) {
			$('#content-publishing-reject-popup').show();
			$('#input_bookid').val(content_id);
			$('#input_status').val('rejected');
		}
		function closedcontent(content_id) {
			$('#content-publishing-reject-popup').show();
			$('#input_bookid').val(content_id);
			$('#input_status').val('closed');
		}
		function cancelaction() {
			$('#content-publishing-approve-popup').hide();
			$('#content-publishing-reject-popup').hide();
		}
		function doaction() {
		    $.ajax({
				type: "POST",
				dataType: 'text',  
		    	url: '/content_inspection/update_status/',
				data: $('#theform').serialize(),
				success: function(res){
					$('#content-publishing-approve-popup').hide();
					$('#content-publishing-reject-popup').hide();
					if (res == 'released') {
						location.reload();
					} else if (res == 'rejected') {
						location.reload();
					} else if (res == 'closed') {
						location.reload();
					} else {
						location.reload();
					}
				}
		     });
		}
		</script>

		<form id="theform" method="POST" action="">
			<input type="hidden" id="input_bookid" name="book_id" value="" />
			<input type="hidden" id="input_status" name="status" value="" />
		</form>

		<!-- content publihsing description popup -->
		<section id="content-publishing-description-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="hidedetails()">
					<img src="../../assets/img/popup_exit.png" >
				</div>
				<div class="popup__heading">
					<h3 id="title"></h3>
					<!-- <h3 id="title">전자금융거래</h3> -->
				</div>
				<div class="popup__content">
					<div class="popup__content__section contentdescription">
					</div>
					<div class="popup__content__section popup__content__section--center">
						<!-- <button class="activate activate--dark-blue activate--white" type="button">확인</button> -->
					</div>
				</div>
			</div>
		</section>
		<!-- end content publihsing description popup -->


		<!-- content inspection action popup -->
		<section id="content-publishing-approve-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="cancelaction()">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<div class="popup__heading">
					<h3>발행</h3>
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section popup__content__section--center">
							<h3 id="title"></h3>
							<!-- <h3>전자금융거래</h3> -->
						</div>
						<div class="popup__content__section contentdescription">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-orange activate--white" type="button" onclick="doaction()">승인</button>
							<!--<button class="activate activate--grey activate--white" type="button">취소</button>-->
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end content inspection action popup -->

		<!-- content inspection action popup -->
		<section id="content-publishing-reject-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="cancelaction()">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<div class="popup__heading">
					<h3>거절</h3>
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section popup__content__section--center">
							<h3 id="title"></h3>
							<!-- <h3>전자금융거래</h3> -->
						</div>
						<div class="popup__content__section contentdescription">
						</div>
						<div class="popup__content__section">
							<input class="full-length" type="text" name="reject_reason" placeholder="거절사유를 적어주세요">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-orange activate--white" type="button" onclick="doaction()">승인</button>
							<!--<button class="activate activate--grey activate--white" type="button">취소</button>-->
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end content inspection action popup -->
		

{include file="common/footer.tpl"}