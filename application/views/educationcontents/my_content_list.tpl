{include file="common/header.tpl"}

		<script type="text/javascript">
			$(document).ready(function() {
				$('#my-content-list').on('click', '#my-content-list__create', function(){
					$('#create-content-basic-popup').show();
					$('#create-content-basic__title').val('');
					$('#description').val('');
				});

				$('#create-content-basic-popup').on('click', '#close-evaluation-list__add', function(){
					$('#create-content-basic-popup').hide();						
				});

				$('#confirmation').on('click', '#close-confirmation', function(){
					$('#confirmation').hide();						
				});

				$('#create-content-basic-popup').on('click', 'img#create_icon', function(){
					$('#create-content-icon-popup').show();
				});

				$('#create-content-basic-popup').on('click', 'img#create_bg', function(){
					$('#create-content-bg-popup').show();
				});

				$('button#iconContent').on('click', function(){
					$('#create-content-icon-popup').hide();
				});

				$('button#bgmindmap').on('click', function(){
					$('#create-content-bg-popup').hide();
				});

				$('#demo').click(function(){
				   $.ajax({
				      type: "POST",
				      url: "/create_content",
				      data: { id: dummy }
				   }).done(function( msg ) {
				      alert( "Data Saved: " + msg );
				   });
				});

				$('body').on('click', '.popup__bg', function(){
					$('#create-content-basic-popup').hide();
					$('#create-content-icon-popup').hide();
					$('#create-content-bg-popup').hide();					
				});

				$(".content.firstArrow").toggleClass("rotate");

				$(document).on('click', 'div#iconsContentdiv', function(){ 
			    	var id = $(this).data("id");
			    	$('div#iconsContentdiv').removeClass('selected');
				    $(this).addClass('selected');
				    $('img#create_icon').attr('src', $('img#img[data-id="'+id+'"]').attr('src'));
				    $('input#icon_id').val(id);
				    //$('input#background_mindmap_id').val();
				    //alert(id);
				 });

				$(document).on('click', 'div#bgmindmapContentdiv', function(){ 
			    	var id = $(this).data("id");
			    	$('div#bgmindmapContentdiv').removeClass('selected');
				    $(this).addClass('selected');
				    $('img#create_bg').attr('src', $('img#bgmindmap[data-id="'+id+'"]').attr('src'));
				    $('input#background_mindmap_id').val(id);
				    //$('input#background_mindmap_id').val();
				    //alert(id);
				 });

				//IconsContent
				$("input#icons_name").keyup(function() {
					$("#mainIcons").empty();
					var keyword = $("#icons_name").val();
					if(keyword.length > 0) {
					    $.ajax({
					      url: "/my_created_contents/get_icon_contents",
					      type: "POST",
					      data: { icons_name: keyword },
					      dataType: "json",
					      success: function(response) {
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#mainIcons").append('<div id="iconsContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--70" data-id="'+ response[i].id +'"><img id="img" src="'+ response[i].picture +'" data-id="'+ response[i].id +'"  width="70px" height="70px"></div>');
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(error);
							  alert('error '+ xhr.responseText);
							}
					    });
					}
					else {
						$.ajax({
					      url: "/my_created_contents/get_icon_contents",
					      type: "POST",
					      data: { icons_name: keyword },
					      dataType: "json",
					      success: function(response) {
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#mainIcons").append('<div id="iconsContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--70" data-id="'+ response[i].id +'"><img id="img" src="'+ response[i].picture +'" data-id="'+ response[i].id +'" width="70px" height="70px"></div>');
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(error);
							  alert('error '+ xhr.responseText);
							}
					    });
					}
					//End IconsContent
				});

				//BG Mindmap
				$("input#bgmindmap_name").keyup(function() {
					$("#mainBGMindmap").empty();
					var keyword = $("#bgmindmap_name").val();
					if(keyword.length > 0) {
					    $.ajax({
					      url: "/my_created_contents/get_background_mindmap",
					      type: "POST",
					      data: { bgmindmap_name: keyword },
					      dataType: "json",
					      success: function(response) {
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#mainBGMindmap").append('<div id="bgmindmapContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--125" data-id="'+ response[i].id +'"><img id="img" src="'+ response[i].picture +'" data-id="'+ response[i].id +'" width="125px" height="150px"></div>');
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(error);
							  alert('error '+ xhr.responseText);
							}
					    });
					}
					else {
						$.ajax({
					      url: "/my_created_contents/get_background_mindmap",
					      type: "POST",
					      data: { bgmindmap_name: keyword },
					      dataType: "json",
					      success: function(response) {
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#mainBGMindmap").append('<div id="bgmindmapContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--125" data-id="'+ response[i].id +'"><img id="img" src="'+ response[i].picture +'" data-id="'+ response[i].id +'" width="125px" height="150px"></div>');
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(error);
							  alert('error '+ xhr.responseText);
							}
					    });
					}
				});
				//End BG Mindmap
			});

		</script>

		<script type="text/javascript">
			function set_book_id(id){
				var text = $('a#'+id).text();
				$('#change_status').attr('action', '/my_created_contents/change_status/'+id);
				$('div#confirm_message').text(text+' 을(를) 출시 신청 하시겠습니까?')
				$('#confirmation').show();
			}
		</script>

		<style>
			.popup__content__section__scroll__item_active {
				background-color: #ccebf8;
			}
		</style>

		<div style="display:none">{counter start=0 skip=1}</div>

		<!-- my content list -->
		<section id="content">
			<nav id="content__nav">
				home > 프로필 > 나의 교육 컨텐츠
			</nav>
			<section id="my-content-list" class="content-container">
				<h1 id="content__heading" class="blue">나의 교육 컨텐츠</h1>
				<!-- {if {$smarty.session.role} != 'admin' and {$smarty.session.role} != 'author'}
					{include file="common/permission.tpl"}
{$smarty.session.role}
					<script type="text/javascript">
					$(document).ready(function() {
						$('div#my-content-list__options').empty();
						$('div.table-container').empty();
					});
					</script>
				{/if} -->
				<div id="my-content-list__options" class="table-options">
					{if {$smarty.session.role} eq 'admin'}
					<a href="/content_inspection">
					<button id="my-content-list__evaluate" class="activate activate--green activate--white activate--icon" style="width:158px">
						<img src="{$base_url}assets/img/content_my_list_evaluate_icon.png">
						교육컨텐츠 심사
					</button>
					</a>
					{/if}
					<button id="my-content-list__create" class="activate activate--dark-blue activate--white activate--icon">
						<img src="{$base_url}assets/img/content_my_list_create_icon.png">
						컨텐츠 생성
					</button>
				</div>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<!-- <div id="my-content-list__num" class="table__cell">
							<p>No.</p>
						</div> -->
						<div id="my-content-list__name" class="table__cell">
							<p>컨텐츠명</p>
						</div>
						<div id="my-content-list__question" class="table__cell">
							<p>출제문제</p>
						</div>
						<div id="my-content-list__status" class="table__cell">
							<p>상태</p>
						</div>
						<div id="my-content-list__print" class="table__cell">
							<p>기능</p>
						</div>
					</div>
					{foreach from=$contents item=items}
					{foreach from=$items item=value}
					<div class="table__row">
						<!-- <div class="table__cell table__cell--center">
							<p>{counter}</p>
						</div> -->
						<div class="table__cell">
							{if $value->current_status == 'rejected'}
							<p class="red">
							{elseif $value->current_status == 'released'}
							<p class="blue">
							{else}
							<p>
							{/if}
								<a id="{$value->item_id}" href="/create_content/index/{$value->item_id}">{$value->title}</a>
							</p>
						</div>
						<div class="table__cell table__cell--center">
							<p><a href="/examination_list/index/{$value->item_id}">{$value->count}</a></p>
						</div>
						<div class="table__cell">
							{if $value->current_status == 'rejected'}
							<p class="red">
							{elseif $value->current_status == 'released'}
							<p class="blue">
							{else}
							<p>
							{/if}
								{if $value->current_status == 'compilation'}
								    편집중
								    {if ! empty($value->modified)}
										({$value->modified})
									{/if}
								{elseif $value->current_status == 'requested'}
								    출시신청
								    {if ! empty($value->modified)}
										({$value->modified})
									{/if}
								{elseif $value->current_status == 'approved'}
								    출시승인
								    {if ! empty($value->modified)}
										({$value->modified})
									{/if}
								{elseif $value->current_status == 'rejected'}
								    출시거절
								    {if ! empty($value->modified)}
										({$value->modified})
									{/if}									
								{elseif $value->current_status == 'released'}
								    출시완료
								    {if ! empty($value->modified)}
										({$value->modified})
									{/if}
								{elseif $value->current_status == 'paused'}
								    출시중지
								    {if ! empty($value->modified)}
										({$value->modified})
									{/if}
								{elseif $value->current_status == 'closed'}
								    출시취소
								    {if ! empty($value->modified)}
										({$value->modified})
									{/if}
								{else}								    
								{/if}
							</p>
						</div>
						<div class="table__cell table__cell--center my-content-list__print">
							<p class="inline-middle">
								<a href="javascript:void(0);" onclick="set_book_id({$value->item_id})">
									<!-- <img src="{$base_url}assets/img/content_my_list_print_icon.png"> -->
									<span>
										{if $value->current_status == 'compilation'}
										<img src="{$base_url}assets/img/content_my_list_print_icon.png">
										    출시신청
										{elseif $value->current_status == 'requested'}
										<img src="{$base_url}assets/img/content_my_list_print_icon.png">
										    출시취소
										{elseif $value->current_status == 'rejected'}
										<img src="{$base_url}assets/img/content_my_list_print_icon.png">
										    출시신청
										{elseif $value->current_status == 'closed'}
										<img src="{$base_url}assets/img/content_my_list_print_icon.png">
										    출시신청
										{else}
										{/if}
									</span>
								</a>
							</p>
						</div>
					</div>
					<!-- <div class="table__row">
						<div class="table__cell table__cell--center">
							<p>1</p>
						</div>
						<div class="table__cell">
							<p class="blue">보슈아의 실리콘밸리 영어</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>12</p>
						</div>
						<div class="table__cell">
							<p class="blue">발행완료 (2015.01.23)</p>
						</div>
						<div class="table__cell table__cell--center my-content-list__print">
							<p class="inline-middle">
								<img src="{$base_url}assets/img/content_my_list_print_icon.png">
								<span>발행신청</span>
							</p>
						</div>
					</div>
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>1</p>
						</div>
						<div class="table__cell">
							<p class="red">보슈아의 실리콘밸리 영어</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>12</p>
						</div>
						<div class="table__cell">
							<p class="red">거절 (2015.01.23)</p>
						</div>
						<div class="table__cell table__cell--center my-content-list__print">
							<p class="inline-middle">
								<img src="{$base_url}assets/img/content_my_list_print_icon.png">
								<span>발행신청</span>
							</p>
						</div>
					</div>
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>1</p>
						</div>
						<div class="table__cell">
							<p>보슈아의 실리콘밸리 영어</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>12</p>
						</div>
						<div class="table__cell">
							<p>발행신청 (2015.01.23)</p>
						</div>
						<div class="table__cell table__cell--center my-content-list__print">
							<p class="inline-middle">
								<img src="{$base_url}assets/img/content_my_list_print_icon.png">
								<span>발행신청</span>
							</p>
						</div>
					</div> -->
					{/foreach}
					{/foreach}
				</div>
			</section>
		</section>
		<!-- end my content list -->
		<!-- create content popup delete -->
		<section id="create-content-basic-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-evaluation-list__add">
					<img src="../../assets/img/popup_exit.png">
				</div>
				<div class="popup__heading inline-middle">
					<img src="{$base_url}assets/img/content_create_icon.png">
					<h3>컨텐츠 생성</h3>
				</div>
				<div class="popup__content">
					<form method="POST" action="/create_content_basic/create_content_basic">
						<div class="popup__content__section">
							<h4 id="create-content-basic__title-heading" class="blue">컨텐츠명</h4>
							<input id="create-content-basic__title" type="text" name="title" autocomplete="off" required>
						</div>
						<div class="popup__content__section">
							<p>컨텐츠에 대한 간략한 설명</p>
							<textarea id="description" name="description" placeholder="간략한 설명을 입력해 주세요" required></textarea>
						</div>
						<div class="popup__content__section popup__content__element">
							<div class="popup__content__element__half popup__content__section--center popup__content__section--bottom">
								<img id="create_icon" class="create-content-basic__icon" src="{$base_url}assets/img/content_create_icon_image.png" width="64px" height="64px">
								<p class="popup__black">ICON 올리기/수정</p>
								<p class="blue">(사이즈권장: 512x512px)</p>
							</div>
							<div class="popup__content__element__half popup__content__section--center popup__content__section--bottom">
								<img id="create_bg" class="create-content-basic__icon" src="{$base_url}assets/img/content_create_bg_image.png" width="64px" height="64px">
								<p class="popup__black">마인드맵 배경 올리기/수정</p>
								<p class="blue">(사이즈권장: 1536x2048px)</p>
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" id="icon_id" name="icon_id">
							<input type="hidden" id="background_mindmap_id" name="background_mindmap_id">
							<button class="activate activate--dark-blue activate--white">컨텐츠 생성</button>
							<!-- <button type="button" id="cancel" class="activate activate--grey activate--white">취소</button> -->
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end create content popup delete -->
		<!-- create content icon popup -->
		<section id="create-content-icon-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">				
				<div class="popup__heading inline-middle clearfix">
					<h3 class="popup__heading__sub">아이콘 선택</h3>
					<div class="popup__heading__sub-options" style="margin-right: 0px;">
						<input id="icons_name" name="icons_name" type="text" placeholder="검색어를 입력하세요" style="width:200px">
						<!-- <p class="grey">분류</p>
						<select class="custom-select" data-width="150" data-placeholder="선택하세요">
							<option value="1">카테고리1</option>
							<option value="2">카테고리2</option>
							<option value="3">카테고리3</option>
						</select> -->
					</div>
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section">
							<div id="mainIcons" class="popup__content__section__scroll popup__content__section__scroll--70">
								{if {$iconContents|@count} gt 0}
									{foreach from=$iconContents item=items}
									<div id="iconsContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--70" data-id="{$items->id}">
										<img id="img" src="{$items->picture}" data-id="{$items->id}" width="70px" height="70px">
									</div>
									{/foreach}
								{/if}
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="iconContent" class="activate activate--dark-blue activate--white" type="button">선택</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end create content icon popup -->
		<!-- create content bg popup -->
		<section id="create-content-bg-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle clearfix">
					<h3 class="popup__heading__sub">배경 선택</h3>
					<div class="popup__heading__sub-options" style="margin-right: 0px;">
						<input id="bgmindmap_name" name="bgmindmap_name" type="text" placeholder="검색어를 입력하세요" style="width:200px">
						<!-- <p class="grey">분류</p>
						<select class="custom-select" data-width="150" data-placeholder="선택하세요">
							<option value="1">카테고리1</option>
							<option value="2">카테고리2</option>
							<option value="3">카테고리3</option>
						</select> -->
					</div>
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section">
							<div id="mainBGMindmap" class="popup__content__section__scroll popup__content__section__scroll--125">
								{if {$bgMindmap|@count} gt 0}
									{foreach from=$bgMindmap item=items}
									<div id="bgmindmapContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--125" data-id="{$items->id}">
										<img id="bgmindmap" src="{$items->picture}" data-id="{$items->id}" width="125px" height="150px">
									</div>
									{/foreach}
								{/if}
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="bgmindmap" class="activate activate--dark-blue activate--white" type="button">선택</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end create content bg popup -->
		<!-- confirmation -->
		<section id="confirmation" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-confirmation">
					<img src="../../assets/img/popup_exit.png">
				</div>
				<form id="change_status" method="POST" action="/my_created_contents/change_status">
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div id="confirm_message" class="popup__content__section--center">
							
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">확인</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end confirmation -->

{include file="common/footer.tpl"}