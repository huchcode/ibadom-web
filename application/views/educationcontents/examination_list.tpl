{include file="common/header.tpl"}
		<link rel="stylesheet" href="{$base_url}assets/css/education_contents.css">
		<style type="text/css">
		.fileUpload {
			position: relative;
			overflow: hidden;
			margin: 10px;
		}
		.fileUpload input.upload {
			position: absolute;
			top: 0;
			right: 0;
			margin: 0;
			padding: 0;
			font-size: 20px;
			cursor: pointer;
			opacity: 0;
			filter: alpha(opacity=0);
		}
		.btn {
			color: #000;
			display: inline-block;
			padding: 0px 24px;
			margin-bottom: 0;
			font-size: 14px;
			font-weight: 400;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			-ms-touch-action: manipulation;
			touch-action: manipulation;
			cursor: pointer;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			background-image: none;
			background-color: #ADDCF0;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		.multiple_input_div {
			clear:both;
		}
		</style>

		<!-- evaluation list -->
		<section id="content">
			<nav id="content__nav">
				home > 프로필 > 문제출제
			</nav>
			<section id="evaluation-list" class="content-container">
				<h1 id="content__heading" class="blue">문제출제</h1>
				<div id="evaluation-list__options" class="content-options clearfix">
					<button id="evaluation-list__order" class="activate activate--blue content-options--left" onclick="sortexamlist()">
						<input type="hidden" name="list_sort" id="list_sort" value="{$list_sort}" />
						<p>순서 <span class="blue">&#x25B2; / &#x25BC;</span></p>
					</button>
					<button id="evaluation-list__add" class="activate activate--dark-blue activate--white activate--icon content-options--right">
						<img src="{$base_url}assets/img/question_white_icon.png" alt="Add question">
						문제 추가
					</button>
				</div>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<div id="evaluation-list__num" class="table__cell">
							<p>No.</p>
						</div>
						<div id="evaluation-list__question" class="table__cell">
							<p>문제</p>
						</div>
						<div id="evaluation-list__chapter" class="table__cell">
							<p>관련단원</p>
						</div>
						<div id="evaluation-list__question-type" class="table__cell">
							<p>문제유형</p>
						</div>
						<div id="evaluation-list__answer-type" class="table__cell">
							<p>답변유형</p>
						</div>
					</div>
					{if $list_sort=='desc'}{assign var=number value=count($examination_list)}{else}{assign var=number value=1}{/if}
					{foreach from=$examination_list key=i item=content}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>{if $list_sort=='desc'}{$number--}{else}{$number++}{/if}</p>
						</div>
						<div class="table__cell" onclick="showexamdetails({$content->id})">
							<p>{$content->question}</p>
						</div>
						<div class="table__cell table__cell--center" onclick="showexamdetails({$content->id})">
							<p>{$content->title}</p>
						</div>
						<div class="table__cell table__cell--center" onclick="showexamdetails({$content->id})">
							<p>{if $content->type=='multiple'}객관식{else}주관식{/if}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p onclick="deletethisexam({$content->id})">삭제</p>
						</div>
					</div>
					{/foreach}
				</div>
				<!--<div class="paging">
					<a class="paging__first" href="">&#9668;&#9668;</a>
					<a class="paging__prev" href="">&#9668;</a>
					<a class="paging__to blue" href="">1</a>
					<a class="paging__to" href="">2</a>
					<a class="paging__to" href="">3</a>
					<a class="paging__to" href="">4</a>
					<a class="paging__next" href="">&#9658;</a>
					<a class="paging__last" href="">&#9658;&#9658;</a>
				</div>-->
			</section>
		</section>
		<!-- end evaluation list -->

		<!-- examination question basic -->
		<section id="examination-question-basic-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-evaluation-list__add">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form id="addexamform" method="POST" action="" onsubmit="return validateForm('addexamform');">
					<div class="popup__heading">
						<h3>문제추가</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<div class="popup__content__section__inline">
								<label class="popup__black">문제</label>
								<textarea class="popup__content__section__inline__long-text" name="question" placeholder="문제를 입력해주세요"></textarea>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black">첨부</label>

								<div class="fileUpload btn">
								    <label for="attached_image">파일추가</label>
								    <input type="file" id="attached_image" onchange="upload_attached_image()" class="upload" accept="image/x-png, image/gif, image/jpeg" name="attached_image" value=""/>
								</div>
								<label id="attached_photo_url_text">
									<p class="blue">첨부파일을 선택해주세요</p>
								</label>
								<input type="hidden" id="attached_photo_url" name="attached_photo_url" value="" />
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black">유형</label>
								<input type="radio" name="question_type" value="multiple" checked id="select_multiple_choice">
								<label>객관식</label>
								<input type="radio" name="question_type" value="short" id="select_text_answer">
								<label>주관식</label>
							</div>
						</div>

						<div class="popup__content__section popup__content__section--grey popup__content__section--dashed-border" id="multiple_choice_options" style="display:;min-height:70px;">
							<a id="examination-question__add" class="blue" style="cursor:pointer;" onclick="addrowmultiple()">추가</a>
							<div class="popup__content__section__inline examination-question__answer">
								<label class="popup__black" style="vertical-align:top;">정답</label>
								<div>
									<div class="multiple_input_div" style="height:1px;"></div>
								</div>	
							</div>
						</div>

						<div class="popup__content__section popup__content__section--grey popup__content__section--dashed-border" id="text_answer_options" style="display:none;min-height:70px;">
							<a id="examination-question__add" class="blue" style="cursor:pointer;" onclick="addrowtext()">추가</a>
							<div class="popup__content__section__inline examination-question__answer">
								<label class="popup__black" style="vertical-align:top;">정답</label>
								<div>
									<div class="text_input_div" style="height:1px;"></div>
								</div>
							</div>
						</div>


						<div class="popup__content__section">
							<div class="popup__content__section__inline">
								<label class="popup__black">단원</label>
								<select class="custom-select" name="content_id" data-width="200" data-placeholder="일반">
									<option value="" style="display:none;" selected disabled>일반</option>
									{foreach from=$contents key=i item=content}
										{if $content->connection_type=='root'}
											<option value="{$content->id}">{$content->title}</option>
										{else}
											<option value="{$content->id}">{$content->title}</option>
										{/if}
									{/foreach}
								</select>
							</div>
						</div>

						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">추가</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end examination question basic -->


		<!-- examination answer -->
		<section id="examination-answer-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="addrowclose()">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<div class="popup__heading">
					<h3>정답추가</h3>
				</div>
				<div class="popup__content">
					<div class="popup__content__section popup__content__section--center">
						<div class="popup__content__section__inline">
							<label class="popup__black shorten">보기</label>
							<input type="text" id="poppup_answer_input" name="popup_answer">
							<br />

							<div class="fileUpload btn">
							    <label for="answer_image">파일추가</label>
							    <input type="file" id="answer_image" onchange="upload_answer_image()" class="upload" accept="image/x-png, image/gif, image/jpeg" name="answer_image" value=""/>
							</div>
							<label id="answer_image_preview"></label>
							<input type="hidden" id="answer_image_url" name="answer_image_url" value="" />
						</div>

						<br />
						<div id="poppup_answer_correct_wrong">
						<input type="radio" id="poppup_answer_correct" name="poppup_answer_type" value="yes">
						<label>정답</label>
						<input type="radio" id="poppup_answer_wrong"  name="poppup_answer_type" value="no" checked>
						<label>오답</label>
						</div>
					</div>
					<div class="popup__content__section popup__content__section--center">
						<button class="activate activate--dark-blue activate--white" type="button" onclick="insert_answer()">추가</button>
					</div>
				</div>
			</div>
		</section>
		<!-- end examination answer -->

		<!-- examination attached -->
		<section id="examination-attached-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="uploadattachedclose()">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form method="POST" action="">
					<div class="popup__heading">
						<h3>첨부추가</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<div class="popup__content__section__inline">
								<label class="popup__black shorten">첨부</label>
								<select class="custom-select" data-width="200" data-placeholder="그림">
									<option value="1">그림</option><!--photo-->
									<option value="2">동영상</option><!--video-->
									<option value="3">사운드</option><!--audio-->
								</select>
							</div>
							<br>
							<img src="{$base_url}assets/img/content_create_image_image.png">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="button">추가</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end examination attached -->

		<!-- confirm delete -->
		<section id="examination-delete-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="deleteexamclose()">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form method="POST" action="">
					<div class="popup__heading">
						<h3>이시험을삭제?</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" name="delete_exam_id" id="delete_exam_id" value="" />
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="button" onclick="deleteexamaction()">확인</button>
							<button class="activate activate--red" type="button" onclick="deleteexamclose()">취소</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end confirm delete -->

<script type="text/javascript">
$('body').on('click', '.popup__bg', function(){
	$("#examination-delete-popup").hide();
	$("#examination-attached-popup").hide();
	$('#examination-answer-popup').hide();
	$('#examination-question-basic-popup').hide();
	$('#examination-question-popup').hide();
	$('#eqexamination-answer-popup').hide();


	$("#delete_exam_id").val('');
	$("#eqpopup_question_type" ).removeAttr("checked");
	$("#eqpopup_question_type2" ).removeAttr("checked");
	$("#eqpopup_question_array_container" ).html('<div class="popup__black" style="width:74px;float:left;padding:10px 0 0 10px">정답</div><div style="float:left;"><div class="eqpopup_question_array" style="height:1px;"></div></div><div style="clear:both;"></div>');
});
function validateForm(formid) {
    var q = document.forms[formid]["question"].value;
    var c = document.forms[formid]["content_id"].value;
    var m = document.getElementsByName("answermulti[]");
    var mt = document.getElementsByName("answermultitype[]");
    var t = document.getElementsByName("answertext[]");
    var qt = $("input[name='question_type']:checked").val();

    if (q == null || q == "") {
        alert("Question must be filled out");
        return false;
    } else if (c == null || c == "") {
        alert("해당 단원을 입력하세요.");
        return false;
    } else {
    	 if(m.length == 0 && t.length == 0){
	        alert("Missing Answer parameter");  
	        return false;
	    } else {
	    	if(qt == 'multiple' && m.length == 1){
		        alert("Add more answers in multiple choice");  
		        return false;
		    } else {

			    var cntr = 0; 
				for (i = 0; i < mt.length; i++) { 
					if (mt[i].value == 'correct') {
						cntr++;
					}
				}
				if (qt == 'multiple' && cntr == 0) {
					alert("Missing CORRECT answer for multiple choice");  
					return false;
				} else if (qt == 'multiple' && cntr > 1) {
					alert("Only 1 correct answer for multiple choice");  
					return false;
				} else {
		    		return true;
		    	}
		    }
    	}
    }
}
function upload_answer_image() {

    var file_data = $('#answer_image').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('attached_image', file_data);
    $.ajax({
    	url: '/examination_list/upload/{$book_id}',
		dataType: 'text',  
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,                         
		type: 'post',
		success: function(res){
			$("#answer_image_url").val(res);
			var get_filename = res.split("/");
			var filename = get_filename[5];
			$("label#answer_image_preview").html("<img src='"+res+"' height='50'>");
		}
     });

}
function equpload_answer_image() {

    var file_data = $('#eqanswer_image').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('attached_image', file_data);
    $.ajax({
    	url: '/examination_list/upload/{$book_id}',
		dataType: 'text',  
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,                         
		type: 'post',
		success: function(res){
			$("#eqanswer_image_url").val(res);
			var get_filename = res.split("/");
			var filename = get_filename[5];
			$("label#eqanswer_image_preview").html("<img src='"+res+"' height='50'>");
		}
     });

}
function upload_attached_image() {

    var file_data = $('#attached_image').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('attached_image', file_data);
    $.ajax({
    	url: '/examination_list/upload/{$book_id}',
		dataType: 'text',  
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,                         
		type: 'post',
		success: function(res){
			$("#attached_photo_url").val(res);
			var get_filename = res.split("/");
			var filename = get_filename[5];
			$("label#attached_photo_url_text").html("<img src='"+res+"' height='50'>");
		}
     });
}
function equpload_attached_image() {

    var file_data = $('#eqattached_image').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('attached_image', file_data);
    $.ajax({
    	url: '/examination_list/upload/{$book_id}',
		dataType: 'text',  
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,                         
		type: 'post',
		success: function(res){
			$("#eqpopup_attached_photo_url").val(res);
			var get_filename = res.split("/");
			var filename = get_filename[5];
			$("label#eqattached_photo_url_text").html("<img src='"+res+"' height='50'>");
		}
     });
}

function deletethisexam(exam_id) {
	$("#examination-delete-popup").show();
	$("#delete_exam_id").val(exam_id);
}
function deleteexamclose() {
	$("#examination-delete-popup").hide();
	$("#delete_exam_id").val('');
}
function deleteexamaction() {
	var exam_id = $("#delete_exam_id").val();

	$.ajax({
    	url: '/examination_list/ajax_delete_exam/'+exam_id,
		dataType: 'text',
		cache: false,
		contentType: false,
		processData: false,
		type: 'post',
		success: function(res){
			location.reload();
		}
	});
}
function showexamdetails(exam_id) {
    $.ajax({
    	url: '/examination_list/ajax_get_questions/'+exam_id,
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,                   
		type: 'post',
		success: function(res){
			$("#eqpopup_examid").val(res.exam.id);
			$("#eqpopup_bookid").val(res.exam.book_id);
			$(".eqpopup_question_array_container").html('<div class="eqpopup_question_array" style="height:1px;"></div>');
			if (res.exam.picture) {
				$("#eqpopup_attached_photo_url").val(res.exam.picture);

				var attached_image_html_code = '<div class="fileUpload btn">';
				attached_image_html_code += '<label for="eqattached_image">파일추가</label>';
				attached_image_html_code += '<input type="file" id="eqattached_image" onchange="equpload_attached_image()" class="upload" accept="image/x-png, image/gif, image/jpeg" name="eqattached_image" value=""/>';
				attached_image_html_code += '</div>';
				attached_image_html_code += '<label id="eqattached_photo_url_text">';
				attached_image_html_code += '<p class="blue" style="float:left;"><img src="'+res.exam.picture+'" height="50" /></p>';
				attached_image_html_code += '</label>';
				attached_image_html_code += '<div style="clear:both;"></div>';
				
			} else {
				var attached_image_html_code = '<div class="fileUpload btn">';
				attached_image_html_code += '<label for="eqattached_image">파일추가</label>';
				attached_image_html_code += '<input type="file" id="eqattached_image" onchange="equpload_attached_image()" class="upload" accept="image/x-png, image/gif, image/jpeg" name="eqattached_image" value=""/>';
				attached_image_html_code += '</div>';
				attached_image_html_code += '<label id="eqattached_photo_url_text">';
				attached_image_html_code +='<p class="blue">첨부파일을 선택해주세요</p>';;
				attached_image_html_code += '</label>';
			}
			$("#eqpopup_attached_image_container").html(attached_image_html_code);

			$( "#eqpopup_question" ).val(res.exam.question);

			/*var questiontype_container_html = '<label class="popup__black">유형</label>';
			questiontype_container_html += '<input type="radio" id="eqpopup_question_type" name="question_type" value="multiple"';
			if (res.exam.type == 'multiple') { questiontype_container_html += ' checked="checked"'; };
			questiontype_container_html += '>';
			questiontype_container_html += '<label>객관식</label>'
			questiontype_container_html += '<input type="radio" id="eqpopup_question_type2" name="question_type" value="short"';
			if (res.exam.type == 'short') { questiontype_container_html += '  checked="checked"'; };
			questiontype_container_html += '>';
			questiontype_container_html += '<label>주관식</label>';
			$( "#eqpopup_question_type_container" ).html(questiontype_container_html);
			//alert(questiontype_container_html);*/

			if (res.exam.type == 'multiple') {
				$( "#eqpopup_question_type" ).attr("checked",true);
				$( "#eqpopup_question_type2" ).removeAttr("checked");

				$('input:radio[name=question_type][value=multiple]').click();
			} else {
				$( "#eqpopup_question_type" ).removeAttr("checked");
				$( "#eqpopup_question_type2" ).attr("checked",true);

				$('input:radio[name=question_type][value=short]').click();
			}

			if (res.answer.length == 0) {
				var append_text = '<a id="examination-question__add" class="blue" style="cursor:pointer;" onclick="';
				if (res.exam.type=='multiple') {
					append_text += 'eqaddrowmultiple()';
				} else {
					append_text += 'eqaddrowtext()';
				}
				append_text += '">추가</a>';
				$("div.eqpopup_question_array:last").after(append_text);

			}

			for (i = 0; i < res.answer.length; i++) { 

				var answer_text = res.answer[i].answer_text;
				var answer_picture = res.answer[i].answer_picture;
				var correct_flag = res.answer[i].correct_flag;

				if (i==0) {
					var append_text = '<a id="examination-question__add" class="blue" style="cursor:pointer;" onclick="';
					if (res.exam.type=='multiple') {
						append_text += 'eqaddrowmultiple()';
					} else {
						append_text += 'eqaddrowtext()';
					}
					append_text += '">추가</a>';
				} else {
					var append_text = '';
				}
				append_text += '<div class="eqpopup_question_array" style="padding-bottom:5px;">';

				if (res.exam.type == 'multiple') {
					if (answer_picture && answer_picture!='null') {
						append_text += '<input type="hidden" name="answermulti[]" value="'+answer_text+'" />';
						append_text += '<div style="width:150px;float:left;"><img src="'+answer_picture+'" height="50" /></div>';
					} else {
						append_text += '<input class="extend" type="text" name="answermulti[]" placeholder="답을 입력해주세요" value="'+answer_text+'" style="float:left;width:220px;height:30px;">';
					}
					append_text += '<input type="hidden" name="answerimagemulti[]" value="'+answer_picture+'" />';
					append_text += '<p class="examination-question__answer__options" style="float:left;">';
					if (correct_flag=='correct') {
						append_text += '<input type="hidden" name="answermultitype[]" value="correct" style="display:none;">';
						append_text += '<span class="blue" id="blue_correct_answer" style="padding-left:3px">정답</span>';
					} else {
						append_text += '<input type="hidden" name="answermultitype[]" value="wrong" style="display:none;">';
						append_text += '<span style="padding-left:3px">오답</span>';
					}
				} else {
					if (answer_picture) {
						append_text += '<input type="hidden" name="answertext[]" value="'+answer_text+'" />';
						append_text += '<div style="width:150px;float:left;"><img src="'+answer_picture+'" height="50" /></div>';
					} else {
						append_text += '<input class="extend" type="text" name="answertext[]" placeholder="답을 입력해주세요" value="'+answer_text+'" style="float:left;width:220px;height:30px;">';
					}
					append_text += '<p class="examination-question__answer__options" style="float:left;">';
					append_text += '<input type="hidden" name="answertexttype[]" value="correct" style="display:none;">';
					append_text += '<input type="hidden" name="answerimagetext[]" value="'+answer_picture+'" />';
				}
				append_text += '<span class="blue" onclick="removerow(this);" style="cursor:pointer;">삭제</span>';
				append_text += '</p>';
				append_text += '<div style="clear:both;"></div>';
				append_text += '</div>';
				$("div.eqpopup_question_array:last").after(append_text);
			}
			$( "#eqpopup_content_id" ).val(res.exam.content_id);
			$( "#examination-question-popup" ).show();
		}
	});

}
function hideexamdetails() {
	$( "#examination-question-popup" ).hide();
	$( "#eqpopup_question_type" ).removeAttr("checked");
	$( "#eqpopup_question_type2" ).removeAttr("checked");
	$( "#eqpopup_question_array_container" ).html('<div class="popup__black" style="width:74px;float:left;padding:10px 0 0 10px">정답</div><div style="float:left;"><div class="eqpopup_question_array" style="height:1px;"></div></div><div style="clear:both;"></div>');
}


function eqaddrowmultiple() {
	$("#eqpoppup_answer_input").val('');
	$('input[name=eqpoppup_answer_type]:checked').val('no');

	$("#eqpoppup_answer_correct_wrong").show();
	$("#eqexamination-answer-popup").show();
}
function eqaddrowtext() {
	$("#eqpoppup_answer_input").val('');
	$('input[name=eqpoppup_answer_type]:checked').val('no');

	$("#eqpoppup_answer_correct_wrong").hide();
	$("#eqexamination-answer-popup").show();
}
function eqaddrowclose() {
	$("#eqexamination-answer-popup").hide();

}
function eqinsert_answer(){
	var answer_value = $("#eqpoppup_answer_input").val();
	var answer_correct = $('input[name="eqpoppup_answer_type"]:checked').val();
	var question_type = $('input[id=eqpopup_question_type]:checked').val();
	var answer_image_url = $('#eqanswer_image_url').val();
	
	if (question_type=='multiple') {
		eqaddrowquestionmulti(answer_value,answer_correct,answer_image_url);
	} else {
		eqaddrowquestionshort(answer_value,answer_image_url)
	}

	$("#eqexamination-answer-popup").hide();
}

function eqaddrowquestionmulti(answer_value,correct_flag,answer_image_url) {
	var append_text = '';

	append_text += '<div class="eqpopup_question_array" style="padding-bottom:5px;">';
	append_text += '<input class="extend" type="hidden" name="answerimagemulti[]" placeholder="답을 입력해주세요" value="'+answer_image_url+'">';
	if (answer_image_url) {
		append_text += '<input class="extend" type="hidden" name="answermulti[]" placeholder="답을 입력해주세요" value="'+answer_value+'">';
		append_text += '<div style="width:150px;float:left;"><img src="'+answer_image_url+'" height="50" /></div>';
	} else {
		append_text += '<input class="extend" type="text" name="answermulti[]" placeholder="답을 입력해주세요" value="'+answer_value+'" style="float:left;width:220px;height:30px;">';
	}

	append_text += '<p class="examination-question__answer__options" style="float:left;">';
	if (correct_flag=='yes') {
		append_text += '<input type="hidden" name="answermultitype[]" value="correct" style="display:none;">';
		append_text += '<span class="blue" id="blue_correct_answer" style="padding-left:3px">정답</span>';
	} else {
		append_text += '<input type="hidden" name="answermultitype[]" value="wrong" style="display:none;">';
		append_text += '<span style="padding-left:3px">오답</span>';
	}
	append_text += '<span class="blue" onclick="removerow(this);" style="cursor:pointer;">삭제</span>';
	append_text += '</p>';
	append_text += '<div style="clear:both;"></div>';
	append_text += '</div>';
	$("div.eqpopup_question_array:last").after(append_text);
}
function eqaddrowquestionshort(answer_value,answer_image_url) {
	var append_text = '';

	append_text += '<div class="eqpopup_question_array" style="padding-bottom:5px;">';
	//append_text += '<label class="popup__black" style="width:74px;"></label>';
	append_text += '<input class="extend" type="hidden" name="answerimagetext[]" placeholder="답을 입력해주세요" value="'+answer_image_url+'">';

	if (answer_image_url) {
		append_text += '<input class="extend" type="hidden" name="answertext[]" placeholder="답을 입력해주세요" value="'+answer_value+'">';
		append_text += '<div style="width:150px;float:left;"><img src="'+answer_image_url+'" height="50" /></div>';
	} else {
		append_text += '<input class="extend" type="text" name="answertext[]" placeholder="답을 입력해주세요" value="'+answer_value+'" style="float:left;width:220px;height:30px;">';
	}
	append_text += '<p class="examination-question__answer__options" style="float:left;">';

	append_text += '<input type="hidden" name="answertexttype[]" value="correct" style="display:none;">';

	append_text += '<span class="blue" onclick="removerow(this);" style="cursor:pointer;">삭제</span>';
	append_text += '</p>';
	append_text += '<div style="clear:both;"></div>';
	append_text += '</div>';
	$("div.eqpopup_question_array:last").after(append_text);
}
$( "#evaluation-list__add" ).click(function() {
  $( "#examination-question-basic-popup" ).show();
});
$( "#close-evaluation-list__add" ).click(function() {
  $( "#examination-question-basic-popup" ).hide();
});
$( "#select_multiple_choice" ).click(function() {
	$("#multiple_choice_options").show();
	$("#text_answer_options").hide();
});
$( "#select_text_answer" ).click(function() {
	$("#multiple_choice_options").hide();
	$("#text_answer_options").show();
});
$( "#evaluation-list__order" ).click(function() {
	var listsort = $("#list_sort").val();
	if (listsort=='asc') {
		location.href = '/examination_list/index/{$book_id}/desc';
	} else if (listsort=='desc') {
		location.href = '/examination_list/index/{$book_id}/asc';
	}
});

function addrowmultiple() {
	$("#poppup_answer_input").val('');
	$("#eqanswer_image_url").val('');
	$("#answer_image_preview").html('');
	$('input[name=poppup_answer_type]:checked').val('no');

	$("#poppup_answer_correct_wrong").show();
	$("#examination-answer-popup").show();
}
function addrowtext() {
	$("#poppup_answer_input").val('');
	$("#eqanswer_image_url").val('');
	$("#answer_image_preview").html('');
	$('input[name=poppup_answer_type]:checked').val('no');

	$("#poppup_answer_correct_wrong").hide();
	$("#examination-answer-popup").show();
}
function addrowclose() {
	$("#examination-answer-popup").hide();

}
function insert_answer(){
	var answer_value = $("#poppup_answer_input").val();
	var answer_correct = $('input[name=poppup_answer_type]:checked').val();
	var question_type = $('input[name=question_type]:checked').val();
	var answer_image_url = $('#answer_image_url').val();
	
	if (question_type=='multiple') {
		saverowmultiple(answer_value,answer_correct,answer_image_url);
	} else {
		saverowtext(answer_value,answer_image_url)
	}

	$("#examination-answer-popup").hide();
}


function saverowmultiple(answer_value,answer_correct,answer_image_url) {
	var append_text = '<div class="popup__content__section__inline examination-question__answer multiple_input_div">';
	//append_text += '<label class="popup__black blue" style="width:74px;"></label>';
	append_text += '<input class="extend" type="hidden" name="answerimagemulti[]" placeholder="답을 입력해주세요" value="'+answer_image_url+'">';
	if (answer_image_url) {
		append_text += '<input class="extend" type="hidden" name="answermulti[]" placeholder="답을 입력해주세요" value="'+answer_value+'">';
		append_text += '<div style="width:150px;float:left;"><img src="'+answer_image_url+'" height="50" /></div>';
	} else {
		append_text += '<input class="extend" type="text" name="answermulti[]" placeholder="답을 입력해주세요" value="'+answer_value+'" style="float:left;width:220px;height:30px;">';
	}
	append_text += '<p class="examination-question__answer__options" style="float:left;">';
	if (answer_correct=='yes') {
		//$('#blue_correct_answer').html('오답');
		//$('#blue_correct_answer').removeClass( "blue" );
		append_text += '<input type="hidden" name="answermultitype[]" value="correct" style="display:none;">';
		append_text += '<span class="blue" id="blue_correct_answer" style="padding-left:3px">정답</span>';
	} else {
		append_text += '<input type="hidden" name="answermultitype[]" value="wrong" style="display:none;">';
		append_text += '<span style="padding-left:3px">오답</span>';
	}
	append_text += '<span class="blue" onclick="removerow(this);" style="cursor:pointer;">삭제</span>';
	append_text += '</p>';
	append_text += '<div style="clear:both;"></div>';
	append_text += '</div>';
	$("div.multiple_input_div:last").after(append_text);
}
function saverowtext(answer_value,answer_image_url) {

	var append_text = '<div class="popup__content__section__inline examination-question__answer text_input_div">';
	//append_text += '<label class="popup__black blue" style="width:74px;"></label>';
	append_text += '<input class="extend" type="hidden" name="answerimagetext[]" placeholder="답을 입력해주세요" value="'+answer_image_url+'">';

	if (answer_image_url) {
		append_text += '<input class="extend" type="hidden" name="answertext[]" placeholder="답을 입력해주세요" value="'+answer_value+'">';
		append_text += '<div style="width:150px;float:left;"><img src="'+answer_image_url+'" height="50" /></div>';
	} else {
		append_text += '<input class="extend" type="text" name="answertext[]" placeholder="답을 입력해주세요" value="'+answer_value+'" style="float:left;width:220px;height:30px;">';
	}
	append_text += '<p class="examination-question__answer__options" style="float:left;">';
	append_text += '<input type="hidden" name="answertexttype[]" value="correct" style="display:none;">';
	append_text += '<span class="blue" onclick="removerow(this);" style="cursor:pointer;padding-left:3px;">삭제</span>';
	append_text += '</p>';
	append_text += '<div style="clear:both;"></div>';
	append_text += '</div>';
	$("div.text_input_div:last").after(append_text);
}
function removerow(therow) {
	$(therow).closest('div').remove();
}
</script>

		<!-- examination question -->
		<section id="examination-question-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="hideexamdetails()">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form method="POST" action="/examination_list/update/" id="editexamform" onsubmit="return validateForm('editexamform')">
					<input type="hidden" name="examination_id" id="eqpopup_examid" value="" />
					<input type="hidden" name="book_id" id="eqpopup_bookid" value="" />
					<div class="popup__heading">
						<h3>문제추가</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<div class="popup__content__section__inline">
								<label class="popup__black">문제</label>
								<textarea id="eqpopup_question" class="popup__content__section__inline__long-text" name="question" placeholder="문제를 입력해주세요"></textarea>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black">첨부</label>
								<span id="eqpopup_attached_image_container">
								</span>
								<input type="hidden" id="eqpopup_attached_photo_url" name="attached_photo_url" value="" />
							</div>
							<div class="popup__content__section__inline eqpopup_question_type_container">
								<label class="popup__black">유형</label>
								<input type="radio" id="eqpopup_question_type" name="question_type" value="multiple">
								<label>객관식</label>
								<input type="radio" id="eqpopup_question_type2" name="question_type" value="short">
								<label>주관식</label>
							</div>
						</div>
						<div class="popup__content__section popup__content__section--grey popup__content__section--dashed-border" id="eqpopup_question_array_container" style="min-height:70px;">
							<div class="popup__black" style="width:74px;float:left;padding:10px 0 0 10px">정답</div>
							<div style="float:left;">
								<div class="eqpopup_question_array" style="height:1px;"></div>
							</div>
							<div style="clear:both;"></div>
						</div>
						<div class="popup__content__section">
							<div class="popup__content__section__inline">
								<label class="popup__black">단원</label>
								<select class="custom-select-" name="content_id" data-width="200" data-placeholder="일반" id="eqpopup_content_id">{foreach from=$contents key=i item=content}
									<option value="{$content->id}">{$content->title}</option>{/foreach}
								</select>
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">수정</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end examination question -->

		<!-- examination answer -->
		<section id="eqexamination-answer-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="eqaddrowclose()">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form method="POST" action="">
					<div class="popup__heading">
						<h3>정답추가</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<div class="popup__content__section__inline">
								<label class="popup__black shorten">보기</label>
								<input type="text" id="eqpoppup_answer_input" name="popup_answer">
								<br />

								<div class="fileUpload btn">
								    <label for="eqanswer_image">파일추가</label>
								    <input type="file" id="eqanswer_image" onchange="equpload_answer_image()" class="upload" accept="image/x-png, image/gif, image/jpeg" name="eqanswer_image" value=""/>
								</div>
								<label id="eqanswer_image_preview"></label>
								<input type="hidden" id="eqanswer_image_url" name="eqanswer_image_url" value="" />
							</div>

							<br />
							<div id="eqpoppup_answer_correct_wrong">
							<input type="radio" id="eqpoppup_answer_correct" name="eqpoppup_answer_type" value="yes">
							<label>정답</label>
							<input type="radio" id="eqpoppup_answer_wrong"  name="eqpoppup_answer_type" value="no" checked>
							<label>오답</label>
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="button" onclick="eqinsert_answer()">추가</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end examination answer -->

{include file="common/footer.tpl"}