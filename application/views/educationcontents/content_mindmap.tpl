<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="{$base_url}/assets/css/base.css">
		<link rel="stylesheet" href="{$base_url}/assets/css/module.css">
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
		<title>Content Mindmap</title>
		<!-- jquery -->
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/Preview/jquery.min.js"></script>
		<!-- for parsing json -->
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/Preview/json2.js"></script>
		<script type="text/javascript">
		<!--
		var unityObjectUrl = "UnityObject2.js";
		if (document.location.protocol == 'https:')
			unityObjectUrl = unityObjectUrl.replace("http://", "https://ssl-");
		document.write('<script type="text\/javascript" src="{$base_url}assets/Preview/' + unityObjectUrl + '"><\/script>');
		-->
		</script>
		<script type="text/javascript">
		<!--
			var config = {
				width: 410, 
				height: 370,
				params: { enableDebugging:"1" }
				
			};
			var u = new UnityObject2(config);

			var id = getUrlParameter('id');
			var gfgAPI = "/api/get_contents/"+id;
			var gfgJSON;

			jQuery(function() {
				$.ajax({
				    url: gfgAPI,
				    dataType: "json",
				    // Work with the response
				    success: function( response ) {
				        gfgJSON = response
				    }
				});

				var $missingScreen = jQuery("#unityPlayer").find(".missing");
				var $brokenScreen = jQuery("#unityPlayer").find(".broken");
				$missingScreen.hide();
				$brokenScreen.hide();
				
				u.observeProgress(function (progress) {
					switch(progress.pluginStatus) {
						case "broken":
							$brokenScreen.find("a").click(function (e) {
								e.stopPropagation();
								e.preventDefault();
								u.installPlugin();
								return false;
							});
							$brokenScreen.show();
						break;
						case "missing":
							$missingScreen.find("a").click(function (e) {
								e.stopPropagation();
								e.preventDefault();
								u.installPlugin();
								return false;
							});
							$missingScreen.show();
						break;
						case "installed":
							$missingScreen.remove();
						break;
						case "first":
						break;
					}
				});
				u.initPlugin(jQuery("#unityPlayer")[0], "{$base_url}assets/Preview/Preview.unity3d");
			});
		-->
		function getUrlParameter(sParam)
		{
			var sPageURL = window.location.search.substring(1);
			var sURLVariables = sPageURL.split('&');
			for (var i = 0; i < sURLVariables.length; i++) 
			{
				var sParameterName = sURLVariables[i].split('=');
				if (sParameterName[0] == sParam) 
				{
					return sParameterName[1];
				}
			}
		}
		initalUpdate = function (json)
		{
			console.log (json);

			//Update
			$.ajax({
				url: "/api/update_position/",
				type: "POST",
				dataType: "json",
				data: { json: json },
				success: function(response) {
					console.log (response);
					//alert('success');
				},
				error: function(xhr, status, error) {
					console.log(xhr.responseText);
				  	alert('error '+ xhr.responseText);
				}
			});
		}
		saveButton = function(json)
		{
			$("button#save").fadeIn("500");
			$("button#save").attr("ref", json);
		}
		saveThis = function()
		{
			alert("Your changes has been saved!");
		}
		get_jsonData = function()
		{
			//	alert("Retrieving json data");
			//	alert(JSON.stringify(gfgJSON));
			u.getUnity().SendMessage("HERE", "LOAD", JSON.stringify(gfgJSON));
		}

		$(document).ready(function() {
				$('button#save').on('click', function() {
					var json = $(this).attr("ref");
					//Update
					$.ajax({
						url: "/api/update_position/",
						type: "POST",
						dataType: "json",
						data: { json: json },
						success: function(response) {
							u.getUnity().SendMessage("HERE", "CLEAR", "");
							$("button#save").fadeOut("500");							
						},
						error: function(xhr, status, error) {
							console.log(error);
						  	alert('error '+ xhr.responseText);
						}
					});
				});
			}
		);
		</script>
	</head>
	<body>
		</a>
		<!-- mindmap -->
		<section id="create-content-attributes-popup-pictogram" class="popup">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<form method="POST" action="">
					<div class="popup__heading inline-middle clearfix">
						<h3 class="popup__heading__sub">픽토그램</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-pictogram');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__heading inline-middle clearfix">
						
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<div class="popup__content__section__scroll popup__content__section__scroll--60">
								<!--Content here-->

								<div id="unityPlayer">
									<div class="missing">
										<a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now!">
											<img alt="Unity Web Player. Install now!" src="http://webplayer.unity3d.com/installation/getunity.png" width="193" height="63" />
										</a>
									</div>
									<div class="broken">
										<a href="http://unity3d.com/webplayer/" title="Unity Web Player. Install now! Restart your browser after install.">
											<img alt="Unity Web Player. Install now! Restart your browser after install." src="http://webplayer.unity3d.com/installation/getunityrestart.png" width="193" height="63" />
										</a>
									</div>
								</div>

							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button style="display:none;" id="save" class="activate activate--dark-blue activate--white" type="button">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end mindmap -->
	</body>
</html>