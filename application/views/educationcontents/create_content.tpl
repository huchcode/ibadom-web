{include file="common/header.tpl"}

		<script type="text/javascript" charset="utf8" src="{$base_url}assets/lib/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/easeljs-0.8.2.min.js"></script>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery.mousewheel.min.js"></script>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/async.js"></script>

		<script src="{$base_url}assets/js/jquery-ui.js"></script>

		<style type="text/css">
		#content_title, #content_subtitle, #content_pictogram, #content_pictogrambg,
		#content_detail, #content_video, #content_image, #content_sound,
		#img_content_pictogram, #img_icon, #img_bg, #edit_title, #edit_description {
			cursor: pointer;
		}
		</style>

		<script type="text/javascript">
		$(document).ready(function() {

			if('{$author_id}' != '{$smarty.session.user_id}') {
				$("button").hide();
				$('input').prop('disabled', true);
				$('textarea').prop('disabled', true);
				$("input[name=reso]:radio").prop('disabled', false);
				$("button#create-content__preview").show();
				$("button#create-content__preview_attribute").show();
				$("img#up").hide();
				$("img#down").hide();
				$("img#left").hide();
				$("font#position").hide();
			}

			var song = $('#tunog');
			var isSelected = false;
			var connection_type = "";
			var id;
			var icon_id;
			var bgmindmap_id;
			var content_id;
			var preview_vid_count = 0;
			var preview_img_count = 0;
			var preview_sound_count = 0;
			var img_preview_1 = "";
			var img_preview_2 = "";
			var img_preview_3 = "";


			var x = "<div class='popup__content__section popup__content__section--center'>" +
					"<div id='sound_delete_button' class='popup__delete-button'></div>" +
					"<p id='sound_title'></p></div>" +
					"<div id='sound' class='popup__content__section'>" +
					"<center>" +
					"<audio id='tunog' src='music.ogg' controls='controls'>" +
					"Your browser does not support the audio element." +
					"</audio>" +
					"</center>" +
					"</div>";

			$('#fileSelect').on('click', function() {
			  $('#fileElem').click();
			});

			$('div#sound_delete_button').on('click', function() {
			  $( "div#sound" ).empty();
			});

			$('#mindmap-wrap').on('click', '#content-edit-icon', function() {

				if(isSelected) {
			  		$( "#create-content-popup" ).show();
			  	} else {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}

				$('#add_choices').empty();

				if(connection_type != 'root' && connection_type != undefined)
				{
					$('#add_choices').append('<input id="same_level" type="radio" name="wheel" value="same"  TABINDEX="3" checked>' +
						'<span class="popup__black">같은레벨 휠</span>' +
						'<input id="sub_level" type="radio" name="wheel" value="child" TABINDEX="4"><span class="popup__black">하위 휠</span>');
				}
				else
				{
					$('#add_choices').append('<input id="same_level" type="radio" name="wheel" value="same"  TABINDEX="3" disabled="">' +
						'<span class="popup__black">같은레벨 휠</span>' +
						'<input id="sub_level" type="radio" name="wheel" value="child" TABINDEX="4" checked><span class="popup__black">하위 휠</span>');
				}
			});

			$('#mindmap-wrap').on('click', 'button#content-delete-icon', function() {
				if(connection_type == 'root' || connection_type == undefined)
				{
					$('#create-content-attributes-popup-alert').show();
					$("p#alert-content-popup__selected").text('루트를 삭제할 수 없습니다');
			  		return;
				}

				if(isSelected) {
			  		$( "#create-content-popup-delete" ).show();
			  		$('input#delete_child').prop('disabled', false);
			  		if(connection_type == 'leaf') {
						$('input#delete_child').prop('disabled', true);
					}
					$('input#delete_same').prop('checked',true)
			  	} else {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}
			});

			$(document).on('click', '#alert_close', function(){
				$('#create-content-attributes-popup-alert').hide();
				$("html").getNiceScroll().show();
			});

			$('#create-content-attributes-popup-alert').on('click', '#close-alert', function(){
				$('#create-content-attributes-popup-alert').hide();
				$("html").getNiceScroll().show();
			});

			$('#create-content-attributes-popup-sound').on('click', '#close-sound', function(){
					$('#create-content-attributes-popup-sound').hide();
				$("html").getNiceScroll().show();
			});

			$('#fileElem').change( function(event) {
			    $("div#sound_content").append(x);
			    $("audio").attr('src',URL.createObjectURL(event.target.files[0]));
			    $("p#sound_title").text(this.value);
			    song[0].play();
			});

			$('#info__container').on('click', '#edit_title', function(){
				$('#edit-title-popup').show();
				$('input#edit_title').val($('h3#info__name__content-name').text().trim());
				$("html").getNiceScroll().hide();
			});

			$('#info__description').on('click', '#edit_description', function(){
				$('#edit-description-popup').show();
				$('textarea#edit_subject').val($('p#info__content-description').text().trim());
				$("html").getNiceScroll().hide();
			});

			$('#mindmap-attributes').on('click', '#content_title', function(){
				if( ! isSelected) {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}
				$('#create-content-attributes-popup-title').show();
				$('input#title_attribute').val($('span#title_attribute').text().trim());
				$("html").getNiceScroll().hide();
			});

			$('#mindmap-attributes').on('click', '#content_pictogram', function(){
				if( ! isSelected) {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}

			  	$.ajax({
			      url: "/create_content/get_pictogram_contents",
			      type: "POST",
			      dataType: "json",
			      success: function(response) {
			      	console.log(response.length);
			      	$.each(response, function(i, item) {
			      		$("div#loadpictogram").append('<div id="pictogramdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--60" data-id="'+response[i].id+'"><img id="imgpictogram" src="'+response[i].picture+'" data-id="'+response[i].id+'" width="60px" height="60px"></div>');
					});
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

				$('#create-content-attributes-popup-pictogram').show();
				$('textarea#edit_subject').val($('p#info__content-description').text().trim());
				$("html").getNiceScroll().hide();
			});

			$('#mindmap-attributes').on('click', '#content_pictogrambg', function(){
				if( ! isSelected) {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}
				$('#create-content-pictogrambg-popup').show();
				$('textarea#edit_subject').val($('p#info__content-description').text().trim());
				$("html").getNiceScroll().hide();
			});

			$('#mindmap-attributes').on('click', '#content_subtitle', function(){
				if( ! isSelected) {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}
				$('#create-content-attributes-popup-subject').show();
				$('textarea#subtitle').val($('span#subtitle').text().trim());
				$("html").getNiceScroll().hide();
			});

			$('#mindmap-attributes').on('click', '#content_detail', function(){
				if( ! isSelected) {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}
				$('#create-content-attributes-popup-description').show();

				if($('span#description').text().trim() != "None") {
					CKEDITOR.instances.editor1.setData($('span#description').text().trim());
					//$('textarea#editor1').val($('span#description').text().trim());
				} else {
					CKEDITOR.instances.editor1.setData('');
				}
				$("html").getNiceScroll().hide();
			});

			$('#mindmap-attributes').on('click', '#content_video', function(){
				if( ! isSelected) {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}

			  	//Video
			     $.ajax({
			      url: "/create_content/get_contentsVideo/"+content_id,
			      type: "POST",
			      dataType: "json",
			      //data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
			      success: function(response) {
			      	$("video#vid_content1").removeAttr('src');
			     	$("video#vid_content2").removeAttr('src');
			     	$("video#vid_content3").removeAttr('src');
			      	var total = response.length;
			      	if(total > 0)
			      	{
			      		$("span#vid_count").text(total);
			      		var x = 0;
				      	$.each(response, function(i, item) {
				      		//x = x + 1;
				      		if(response[i].video.indexOf("contentsVideo1") > -1){
							    x = 1;
							}

							if(response[i].video.indexOf("contentsVideo2") > -1){
							    x = 2;
							}

							if(response[i].video.indexOf("contentsVideo3") > -1){
							    x = 3;
							}

						    $("video#vid_content"+x).attr('src', response[i].video);
						});
			      	}
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

				$('#create-content-attributes-popup-movie').show();
				$('textarea#edit_subject').val($('p#info__content-description').text().trim());
				$("html").getNiceScroll().hide();
			});

			$('#mindmap-attributes').on('click', '#content_image', function(){
				if( ! isSelected) {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}

			  	//Image
			     $.ajax({
			      url: "/create_content/get_contentsAlbum/"+content_id,
			      type: "POST",
			      dataType: "json",
			      //data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
			      success: function(response) {
			      	$("img#img_content1").removeAttr('src');
				    $("img#img_content2").removeAttr('src');
				    $("img#img_content3").removeAttr('src');
			      	var total = response.length;
			      	if(total > 0)
			      	{
			      		$("span#img_count").text(total);
			      		var x = 0;
				      	$.each(response, function(i, item) {
				      		//x = x + 1;
				      		if(response[i].picture.indexOf("contentsAlbum1") > -1){
							    x = 1;
							}

							if(response[i].picture.indexOf("contentsAlbum2") > -1){
							    x = 2;
							}

							if(response[i].picture.indexOf("contentsAlbum3") > -1){
							    x = 3;
							}

						    $("img#img_content"+x).attr('src', response[i].picture);
						});

						//alert($("input#img_upload1").val());
			      	}
			      },
			      error: function(xhr, status, error) {
			      		console.log(error);
					  	alert('error '+ xhr.responseText);
					}
			    });

				$('#create-content-attributes-popup-image').show();
				$('textarea#edit_subject').val($('p#info__content-description').text().trim());
				$("html").getNiceScroll().hide();
			});

			$('#mindmap-attributes').on('click', '#content_sound', function(){
				if( ! isSelected) {
			  		$('#create-content-attributes-popup-alert').show();
			  		return;
			  	}

			  	//Sound
			     $.ajax({
			      url: "/create_content/get_contentsSound/"+content_id,
			      type: "POST",
			      dataType: "json",
			      //data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
			      success: function(response) {
			      	var total = response.length;
			      	if(total > 0)
			      	{
			      		$("span#sound_count").text(total);
			      		$("span#sound_title").text("1");
			      		var x = 0;
				      	$.each(response, function(i, item) {
				      		x = x + 1;
						    $("audio#sound_content"+x).attr('src', response[i].voice);
						});
			      	} else {
			      		$("span#sound_title").text("0");
			      	}
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

				$('#create-content-attributes-popup-sound').show();
				$('textarea#edit_subject').val($('p#info__content-description').text().trim());
				$("html").getNiceScroll().hide();
			});

			// $('section#create-content').on('click', '#create-content__preview', function(){
			// 	$('#preview').show();
			// 	$("html").getNiceScroll().hide();
			// });

			$(document).on('click', '#create-content__preview_attribute', function(){
				$('#attribute-preview').show();
				$("html").getNiceScroll().hide();
			});

			$('#create-content').on('click', '#create-content__delete', function(){
				var text = $('h3#info__name__content-name').text();
				$('div#delete_message').text(text+' 을(를) 삭제하시겠습니까?');
				$('#confirmation').show();
				$("html").getNiceScroll().hide();
			});

			$('#confirmation').on('click', '#close-confirmation', function(){
				$('#confirmation').hide();
			});

			$('#fileexceeded').on('click', '#close-fileexceeded', function(){
				$('#fileexceeded').hide();
			});

			$('body').on('click', '.popup__bg', function(){
				//$('#create-content-attributes-popup-mindmap-preview').hide();
				$('#edit-title-popup').hide();
				$('#edit-description-popup').hide();
				$('#create-content-attributes-popup-title').hide();
				$('#create-content-attributes-popup-pictogram').hide();
				$('#create-content-attributes-popup-subject').hide();
				$('#create-content-attributes-popup-description').hide();
				$('#create-content-attributes-popup-movie').hide();
				$('#create-content-attributes-popup-image').hide();
				$('#create-content-attributes-popup-sound').hide();
				$('#create-content-pictogrambg-popup').hide();
				$('#create-content-popup-delete').hide();
				$('#create-content-popup').hide();
				$('#create-book-pictogrambg-popup').hide();
				$('#create-content-icon-popup').hide();
				$('#create-content-bg-popup').hide();
				$('#create-content-attributes-popup-mindmap-preview').hide();
				$('#attribute-preview').hide();
				$('#attribute-preview-video').hide();
				$('#attribute-preview-image').hide();
				$('#attribute-preview-sound').hide();
				$("label#mindmap_message").hide();				
				$("button#save").hide();
				$('#confirmation').hide();
				$("audio#sound_content1").attr('src','');
				$("video#vid_content1").attr('src','');
				$("video#vid_content2").attr('src','');
				$("video#vid_content3").attr('src','');
				$("p#sound_title").text('');
				$("html").getNiceScroll().show();
			});

			$('.node').click(function(){
			     // var clickedItemID = $(this).attr('id');
			     var type = $(this).data("type");
			     id = $(this).data("id");
			     content_id = id;
			     var text = $(this).text().trim();
			     //alert(id);
			     $("p#delete-content-popup__selected").text('선택휠: ' + text);
			     isSelected = true;
			     connection_type = type;

			     //alert(type);

			     if(type != 'leaf') {
			     	$('span#end_wheel').text('아니오');
			     } else {
			     	$('span#end_wheel').text('예');
			     }

			     $.ajax({
			        url: '{$base_url}create_content/parent_id/',
			        type: 'POST',
			        data: {
			            item_id: id
			        },
			        // success: function() {
			        //     // alert(item_id);
			        //     window.location.href = '{$base_url}insert_child_content';
			        // }
			    });

			     $("img#img_content1").removeAttr('src');
			     $("img#img_content2").removeAttr('src');
			     $("img#img_content3").removeAttr('src');
			     $("video#vid_content1").removeAttr('src');
			     $("video#vid_content2").removeAttr('src');
			     $("video#vid_content3").removeAttr('src');
			     $("audio#sound_content1").removeAttr('src');
			     $("span#vid_count").text('0');
			     $("span#sound_count").text('0');
			     $("span#img_count").text('0');

			     //Image
			     $.ajax({
			      url: "/create_content/get_contentsAlbum/"+id,
			      type: "POST",
			      dataType: "json",
			      //data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
			      success: function(response) {
			      	$("img#img_content1").removeAttr('src');
				    $("img#img_content2").removeAttr('src');
				    $("img#img_content3").removeAttr('src');
			      	var total = response.length;
			      	preview_img_count = total;
			      	if(total > 0)
			      	{
			      		$("span#img_count").text(total);
			      		var x = 0;
				      	$.each(response, function(i, item) {
				      		//x = x + 1;
				      		if(response[i].picture.indexOf("contentsAlbum1") > -1){
							    x = 1;
							    img_preview_1 = response[i].picture;
							}

							if(response[i].picture.indexOf("contentsAlbum2") > -1){
							    x = 2;
							    img_preview_2 = response[i].picture;
							}

							if(response[i].picture.indexOf("contentsAlbum3") > -1){
							    x = 3;
							    img_preview_3 = response[i].picture;
							}

						    $("img#img_content"+x).attr('src', response[i].picture);
						});

						//alert($("input#img_upload1").val());
			      	}
			      },
			      error: function(xhr, status, error) {
			      		console.log(error);
					  	alert('error '+ xhr.responseText);
					}
			    });

			     //Video
			     $.ajax({
			      url: "/create_content/get_contentsVideo/"+id,
			      type: "POST",
			      dataType: "json",
			      //data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
			      success: function(response) {
			      	$("video#vid_content1").removeAttr('src');
			     	$("video#vid_content2").removeAttr('src');
			     	$("video#vid_content3").removeAttr('src');
			     	$("video#homevideo").attr('src', '');
			      	var total = response.length;
			      	preview_vid_count = total;
			      	if(total > 0)
			      	{
			      		$("span#vid_count").text(total);
			      		var x = 0;
				      	$.each(response, function(i, item) {
				      		//x = x + 1;
				      		if(response[i].video.indexOf("contentsVideo1") > -1){
							    x = 1;
							}

							if(response[i].video.indexOf("contentsVideo2") > -1){
							    x = 2;
							}

							if(response[i].video.indexOf("contentsVideo3") > -1){
							    x = 3;
							}

						    $("video#vid_content"+x).attr('src', response[i].video);
						});
			      	}
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

			     //Sound
			     $.ajax({
			      url: "/create_content/get_contentsSound/"+id,
			      type: "POST",
			      dataType: "json",
			      //data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
			      success: function(response) {
			      	var total = response.length;
			      	preview_sound_count = total;
			      	if(total > 0)
			      	{
			      		$("span#sound_count").text(total);
			      		$("span#sound_title").text("1");
			      		var x = 0;
				      	$.each(response, function(i, item) {
				      		x = x + 1;
						    $("audio#sound_content"+x).attr('src', response[i].voice);
						    $("audio#sound_preview").attr('src', response[i].voice);
						});
			      	} else {
			      		$("span#sound_title").text("0");
			      	}
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

			     //Description
			     $.ajax({
			      url: "/create_content/get_contentsDescription/"+id,
			      type: "POST",
			      dataType: "json",
			      //data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
			      success: function(response) {
			      	var total = response.length;
			      	if(total > 0)
			      	{
				      	$.each(response, function(i, item) {
				      		var desc = response[i].description;
				      		if(desc == null)
				      		{
				      			desc = "None";
				      		}
						    $("span#description").text(desc);

						    if(desc == "None") desc = "최종휠만 상세설명이 보여집니다.";
						    $("div#description_preview").html(desc);
						});
			      	}
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

				//Subtitle
				$.ajax({
					url: "/create_content/get_contentsSubtitle/"+id,
					type: "POST",
					dataType: "json",
					//data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
					success: function(response) {
						var total = response.length;
						if(total > 0)
						{
					  	$.each(response, function(i, item) {
					  		var subtitle = response[i].subtitle;
					  		if(subtitle == null)
					  		{
					  			subtitle = "None";
					  		}
						    $("span#subtitle").text(subtitle);
						    $("div#subtitle_preview").text(subtitle);
						});
						}
					},
					error: function(xhr, status, error) {
						console.log(error);
					  alert('error '+ xhr.responseText);
					}
				});

				//Pictogram
				$.ajax({
					url: "/create_content/get_contentsPictogram/"+id,
					type: "POST",
					dataType: "json",
					//data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
					success: function(response) {
						var total = response.length;
						if(total > 0)
						{
						  	$.each(response, function(i, item) {
						  		$.ajax({
								    url: response[i].picture,
								    type:'HEAD',
								    error: function()
								    {
								        $("img#img_pictogram").attr('src', '/assets/img/content_document.png');
								    },
								    success: function()
								    {
								        $('img#img_pictogram').attr('src', response[i].picture);
								    }
								});
							});
						} else {
							$("img#img_pictogram").attr('src', '/assets/img/content_document.png');
						}
					},
					error: function(xhr, status, error) {
						console.log(error);
					  alert('error '+ xhr.responseText);
					}
				});

				//PictogramBackground
				$.ajax({
					url: "/create_content/get_contentsPictogramBackground/"+id,
					type: "POST",
					dataType: "json",
					//data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
					success: function(response) {
						var total = response.length;
						if(total > 0)
						{
						  	$.each(response, function(i, item) {
						  		$.ajax({
								    url: response[i].picture,
								    type:'HEAD',
								    error: function()
								    {
								        $("img#img_pictogramBackground").attr('src', '/assets/img/content_document.png');
								    },
								    success: function()
								    {
								        $('img#img_pictogramBackground').attr('src', response[i].picture);
								    }
								});
							});
						} else {
							$("img#img_pictogramBackground").attr('src', '/assets/img/content_document.png');
						}
					},
					error: function(xhr, status, error) {
						console.log(error);
					  alert('error '+ xhr.responseText);
					}
				});

				$('h3#attribute_title_preview').text(text);
			    $('span#title_attribute').text(text);
				$('#add_content').attr('action', '/create_content/insert_content/'+id);
		     	$('#delete_content').attr('action', '/create_content/delete_content/'+id);
		     	$('form#title_attribute').attr('action', '/create_content/update_content/'+id);
		     	$('#subtitle_attribute').attr('action', '/create_content_attribute/contentsSubtitle/'+id);
		     	$('#description_attribute').attr('action', '/create_content_attribute/contentsDetail/'+id);
		     	$('#contentsAlbum').attr('action', '/create_content/insert_contentsAlbum/'+id);
		     	$('#contentsVideo').attr('action', '/create_content/insert_contentsVideo/'+id);
		     	$('#contentsSound').attr('action', '/create_content/insert_contentsSound/'+id);

		     	if(type == 'root' || type == undefined) {
		     		$('#add_content').attr('action', '/create_content/insert_child_content/'+id);
		     	}
			});

		    $(document).on('click', 'input#same_level', function(){
		    	$('#add_content').attr('action', '/create_content/insert_content/'+id);
			     //alert($('#add_content').attr('action'));
			 });

		    $(document).on('click', 'input#sub_level', function(){
		    	$('#add_content').attr('action', '/create_content/insert_child_content/'+id);
			     //alert($('#add_content').attr('action'));
			 });

		    $(document).on('click', 'input#delete_same', function(){
		    	$('#delete_content').attr('action', '/create_content/delete_content/'+id);
			     //alert($('#delete_content').attr('action'));
			 });

		    $(document).on('click', 'input#delete_child', function(){
		    	$('#delete_content').attr('action', '/create_content/delete_child/'+id);
			     //alert($('#delete_content').attr('action'));
			});

			$('#create-content__info').on('click', 'img#img_pictogram', function(){
				$('#create-content-pictogrambg-popup').show();
				$("html").getNiceScroll().hide();
			});

			$('#create-content__info').on('click', 'img#img_content_pictogram', function(){
				$('#create-book-pictogrambg-popup').show();
				$("html").getNiceScroll().hide();
			});

		    $('#create-content__info').on('click', 'img#img_icon', function(){
				$('#create-content-icon-popup').show();
				$("html").getNiceScroll().hide();
			});

			$('#create-content__info').on('click', 'img#img_bg', function(){
				$('#create-content-bg-popup').show();
				$("html").getNiceScroll().hide();
			});

			$('button#contentpictogrambg').on('click', function(){
				$.ajax({
			      url: "/create_content/update_pictogram_bg",
			      type: "POST",
			      data: { book_id: {$book_id}, icon_id: icon_id },
			      success: function(response) {
			      	$('img#img_content_pictogram').attr('src', $('img#contentimgpictogrambg[data-id="'+icon_id+'"]').attr('src'));
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

				$('#create-book-pictogrambg-popup').hide();
				$("html").getNiceScroll().show();
			});

			$('button#iconContent').on('click', function(){
				$.ajax({
			      url: "/create_content/update_icon",
			      type: "POST",
			      data: { book_id: {$book_id}, icon_id: icon_id },
			      success: function(response) {
			      	$('img#img_icon').attr('src', $('img#img[data-id="'+icon_id+'"]').attr('src'));
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

				$('#create-content-icon-popup').hide();
				$("html").getNiceScroll().show();
			});

			$('button#bgmindmap').on('click', function(){
				$.ajax({
			      url: "/create_content/update_bgmindmap",
			      type: "POST",
			      data: { book_id: {$book_id}, bgmindmap_id: bgmindmap_id },
			      success: function(response) {
			      	$('img#img_bg').attr('src', $('img#bgmindmap[data-id="'+bgmindmap_id+'"]').attr('src'));
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });
				$('#create-content-bg-popup').hide();
				$("html").getNiceScroll().show();
			});

			$(document).on('click', 'div#pictogramdiv', function(){
		    	var id = $(this).data("id");
		    	$('div#pictogramdiv').removeClass('selected');
			    $(this).addClass('selected');
			    //$('img#img_pictogram').attr('src', '/assets/img/content_document.png');
			    // $('img#img_pictogram').attr('src', $('img#imgpictogram[data-id="'+id+'"]').attr('src'));
			    icon_id = id;
			    $('input#pictogram_id').val(id);
			    //$('input#background_mindmap_id').val();
			    //alert(id);
			 });

			$(document).on('click', 'div#pictogrambgdiv', function(){
		    	var id = $(this).data("id");
		    	$('div#pictogrambgdiv').removeClass('selected');
			    $(this).addClass('selected');
			    // $('img#img_pictogramBackground').attr('src', $('img#imgpictogrambg[data-id="'+id+'"]').attr('src'));
			    icon_id = id;
			    $('input#img_pictogramBackground_id').val(id);
			    //$('input#background_mindmap_id').val();
			    //alert(id);
			 });

			$(document).on('click', 'div#contentpictogrambgdiv', function(){
		    	var id = $(this).data("id");
		    	$('div#contentpictogrambgdiv').removeClass('selected');
			    $(this).addClass('selected');
			    // $('img#img_content_pictogram').attr('src', $('img#contentimgpictogrambg[data-id="'+id+'"]').attr('src'));
			    icon_id = id;
			    $('input#img_pictogramBackground_id').val(id);
			    //$('input#background_mindmap_id').val();
			    //alert(id);
			 });

			$(document).on('click', 'div#iconsContentdiv', function(){
		    	var id = $(this).data("id");
		    	$('div#iconsContentdiv').removeClass('selected');
			    $(this).addClass('selected');
			    // $('img#img_icon').attr('src', $('img#img[data-id="'+id+'"]').attr('src'));
			    icon_id = id;
			    $('input#icon_id').val(id);
			    //$('input#background_mindmap_id').val();
			    //alert(id);
			 });

			$(document).on('click', 'div#bgmindmapContentdiv', function(){
		    	var id = $(this).data("id");
		    	$('div#bgmindmapContentdiv').removeClass('selected');
			    $(this).addClass('selected');
			    // $('img#img_bg').attr('src', $('img#bgmindmap[data-id="'+id+'"]').attr('src'));
			    bgmindmap_id = id;
			    $('input#background_mindmap_id').val(id);
			    //$('input#background_mindmap_id').val();
			    //alert(id);
			 });

			//Up Down Left
			$("img#up").on("click", function() {
				if(connection_type != 'root' && connection_type != undefined) {
					$.ajax({
						url: "/create_content/update_content_sort",
						type: "POST",
						//dataType: "json",
						data: { position: "up", content_id: content_id },
						success: function(response) {
							if(response == "1") {
								location.reload();
							}
						},
						error: function(xhr, status, error) {
							console.log(error);
						  alert('error '+ xhr.responseText);
						}
					});
				}
			});

			$("img#down").on("click", function() {
				if(connection_type != 'root' && connection_type != undefined) {
					$.ajax({
						url: "/create_content/update_content_sort",
						type: "POST",
						//dataType: "json",
						data: { position: "down", content_id: content_id },
						success: function(response) {
							if(response == "1") {
								location.reload();
							}
						},
						error: function(xhr, status, error) {
							console.log(error);
						  alert('error '+ xhr.responseText);
						}
					});
				}
			});

			$("img#left").on("click", function() {
				if(connection_type != 'root' && connection_type != undefined) {
					$.ajax({
						url: "/create_content/update_content_sort",
						type: "POST",
						//dataType: "json",
						data: { position: "left", content_id: content_id },
						success: function(response) {
							if(response == "1") {
								location.reload();
							}
						},
						error: function(xhr, status, error) {
							console.log(error);
						  alert('error '+ xhr.responseText);
						}
					});
				}
			});
			//End Up Down Left

			//Previous Next
			$("img#preview_left").on("click", function() {
				$.ajax({
					url: "/create_content/attribute_preview",
					type: "POST",
					dataType: "json",
					data: { position: "previous", content_id: content_id },
					success: function(response) {
						var total = response.length;
						if(total > 0)
						{
						  	$.each(response, function(i, item) {
						  		var subtitle = response[i].subtitle;
						  		var title = response[i].title;
						  		var desc = response[i].description;
						  		content_id = response[i].id;
						  		if(subtitle == null)
						  		{
						  			subtitle = "None";
						  		}
						  		$("h3#attribute_title_preview").text(title);
							    $("div#subtitle_preview").text(subtitle);
							    $("div#description_preview").html(desc);
							});

							$("div.node__text").each(function(){
								id = $(this).data("id");
							    if(id == content_id) {
							        $(this).click();
							    }
							});
						}
					},
					error: function(xhr, status, error) {
						console.log(error);
					  alert('error '+ xhr.responseText);
					}
				});
			});

			$("img#preview_right").on("click", function() {
				$.ajax({
					url: "/create_content/attribute_preview",
					type: "POST",
					dataType: "json",
					data: { position: "next", content_id: content_id },
					success: function(response) {
						var total = response.length;
						if(total > 0)
						{
						  	$.each(response, function(i, item) {
						  		var subtitle = response[i].subtitle;
						  		var title = response[i].title;
						  		var desc = response[i].description;
						  		content_id = response[i].id;
						  		if(subtitle == null)
						  		{
						  			subtitle = "None";
						  		}
						  		$("h3#attribute_title_preview").text(title);
							    $("div#subtitle_preview").text(subtitle);
							    $("div#description_preview").html(desc);
							});

							$("div.node__text").each(function(){
								id = $(this).data("id");
							    if(id == content_id) {
							        $(this).click();
							    }
							});
						}
					},
					error: function(xhr, status, error) {
						console.log(error);
					  	alert('error '+ xhr.responseText);
					}
				});
			});
			//End Previous Next

			$('button#pictogram').on('click', function(){
				$.ajax({
			      url: "/create_content/update_content_pictogram",
			      type: "POST",
			      data: { content_id: content_id, icon_id: icon_id },
			      success: function(response) {
			      	$('img#img_pictogram').attr('src', '/assets/img/content_document.png');
			      	$('img#img_pictogram').attr('src', $('img#imgpictogram[data-id="'+icon_id+'"]').attr('src'));
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

				$('#create-content-attributes-popup-pictogram').hide();
				$("html").getNiceScroll().show();
			});

			$('button#pictogrambg').on('click', function(){
				$.ajax({
			      url: "/create_content/update_content_pictogrambg",
			      type: "POST",
			      data: { content_id: content_id, icon_id: icon_id },
			      success: function(response) {
			      	$('img#img_pictogramBackground').attr('src', $('img#imgpictogrambg[data-id="'+icon_id+'"]').attr('src'));
			      },
			      error: function(xhr, status, error) {
			      	console.log(error);
					  alert('error '+ xhr.responseText);
					}
			    });

				$('#create-content-pictogrambg-popup').hide();
				$("html").getNiceScroll().show();
			});

			//IconsContent
			$("input#icons_name").keyup(function() {
				$("#mainIcons").empty();
				var keyword = $("#icons_name").val();
				if(keyword.length > 0) {
				    $.ajax({
				      url: "/my_created_contents/get_icon_contents",
				      type: "POST",
				      data: { icons_name: keyword },
				      dataType: "json",
				      success: function(response) {
				      	var total = response.length;
				      	if(total > 0)
				      	{
					      	$.each(response, function(i, item) {
							    $("#mainIcons").append('<div id="iconsContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--70" data-id="'+ response[i].id +'"><img id="img" src="'+ response[i].picture +'" data-id="'+ response[i].id +'"  width="70px" height="70px"></div>');
							});
				      	}
				      },
				      error: function(xhr, status, error) {
				      	console.log(error);
						  alert('error '+ xhr.responseText);
						}
				    });
				}
				else {
					$.ajax({
				      url: "/my_created_contents/get_icon_contents",
				      type: "POST",
				      data: { icons_name: keyword },
				      dataType: "json",
				      success: function(response) {
				      	var total = response.length;
				      	if(total > 0)
				      	{
					      	$.each(response, function(i, item) {
							    $("#mainIcons").append('<div id="iconsContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--70" data-id="'+ response[i].id +'"><img id="img" src="'+ response[i].picture +'" data-id="'+ response[i].id +'"  width="70px" height="70px"></div>');
							});
				      	}
				      },
				      error: function(xhr, status, error) {
				      	console.log(error);
						  alert('error '+ xhr.responseText);
						}
				    });
				}
			});
			//End IconsContent

			//BG Mindmap
			$("input#bgmindmap_name").keyup(function() {
				$("#mainBGMindmap").empty();
				var keyword = $("#bgmindmap_name").val();
				if(keyword.length > 0) {
				    $.ajax({
				      url: "/my_created_contents/get_background_mindmap",
				      type: "POST",
				      data: { bgmindmap_name: keyword },
				      dataType: "json",
				      success: function(response) {
				      	var total = response.length;
				      	if(total > 0)
				      	{
					      	$.each(response, function(i, item) {
							    $("#mainBGMindmap").append('<div id="bgmindmapContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--125" data-id="'+ response[i].id +'"><img id="bgmindmap" src="'+ response[i].picture +'" data-id="'+ response[i].id +'" width="125px" height="150px"></div>');
							});
				      	}
				      },
				      error: function(xhr, status, error) {
				      	console.log(error);
						  alert('error '+ xhr.responseText);
						}
				    });
				}
				else {
					$.ajax({
				      url: "/my_created_contents/get_background_mindmap",
				      type: "POST",
				      data: { bgmindmap_name: keyword },
				      dataType: "json",
				      success: function(response) {
				      	var total = response.length;
				      	if(total > 0)
				      	{
					      	$.each(response, function(i, item) {
							    $("#mainBGMindmap").append('<div id="bgmindmapContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--125" data-id="'+ response[i].id +'"><img id="bgmindmap" src="'+ response[i].picture +'" data-id="'+ response[i].id +'" width="125px" height="150px"></div>');
							});
				      	}
				      },
				      error: function(xhr, status, error) {
				      	console.log(error);
						  alert('error '+ xhr.responseText);
						}
				    });
				}
			});
			//End BG Mindmap

			var video_count = 1, image_count = 1, videoPlayer = document.getElementById("homevideo");

			var play = document.getElementById("playButton");
			if (play.addEventListener) {
				play.addEventListener("click", vidplay, false);
			} else {
				play.attachEvent('onclick', vidplay);
			}

			var el = document.getElementById("nextButton");
			if (el.addEventListener) {
				el.addEventListener("click", videoNext, false);
			} else {
				el.attachEvent('onclick', videoNext);
			}

			var el = document.getElementById("previousButton");
			if (el.addEventListener) {
				el.addEventListener("click", videoPrevious, false);
			} else {
				el.attachEvent('onclick', videoPrevious);
			}

			var el = document.getElementById("img_nextButton");
			if (el.addEventListener) {
				el.addEventListener("click", imageNext, false);
			} else {
				el.attachEvent('onclick', imageNext);
			}

			var el = document.getElementById("img_previousButton");
			if (el.addEventListener) {
				el.addEventListener("click", imagePrevious, false);
			} else {
				el.attachEvent('onclick', imagePrevious);
			}

			function vidplay() {
		       if (videoPlayer.paused) {
		          videoPlayer.play();
		          //button.textContent = "||";
		          play.setAttribute('src', '{$base_url}assets/img/attribute_preview/pause_btn.png');
		       } else {
		          videoPlayer.pause();
		          //button.textContent = ">";
		          play.setAttribute('src', '{$base_url}assets/img/attribute_preview/play_btn.png');
		       }
		    }

			function videoNext(){
				//alert(content_id);
				video_count++;
				if (video_count > preview_vid_count) video_count = 1;
				var nextVideo = "/assets/uploads/contentsVideo/"+ content_id +"/contentsVideo"+video_count+".mp4";
				videoPlayer.src = nextVideo;
				play.setAttribute('src', '{$base_url}assets/img/attribute_preview/play_btn.png');
				//videoPlayer.play();
			}

			function videoPrevious(){
				//alert(content_id);
				video_count--;
				if (video_count == 0) video_count = preview_vid_count;
				var nextVideo = "/assets/uploads/contentsVideo/"+ content_id +"/contentsVideo"+video_count+".mp4";
				videoPlayer.src = nextVideo;
				play.setAttribute('src', '{$base_url}assets/img/attribute_preview/play_btn.png');
				//videoPlayer.play();
			}

			function imageNext(){
				//alert(content_id);
				image_count++;
				if (image_count > preview_img_count) image_count = 1;

				if(image_count == 1) {
					$("img#img_preview").attr('src', img_preview_1);
				}

				if(image_count == 2) {
					$("img#img_preview").attr('src', img_preview_2);
				}

				if(image_count == 3) {
					$("img#img_preview").attr('src', img_preview_3);
				}
			}

			function imagePrevious(){
				//alert(content_id);
				image_count--;
				if (image_count == 0) image_count = preview_img_count;

				if(image_count == 1) {
					$("img#img_preview").attr('src', img_preview_1);
				}

				if(image_count == 2) {
					$("img#img_preview").attr('src', img_preview_2);
				}

				if(image_count == 3) {
					$("img#img_preview").attr('src', img_preview_3);
				}
			}

			$("video#homevideo").bind("ended", function() {
			    play.setAttribute('src', '{$base_url}assets/img/attribute_preview/play_btn.png');
			});

			$("img#vid_preview").on("click", function(){
				if(preview_vid_count > 0) {
					video_count = 1;
					$("video#homevideo").attr('src', '/assets/uploads/contentsVideo/'+ content_id +'/contentsVideo1.mp4');
					$("section#attribute-preview-video").show();
				}
			});

			$("img#image_preview").on("click", function(){
				if(preview_img_count > 0) {
					image_count = 1;
					$("img#img_preview").attr('src', img_preview_1);
					$("section#attribute-preview-image").show();
				}
			});

			$("img#sound_preview").on("click", function(){
				if(preview_sound_count > 0) {
					$("section#attribute-preview-sound").show();
				}
			});
		});
		</script>

		<script type="text/javascript">
			function parent_id(item_id){
				$.ajax({
			        url: '{$base_url}create_content/parent_id/',
			        type: 'POST',
			        data: {
			            item_id: item_id
			        },
			        success: function() {
			            // alert(item_id);
			            window.location.href = '{$base_url}insert_child_content';
			        }
			    });
			}

			function set_book_id(item_id){
				$.ajax({
			        url: '{$base_url}create_content/set_id/',
			        type: 'POST',
			        data: {
			            item_id: item_id
			        },
			        success: function() {
			        	$('#confirmation').show();
			            // alert(item_id);
			            //window.location.href = '{$base_url}My_created_contents/change_status';
			        }
			    });
			}
		</script>

		<!-- create content -->
		<section id="content">
			<nav id="content__nav">
				home > 교육컨텐츠관리 > 컨텐츠 제작
			</nav>
			<section id="create-content" class="content-container">
				<h1 id="content__heading" class="blue">컨텐츠 제작</h1>
				<!-- line -->
				<a href="#" class="btn-delete-content" id="create-content__delete">컨텐츠 삭제</a>
				<div class="content__line"></div>
				<!-- end line -->
				<div id="create-content__info">
					<div id="info__options">
						<div id="info__icon">
							<img id="img_content_pictogram" src="{$book_info->pictogramBackground_picture}">
							<p>픽토그램 배경</p>
						</div>
						<div id="info__icon">
							<img id="img_icon" src="{$book_info->icon_picture}">
							<p>컨텐츠아이콘</p>
						</div>
						<div id="info__bg">
							<img id="img_bg" src="{$book_info->bgmindmap_picture}">
							<p>마인드맵배경</p>
						</div>
						<!-- <div id="info__edit"> -->
							<!-- <button id="create-content__delete" class="activate activate--blue activate--icon">
								<img src="{$base_url}assets/img/content_trash_icon.png">
								컨텐츠 삭제
							</button> -->
							<!-- <button onclick="showMindMap(1024, 700);" id="create-content__preview" class="activate activate--blue activate--icon">
								<img src="{$base_url}assets/img/content_preview_icon.png">
								미리보기
							</button> -->
						<!-- </div> -->
					</div>
					<div id="info__container">
						<div id="info__name" class="inline-middle">
							<h3 id="info__name__heading" class="blue inline-middle">
								<img src="{$base_url}assets/img/content_title_icon.png">
								<span>컨텐츠명:</span>
							</h3>
							<h3 id="info__name__content-name" style="max-width: 380px;">{$book_info->title}</h3>
							<div class="btn-edit" id="edit_title"></div>
						</div>
						<div id="info__description" class="inline-middle">
							<p id="info__content-description" style="max-width: 500px;">
								{$book_info->description}
							</p>
							<div id="edit_description" class="btn-edit"></div>
						</div>
					</div>
				</div>
				<!-- line -->
				<div class="content__line"></div>
				<!-- end line -->
				<div id="create-content__mindmap">
					<div id="mindmap__name" class="inline-middle">
						<h3 id="mindmap-name-heading" class="blue inline-middle">
							<img src="{$base_url}assets/img/content_mindmap_title.png">
							<span>마인드맵 만들기</span>
						</h3>
					</div>
					<div id="mindmap-wrap">
						<div id="mindmap-container" >

							<div id="mindmap__heading">
								<a href="#" class="btn-mindmap" onclick="showMindMap(1024, 700); return false">마인드맵</a>
								<font id="position" style="float: left; font-size: 14px; padding-left: 27px;" color="#29abe1">휠순서변경</font>
								<div id="up" class="btn-up flt-l"></div>
								<div id="down" class="btn-down flt-l"></div>
								<div id="left" class="btn-left flt-l"></div>

								<div class="flt-r">
									<a href="#" id="content-edit-icon" onclick="return false;" class="btn-addwheel mgn-r10">휠 추가</a>
									<a href="#" id="content-delete-icon" onclick="return false;" class="btn-deletewheel">휠 삭제</a>
								</div>
							</div>
							<div id="mindmap">
								<!-- mindmap -->

								<div class="mindmap">
									{foreach from=$children item=root}
										{if $root.connection_type == 'root'}
											<div class="node node_root" type="{$root.connection_type}" data-id="{$root.id}">
												<div id="node__text" class="node__text" align="center" data-type="{$root.connection_type}" data-id="{$root.id}">
													{$root.title}
												</div>
											</div>
										{/if}
									{/foreach}

									<ol class="children children_rightbranch">
									{foreach from=$children item=node}
										{if $node.connection_type != 'root'}
											<li id="draggable" class="children__item">
												<div class="node" data-type="{$node.connection_type}" data-id="{$node.id}">
													<div class="node__text" style="display: block;" data-type="{$node.connection_type}" data-id="{$node.id}">
														{$node.title}
													</div>
												</div>
												{if $node.children|count gt 0}
													{include file='./content.inc.tpl' childrens=$node.children}
												{/if}
											</li>
										{/if}
									{/foreach}
									</ol>
								</div>

							</div>
						</div>
						<div id="mindmap-attributes">
							<div id="mindmap-attributes__heading" class="mindmap-attributes__list">
								<p>마인드맵 휠 속성</p>
							</div>
							<!-- mindmap attributes list -->
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									최종휠
								</span>
								<span id="end_wheel" class="mindmap-attributes__list__value">
									아니오
								</span>
								<span class="mindmap-attributes__list__icon"></span>
							</div>
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									제목
								</span>
								<span id="title_attribute" class="mindmap-attributes__list__value blue">
									{foreach from=$children item=root}
									{if {$root.connection_type} == 'root'}
									{$root.title}
									{/if}
									{/foreach}
								</span>
								<span class="mindmap-attributes__list__icon">
									<div id="content_title" class="btn-edit2"></div>
								</span>
							</div>
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									자막
								</span>
								<span id="subtitle" class="mindmap-attributes__list__value">
									None
								</span>
								<span class="mindmap-attributes__list__icon">
									<div id="content_subtitle" class="btn-edit2"></div>
								</span>
							</div>
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									픽토그램
								</span>
								<span class="mindmap-attributes__list__value">
									<img id="img_pictogram" src="{$base_url}assets/img/content_document.png"  width="40px" height="40px">
								</span>
								<span class="mindmap-attributes__list__icon">
									<div id="content_pictogram" class="btn-edit2"></div>
								</span>
							</div>
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									픽토 배경
								</span>
								<span class="mindmap-attributes__list__value">
									<img id="img_pictogramBackground" src="{$base_url}assets/img/content_document.png" width="40px" height="40px">
								</span>
								<span class="mindmap-attributes__list__icon">
									<div id="content_pictogrambg" class="btn-edit2"></div>
								</span>
							</div>
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									상세설명
								</span>
								<span id="description"  class="mindmap-attributes__list__value">
									None
								</span>
								<span class="mindmap-attributes__list__icon">
									<div id="content_detail" class="btn-explanation"></div>
								</span>
							</div>
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									동영상
								</span>
								<span id="vid_count" class="mindmap-attributes__list__value">
									0
								</span>
								<span class="mindmap-attributes__list__icon">
									<div id="content_video" class="btn-video"></div>
								</span>
							</div>
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									이미지
								</span>
								<span id="img_count" class="mindmap-attributes__list__value">
									0
								</span>
								<span class="mindmap-attributes__list__icon">
									<div id="content_image" class="btn-image"></div>
								</span>
							</div>
							<div class="mindmap-attributes__list">
								<span class="mindmap-attributes__list__heading">
									사운드
								</span>
								<span id="sound_title" class="mindmap-attributes__list__value">
									None
								</span>
								<span class="mindmap-attributes__list__icon">
									<div id="content_sound" class="btn-sound"></div>
								</span>
							</div>
							<div class="mindmap-attributes__list">
								<center>
									<!-- <button id="create-content__preview_attribute" class="activate activate--blue activate--icon" onmouseover="hovercreatecontent__preview_attribute()" onmouseout="unhovercreatecontent__preview_attribute()"></button> -->
									<a id="create-content__preview_attribute" href="#" class="btn-preview" onclick="return false;">미리보기</a>	
								</center>
							</div>
							<!-- end mindmap attribtues list -->
						</div>
					</div>
					<div id="create-content__howto">
						<img id="howto" src="{$base_url}assets/img/content_howto.png">
					</div>
				</div>
			</section>
		</section>
		<!-- end create content -->
		<!-- create content popup add -->
		<section id="create-content-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle">
					<h3>휠 추가</h3>
					<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-popup');">
							<img src="{$base_url}assets/img/popup_exit.png">
					</div>
				</div>
				<form id="add_content" method="POST" action="/create_content/insert_content" onsubmit="alert($(this).attr('action')))">
				<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<!-- <p id="create-content-popup__selected" class="popup__black">선택휠: child2</p> -->
							<p id="create-content-popup__selected" class="popup__black">제목: <input id="title" type="text" name="title" style="display: inline-block; width: 235px;" TABINDEX="1" autocomplete="off" required>
							<input type="hidden" name="book_id" value="{$book_id}">
							</p>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<p id="create-content-popup__selected" class="popup__black">자막: <input id="subtitle" type="text" name="subtitle" style="display: inline-block; width: 235px;" TABINDEX="2" autocomplete="off">
							</p>
						</div>
						<div id="add_choices" class="popup__content__section popup__content__section--center">
							<!-- {if isset($count)}
							<input type="radio" name="wheel" value="same" checked><span class="popup__black">같은레벨 휠</span>
							<input type="radio" name="wheel" value="child"><span class="popup__black">하위 휠</span>
							{else}
							<input type="radio" name="wheel" value="same" disabled=""><span class="popup__black">같은레벨 휠</span>
							<input type="radio" name="wheel" value="child" checked><span class="popup__black">하위 휠</span>
							{/if} -->
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
							<input type="submit">adsasd</input>
						</div>					
					</div>
				</form>
			</div>
		</section>
		<!-- end create content popup add -->
		<!-- create content popup delete -->
		<section id="create-content-popup-delete" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle">
					<h3>휠 삭제</h3>
					<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-popup-delete');">
							<img src="{$base_url}assets/img/popup_exit.png">
					</div>
				</div>
				<div class="popup__content">
					<form id="delete_content" method="POST" action="/create_content/delete_child/">
						<div class="popup__content__section popup__content__section--center">
							<p id="delete-content-popup__selected">선택휠: child2</p>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input id="delete_same" type="radio" name="wheel" value="same" checked><span>같은레벨 휠</span>
							<input id="delete_child" type="radio" name="wheel" value="child"><span>하위 휠</span>
							<input type="hidden" name="book_id" value="{$book_id}">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--orange" type="submit">저장</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end create content popup delete -->
		<!-- edit title -->
		<section id="edit-title-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<form method="POST" action="/create_content/edit_title">
					<div class="popup__heading inline-middle">
						<h3>편집 제목</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('edit-title-popup');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<input id="edit_title" class="full-length" type="text" name="edit_title" autocomplete="off">
							<input type="hidden" name="book_id" value="{$book_id}">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end edit title -->
		<!-- edit description -->
		<section id="edit-description-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container popup__container--wide">
			<form method="POST" action="/create_content/edit_description">
					<div class="popup__heading inline-middle">
						<h3>편집 설명</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('edit-description-popup');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<textarea id="edit_subject" name="edit_description"></textarea>
							<input type="hidden" name="book_id" value="{$book_id}">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end edit description -->
		<!-- create attributes title -->
		<section id="create-content-attributes-popup-title" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<form id="title_attribute" method="POST" action="">
					<div class="popup__heading inline-middle">
						<h3>제목</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-title');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<input id="title_attribute" class="full-length" type="text" name="title_attribute" autocomplete="off">
							<input type="hidden" name="book_id" value="{$book_id}">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end create attributes title -->
		<!-- create attributes description -->
		<section id="create-content-attributes-popup-description" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container popup__container--wide">
			<form id="description_attribute" method="POST" action="">
					<div class="popup__heading inline-middle">
						<h3>상세설명</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-description');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<textarea id="editor1" name="editor1"></textarea>
							<script>
							var url = "/assets";
				                // Replace the <textarea id="editor1"> with a CKEditor
				                // instance, using default configuration.
				                CKEDITOR.replace('editor1', {
									"filebrowserImageUploadUrl": "/assets/lib/ckeditor/plugins/imgupload.php?path="+url
								});
				            </script>
							<input type="hidden" name="book_id" value="{$book_id}">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end create attributes description -->
		<!-- create attributes image -->
		<section id="create-content-attributes-popup-image" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container popup__container--wide">
				<form id="contentsAlbum" method="POST" enctype="multipart/form-data" action="/create_content/insert_contentsAlbum">
					<div class="popup__heading inline-middle">
						<h3>이미지</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-image');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section popup__content__element">
							<div class="popup__content__element__one_third">
								<div id="del_img_content1" class="popup__delete-button popup__delete-button--corner"></div>
								<div id="img_content1" class="popup__content__element__image">
									<img id="img_content1" width="150px" height="100px">
								</div>
							</div>
							<div class="popup__content__element__one_third">
								<div id="del_img_content2" class="popup__delete-button popup__delete-button--corner"></div>
								<div id="img_content2" class="popup__content__element__image">
									<img id="img_content2" width="150px" height="100px">
								</div>
							</div>
							<div class="popup__content__element__one_third">
								<div id="del_img_content3" class="popup__delete-button popup__delete-button--corner"></div>
								<div id="img_content3" class="popup__content__element__image">
									<img id="img_content3" width="150px" height="100px">
								</div>
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" name="book_id" value="{$book_id}">
							<input type="file" name="img_content1" id="img_upload1"/>
							<input type="file" name="img_content2" id="img_upload2"/>
							<input type="file" name="img_content3" id="img_upload3"/>
							<input type="hidden" name="img_delete1" id="img_delete1"/>
							<input type="hidden" name="img_delete2" id="img_delete2"/>
							<input type="hidden" name="img_delete3" id="img_delete3"/>

							<div class="progress">
							    <div class="bar" style="background-color: #29abe1; width: 0px; height: 20px;"></div>
							    <div class="percent">0%</div>
							</div>

							<div id="status"></div>

							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end create attributes image -->
		<!-- create attributes movie -->
		<section id="create-content-attributes-popup-movie" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container popup__container--wide">
				<form id="contentsVideo" method="POST" action="/create_content/insert_contentsVideo" enctype="multipart/form-data">
					<div class="popup__heading inline-middle">
						<h3>동영상</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-movie');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section popup__content__element">
							<div class="popup__content__element__one_third">
							</div>
							<div class="popup__content__element__one_third">
								<div id="del_vid_content1" class="popup__delete-button popup__delete-button--corner"></div>
								<div id="vid_content1" class="popup__content__element__image">
									<video id="vid_content1" width="150px" height="100px" controls>
									  <source id="vid_content1" src="" type="video/mp4">
									  Your browser does not support HTML5 video.
									</video>
								</div>
							</div>
							<!-- <div class="popup__content__element__one_third">
								<div id="del_vid_content2" class="popup__delete-button popup__delete-button--corner"></div>
								<div id="vid_content2" class="popup__content__element__image">
									<video id="vid_content2" width="150px" height="100px" controls>
									  <source id="vid_content2" src="" type="video/mp4">
									  Your browser does not support HTML5 video.
									</video>
								</div>
							</div>
							<div class="popup__content__element__one_third">
								<div id="del_vid_content3" class="popup__delete-button popup__delete-button--corner"></div>
								<div id="vid_content3" class="popup__content__element__image">
									<video id="vid_content3" width="150px" height="100px" controls>
									  <source id="vid_content3" src="" type="video/mp4">
									  Your browser does not support HTML5 video.
									</video>
								</div>
							</div> -->
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" name="book_id" value="{$book_id}">
							<input type="file" name="vid_content1" id="vid_upload1" accept="video/mp4"/>
							<!-- <input type="file" name="vid_content2" id="vid_upload2" accept="video/mp4"/>
							<input type="file" name="vid_content3" id="vid_upload3" accept="video/mp4"/> -->
							<input type="hidden" name="vid_delete1" id="vid_delete1"/>
							<!-- <input type="hidden" name="vid_delete2" id="vid_delete2"/>
							<input type="hidden" name="vid_delete3" id="vid_delete3"/> -->


							<div class="progress">
							    <div class="bar" style="background-color: #29abe1; width: 0px; height: 20px;"></div>
							    <div class="percent">0%</div>
							</div>

							<div id="status"></div>

							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end create attributes movie -->
		<!-- create attributes pictogram -->
		<section id="create-content-attributes-popup-pictogram" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<form method="POST" action="">
					<div class="popup__heading inline-middle clearfix">
						<h3 class="popup__heading__sub">픽토그램</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-pictogram');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
						<!-- <div class="popup__heading__sub-options">
							<p class="grey">분류</p>
							<select class="custom-select" data-width="150" data-placeholder="선택하세요">
								<option value="1">카테고리1</option>
								<option value="2">카테고리2</option>
								<option value="3">카테고리3</option>
							</select>
						</div> -->
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<div id="popscroll" class="popup__content__section__scroll popup__content__section__scroll--60">
									<div id="pictogramdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--60" data-id="0">
										<img id="imgpictogram" data-id="0" width="60px" height="60px">
									</div>
							<div id="loadpictogram"></div>
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" name="book_id" value="{$book_id}">
							<button id="pictogram" class="activate activate--dark-blue activate--white" type="button">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end create attributes pictogram -->
		<!-- create attributes sound -->
		<section id="create-content-attributes-popup-sound" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-sound">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form id="contentsSound" method="POST" action="/create_content/insert_contentsSound/" enctype="multipart/form-data">
					<div class="popup__heading">
						<h3>사운드</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-sound');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<div id="del_sound_content1" class="popup__content__element__delete-button"></div>
							<p id="sound_title"></p>
						</div>
						<div id="sound_content1" class="popup__content__section popup__content__section--center">
							<audio id="sound_content1" style="width: 100%;" controls="controls">
							</audio>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="file" name="sound_content1" id="sound_upload1" accept="audio/mp3"/>

							<div class="progress">
							    <div class="bar" style="background-color: #29abe1; width: 0px; height: 20px;"></div>
							    <div class="percent">0%</div>
							</div>

							<div id="status"></div>

							<button id="sound_content1" class="activate activate--dark-blue activate--white" type="button">추가</button>
							<input type="hidden" name="book_id" value="{$book_id}">
							<input type="hidden" name="sound_delete1" id="sound_delete1"/>
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end create attributes sound -->
		<!-- create attributes subject -->
		<section id="create-content-attributes-popup-subject" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<form id="subtitle_attribute" method="POST" action="">
					<div class="popup__heading inline-middle">
						<h3>자막</h3>
						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-subject');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<textarea id="subtitle" name="subtitle"></textarea>
							<input type="hidden" name="book_id" value="{$book_id}">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end create attributes subject -->
		<!-- alert message -->
		<section id="create-content-attributes-popup-alert" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-alert');">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<div class="popup__heading inline-middle">
					<h3>메시지</h3>
				</div>
				<form method="POST" action="">
					<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<p id="alert-content-popup__selected" class="popup__black red">내용을 선택하십시오.</p>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="alert_close" class="activate activate--dark-blue activate--white" type="button">취소</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end alert message -->
		<!-- create attribute pictogrambg popup -->
		<section id="create-content-pictogrambg-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle clearfix">
					<h3 class="popup__heading__sub">픽토그램 배경</h3>
					<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-pictogrambg-popup');">
						<img src="{$base_url}assets/img/popup_exit.png">
					</div>
					<!-- <div class="popup__heading__sub-options" style="margin-right: 0px;">
						<input id="pictogrambg_name" name="pictogrambg_name" type="text" placeholder="검색어를 입력하세요" style="width:200px">
						<p class="grey">분류</p>
						<select class="custom-select" data-width="150" data-placeholder="선택하세요">
							<option value="1">카테고리1</option>
							<option value="2">카테고리2</option>
							<option value="3">카테고리3</option>
						</select>
					</div> -->
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section">
							<div id="mainpictogrambg" class="popup__content__section__scroll popup__content__section__scroll--70">
								{if {$pictogramBackground|@count} gt 0}
									{foreach from=$pictogramBackground item=items}
									<div id="pictogrambgdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--70" data-id="{$items->id}">
										<img id="imgpictogrambg" src="{$items->picture}" data-id="{$items->id}" width="60px" height="60px">
									</div>
									{/foreach}
								{/if}
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="pictogrambg" class="activate activate--dark-blue activate--white" type="button">저장</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end create attribute pictogrambg popup -->
		<!-- create content pictogrambg popup -->
		<section id="create-book-pictogrambg-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle clearfix">
					<h3 class="popup__heading__sub">픽토그램 배경</h3>
					<div class="popup__exit" id="close-pictogram" onclick="close_section('create-book-pictogrambg-popup');">
						<img src="{$base_url}assets/img/popup_exit.png">
					</div>
					<!-- <div class="popup__heading__sub-options" style="margin-right: 0px;">
						<input id="pictogrambg_name" name="pictogrambg_name" type="text" placeholder="검색어를 입력하세요" style="width:200px">
						<p class="grey">분류</p>
						<select class="custom-select" data-width="150" data-placeholder="선택하세요">
							<option value="1">카테고리1</option>
							<option value="2">카테고리2</option>
							<option value="3">카테고리3</option>
						</select>
					</div> -->
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section">
							<div id="mainpictogrambg" class="popup__content__section__scroll popup__content__section__scroll--70">
								{if {$pictogramBackground|@count} gt 0}
									{foreach from=$pictogramBackground item=items}
									<div id="contentpictogrambgdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--70" data-id="{$items->id}">
										<img id="contentimgpictogrambg" src="{$items->picture}" data-id="{$items->id}" width="60px" height="60px">
									</div>
									{/foreach}
								{/if}
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="contentpictogrambg" class="activate activate--dark-blue activate--white" type="button">저장</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end create content pictogrambg popup -->
		<!-- create content icon popup -->
		<section id="create-content-icon-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle clearfix">
					<h3 class="popup__heading__sub">아이콘 선택</h3>
					<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-icon-popup');">
						<img src="{$base_url}assets/img/popup_exit.png">
					</div>
					<div class="popup__heading__sub-options" style="margin-right: 0px;">
						<input id="icons_name" name="icons_name" type="text" placeholder="검색어를 입력하세요" style="width:200px" autocomplete="off">
						<!-- <p class="grey">분류</p>
						<select class="custom-select" data-width="150" data-placeholder="선택하세요">
							<option value="1">카테고리1</option>
							<option value="2">카테고리2</option>
							<option value="3">카테고리3</option>
						</select> -->
					</div>
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section">
							<div id="mainIcons" class="popup__content__section__scroll popup__content__section__scroll--70">
								{if {$iconContents|@count} gt 0}
									{foreach from=$iconContents item=items}
									<div id="iconsContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--70" data-id="{$items->id}">
										<img id="img" src="{$items->picture}" data-id="{$items->id}" width="70px" height="70px">
									</div>
									{/foreach}
								{/if}
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="iconContent" class="activate activate--dark-blue activate--white" type="button">저장</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end create content icon popup -->
		<!-- create content bg popup -->
		<section id="create-content-bg-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle clearfix">
					<h3 class="popup__heading__sub">배경 선택</h3>
					<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-bg-popup');">
						<img src="{$base_url}assets/img/popup_exit.png">
					</div>
					<div class="popup__heading__sub-options" style="margin-right: 0px;">
						<input id="bgmindmap_name" name="bgmindmap_name" type="text" placeholder="검색어를 입력하세요" style="width:200px" autocomplete="off">
						<!-- <p class="grey">분류</p>
						<select class="custom-select" data-width="150" data-placeholder="선택하세요">
							<option value="1">카테고리1</option>
							<option value="2">카테고리2</option>
							<option value="3">카테고리3</option>
						</select> -->
					</div>
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section">
							<div id="mainBGMindmap" class="popup__content__section__scroll popup__content__section__scroll--125">
								{if {$bgMindmap|@count} gt 0}
									{foreach from=$bgMindmap item=items}
									<div id="bgmindmapContentdiv" class="popup__content__section__scroll__item popup__content__section__scroll__item--125" data-id="{$items->id}">
										<img id="bgmindmap" src="{$items->picture}" data-id="{$items->id}" width="125px" height="150px">
									</div>
									{/foreach}
								{/if}
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="bgmindmap" class="activate activate--dark-blue activate--white" type="button">저장</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end create content bg popup -->
		<!-- confirmation -->
		<section id="confirmation" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-pictogram" onclick="close_section('confirmation');">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form id="change_status" method="POST" action="/create_content/delete_book">
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div id="delete_message" class="popup__content__section--center">

						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" id="book_id" name="book_id" value="{$book_id}">
							<button class="activate activate--dark-blue activate--white" type="submit">확인</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end confirmation -->
		<!-- file exceeded -->
		<section id="fileexceeded" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-fileexceeded">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form method="POST">
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section--center red">
							업로드 제한용량을 초과하였습니다. (최대 20MB)
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="close-fileexceeded" class="activate activate--dark-blue activate--white" type="button">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end file exceeded -->
		<!-- deletable -->
		<section id="deletable" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-fileexceeded" onclick="close_section('deletable');">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form method="POST">
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section--center red">
							출시완료된 컨텐츠는 삭제할 수 없습니다.
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="close-fileexceeded" class="activate activate--dark-blue activate--white" type="button" onclick="close_section('deletable');">확인</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end deletable -->

		<script src="{$base_url}assets/js/mindmap.min.js"></script>
		<script type="text/javascript">
		$(function(){
			$('.mindmap').mindmap();
			//$('div#node__text.node__text').click();

			var selected_content_id = "{if isset({$smarty.session.content_id})}{$smarty.session.content_id}{else}0{/if}";

			$("div.node__text").each(function(){
				id = $(this).data("id");
			    if(id == selected_content_id) {
			        $(this).trigger('click');
			        //alert(selected_content_id);
			    }
			});

			if('{$error}' != '') {
		    	$('#create-content-attributes-popup-alert').show();
				$("p#alert-content-popup__selected").html('{$error}');
		    }
		});

		$(document).ready(function() {
			//Image Upload
		    $('div#img_content1').on('click',function(evt){
				evt.preventDefault();
				$('#img_upload1').trigger('click');
		    });

		    $('div#img_content2').on('click',function(evt){
				evt.preventDefault();
				$('#img_upload2').trigger('click');
		    });

		    $('div#img_content3').on('click',function(evt){
				evt.preventDefault();
				$('#img_upload3').trigger('click');
		    });

		    $("input#img_upload1").change(function(){
		        readURL(this, 'img_content1');
		    });

		    $("input#img_upload2").change(function(){
		        readURL(this, 'img_content2');
		    });

		    $("input#img_upload3").change(function(){
		        readURL(this, 'img_content3');
		    });

		    $('div#del_img_content1').on('click',function(evt){
		    	$("input#img_delete1").val($("img#img_content1").attr('src'));
				$("input#img_upload1").replaceWith($("input#img_upload1").val('').clone(true));
				$("img#img_content1").removeAttr('src');
				$(".bar").width(0);
				$(".percent").html('0%');
		    });

		    $('div#del_img_content2').on('click',function(evt){
		    	$("input#img_delete2").val($("img#img_content2").attr('src'));
				$("input#img_upload2").replaceWith($("input#img_upload2").val('').clone(true));
				$("img#img_content2").removeAttr('src');
				$(".bar").width(0);
				$(".percent").html('0%');
		    });

		    $('div#del_img_content3').on('click',function(evt){
		    	$("input#img_delete3").val($("img#img_content3").attr('src'));
				$("input#img_upload3").replaceWith($("input#img_upload3").val('').clone(true));
				$("img#img_content3").removeAttr('src');
				$(".bar").width(0);
				$(".percent").html('0%');
		    });
		    //End Image Upload

		    //Video Upload
		    $('div#vid_content1').on('click',function(evt){
				evt.preventDefault();
				$('#vid_upload1').trigger('click');
		    });

		    $('div#vid_content2').on('click',function(evt){
				evt.preventDefault();
				$('#vid_upload2').trigger('click');
		    });

		    $('div#vid_content3').on('click',function(evt){
				evt.preventDefault();
				$('#vid_upload3').trigger('click');
		    });

		    $("input#vid_upload1").change(function(){
		        loadVideo(this, 'vid_content1');
		    });

		    $("input#vid_upload2").change(function(){
		        loadVideo(this, 'vid_content2');
		    });

		    $("input#vid_upload3").change(function(){
		        loadVideo(this, 'vid_content3');
		    });

		    $('div#del_vid_content1').on('click',function(evt){
		    	$("input#vid_delete1").val($("video#vid_content1").attr('src'));
				$("input#vid_upload1").replaceWith($("input#vid_upload1").val('').clone(true));
				$("video#vid_content1").removeAttr('src');
				$("video#vid_content1")[0].pause();
				$(".bar").width(0);
				$(".percent").html('0%');
		    });

		    $('div#del_vid_content2').on('click',function(evt){
		    	$("input#vid_delete2").val($("video#vid_content2").attr('src'));
				$("input#vid_upload2").replaceWith($("input#vid_upload2").val('').clone(true));
				$("video#vid_content2").removeAttr('src');
				$("video#vid_content2")[0].pause();
				$(".bar").width(0);
				$(".percent").html('0%');
		    });

		    $('div#del_vid_content3').on('click',function(evt){
		    	$("input#vid_delete3").val($("video#vid_content3").attr('src'));
				$("input#vid_upload3").replaceWith($("input#vid_upload3").val('').clone(true));
				$("video#vid_content3").removeAttr('src');
				$("video#vid_content3")[0].pause();
				$(".bar").width(0);
				$(".percent").html('0%');
		    });
		    //End Video Upload

		    //Sound Upload
		    $('button#sound_content1').on('click',function(evt){
				evt.preventDefault();
				$('#sound_upload1').trigger('click');
		    });

		    $("input#sound_upload1").change(function(){
		    	$("p#sound_title").text(this.value);
		        loadSound(this, 'sound_content1');
		    });

		    $('div#del_sound_content1').on('click',function(evt){
		    	$("input#sound_delete1").val($("audio#sound_content1").attr('src'));
				$("input#sound_upload1").replaceWith($("input#sound_upload1").val('').clone(true));
				$("audio#sound_content1").removeAttr('src');
				$("audio#sound_content1")[0].pause();
				$("p#sound_title").text('');
				$(".bar").width(0);
				$(".percent").html('0%');
		    });
		    //End Sound Upload
		});

		function readURL(input, id) {
	        if (input.files && input.files[0]) {
	        	var size = input.files[0].size / 3072000;
	        	if(size > 10) {
	        		$('#fileexceeded').show();
	        		return false;
	        	}
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('img#'+id).attr('src', e.target.result);
	            }
	            reader.readAsDataURL(input.files[0]);
	        }
	    }

	    function loadVideo(input, id) {
	        if (input.files && input.files[0]) {
	        	var size = input.files[0].size / 3072000;
	        	if(size > 10) {
	        		$('#fileexceeded').show();
	        		return false;
	        	}
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('source#'+id).attr('src', e.target.result);
	                $("video#"+id)[0].load();
	                $("video#"+id)[0].play();
	            }
	            reader.readAsDataURL(input.files[0]);
	        }
	    }

	    function loadSound(input, id) {
	        if (input.files && input.files[0]) {
	        	var size = input.files[0].size / 2048000;
	        	if(size > 10) {
	        		$('#fileexceeded').show();
	        		return false;
	        	}
	            var reader = new FileReader();
	            reader.onload = function (e) {
	                $('audio#'+id).attr('src', e.target.result);
	                $("audio#"+id)[0].load();
	                $("audio#"+id)[0].play();
	            }
	            reader.readAsDataURL(input.files[0]);
	        }
	    }

	    function close_section(id) {
	     	//$('section#'+id).hide();
	     	$(".bar").width(0);
			$(".percent").html('0%');
			$('.popup__bg').click();
	     	$("html").getNiceScroll().show();
	    }

	    function hovercreatecontent__preview_attribute() {
	    	$("button#create-content__preview_attribute").css("background-color", "#3BA6D1");
		}

		function unhovercreatecontent__preview_attribute() {
		    $("button#create-content__preview_attribute").css("background-color", "#addcf0");
		}

	    function hoverpreview_attribute() {
	    	$("button#create-content__preview").css("background-image", "url('{$base_url}assets/img/attribute_preview/mindmap_btn_on.png')");
		}

		function unhoverpreview_attribute() {
		    $("button#create-content__preview").css("background-image", "url('{$base_url}assets/img/attribute_preview/mindmap_btn.png')");
		}

	    function hoverup(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/wheelchange_up_btn_on.png');
		}

		function unhoverup(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/wheelchange_up_btn.png');
		}

		function hoverdown(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/wheelchange_down_btn_on.png');
		}

		function unhoverdown(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/wheelchange_down_btn.png');
		}

		function hoverleft(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/wheelchange_left_btn_on.png');
		}

		function unhoverleft(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/wheelchange_left_btn.png');
		}

		function hoverpreviewleft(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/previous_title_btn_on.png');
		}

		function unhoverpreviewleft(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/previous_title_btn.png');
		}

		function hoverpreviewright(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/next_title_btn_on.png');
		}

		function unhoverpreviewright(element) {
		    element.setAttribute('src', '{$base_url}assets/img/attribute_preview/next_title_btn.png');
		}
		</script>






		<!-- Mindmap Popup -->
		<!-- for parsing json -->
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/Preview/json2.js"></script>
		<script type="text/javascript">
		<!--
		// var unityObjectUrl = "UnityObject2.js";
		// if (document.location.protocol == 'https:')
		// 	unityObjectUrl = unityObjectUrl.replace("http://", "https://ssl-");
		// document.write('<script type="text\/javascript" src="{$base_url}assets/Preview/' + unityObjectUrl + '"><\/script>');

		// get_jsonData = function()
		// {
		// 	u.getUnity().SendMessage("HERE", "baseURL", "{$base_url}");
		// 	//	alert("Retrieving json data");
		// 	//	alert(JSON.stringify(gfgJSON));

		// 	u.getUnity().SendMessage("HERE", "LOAD", JSON.stringify(gfgJSON));
		// 	get_zoomLevel(8);
		// }
		-->
		</script>
		<script type="text/javascript">

		// function showMindMap(w, h) {
		// 	$('input#pc').click();
		// 	{literal}
		// 	$('input#width').val(Math.round(w));
		// 	$('input#height').val(Math.round(h));
		// 	LoadMindMap(w, h);
		// 	$('div#resolution').animate({width: w+"px"}, 400);
		// 	$('section#create-content-attributes-popup-mindmap-preview').show();
		// 	$("html").getNiceScroll().hide();
		// 	{/literal}
		// }

		//var zoomLevel = 8;

		//var unityObjectUrl = "UnityObject2.js";
		//if (document.location.protocol == 'https:')
			//unityObjectUrl = unityObjectUrl.replace("http://", "https://ssl-");
		//document.write('<script type="text\/javascript" src="{$base_url}assets/Preview/' + unityObjectUrl + '"><\/script>');

		//get_jsonData = function()
		//{
			//	alert("Retrieving json data");
			//	alert(JSON.stringify(gfgJSON));
			//u.getUnity().SendMessage("HERE", "baseURL", "{$base_url}");
			//u.getUnity().SendMessage("HERE", "LOAD", JSON.stringify(gfgJSON));

			//get_zoomLevel(zoomLevel);
		//}

		$(document).ready(function() {
				$('button#save').on('click', function() {
					var json = [];
					$.each(modified, function(i, v){
						var plot = {};
						plot['mindmap_angle'] = plots[v].mindmap_angle;
						plot['distance'] = plots[v].distance;
						plot['id'] = v;
						json.push(plot);
					});
					//Update
					generate_minimap(function(image){
						$.ajax({
							url: "/api/update_position/",
							type: "POST",
							dataType: "json",
							data: { json: JSON.stringify(json), minimap: image },
							success: function(response) {
								$("label#mindmap_message").fadeIn("500");
								$("button#save").fadeOut("500");
								$("button#save").addClass('hidden');
								$("button#save").hide();
								modified = [];
								performed_save = true;
							},
							error: function(xhr, status, error) {
							  	alert('error '+ xhr.responseText);
							  	console.log(xhr);
								}
							});
						});
					});
				});

				$(function() {
					{literal}
					  $('#create-content-attributes-popup-mindmap-preview').on('keydown', 'input#width', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});

					  $('#create-content-attributes-popup-mindmap-preview').on('keydown', 'input#height', function(e){-1!==$.inArray(e.keyCode,[46,8,9,27,13,110,190])||/65|67|86|88/.test(e.keyCode)&&(!0===e.ctrlKey||!0===e.metaKey)||35<=e.keyCode&&40>=e.keyCode||(e.shiftKey||48>e.keyCode||57<e.keyCode)&&(96>e.keyCode||105<e.keyCode)&&e.preventDefault()});
					{/literal}
				})

				$("input[name=reso]:radio").change(function () {
					var id = $(this).attr('id');
					var zoom = .28;
					//var zoomLevel = 10;

					//alert(id);

					if(id == 'pc') {
						unity_width = 1024;
						unity_height = 700;
						zoomLevel = 8;
					}

					if(id == 'phone') {
						unity_width = 1080*.3;
						unity_height = 1920*.3;
						zoomLevel = 12;
						//get_zoomLevel(15);
					}

					if(id == 'tablet') {
						unity_width = 600;
						unity_height = 700;
						zoomLevel = 12;
					}

					if(id == 'custom') {
						$('input#width').val('');
						$('input#height').val('');
						$('input#width').prop('readonly', false);
						$('input#height').prop('readonly', false);
						$("button#custom_size").fadeIn("500");
						$('input#width').focus();
						zoomLevel = 12;
						return false;
					}

					$('input#width').prop('readonly', true);
					$('input#height').prop('readonly', true);
					$("button#custom_size").fadeOut("500");
					$('input#width').val(Math.round(unity_width));
					$('input#height').val(Math.round(unity_height));

					//alert(unity_width + ' x ' + unity_height)

					{literal}
					$('div#resolution').animate({width: unity_width+"px"}, 400);
					{/literal}
					canvas_height = unity_height;
					canvas_width = unity_width;
					LoadMindMap();
				});

				$('button#custom_size').on('click', function() {
					unity_width = $('input#width').val();
					unity_height = $('input#height').val();
					{literal}
					$('div#resolution').animate({width: unity_width+"px"}, 400);
					{/literal}
					canvas_height = unity_height;
					canvas_width = unity_width;
					LoadMindMap();
				});
		</script>

		<!-- mindmap -->
		<section id="create-content-attributes-popup-mindmap-preview" class="popup" style="top:10px; display:none;">
			<div class="popup__bg"></div>
			<div id="resolution" class="popup__container" style="width: 819px; margin: 0px auto 0;">
				<form method="POST" action="">
					<div class="popup__heading inline-middle clearfix">
						<!-- <h3 class="popup__heading__sub">픽토그램</h3> -->

						<input id="pc" type="radio" name="reso" value="name" checked>
						<label for="pc" class="grey">PC</label>
						<input id="phone" type="radio" name="reso" value="dept">
						<label for="phone" class="grey">Phone</label>
						<input id="tablet" type="radio" name="reso" value="content">
						<label for="tablet" class="grey">Tablet</label>
						<input id="custom" type="radio" name="reso" value="all">
						<label for="custom" class="grey">Custom</label>
						<br/>
						<label for="custom" class="grey">Width:</label>
						<input type="text" name="width" id="width" maxlength="4" style="width:55px; height: 30px; text-align: center;" readonly="true" autocomplete="off">
						<label for="custom" class="grey">Height:</label>
						<input type="text" name="height" id="height" maxlength="4" style="width:55px; height: 30px; text-align: center;" readonly="true" autocomplete="off">
						<button id="custom_size" style="display:none; width: 75px" id="save" class="activate activate--dark-blue activate--white" type="button">적용</button>

						<div class="popup__exit" id="close-pictogram" onclick="close_section('create-content-attributes-popup-mindmap-preview');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__heading inline-middle clearfix">

					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<div class="popup__content__section__scroll popup__content__section__scroll--60" style="overflow: hidden; height:auto">
								<!--Content here-->

								<canvas id="mindmap_canvas"></canvas>

							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button style="display:none;" id="save" class="activate activate--dark-blue activate--white" type="button">저장</button><label style="display:none;" id="mindmap_message" style="color:green">Successfully saved!</label>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end mindmap -->
		<!-- attribute preview -->
		<section id="attribute-preview" class="popup" style="display: none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<form method="POST" action="/create_content/edit_title">
					<div class="popup__heading inline-middle" style="background-color: #3BA6D1;">
						<img id="preview_left" style="cursor: pointer;" src="{$base_url}assets/img/attribute_preview/previous_title_btn.png" height="30px" onmouseover="hoverpreviewleft(this);" onmouseout="unhoverpreviewleft(this);">
						<h3 id="attribute_title_preview" style="color: #FFFFFF; padding-left: 30px; padding-right: 30px;">편집 제목</h3>
						<img id="preview_right" style="cursor: pointer;" src="{$base_url}assets/img/attribute_preview/next_title_btn.png" height="30px" onmouseover="hoverpreviewright(this);" onmouseout="unhoverpreviewright(this);">
						<div class="popup__exit" id="close-pictogram" onclick="close_section('attribute-preview');">
							<img src="{$base_url}assets/img/popup_exit.png">
						</div>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<div id="attribute_preview" style="height: 300px;">
								<!-- <div id="subtitle_preview" style="background-color: #BDD7EE; color: #000; border: 1px solid #3586ab; border-radius: 5px; padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 10px; display: table-cell; height: 100%; overflow: hidden; width: 380px; max-width: 380px;">
								Subtitle Here
								</div><br/> -->
								<div id="subtitle_preview" style="background-color: #ADDEF2; color: #000000;  padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 10px; min-height: 50px; width: 380px; max-width: 380px;">
								</div>
								<div style="background-color: #FFFFFF; color: #000; padding-top: 5px; padding-right: 10px; padding-bottom: 5px; padding-left: 10px; display: table-cell; height: 100%; overflow: hidden; width: 380px; max-width: 380px;">
									<center>
										<div align="center" style="position: relative; top: 5px; background-color: #2186B0; border: 1px solid #3586ab; border-radius: 5px; display: table-cell; height: 30px; overflow: hidden; width: 300px; color: #FFFFFF">
											<img id="image_preview" src="{$base_url}assets/img/attribute_preview/photo_btn.png" height="25px">(<span id="img_count"></span>)
											<img id="vid_preview" src="{$base_url}assets/img/attribute_preview/vod_btn.png" height="25px">(<span id="vid_count"></span>)
											<img id="sound_preview" src="{$base_url}assets/img/attribute_preview/sound_btn.png" height="25px">(<span id="sound_title"></span>)
										</div>
									</center>
									<br/>
									<div id="description_preview">
									<!-- Description Here -->
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div> -->
					</div>
				</form>
			</div>
		</section>
		<!-- end attribute preview -->
		<!-- video attribute preview -->
		<section id="attribute-preview-video" class="popup" style="display: none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
					<div class="popup__content" style="background-color: #000000">
						<div class="popup__content__section">
							<video id="homevideo" width="100%" height="300px">
							    <source src="" type='video/mp4'/>
							</video>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<img id="previousButton" src="/assets/img/attribute_preview/backward_btn.png" height="30px">
							<img id="playButton" src="/assets/img/attribute_preview/play_btn.png" height="30px">
							<img id="nextButton" src="/assets/img/attribute_preview/forward_btn.png" height="30px">
						</div>
					</div>
			</div>
		</section>
		<!-- end video attribute preview -->
		<!-- image attribute preview -->
		<section id="attribute-preview-image" class="popup" style="display: none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
					<div class="popup__content" style="background-color: #000000">
						<div class="popup__content__section">
							<img id="img_preview" width="100%" height="300px">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<img id="img_previousButton" src="/assets/img/attribute_preview/backward_btn.png" height="30px">
							<img id="img_nextButton" src="/assets/img/attribute_preview/forward_btn.png" height="30px">
						</div>
					</div>
			</div>
		</section>
		<!-- end image attribute preview -->
		<!-- image attribute preview -->
		<section id="attribute-preview-sound" class="popup" style="display: none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
					<div class="popup__content" style="background-color: #000000">
						<div class="popup__content__section">
							<audio id="sound_preview" width="100%" controls="controls">
							</audio>
						</div>
						<!-- <div class="popup__content__section popup__content__section--center">
							<img id="img_previousButton" src="/assets/img/attribute_preview/backward_btn.png" height="30px">
							<img id="img_nextButton" src="/assets/img/attribute_preview/forward_btn.png" height="30px">
						</div> -->
					</div>
			</div>
		</section>
		<!-- end image attribute preview -->
		<!-- End Mindmap Popup -->
		<script type="text/javascript">
			var cr = 40,
			performed_save = false,
			plots = {},
			items = [],
			clor = '#99ffdd',
			root_id,
			line_index = 0,
			maps = [],
			modified = [],
			lines = [],
			childrens = {},
			scale = 1,
			canvas_width,
			canvas_height,
			canvas = document.getElementById('mindmap_canvas'),
			stage = new createjs.Stage(canvas),
			mindmap_bg = "/api/mindmapbg/"+{$book_id};


			createjs.Touch.enable(stage);
		</script>

		<script src="{$base_url}assets/js/mindmap.js"></script>

		<script type="text/javascript">
			var unity_width = 610;
			var unity_height = 570;
			var zoom;

			var id = getUrlParameter('id');
			var gfgAPI = "/api/get_contents/"+{$book_id};
			var gfgJSON;
			function LoadMindMap() {

				$("label#mindmap_message").hide();
				$("button#save").hide();
				$("button#save").addClass('hidden');
				modified = [];

				if( !performed_save )
					plots = {};

				maps = [];
				line_index = 0;
				lines = [];
				childrens = {};
				scale = 1;
				stage.removeAllChildren();
				stage.update();

				if( items.length > 0 ){
					initMindMap();
				} else {
					jQuery(function() {
						$.ajax({
						    url: gfgAPI,
						    dataType: "json",
						    // Work with the response
						    success: function( response ) {
									gfgJSON = response;
									items = gfgJSON;
									initMindMap();
						    }
						});
					});
				}
			}

			function getUrlParameter(sParam)
			{
				var sPageURL = window.location.search.substring(1);
				var sURLVariables = sPageURL.split('&');
				for (var i = 0; i < sURLVariables.length; i++)
				{
					var sParameterName = sURLVariables[i].split('=');
					if (sParameterName[0] == sParam)
					{
						return sParameterName[1];
					}
				}
			}
			initalUpdate = function (json)
			{
				console.log (json);

				//Update
				$.ajax({
					url: "/api/update_position/",
					type: "POST",
					dataType: "json",
					data: { json: json },
					success: function(response) {
						console.log (response);
						//alert('success');
					},
					error: function(xhr, status, error) {
						console.log(error);
					  	alert('error '+ xhr.responseText);
					}
				});
			}
			saveButton = function(json)
			{
				if('{$author_id}' == '{$smarty.session.user_id}') {
					//u.getUnity().SendMessage("HERE", "CLEAR", "");
					$("label#mindmap_message").hide();
					$("button#save").fadeIn("500");
					$("button#save").attr("ref", json);
				}

				//alert("Your changes has been saved!");
			}
			saveThis = function()
			{
				alert("Your changes has been saved!");
			}
			get_zoomLevel = function(zoom)
			{
				//	alert("Retrieving json data");
				//	alert(JSON.stringify(gfgJSON));

				u.getUnity().SendMessage("HERE", "zoomLevel", zoom);
				//alert('test');
			}

			setTitle = function(title)
			{
				//$("h3.popup__heading__sub").html(title);
				$('input#width').click();
				$('input#height').click();
			}

			showALERT = function(thisisthealert) {
				console.log(thisisthealert);
				//alert(thisisthealert);
			}

			function showMindMap(w, h) {
				$('input#pc').click();
				{literal}
				$('input#width').val(Math.round(w));
				$('input#height').val(Math.round(h));
				canvas_width = w;
				canvas_height = h;
				LoadMindMap();
				$('div#resolution').animate({width: w+"px"}, 400);
				$('section#create-content-attributes-popup-mindmap-preview').show();
				$("html").getNiceScroll().hide();
				{/literal}
			}

			if('{$smarty.session.isDeletable}' != '') {
				$("section#deletable").show();
				$.ajax({
					url: "/create_content/clear_session/",
					type: "POST",
					//dataType: "json",
					//data: { json: json },
					success: function(response) {
					},
					error: function(xhr, status, error) {
					}
				});
			}
		</script>

{include file="common/footer.tpl"}

<script>
$(function() {

    var bar = $('.bar');
    var percent = $('.percent');
    var status = $('#status');
    var percentVal = 0;

    $('form').ajaxForm({
    	beforeSubmit: function() {
    		console.log(percent.text());
    		if(percent.text() == '100%') {
    			$('body').trigger('click');
    			return false;
    		}
    	},
        beforeSend: function() {        	
        	// return;
        	// if($('div#status').text() == '100%') {
        	// 	alert($('#status').text());
        	// 	$('body').trigger('click');
        	// 	return;
        	// }
            status.empty();
            var percentVal = '0%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        uploadProgress: function(event, position, total, percentComplete) {
            percentVal = percentComplete + '%';
            bar.width(percentVal);
            percent.html(percentVal);
        },
        complete: function(xhr) {
        	location.reload();
            status.html(xhr.responseText);
            //alert(percentVal);
        }
    });
});
// $( "#draggable" ).draggable();
</script>