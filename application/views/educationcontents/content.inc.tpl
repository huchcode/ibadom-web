
<ol class="children">
	{foreach from=$childrens item=node}
		{if $node.connection_type != 'root'}
			<li id="draggable" class="children__item">
				<div class="node" data-type="{$node.connection_type}" data-id="{$node.id}">
					<div class="node__text" style="display: block;" data-type="{$node.connection_type}" data-id="{$node.id}">
						{$node.title}
					</div>
				</div>
				{if $node.children|count gt 0}
					{include file='./content.inc.tpl' childrens=$node.children}
				{/if}
			</li>
		{/if}
	{/foreach}
</ol>