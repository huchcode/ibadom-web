<!DOCTYPE html>
<html>
	<head>
		<title>Gpace Test Form</title>
		<link rel="stylesheet" href="http://gpace-api.hubbleplay.com/stylesheets/style.css">
		<link rel="stylesheet" href="http://gpace-api.hubbleplay.com/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="http://gpace-api.hubbleplay.com/bootstrap/css/bootstrap-responsive.min.css">
		<link rel="stylesheet" href="http://gpace-api.hubbleplay.com/stylesheets/style.css">
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
		<script src="http://gpace-api.hubbleplay.com/bootstrap/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
			<h3>User</h3>
			<div>
				<form name="login" method="POST" action="/api/login" class="form-horizontal">
					<input type="text" name="username" placeholder="username" required>
					<input type="password" name="password" placeholder="password" required>
					<input type="submit" value="login" class="btn">
				</form>
			</div>

			<div>
				<form method="POST" action="/api/sync2mobile" class="form-horizontal">
					<input type="text" name="token" placeholder="token" required>
					<input type="number" name="user_id" placeholder="user_id" required>
					<input type="number" name="sync_id" placeholder="sync_id" required>
					<input type="text" name="device_information" placeholder="device_information" class="form-horizontal">
					<input type="submit" value="sync2mobile" class="btn">
				</form>
			</div>

			<div>
				<form method="POST" action="/api/synclearningactual" class="form-horizontal">
					<input type="text" name="token" placeholder="token">
					<input type="number" name="user_id" placeholder="user_id">
					<input type="number" name="sync_id" placeholder="sync_id">
					<input type="text" name="data" placeholder="data">
					<input type="submit" value="synclearningactual" class="btn">
				</form>
			</div>

			<div>
				<form method="POST" action="/api/updateuserphoto" class="form-horizontal" enctype="multipart/form-data" >
					<input type="text" name="token" placeholder="token">
					<input type="number" name="user_id" placeholder="user_id">
					<input type="file" name="userpic" size="20" />
					<input type="submit" value="updateuserphoto" class="btn">
				</form>
			</div>

			<div>
				<form method="POST" action="/api/changepassword" class="form-horizontal">
					<input type="text" name="token" placeholder="token">
					<input type="number" name="user_id" placeholder="user_id">
					<input type="text" name="old_password" placeholder="old_password">
					<input type="text" name="new_password" placeholder="new_password">
					<input type="submit" value="changepassword" class="btn">
				</form>
			</div>

			<div>
				<form method="POST" action="/api/forgetpassword" class="form-horizontal">
					<input type="text" name="username" placeholder="username">
					<input type="text" name="name" placeholder="name">
					<input type="text" name="email" placeholder="email">
					<input type="submit" value="forgetpassword" class="btn">
				</form>
			</div>
		</div>


	</body>
</html>
