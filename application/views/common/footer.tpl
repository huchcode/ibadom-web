	<!-- footer -->

	<!--<footer>
		<div class="container">
			<div class="footer__list">
				<img src="{$base_url}assets/img/bullet.png">
				<p>
					(주)지니어스팩토리 본사<br>
					TEL:070-4367-4725<br>
					인천광역시 연수구 송도미래로 30,<br>
					스마트밸리 D동-1007호
				</p>
			</div>
			<div class="footer__list">
				<img src="{$base_url}assets/img/bullet.png">
				<p>
					(주)지니어스팩토리 서울사무소<br>
					TEL:02-554-3188<br>
					서울특별시 봉은사로 18길4, 523호
				</p>
			</div>
			<div class="footer__list">
				<img src="{$base_url}assets/img/bullet.png">
				<p>
					(주)지니어스팩토리 기업부설연구소<br>
					TEL:070-4367-4725<br>
					인천광역시 부평구 길주로 647번길4,<br>
					프리엘림 403호
				</p>
			</div>
			<div id="copyright">
				<p>
					Copyright &copy; GENIUS FACTORY.co.ltd<span class="spacer"></span>2015 All Rights Reserved.<br>
					대표: 이주환<span class="spacer"></span>사업자등록번호:122-86-37650
				</p>
			</div>
		</div>
	</footer>-->
	<!-- end footer -->
	
	<!-- footer -->
		<footer>
			<div class="footer__list">
				<img src="{$base_url}assets/img/footer_long_bullet.png">
				<p>
					<span id="footer__cs-center" class="blue">CS CENTER</span>
					<span id="footer__phone-number">070-4367-4725</span>
				</p>
			</div>
			<div class="footer__list">
				<p>
					평일 : 09:00 ~ 18:00<br>
					일요일/토요일/공휴일 휴무<br>
					점심시간 : 12:00 ~ 13:00
				</p>
			</div>
			<div class="footer__list">
				<span style="width: 310px"></span>
			</div>
			<div id="copyright">
				<p>
					Copyright &copy; GENIUS FACTORY.co.ltd<span class="spacer"></span>2016 All Rights Reserved.<br>
					대표: 이주환<span class="spacer"></span>사업자등록번호:122-86-37650
				</p>
			</div>
		</footer>
		<!-- end footer -->
	</body>
</html>