<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="{$base_url}assets/css/base.css">
		<!-- <link rel="stylesheet" href="{$base_url}assets/css/header.css"> -->
		<link rel="stylesheet" href="{$base_url}assets/css/footer.css">
		<link rel="stylesheet" href="{$base_url}assets/css/main.css">	
		<link rel="stylesheet" href="{$base_url}assets/css/intro.css">
		<link rel="stylesheet" href="{$base_url}assets/css/form.css">
		<link rel="stylesheet" href="{$base_url}assets/css/login.css">
		<link rel="stylesheet" href="{$base_url}assets/css/register.css">
		<link rel="stylesheet" href="{$base_url}assets/css/register_form.css">
		<link rel="stylesheet" href="{$base_url}assets/css/content.css">
		<link rel="stylesheet" href="{$base_url}assets/css/table.css">
		<link rel="stylesheet" href="{$base_url}assets/css/content_list.css">
		<link rel="stylesheet" href="{$base_url}assets/css/content_create.css">
		<link rel="stylesheet" href="{$base_url}assets/css/popup.css">
		<link rel="stylesheet" href="{$base_url}assets/css/module.css">
		<link rel="stylesheet" href="{$base_url}assets/css/mindmap.css">
		<link rel="stylesheet" href="{$base_url}assets/css/layout.css">
		<link rel="stylesheet" href="{$base_url}assets/css/education_placement.css">
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" charset="utf8" src="http://malsup.github.io/jquery.form.js"></script>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery.nicescroll.js"></script>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/form_handler.js"></script>
		<script src="{$base_url}assets/js/base.js"></script>
		<title>{$title}</title>
		<script type="text/javascript">
		$(document).ready(
			  function() {
			  	$("html").niceScroll();
			    $("div#mindmap").niceScroll();
			    $("div.search__results").niceScroll();
			    $("div#popscroll").niceScroll();
			    $("div#mainBGMindmap").niceScroll();
			    $("div#mainIcons").niceScroll();
			    $("section#create-content-attributes-popup-mindmap-preview").niceScroll();
			    $("div#attribute_preview").niceScroll();
			  }
		);
		</script>
		<style>
		.selected {
			background-color: #29ABE1;
			/*border-style: solid;*/
			border-color: #29ABE1;
			opacity:0.7;
		}
		</style>
	</head>
	<body>
		<!-- header -->
		<header>
			{if empty($smarty.session.master_user_id)}
			<nav id="header__user-nav" class="inline-middle">
				<img src="{$base_url}assets/img/user_bullet.png">
				<a href="{$base_url}login">로그인</a>
				<a href="{$base_url}registration">회원가입</a>
			<!--	<a href="{$base_url}contact">고객센터</a> -->
			</nav>
			{else}
			<nav id="header__user-nav" class="inline-middle">
				{if {$smarty.session.type} eq 'master'}
					<img src="{$base_url}assets/img/master_bullet.png">
				{/if}
				<a href="/profile">{$smarty.session.name}</a>
				<div id="header__profile-image">
					<img src="{if ! empty($smarty.session.profile_picture)}{$smarty.session.profile_picture}{else}{$base_url}assets/img/user_blue_icon.png{/if}">
				</div>
				<a href="/logout">로그아웃</a>
			<!--	<a href="{$base_url}contact">고객센터</a> -->
			</nav>
			{/if}
			<a href="{$base_url}"><img id="header__logo" src="{$base_url}assets/img/logo.png" alt="Logo"></a>
			<div class="header__line"></div>
			<nav id="header__link-nav">
				<a href="/my_created_contents">교육 컨텐츠 관리</a>
				<a href="/content_assign">교육 배정</a>
				<a href="/learning_progress">교육 진도 관리</a>
				<a href="/user_management">직원(부서) 등록</a>
				<a href="/template_mindmap_background">템플릿</a>
				<a href="/customercenter">고객센터</a>
			</nav>
		</header>
		<!-- end header -->