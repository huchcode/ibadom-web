{include file="common/header.tpl"}
<link rel="stylesheet" href="{$base_url}assets/css/module.css">
	
	<!-- service change -->
	<section id="content">
		<nav id="content__nav">
			home > 프로필 > 서비스상태변경
		</nav>
		<section id="service-change" class="content-container">
			<h1 id="content__heading" class="blue">서비스 상태변경</h1>
			<form id="service-change" class="form-container" method="POST" action="">
				<input type="hidden" name="state" value="active">
				<div class="message__box  message__box--border message__box--fill">
					<div class="message__box__middle message__box__middle--expand">
						<p class="blue">
							서비스를 ‘정상’으로 변경합니다.<br>
							정상으로 변경하시면 모든 서비스를 즉시 이용하실 수 있으며, 요금은 익월에 청구됩니다.<br>
							청구된 요금은 정산관리를 통해 확인가능합니다.<br>
							<br>
							지팩엔터프라이즈 서비스를 다시 이용해주셔서 감사합니다.<br>
							지팩엔터프라이즈는 더욱 만족스런 서비스를  제공하기 위해 항상 노력하고 있습니다.<br>
						</p>
					</div>
				</div>
				<div id="service-change__basic" class="form__table__container">
					<div class="service-change__table form__table">
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>현재 서비스상태</p>
							</div>
							<div class="form__table__list__value">
								<p class="blue">{$status_ko}</p>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>변경 서비스상태</p>
							</div>
							<div class="form__table__list__value">
								<p>정상</p>
							</div>
						</div>
					</div>
				</div>
				<div class="form__table__container form__table__container--center">
					<button class="confirm" type="submit">변경</button>
					<button class="cancel" type="reset">취소</button>
				</div>
			</form>
		</section>
	</section>
	<!-- end service change -->

{include file="common/footer.tpl"}
