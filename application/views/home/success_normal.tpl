{include file="common/header.tpl"}

		<!-- service change change -->
		<section id="content">
			<nav id="content__nav">
				home > 프로필 > 서비스상태변경
			</nav>
			<section id="service-change-change" class="content-container">
				<h1 id="content__heading" class="blue">서비스 상태변경</h1>
				<div class="form-container">
					<div class="message__box message__box--fill">
						<div class="message__box__top">
							<h3 class="blue">지팩 엔터프라이즈 서비스가 정상으로 변경되었습니다.</h3>
						</div>
						<div class="message__box__middle message__box__middle--expand">
							<p>
								지금부터 관리자, 저자, 사용자의 로그인이 중지되고 컨텐츠 저작, 변경등<br>
								모든 서비스의 사용이 정상으로 가능합니다.<br>
								<br>
								서비스가 정상으로 접수되었으므로 지금부터 서비스 사용료가 자동으로 정산됩니다.<br>
							</p>
						</div>
					</div>
					<div class="form__table__container form__table__container--center">
						<a href="">
							<button class="confirm">프로필</button>
						</a>
					</div>
				</div>
			</section>
		</section>
		<!-- end service change change-->

{include file="common/footer.tpl"}