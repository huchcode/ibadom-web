{include file="common/header.tpl"}
<link rel="stylesheet" href="{$base_url}assets/css/module.css">

	<!-- service change -->
	<section id="content">
		<nav id="content__nav">
			home > 프로필 > 서비스상태변경
		</nav>
		<section id="service-change" class="content-container">
			<h1 id="content__heading" class="blue">서비스 상태변경</h1>
			<form id="service-change" class="form-container" method="POST" action="">
				<div class="message__box  message__box--border message__box--fill">
					<div class="message__box__middle message__box__middle--expand">
						<p class="blue">
							서비스 일시정지(해지)하시면 관리자, 사용자, 저자 모두에게 컨텐츠 저작, 변경, 교육등<br>
							모든 서비스가 즉시 중지 됩니다.<br>
							<br>
							지팩E 서비스 사용료는 서비스 일시정지(해지) 신청이 접수된 일시까지 정산되니 참고바랍니다.<br>
							<br>
							서비스 일시정지(해지) 후 다시 정상으로 서비스 상태를 변경하시면 모든<br>
							서비스를 정상적으로 사용할 수 있으니 참조바랍니다.<br>
							<br>
							해지후 2주 이내에 정상으로 변경하지 않을 시 사용하시던 모든 계정 및 데이타는 재사용하실수 없습니다.
						</p>
					</div>
				</div>
				<div id="service-change__basic" class="form__table__container">
					<div class="service-change__table form__table">
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>현재 서비스상태</p>
							</div>
							<div class="form__table__list__value">
								<p class="blue">정상</p>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>변경 서비스상태</p>
							</div>
							<div class="form__table__list__value">
								<input type="radio" name="state" value="pause" checked>
								<label>일시정지</label>
								<input type="radio" name="state" value="termination">
								<label>해지</label>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>변경 사유</p>
							</div>
							<div class="form__table__list__value">
								<input class="extend" type="text" name="reason">
							</div>
						</div>
					</div>
				</div>
				<div class="form__table__container form__table__container--center">
					<button class="confirm" type="submit">변경</button>
					<button class="cancel" type="reset">취소</button>
				</div>
			</form>
		</section>
	</section>
	<!-- end service change -->

{include file="common/footer.tpl"}
