{include file="common/header.tpl"}

		<style type="text/css">
		.fileUpload {
			position: relative;
			overflow: hidden;
			margin: 10px;
		}
		.fileUpload input.upload {
			position: absolute;
			top: 0;
			right: 0;
			margin: 0;
			padding: 0;
			font-size: 20px;
			cursor: pointer;
			opacity: 0;
			filter: alpha(opacity=0);
		}
		.btn {
			display: inline-block;
			padding: 6px 12px;
			margin-bottom: 0;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.42857143;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			-ms-touch-action: manipulation;
			touch-action: manipulation;
			cursor: pointer;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		</style>

		<!-- user profile -->
		<section id="content">
			<nav id="content__nav">
				home > 프로필
			</nav>
			<section id="user-profile" class="form-container">
				<h1 id="content__heading" class="blue">프로필</h1>
				<div id="user-profile__basic" class="form__table__container">
					<div class="form__table__heading inline-middle">
						<img src="{$base_url}assets/img/register_info_icon.png">
						<p>기본정보</p>
					</div>
					<div class="user-profile__table form__table">
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>아이디</p>
							</div>
							<div class="form__table__list__value">
								<p>{$user_info->username}</p>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>비밀번호</p>
							</div>
							<div class="form__table__list__value">
								<span id="password_data">&bull;&bull;&bull;&bull;&bull;&bull;</span>
								<span id="password_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('password','user_info');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('password','&bull;&bull;&bull;&bull;&bull;&bull;');">취소</span>
								</span>
								<span id="password_edit" class="blue" onclick="editprofile('password','')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>이름</p>
							</div>
							<div class="form__table__list__value">
								<span id="name_data">{$user_info->name}</span>
								<span id="name_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('name','user_info');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('name','{$user_info->name}');">취소</span>
								</span>
								<span id="name_edit" class="blue" onclick="editprofile('name','{$user_info->name}')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>이메일</p>
							</div>
							<div class="form__table__list__value">
								{if empty($user_detail->email)}
								<span id="email_data"></span>
								<span id="email_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('email','user_detail');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('email','');">취소</span>
								</span>
								<span id="email_edit" class="blue" onclick="editprofile('email','')" style="cursor:pointer;">수정</span>
								{else}
								<span id="email_data">{$user_detail->email}</span>
								<span id="email_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('email','user_detail');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('email','{$user_detail->email}');">취소</span>
								</span>
								<span id="email_edit" class="blue" onclick="editprofile('email','{$user_detail->email}')" style="cursor:pointer;">수정</span>
								{/if}
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>부서</p>
							</div>
							<div class="form__table__list__value">
								{if empty($user_detail->department_name)}
								<span id="department_name_data"></span>
								<span id="department_name_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('department_name','department');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('department_name','');">취소</span>
								</span>
								<span id="department_name_edit" class="blue" onclick="editprofile('department_name','')" style="cursor:pointer;">수정</span>
								{else}
								<span id="department_name_data">{$user_detail->department_name}</span>
								<span id="department_name_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('department_name','department');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('department_name','{$user_detail->department_name}');">취소</span>
								</span>
								<span id="department_name_edit" class="blue" onclick="editprofile('department_name','{$user_detail->department_name}')" style="cursor:pointer;">수정</span>
								{/if}
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>프로필사진</p>
							</div>
							<div class="form__table__list__value">
								<div id="profile-image">
									<img src="{$base_url}{$user_info->profile_picture}" style="max-height:150px"  />
								</div>
								<!--<button class="activate activate--blue">변경</button>
								<input type="file" accept="image/x-png, image/gif, image/jpeg" id="profile_image" name="profile_image">-->
								<div class="fileUpload btn" style="background-color: #ADDCF0;">
								    <label for="profile_image">변경</label>
								    <input type="file" id="profile_image" class="upload" accept="image/x-png, image/gif, image/jpeg" name="profile_image" />
								</div>
								<label id="profile_image"></label>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>권한</p>
							</div>
							<div class="form__table__list__value">
								<p>
									{if $user_info->role == 'admin'}
									관리자
									{else} {if $user_info->role == 'author'}
									저자
									{else}
									교육생
							    </p>
								{/if} {/if}
							</div>
						</div>
					</div>
				</div>
			</section>
		</section>
		<!-- end user profile -->

<script type="text/javascript">

function editprofile(what,thedata) {
	if (what == 'password') {
		var input_type = 'password';
	} else {
		var input_type = 'text';
	}
	$('#'+what+'_data').html('<input id="'+what+'_input" type="text" name="'+what+'" value="'+thedata+'" />');
	$('#'+what+'_action').show();
	$('#'+what+'_edit').hide();
}
function cancel_action(what,thedata) {
	$('#'+what+'_data').html(thedata);
	$('#'+what+'_action').hide();
	$('#'+what+'_edit').show();	
}
function update_profile(what,tableref) {
	var thedata = $('#'+what+'_input').val();
	$.post( "/profile/ajax_update_profile/"+what+"/{$user_info->id}", { 'value':thedata,'tableref':tableref }).done(function(res) {
		/*$('#'+what+'_data').html(thedata);
		$('#'+what+'_action').hide();
		$('#'+what+'_edit').show();	*/
		location.reload();
	});
}

$('input#profile_image').change(function () {
    var file_data = $('#profile_image').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('profile_image', file_data);        
    $.ajax({
    	url: '/profile/upload/{$user_info->id}',
		dataType: 'text',  
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,                         
		type: 'post',
		success: function(res){
			//$('#profile-image').html('<img src="'+res+'" style="max-height:150px" />'); 
			location.reload();
		}
     });
});
</script>
{include file="common/footer.tpl"}