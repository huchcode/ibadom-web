{include file="common/header.tpl"}

		<!-- service change -->
		<section id="content">
			<nav id="content__nav">
				home > 프로필 > 서비스 변경기록
			</nav>
			<section id="service-change" class="content-container">
				<h1 id="content__heading" class="blue">서비스 변경기록</h1>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<div id="service-change__date" class="table__cell" style="width:140px;">
							<p>일시</p>
						</div>
						<div id="service-change__status" class="table__cell" style="width:76px;">
							<p>상태</p>
						</div>
						<div id="service-change__description" class="table__cell">
							<p>사유</p>
						</div>
					</div>
					{foreach from=$user_status_history key=i item=history}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>{$history->created}</p>
						</div>
						<div class="table__cell table__cell--center">
							{if ($history->status == 'active')}<p>정상</p>
							{elseif ($history->status == 'pause')}<p>일시정지</p>
							{elseif ($history->status == 'stop')}<p class="red">정지</p>
							{elseif ($history->status == 'temporary')}<p>가입</p>
							{elseif ($history->status == 'termination')}<p class="red">해지</p>
							{/if}
						</div>
						<div class="table__cell">
							<p>{$history->comment}</p>
						</div>
					</div>
					{/foreach}
				</div>
			</section>
		</section>
		<!-- end service change -->

{include file="common/footer.tpl"}