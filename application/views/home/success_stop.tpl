{include file="common/header.tpl"}

		<!-- service change change -->
		<section id="content">
			<nav id="content__nav">
				home > 프로필 > 서비스상태변경
			</nav>
			<section id="service-change-change" class="content-container">
				<h1 id="content__heading" class="blue">서비스 상태변경</h1>
				<div class="form-container">
					<div class="message__box message__box--fill">
						<div class="message__box__top">
							<h3 class="red inline-middle">
								<img src="{$base_url}assets/img/service_stop.png">
								<span>지팩 엔터프라이즈 서비스가 일시정지 (해지) 되었습니다.</span>
							</h3>
						</div>
						<div class="message__box__middle message__box__middle--expand">
							<p>
								접수된 시간부터 관리자, 저자, 사용자의 로그인이 중지되고 컨텐츠 저작, 변경, 사용이 중지되며<br>
								관리자와 저자의 신규등록, 정보변경 등을 할 수 없음을 다시한번 알려드립니다.<br>
								<br>
								아울러 지팩E에 서비스 일시정지(해지)가 접수되었으므로 접수된 시간까지 서비스 사용료가 자동으로 정산됩니다.
							</p>
						</div>
					</div>
					<div class="form__table__container form__table__container--center">
						<a href="">
							<button class="confirm">프로필</button>
						</a>
					</div>
				</div>
			</section>
		</section>
		<!-- end service change change-->

{include file="common/footer.tpl"}