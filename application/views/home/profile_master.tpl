{include file="common/header.tpl"}

		<style type="text/css">
		.fileUpload {
			position: relative;
			overflow: hidden;
			margin: 10px;
		}
		.fileUpload input.upload {
			position: absolute;
			top: 0;
			right: 0;
			margin: 0;
			padding: 0;
			font-size: 20px;
			cursor: pointer;
			opacity: 0;
			filter: alpha(opacity=0);
		}
		.btn {
			display: inline-block;
			padding: 6px 12px;
			margin-bottom: 0;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.42857143;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			-ms-touch-action: manipulation;
			touch-action: manipulation;
			cursor: pointer;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		</style>

		<!-- master profile -->
		<section id="content">
			<nav id="content__nav">
				home > 프로필
			</nav>
			<section id="master-profile" class="form-container">
				<h1 id="content__heading" class="blue">프로필</h1>
				<div id="master-profile__basic" class="form__table__container">
					<div class="form__table__heading inline-middle">
						<img src="{$base_url}assets/img/register_info_icon.png">
						<p>기본정보</p>
					</div>
					<div class="master-profile__table form__table">
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>아이디</p>
							</div>
							<div class="form__table__list__value">
								<p>{$user_info->username}</p>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>비밀번호</p>
							</div>
							<div class="form__table__list__value">
								<span id="password_data">&bull;&bull;&bull;&bull;&bull;&bull;</span>
								<span id="password_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('password','user_info');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('password','&bull;&bull;&bull;&bull;&bull;&bull;');">취소</span>
								</span>
								<span id="password_edit" class="blue" onclick="editprofile('password','')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>담당자 이름</p>
							</div>
							<div class="form__table__list__value">
								{$user_detail->admin_name} 
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>담당자 전화번호</p>
							</div>
							<div class="form__table__list__value">
								<span id="admin_phone_number_data">{$user_detail->admin_phone_number}</span>
								<span id="admin_phone_number_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('admin_phone_number','user_detail');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('admin_phone_number','{$user_detail->admin_phone_number}');">취소</span>
								</span>
								<span id="admin_phone_number_edit" class="blue" onclick="editprofile('admin_phone_number','{$user_detail->admin_phone_number}')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>담당자 이메일</p>
							</div>
							<div class="form__table__list__value">
								<span id="admin_email_data">{$user_detail->admin_email}</span>
								<span id="admin_email_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('admin_email','user_detail');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('admin_email','{$user_detail->admin_email}');">취소</span>
								</span>
								<span id="admin_email_edit" class="blue" onclick="editprofile('admin_email','{$user_detail->admin_email}')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>담당자 부서</p>
							</div>
							<div class="form__table__list__value">
								<span id="admin_dept_data">{$user_detail->admin_dept}</span>
								<span id="admin_dept_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('admin_dept','user_detail');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('admin_dept','{$user_detail->admin_dept}');">취소</span>
								</span>
								<span id="admin_dept_edit" class="blue" onclick="editprofile('admin_dept','{$user_detail->admin_dept}')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>담당자 직위</p>
							</div>
							<div class="form__table__list__value">
								<span id="admin_position_data">{$user_detail->admin_position}</span>
								<span id="admin_position_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('admin_position','user_detail');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('admin_position','{$user_detail->admin_position}');">취소</span>
								</span>
								<span id="admin_position_edit" class="blue" onclick="editprofile('admin_position','{$user_detail->admin_position}')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>회사 로고</p>
							</div>
							<div class="form__table__list__value">
								<div id="profile-image">
									{if !empty($user_info->profile_picture)}<img src="{$base_url}{$user_info->profile_picture}" style="max-height:150px"  />{/if}
								</div>
								<!--<button class="activate activate--blue">변경</button>
								<input type="file" accept="image/x-png, image/gif, image/jpeg" id="profile_image" name="profile_image">-->
								<div class="fileUpload btn" style="background-color: #ADDCF0;">
								    <label for="profile_image">변경</label>
								    <input type="file" id="profile_image" class="upload" accept="image/x-png, image/gif, image/jpeg" name="profile_image" />
								</div>
								<label id="profile_image"></label>
							</div>
						</div>
					</div>
				</div>
				<div id="master-profile__company" class="form__table__container">
					<div class="form__table__heading inline-middle">
						<img src="{$base_url}assets/img/register_info_icon.png">
						<p>업체정보</p>
					</div>
					<div class="master-profile__table form__table">
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend">
								<p>사업자등록번호</p>
							</div>
							<div class="form__table__list__value">
								<p>{$user_detail->business_registration_number}</p>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend">
								<p>회사명</p>
							</div>
							<div class="form__table__list__value">
								<span id="name_data">{$user_info->name}</span>
								<span id="name_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('name','user_info');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('name','{$user_info->name}');">취소</span>
								</span>
								<span id="name_edit" class="blue" onclick="editprofile('name','{$user_info->name}')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend">
								<p>대표자 성명</p>
							</div>
							<div class="form__table__list__value">
								<span id="owner_name_data">{$user_detail->owner_name}</span>
								<span id="owner_name_action" class="blue" style="display:none;">
									<span style="cursor:pointer;" onclick="update_profile('owner_name','user_detail');">저장</span> <span style="cursor:pointer;" onclick="cancel_action('owner_name','{$user_detail->owner_name}');">취소</span>
								</span>
								<span id="owner_name_edit" class="blue" onclick="editprofile('owner_name','{$user_detail->owner_name}')" style="cursor:pointer;">수정</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend">
								<p>이니셜 ※</p>
							</div>
							<div class="form__table__list__value">
								<p>{$user_detail->company_initial}</p>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend">
								<p>서비스 상태 (<a href="{$base_url}service_change_history/index/{$user_info->id}">이력</a>)</p>
							</div>
							<div class="form__table__list__value">
								<p class="blue">
									<a href="{$base_url}service_change/index/{$user_info->id}">{$status_ko}</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div id="master-profile__company-image" class="form__table__container inline-middle">
					<p>사업자등록증</p>
						<img src="{$base_url}assets/img/profile_company_image.png">
					<button class="activate activate--blue" id="show_certification">보기</button>
					<input type="file" accept="image/x-png, image/gif, image/jpeg" name="company_image">
				</div>
			</section>
		</section>
		<!-- end master profile -->


		<!-- company certification -->
		<section id="company-certification-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close_certification">
					<img src="../../assets/img/popup_exit.png">
				</div>
				<div class="popup__content">
					<form method="POST" action="">
						<div class="popup__content__section popup__content__section--center">
							<img id="company-certification" src="{$base_url}{$user_detail->location_business_registration}" height="420">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<!--<button class="activate activate--orange">변경</button>
							<input type="file" name="company_certification">-->
							<div class="fileUpload btn" style="background-color:#f68e56;color:#000;" id="uploadcertpreview">
							    <label for="company_certification_image">변경</label>				    
							</div>
							<div class="fileUpload btn" style="background-color: #ADDCF0;display:none;" id="uploadcertaction">
							    <div onclick="upload_cert_image()">저장</div>
							</div>
							<input type="file" id="company_certification_image" class="upload" accept="image/x-png, image/gif, image/jpeg" name="profile_image" />	
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end company certification -->

<script type="text/javascript">
$( "#show_certification" ).click(function() {
  $( "#company-certification-popup" ).show();
});
$( "#close_certification" ).click(function() {
  $( "#company-certification-popup" ).hide();
});

function editprofile(what,thedata) {
	if (what == 'password') {
		var input_type = 'password';
	} else {
		var input_type = 'text';
	}
	$('#'+what+'_data').html('<input id="'+what+'_input" type="'+input_type+'" name="'+what+'" value="'+thedata+'" />');
	$('#'+what+'_action').show();
	$('#'+what+'_edit').hide();
}
function cancel_action(what,thedata) {
	$('#'+what+'_data').html(thedata);
	$('#'+what+'_action').hide();
	$('#'+what+'_edit').show();	
}
function update_profile(what,tableref) {
	var thedata = $('#'+what+'_input').val();
	$.post( "/profile/ajax_update_profile/"+what+"/{$user_info->id}", { 'value':thedata,'tableref':tableref }).done(function(res) {
		/*$('#'+what+'_data').html(thedata);
		$('#'+what+'_action').hide();
		$('#'+what+'_edit').show();	
		$('#'+what+'_edit').attr("onclick","editprofile('"+what+"','"+thedata+"')");*/
		location.reload();
	});
}

$('input#profile_image').change(function () {
    var file_data = $('#profile_image').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('profile_image', file_data);
    $.ajax({
    	url: '/profile/upload/{$user_info->id}',
		dataType: 'text',  
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,                         
		type: 'post',
		success: function(res){
			//$('#profile-image').html('<img src="'+res+'" style="max-height:150px" />'); 
			location.reload();
		}
     });
});

$('input#company_certification_image').change(function () {
    var reader = new FileReader();
    reader.onload = function(){
      var output = document.getElementById('company-certification');
      output.src = reader.result;
    };
    reader.readAsDataURL(event.target.files[0]);
	$('#uploadcertpreview').hide();
	$('#uploadcertaction').show();
});

function upload_cert_image() {
    var file_data = $('#company_certification_image').prop('files')[0];   
    var form_data = new FormData();                  
    form_data.append('company_image', file_data);        
    $.ajax({
    	url: '/profile/company_cert/{$user_info->id}',
		dataType: 'text',  
		cache: false,
		contentType: false,
		processData: false,
		data: form_data,                         
		type: 'post',
		success: function(res){
			$('img#company-certification').attr("src",res);
			$('#company-certification-popup').hide();
		}
     });
}
</script>
{include file="common/footer.tpl"}