{include file="common/header.tpl"}

		<link rel="stylesheet" href="{$base_url}assets/css/template.css">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="{$base_url}assets/bootstrap/css/bootstrap.min.css">
		<!-- Optional theme -->
		<link rel="stylesheet" href="{$base_url}assets/bootstrap/css/bootstrap-theme.min.css">
		<!-- Latest compiled and minified JavaScript -->
		<script src="{$base_url}assets/bootstrap/js/bootstrap.min.js"></script>

		<!-- template mindmap background list -->
		<section id="content">
			<nav id="content__nav">
				home > 템플릿
			</nav>
			<section id="template-mindmap-bg-list" class="content-container">
				<h1 id="content__heading" class="blue">템플릿</h1>
				<div id="template__nav">
					<a href="/template_mindmap_background" class="blue inline-middle">
						<img src="{$base_url}assets/img/picture_blue_icon.png">
						<span class="no-padding">마인드맵 배경</span>
					</a>
					<a href="/template_book_icon" class="inline-middle">
						<img src="{$base_url}assets/img/rocket_black_icon.png">
						<span style="color: black;" class="no-padding">교재 아이콘</span>
					</a>
					<a href="/template_pictogram" class="inline-middle">
						<img src="{$base_url}assets/img/smile_black_icon.png">
						<span style="color: black;" class="no-padding">픽토그램</span>
					</a>
				</div>
				<div id="template-mindmap-bg-list__options" class="content-options clearfix">
					<form id="search_bgmindmap" method="POST" action="" class="inline-middle content-options--left">
						<div class="search-bar">
							<input id="search" type="text" name="search" placeholder="검색어를 입력하세요" autocomplete="off" style="height: 30px">
							<img src="{$base_url}assets/img/magnifier_grey_icon.png" alt="Search">
						</div>
					</form>
					<button class="activate activate--dark-blue activate--white content-options--right" onclick="show_popup('template-bg-mgnt-popup')">
						추가하기
					</div>
				</div>
				<div id="image-container" class="image-container popup__content__section">
					<div id="book_bgmindmap_list_row" class="image__row">
						{if {$bgMindmap|@count} gt 0}
							{foreach from=$bgMindmap item=items}
							<div id="book_bgmindmap_list" class="image__cell popup__content__section__scroll__item popup__content__section__scroll__item--125" data-id="{$items->id}">
								<img data-toggle="tooltip" id="imgpictogram" src="{$items->picture}" data-id="{$items->id}" data-creator="{$items->creator}" data-creatorname="{$items->username}" width="100px" height="123px" title="{$items->name}" name="{$items->name}">
								<!-- <p>{$items->name}</p> -->
							</div>
							{/foreach}
						{/if}
					</div>
				</div>
				<!-- <div class="paging">
					<a class="paging__first" href="">&#9668;&#9668;</a>
					<a class="paging__prev" href="">&#9668;</a>
					<a class="paging__to blue" href="">1</a>
					<a class="paging__to" href="">2</a>
					<a class="paging__to" href="">3</a>
					<a class="paging__to" href="">4</a>
					<a class="paging__next" href="">&#9658;</a>
					<a class="paging__last" href="">&#9658;&#9658;</a>
				</div>
				<div class="image-button-container">
					<button class="confirm" type="submit">저장</button>
					<button class="cancel" type="reset">취소</button>
				</div> -->
			</section>
		</section>
		<!-- end template mindmap background list -->
		<!-- template mindmap bg mgnt -->
		<section id="template-bg-mgnt-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="hide_popup('template-bg-mgnt-popup')">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form id="upload_mindmapbg" method="POST" enctype="multipart/form-data" action="">
					<div class="popup__heading inline-middle">
						<img src="{$base_url}assets/img/picture_blue_icon.png">
						<h3>마인드맵 배경</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<img id="bgmindmap" src="{$base_url}assets/img/content_create_bg_popup_image.png" height="200">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<p class="blue">※ 용량: 800K미만</p>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<div class="popup__content__section__inline">
								<label class="popup__black" style="width:80px;">배경이름</label>
								<input type="text" name="name" autocomplete="off" required>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black" style="width:80px;">작성자</label>
								<input type="text" name="creater" value="{$smarty.session.name}" readonly="true" autocomplete="off">
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="file" name="bgmindmap_content" id="bgmindmap_upload" accept="image/x-png, image/gif, image/jpeg" style="display:none;" />
							<button class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end template mindmap bg mgnt -->
		<!-- template mindmap bg mgnt preview delete -->
		<section id="template-bg-mgnt-popup-preview" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="hide_popup('template-bg-mgnt-popup-preview')">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form id="upload_mindmapbg" method="POST" enctype="multipart/form-data" action="/template_mindmap_background/edit_background_mindmap">
					<div class="popup__heading inline-middle">
						<img src="{$base_url}assets/img/picture_blue_icon.png">
						<h3>마인드맵 배경</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<img id="bgmindmap_preview" src="{$base_url}assets/img/content_create_bg_popup_image.png" height="200">
						</div>
						<div class="popup__content__section popup__content__section--center">
							<p class="blue">※ 용량: 800K미만</p>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<div class="popup__content__section__inline">
								<label class="popup__black" style="width:80px;">배경이름</label>
								<input id="preview_name" type="text" name="name" maxlength="50" autocomplete="off" required>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black" style="width:80px;">작성자</label>
								<input id="creatorname" type="text" name="creater" value="{$smarty.session.username}" readonly="true" autocomplete="off">
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" id="id" name="id">
							<button id="delete" class="activate activate--orange activate--white" type="button">삭제</button>
							<button id="edit" class="activate activate--dark-blue activate--white" type="submit">저장</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end template mindmap bg mgnt preview delete -->
		<!-- confirmation -->
		<section id="confirmation" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-pictogram" onclick="hide_popup('confirmation');">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form id="delete" method="POST" action="/template_mindmap_background/delete_background_mindmap">
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div id="delete_message" class="popup__content__section--center">
							
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" id="id" name="id">
							<button class="activate activate--dark-blue activate--white" type="submit">확인</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end confirmation -->
		<!-- alert message -->
		<section id="create-content-attributes-popup-alert" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-pictogram" onclick="hide_popup('create-content-attributes-popup-alert')">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<div class="popup__heading inline-middle">
					<h3>메시지</h3>
				</div>
				<form method="POST" action="">
					<div class="popup__content">
						<div class="popup__content__section popup__content__section--center">
							<p id="alert-content-popup__selected" class="popup__black red">내용을 선택하십시오.</p>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="alert_close" class="activate activate--dark-blue activate--white" type="button" onclick="hide_popup('create-content-attributes-popup-alert')">취소</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end alert message -->

		<script type="text/javascript">
			$(document).ready(function() {
				$('body').tooltip({
				    selector: '[data-toggle=tooltip]'
				});

				$('form#search_bgmindmap').on("keyup keypress", function(e) {
					var code = e.keyCode || e.which; 
					if (code  == 13) {
						e.preventDefault();
						return false;
					}
				});
				//Upload button
				$('img#bgmindmap').on('click',function(evt){
					evt.preventDefault();
					$('input#bgmindmap_upload').trigger('click');
			    });

			    $("input#bgmindmap_upload").change(function(){
			        readURL(this, 'bgmindmap');
			    });

			    //Preview-Delete
			    $("img#imgpictogram").on('click',function(){
			    	var src = $(this).attr('src');
			    	var text = $(this).attr('name');
			    	var id = $(this).data("id");
			    	var creator = $(this).data("creator");
			    	var username = $(this).data("creatorname");
					$('div#delete_message').text(text+' 을(를) 삭제하시겠습니까?');
			    	$("img#bgmindmap_preview").attr('src', src);
			    	$("input#preview_name").val(text);
			    	$("input#creatorname").val(username);
			    	$("input#id").val(id);

			    	if(creator != '{$smarty.session.user_id}') {
			    		$('input#preview_name').prop('readonly', true);
			    		$('button#delete').hide();
			    		$('button#edit').hide();
			    	} else {
			    		$('input#preview_name').prop('readonly', false);
			    		$('button#delete').show();
			    		$('button#edit').show();
			    	}

			    	$("section#template-bg-mgnt-popup-preview").show();
			    	$("html").getNiceScroll().hide();
			    });

			    $("button#delete").on('click',function(){			    	
					$('section#confirmation').show();
					$("html").getNiceScroll().hide();
			    });

				//Close Pop-ups
				$('body').on('click', '.popup__bg', function(){
					$('section#template-bg-mgnt-popup').hide();
					$('section#create-content-attributes-popup-alert').hide();
					$("html").getNiceScroll().show();
				});

				//Error Message
				if('{$error}' != '') {
					$("html").getNiceScroll().hide();
			    	$('#create-content-attributes-popup-alert').show();
					$("p#alert-content-popup__selected").html('{$error}');					
			    }

				//BG Mindmap
				$("input#search").keyup(function() {
					$("#book_bgmindmap_list_row").empty();
					var keyword = $("#search").val();
					if(keyword.length > 0) {
					    $.ajax({
					      url: "/my_created_contents/get_background_mindmap",
					      type: "POST",
					      data: { bgmindmap_name: keyword },
					      dataType: "json",
					      success: function(response) {
					      	$("#book_bgmindmap_list_row").empty();
					      	var total = response.length;
					      	if(total > 0) {
					      		$.each(response, function(i, item) {
								    $("div#book_bgmindmap_list_row").append('<div id="book_bgmindmap_list" class="image__cell popup__content__section__scroll__item popup__content__section__scroll__item--125"><img data-toggle="tooltip" src="'+ response[i].picture +'" data-id="'+ response[i].id +'" width="100px" height="123px" title="'+ response[i].name +'"></div>');
								});
				      		}
					      },
					      error: function(xhr, status, error) {
					      	console.log(error);
							  alert('error '+ xhr.responseText);
							}
					    });
					}
					else {
						$.ajax({
					      url: "/my_created_contents/get_background_mindmap",
					      type: "POST",
					      data: { bgmindmap_name: keyword },
					      dataType: "json",
					      success: function(response) {
					      	$("#book_bgmindmap_list_row").empty();
					      	var total = response.length;
					      	if(total > 0) {
					      		$.each(response, function(i, item) {
								    $("div#book_bgmindmap_list_row").append('<div id="book_bgmindmap_list" class="image__cell popup__content__section__scroll__item popup__content__section__scroll__item--125"><img data-toggle="tooltip" src="'+ response[i].picture +'" data-id="'+ response[i].id +'" width="100px" height="123px" title="'+ response[i].name +'"></div>');
								});
				      		}
					      },
					      error: function(xhr, status, error) {
					      	console.log(error);
							  alert('error '+ xhr.responseText);
							}
					    });
					}					
				});
				//End BG Mindmap				
			});

			function show_popup(id) {
				$('section#'+id).show();
				$("html").getNiceScroll().hide();
			}

			function hide_popup(id) {
				$('section#'+id).hide();
				$("html").getNiceScroll().show();
			}

			function readURL(input, id) {
		        if (input.files && input.files[0]) {
		        	var size = input.files[0].size / 1024000;
		        	if(size > 10) {
		        		$('#fileexceeded').show();
		        		return false;
		        	}
		            var reader = new FileReader();
		            reader.onload = function (e) {
		                $('img#'+id).attr('src', e.target.result);
		            }
		            reader.readAsDataURL(input.files[0]);
		        }
		    }
		</script>

{include file="common/footer.tpl"}