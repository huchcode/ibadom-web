<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="{$base_url}assets/css/base.css">
		<link rel="stylesheet" href="{$base_url}assets/css/layout.css">
		<link rel="stylesheet" href="{$base_url}assets/css/progress_tracking.css">
		<link rel="stylesheet" href="{$base_url}assets/css/module.css">
		<link rel="stylesheet" href="{$base_url}assets/css/education_contents.css">
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery.nicescroll.js"></script>
		<!-- Cookie -->
		<script src="{$base_url}assets/js/jquery.cookie.js"></script>
		<title>Progress Tracking for contents</title>
		<script type="text/javascript">
		$(document).ready(
			  function() {
			  	$("html").niceScroll();
			    $("div#mindmap").niceScroll();
			    $("div.search__results").niceScroll();
			    $("div.popup__content__section__scroll.popup__content__section__scroll--60").niceScroll();
			    $("div#mainBGMindmap").niceScroll();
			    $("div#mainIcons").niceScroll();
			  }
		);
		</script>
	</head>
	<body>
		<div style="display:none">{counter start=0 skip=1}</div>
		<!-- learning progress detail question -->
		<section id="content">
			<!-- back -->
			<a id="back" href="#" onclick="javascript: history.go(-1); return false;">
				<img src="{$base_url}assets/img/back.png" alt="Back">
			</a>
			<!-- end back -->
			<section id="learning-progress-detail-question" class="content-container">
				<h1 id="content__heading" class="blue">문제풀이 진도현황</h1>
				{if {$smarty.session.role} != 'admin' and {$smarty.session.role} != 'author' and {$smarty.session.role} != 'master'}
					{include file="common/permission.tpl"}

					<script type="text/javascript">
					$(document).ready(function() {
						$('div#learning-progress-detail-question__options').empty();
						$('div.table-container').empty();
					});
					</script>
				{/if}
				<div id="learning-progress-detail-question__options" class="content-options clearfix">
					<div class="content-options--left inline-middle">
						<img src="{$base_url}assets/img/book_blue_icon.png">
						<span>
							<p class="blue">학습 컨텐츠: </p>
							<p>
							{if {$examinationlist|@count} gt 0}
							{foreach from=$examinationlist item=items}
								{$items->name}
								{break} 
							{/foreach}
							{/if}
							</p>
						<span>
					</div>
					<div class="content-options--right inline-middle">
						<img src="{$base_url}assets/img/user_blue_icon.png">
						<span>
							<p class="blue">직원명: </p>
							<p id="employee"></p>
						</span>
					</div>
				</div>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<div id="learning-progress-detail-question__num" class="table__cell">
							<p>No.</p>
						</div>
						<div id="learning-progress-detail-question__question" class="table__cell">
							<p>출제문제</p>
						</div>
						<div id="learning-progress-detail-question__type" class="table__cell">
							<p>유형</p>
						</div>
						<div id="learning-progress-detail-question__date" class="table__cell">
							<p>풀이일자</p>
						</div>
					</div>
					{if {$examinationlist|@count} gt 0}
					{foreach from=$examinationlist item=items}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>{counter}</p>
						</div>
						<div class="table__cell">
							<a href="javascript:void(0);" class="blue" onclick="showexamdetails({$items->id})">{$items->question}</a>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$items->type}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$items->actual_date}</p>
						</div>
					</div>
					{/foreach}
					{/if}
				</div>
				<p class="table-description blue">※ 문제 제목을 클릭하시면 기출문제를 볼 수 있습니다.</p>
				<!-- <div class="paging">
					<a class="paging__first" href="">&#9668;&#9668;</a>
					<a class="paging__prev" href="">&#9668;</a>
					<a class="paging__to blue" href="">1</a>
					<a class="paging__to" href="">2</a>
					<a class="paging__to" href="">3</a>
					<a class="paging__to" href="">4</a>
					<a class="paging__next" href="">&#9658;</a>
					<a class="paging__last" href="">&#9658;&#9658;</a>
				</div> -->
			</section>
		</section>
		<!-- end learning progress detail question -->

		<!-- learning progress matter -->
		<section id="learning-progress-matter-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form method="POST" action="">
					<div class="popup__heading">
						<h3>출제문제</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<div class="popup__content__section__inline">
								<label class="popup__black">문제</label>
								<h4 id="learning-progress-matter__question" class="popup__content__section__inline__long-text blue">생산성 향상을 위해서 반드시 지켜야 하는 원칙과 거리가 먼것은?</h4>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black">첨부</label>
								<img src="{$base_url}assets/img/content_create_image_image.png" height="80">
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black">정답</label>
								<span class="popup__content__section__inline__long-text learning-progress-matter__answer">
									<div class="learning-progress-matter__answer__wrap">
										<p class="blue">이건 정답입니다.</p>
										<span class="learning-progress-matter__answer__wrap__type blue">정답</span>
									</div>
								</span>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black blue"></label>
								<span class="popup__content__section__inline__long-text learning-progress-matter__answer">
									<div class="learning-progress-matter__answer__wrap">
										<p class="blue">이건 오답입니다.</p>
										<span class="learning-progress-matter__answer__wrap__type">오답</span>
									</div>
								</span>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black"></label>
								<span class="popup__content__section__inline__long-text learning-progress-matter__answer">
									<div class="learning-progress-matter__answer__wrap">
										<p class="blue">이건 오답입니다.</p>
										<span class="learning-progress-matter__answer__wrap__type">오답</span>
									</div>
								</span>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black"></label>
								<span class="popup__content__section__inline__long-text learning-progress-matter__answer">
									<div class="learning-progress-matter__answer__wrap">
										<p class="blue">이건 오답입니다.</p>
										<span class="learning-progress-matter__answer__wrap__type">오답</span>
									</div>
								</span>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black">단원</label>
								<p class="blue">생산성향상</p>
							</div>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end learning progress matter -->
		<!-- examination question -->
		<section id="examination-question-popup" class="popup" style="display:none;">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" onclick="hideexamdetails();">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form method="POST" action="/examination_list/update/" id="editexamform" onsubmit="return validateForm('editexamform')">
					<input type="hidden" name="examination_id" id="eqpopup_examid" value="" />
					<input type="hidden" name="book_id" id="eqpopup_bookid" value="" />
					<div class="popup__heading">
						<h3>문제추가</h3>
					</div>
					<div class="popup__content">
						<div class="popup__content__section">
							<div class="popup__content__section__inline">
								<label class="popup__black">문제</label>
								<!-- <textarea id="eqpopup_question" class="popup__content__section__inline__long-text" name="question" placeholder="문제를 입력해주세요"></textarea> -->
								<h4 id="eqpopup_question" class="popup__content__section__inline__long-text blue">생산성 향상을 위해서 반드시 지켜야 하는 원칙과 거리가 먼것은?</h4>
							</div>
							<div class="popup__content__section__inline">
								<label class="popup__black">첨부</label>
								<span id="eqpopup_attached_image_container">
								</span>
								<input type="hidden" id="eqpopup_attached_photo_url" name="attached_photo_url" value="" />
							</div>
							<div class="popup__content__section__inline eqpopup_question_type_container">
								<label class="popup__black">유형</label>
								<input type="radio" id="eqpopup_question_type" name="question_type" value="multiple">
								<label>객관식</label>
								<input type="radio" id="eqpopup_question_type2" name="question_type" value="short">
								<label>주관식</label>
							</div>
						</div>
						<div class="popup__content__section popup__content__section--grey popup__content__section--dashed-border" id="eqpopup_question_array_container" style="min-height:70px;">
							<div class="popup__black" style="width:74px;float:left;padding:10px 0 0 10px">정답</div>
							<div style="float:left;">
								<div class="eqpopup_question_array" style="height:1px;"></div>
							</div>
							<div style="clear:both;"></div>
						</div>
						<div class="popup__content__section">
							<div class="popup__content__section__inline">
								<label class="popup__black">단원</label>
								<select class="custom-select-" name="content_id" data-width="200" data-placeholder="일반" id="eqpopup_content_id">{foreach from=$contents key=i item=content}
									<option value="{$content->id}">{$content->title}</option>{/foreach}
								</select>
							</div>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button class="activate activate--dark-blue activate--white" type="button" onclick="hideexamdetails();">수정</button>
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end examination question -->

		<script type="text/javascript">
			$(document).ready(function() {
				var text = $.cookie("employee");
				$('p#employee').text(text);
			});

			function showexamdetails(exam_id) {
			    $.ajax({
			    	url: '/examination_list/ajax_get_questions/'+exam_id,
					dataType: 'json',
					cache: false,
					contentType: false,
					processData: false,                   
					type: 'post',
					success: function(res){
						$("#eqpopup_examid").val(res.exam.id);
						$("#eqpopup_bookid").val(res.exam.book_id);
						$(".eqpopup_question_array_container").html('<div class="eqpopup_question_array" style="height:1px;"></div>');
						if (res.exam.picture) {
							$("#eqpopup_attached_photo_url").val(res.exam.picture);

							var attached_image_html_code = '<div class="fileUpload btn">';
							attached_image_html_code += '<label for="eqattached_image">파일추가</label>';
							attached_image_html_code += '<input type="file" id="eqattached_image" onchange="equpload_attached_image()" class="upload" accept="image/x-png, image/gif, image/jpeg" name="eqattached_image" value=""/>';
							attached_image_html_code += '</div>';
							attached_image_html_code += '<label id="eqattached_photo_url_text">';
							attached_image_html_code += '<p class="blue" style="float:left;"><img src="'+res.exam.picture+'" height="50" /></p>';
							attached_image_html_code += '</label>';
							attached_image_html_code += '<div style="clear:both;"></div>';
							
						} else {
							var attached_image_html_code = '<div class="fileUpload btn">';
							attached_image_html_code += '<label for="eqattached_image">파일추가</label>';
							attached_image_html_code += '<input type="file" id="eqattached_image" onchange="equpload_attached_image()" class="upload" accept="image/x-png, image/gif, image/jpeg" name="eqattached_image" value=""/>';
							attached_image_html_code += '</div>';
							attached_image_html_code += '<label id="eqattached_photo_url_text">';
							attached_image_html_code +='<p class="blue">첨부파일을 선택해주세요</p>';;
							attached_image_html_code += '</label>';
						}
						$("#eqpopup_attached_image_container").html(attached_image_html_code);

						$( "#eqpopup_question" ).val(res.exam.question);
						$("h4#eqpopup_question").text(res.exam.question);

						/*var questiontype_container_html = '<label class="popup__black">유형</label>';
						questiontype_container_html += '<input type="radio" id="eqpopup_question_type" name="question_type" value="multiple"';
						if (res.exam.type == 'multiple') { questiontype_container_html += ' checked="checked"'; };
						questiontype_container_html += '>';
						questiontype_container_html += '<label>객관식</label>'
						questiontype_container_html += '<input type="radio" id="eqpopup_question_type2" name="question_type" value="short"';
						if (res.exam.type == 'short') { questiontype_container_html += '  checked="checked"'; };
						questiontype_container_html += '>';
						questiontype_container_html += '<label>주관식</label>';
						$( "#eqpopup_question_type_container" ).html(questiontype_container_html);
						//alert(questiontype_container_html);*/

						if (res.exam.type == 'multiple') {
							$( "#eqpopup_question_type" ).attr("checked",true);
							$( "#eqpopup_question_type2" ).removeAttr("checked");

							$('input:radio[name=question_type][value=multiple]').click();
						} else {
							$( "#eqpopup_question_type" ).removeAttr("checked");
							$( "#eqpopup_question_type2" ).attr("checked",true);

							$('input:radio[name=question_type][value=short]').click();
						}

						if (res.answer.length == 0) {
							var append_text = '<a id="examination-question__add" class="blue" style="cursor:pointer;" onclick="';
							if (res.exam.type=='multiple') {
								append_text += 'eqaddrowmultiple()';
							} else {
								append_text += 'eqaddrowtext()';
							}
							append_text += '">추가</a>';
							$("div.eqpopup_question_array:last").after(append_text);

						}

						for (i = 0; i < res.answer.length; i++) { 

							var answer_text = res.answer[i].answer_text;
							var answer_picture = res.answer[i].answer_picture;
							var correct_flag = res.answer[i].correct_flag;

							if (i==0) {
								var append_text = '<a id="examination-question__add" class="blue" style="cursor:pointer;" onclick="';
								if (res.exam.type=='multiple') {
									append_text += 'eqaddrowmultiple()';
								} else {
									append_text += 'eqaddrowtext()';
								}
								append_text += '">추가</a>';
							} else {
								var append_text = '';
							}
							append_text += '<div class="eqpopup_question_array" style="padding-bottom:5px;">';

							if (res.exam.type == 'multiple') {
								if (answer_picture && answer_picture!='null') {
									append_text += '<input type="hidden" name="answermulti[]" value="'+answer_text+'" />';
									append_text += '<div style="width:150px;float:left;"><img src="'+answer_picture+'" height="50" /></div>';
								} else {
									append_text += '<p class="blue" style="float:left;width:220px;height:30px;">'+answer_text+'</p>';
								}
								append_text += '<input type="hidden" name="answerimagemulti[]" value="'+answer_picture+'" />';
								append_text += '<p class="examination-question__answer__options" style="float:left;">';
								if (correct_flag=='correct') {
									append_text += '<input type="hidden" name="answermultitype[]" value="correct" style="display:none;">';
									append_text += '<span class="blue" id="blue_correct_answer" style="padding-left:3px">정답</span>';
								} else {
									append_text += '<input type="hidden" name="answermultitype[]" value="wrong" style="display:none;">';
									append_text += '<span style="padding-left:3px">오답</span>';
								}
							} else {
								if (answer_picture) {
									append_text += '<input type="hidden" name="answertext[]" value="'+answer_text+'" />';
									append_text += '<div style="width:150px;float:left;"><img src="'+answer_picture+'" height="50" /></div>';
								} else {
									append_text += '<p class="blue" style="float:left;width:220px;height:30px;">'+answer_text+'</p>';
								}
								append_text += '<p class="examination-question__answer__options" style="float:left;">';
								append_text += '<input type="hidden" name="answertexttype[]" value="correct" style="display:none;">';
								append_text += '<input type="hidden" name="answerimagetext[]" value="'+answer_picture+'" />';
							}
							// append_text += '<span class="blue" onclick="removerow(this);" style="cursor:pointer;">삭제</span>';
							append_text += '</p>';
							append_text += '<div style="clear:both;"></div>';
							append_text += '</div>';
							$("div.eqpopup_question_array:last").after(append_text);
						}
						$( "#eqpopup_content_id" ).val(res.exam.content_id);
						$( "#examination-question-popup" ).show();
					}
				});

			}

			function hideexamdetails() {
				$( "#examination-question-popup" ).hide();
				$( "#eqpopup_question_type" ).removeAttr("checked");
				$( "#eqpopup_question_type2" ).removeAttr("checked");
				$( "#eqpopup_question_array_container" ).html('<div class="popup__black" style="width:74px;float:left;padding:10px 0 0 10px">정답</div><div style="float:left;"><div class="eqpopup_question_array" style="height:1px;"></div></div><div style="clear:both;"></div>');
			}

			function show_popup(id) {
				$('section#'+id).show();
				$("html").getNiceScroll().hide();
			}

			function hide_popup(id) {
				$('section#'+id).hide();
				$("html").getNiceScroll().show();
			}
		</script>
	</body>
</html>