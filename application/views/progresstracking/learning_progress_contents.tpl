<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="{$base_url}assets/css/base.css">
		<link rel="stylesheet" href="{$base_url}assets/css/layout.css">
		<link rel="stylesheet" href="{$base_url}assets/css/progress_tracking.css">
		<link rel="stylesheet" href="{$base_url}assets/css/module.css">
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery-1.11.2.min.js"></script>
		<!-- Cookie -->
		<script src="{$base_url}assets/js/jquery.cookie.js"></script>
		<title>Progress Tracking for contents</title>
	</head>
	<body>
		<div style="display:none">{counter start=0 skip=1}</div>
		<!-- learning progress detail study -->
		<section id="content">
			<!-- back -->
			<a id="back" href="#" onclick="javascript: history.go(-1); return false;">
				<img src="{$base_url}assets/img/back.png" alt="Back">
			</a>
			<!-- end back -->
			<section id="learning-progress-detail-study" class="content-container">
				<h1 id="content__heading" class="blue">학습진도현황</h1>
				{if {$smarty.session.role} != 'admin' and {$smarty.session.role} != 'author' and {$smarty.session.role} != 'master'}
					{include file="common/permission.tpl"}

					<script type="text/javascript">
					$(document).ready(function() {
						$('div#learning-progress-detail-study__options').empty();
						$('div.table-container').empty();
					});
					</script>
				{/if}
				<div id="learning-progress-detail-study__options" class="content-options clearfix">
					<div class="content-options--left inline-middle">
						<img src="{$base_url}assets/img/book_blue_icon.png">
						<span>
							<p class="blue">학습 컨텐츠: </p>
							<p>{$book_title}</p>
						<span>
					</div>
					<div class="content-options--right inline-middle">
						<img src="{$base_url}assets/img/user_blue_icon.png">
						<span>
							<p class="blue">직원명: </p>
							<p id="employee"></p>
						</span>
					</div>
				</div>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<div id="learning-progress-detail-study__num" class="table__cell">
							<p>No.</p>
						</div>
						<div id="learning-progress-detail-study__content-name" class="table__cell">
							<p>컨텐츠명</p>
						</div>
						<div id="learning-progress-detail-study__date" class="table__cell">
							<p>학습일</p>
						</div>
					</div>
					{if {$contentslist|@count} gt 0}
					{foreach from=$contentslist item=items}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>{counter}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$items->title}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$items->actual_date}</p>
						</div>
					</div>
					{/foreach}
					{/if}
				</div>
				<!-- <div class="paging">
					<a class="paging__first" href="">&#9668;&#9668;</a>
					<a class="paging__prev" href="">&#9668;</a>
					<a class="paging__to blue" href="">1</a>
					<a class="paging__to" href="">2</a>
					<a class="paging__to" href="">3</a>
					<a class="paging__to" href="">4</a>
					<a class="paging__next" href="">&#9658;</a>
					<a class="paging__last" href="">&#9658;&#9658;</a>
				</div> -->
			</section>
		</section>
		<!-- end learning progress detail study -->

		<script type="text/javascript">
			$(document).ready(function() {
				var text = $.cookie("employee");
				$('p#employee').text(text);
			});
		</script>
	</body>
</html>