{include file="common/header.tpl"}
		
		<link rel="stylesheet" href="{$base_url}assets/css/progress_tracking.css">
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="{$base_url}assets/bootstrap/css/bootstrap.min.css">

		<!-- Optional theme -->
		<link rel="stylesheet" href="{$base_url}assets/bootstrap/css/bootstrap-theme.min.css">

		<!-- Latest compiled and minified JavaScript -->
		<script src="{$base_url}assets/bootstrap/js/bootstrap.min.js"></script>

		<!-- Cookie -->
		<script src="{$base_url}assets/js/jquery.cookie.js"></script>

		<script type="text/javascript">
		$(document).ready(function() {
			$('body').click(function() {
				$("#search_result").empty();
			});
			//Search ALL
			$("input#search").keyup(function() {
				$("#search_result").empty();
				var name = $("#search").val();
				if(name.length > 0)
				{
					$("#search_result").empty();
					//Search ALL
					// if ($('input#all').is(':checked') || $('input#contents').is(':checked')) {
					//     $.ajax({
					//       url: "/learning_progress/search_employee",
					//       type: "POST",
					//       data: { name: name },
					//       dataType: "json",
					//       success: function(response) {
					//       	$("#search_result").empty();
					//       	console.log(response);
					//       	var total = response.length;
					//       	if(total > 0)
					//       	{
					// 	      	$.each(response, function(i, item) {
					// 			    $("#search_result").append('<div id="search_result_list" class="search__results__item" data-id="' + response[i].input_user_id + '" data-type="' + response[i].type + '">' + response[i].name + '</div>');
					// 			});
					//       	}
					//       },
					//       error: function(xhr, status, error) {
					//       	console.log(xhr);
					// 		  alert(xhr.responseText);
					// 		}
					//     });
					// }
					//Search Department
					if ($('input#dept').is(':checked')) {
						$.ajax({
					      url: "/learning_progress/search_department",
					      type: "POST",
					      data: { name: name },
					      dataType: "json",
					      success: function(response) {
					      	$("#search_result").empty();
					      	console.log(response);
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#search_result").append('<div id="search_result_list" class="search__results__item" data-id="' + response[i].input_department_id + '" data-type="' + response[i].type + '">' + response[i].department_name + '</div>');
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(xhr);
							  alert(xhr.responseText);
							}
					    });
					}
					//Search Employee
					if ($('input#name').is(':checked')) {
						$.ajax({
					      url: "/learning_progress/search_employee",
					      type: "POST",
					      data: { name: name },
					      dataType: "json",
					      success: function(response) {
					      	$("#search_result").empty();
					      	console.log(response);
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#search_result").append('<div id="search_result_list" class="search__results__item" data-id="' + response[i].input_user_id + '" data-type="' + response[i].type + '">' + response[i].name + '</div>');
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(xhr);
							  alert(xhr.responseText);
							}
					    });
					}
					//Search Book
					if ($('input#contents').is(':checked')) {
						$.ajax({
					      url: "/learning_progress/search_book",
					      type: "POST",
					      data: { name: name },
					      dataType: "json",
					      success: function(response) {
					      	$("#search_result").empty();
					      	console.log(response);
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#search_result").append('<div id="search_result_list" class="search__results__item" data-id="' + response[i].id + '" data-type="' + response[i].type + '">' + response[i].title + '</div>');
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(xhr);
							  alert(xhr.responseText);
							}
					    });
					}
				}
			});

			$('form#progress_list').on("keyup keypress", function(e) {
				var code = e.keyCode || e.which; 
				if (code  == 13) {
					e.preventDefault();
					return false;
				}
			});

			$("input[name=dept]:radio").change(function () {
				$('input#search').val('');
				$('#search_result').empty();
				$("input#search").prop("disabled", false)
			});

			$("input#all").change(function () {
				if ($('input#all').is(':checked')) {
		            var text = $(this).text().trim();
			    	$('input#type').val('all');
					$('input#search').val(text);
					$("form#progress_list").submit();
		        }
	        });

			$(document).on('click', '#search_result_list', function(){ 
		    	var text = $(this).text().trim();
		    	var id = $(this).data("id");
		    	var type = $(this).data("type");
		    	$('input#id').val(id);
		    	$('input#type').val(type);
				$('input#search').val(text);
				$("form#progress_list").submit();
			 });

			if('{$type}' == 'all') {
				$("input#all").prop("checked", true);
				$("input#search").prop("disabled", true);
			}

			if('{$type}' == 'employee') {
				$("input#name").prop("checked", true);
			}

			if('{$type}' == 'department') {
				$("input#dept").prop("checked", true);
			}

			if('{$type}' == 'book') {
				$("input#contents").prop("checked", true);
			}
		});

		function setCookie(employee) {
			$.cookie("employee", employee);
		}
		</script>

		<div style="display:none">{counter start=0 skip=1}</div>

		<!-- learning progress all -->
		<section id="content">
			<nav id="content__nav">
				home > 진도현황(컨텐츠별)
			</nav>
			<section id="learning-progress-all" class="content-container">
				<h1 id="content__heading" class="blue">진도현황 (컨텐츠별)</h1>
				{if {$smarty.session.role} != 'admin' and {$smarty.session.role} != 'author' and {$smarty.session.role} != 'master'}
					{include file="common/permission.tpl"}

					<script type="text/javascript">
					$(document).ready(function() {
						$('div#learning-progress-all__options').empty();
						$('div.table-container').empty();
					});
					</script>
				{/if}
				<div id="learning-progress-all__options" class="content-options clearfix">
					<div class="content-options--left">
						<form id="progress_list" method="POST" action="">
							<input id="name" type="radio" name="dept" value="name" checked>
							<label for="name" class="grey">이름</label>
							<input id="dept" type="radio" name="dept" value="dept">
							<label for="dept" class="grey">부서명</label>
							<input id="contents" type="radio" name="dept" value="content">
							<label for="contents" class="grey">컨텐츠명</label>
							<input id="all" type="radio" name="dept" value="all">
							<label for="all" class="grey">전직원</label>
							<div class="search-bar">
								<input type="text" id="search" name="search" placeholder="검색어를 입력하세요" value="{if isset($smarty.post.search)}{$smarty.post.search}{/if}" autocomplete="off">
								<img src="{$base_url}assets/img/magnifier_grey_icon.png" alt="Search">
								<!-- search results -->
								<div id="search_result" class="search__results" style="max-height:290px; overflow-y: scroll">
								</div>
								<!-- end search results -->
							</div>
							<input id="type" type="hidden" name="type">
							<input id="id" type="hidden" name="id">
							<input id="name" type="hidden" name="name">
						</form>
					</div>
				</div>
				<div class="table-container">					
					<div class="table__row table__row--heading">
						<div id="learning-progress-all__num" class="table__cell">
							<p>No.</p>
						</div>
						<div id="learning-progress-all__name" class="table__cell">
							<p>직원명</p>
						</div>
						<div id="learning-progress-all__content-name" class="table__cell">
							<p>컨텐츠명</p>
						</div>
						<div id="learning-progress-all__study-progress" class="table__cell">
							<p>학습진도</p>
						</div>
						<div id="learning-progress-all__question" class="table__cell">
							<p>문제풀이진도</p>
						</div>
					</div>					
					{if {$progresslist|@count} gt 0}
					{foreach from=$progresslist item=items}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>{counter}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$items->name}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$items->title}</p>
						</div>
						<div class="table__cell table__cell--center" style="width:180px;">
							<div class="asd" style="height: 100% !important">
							<a href="/learning_progress_contents/index/{$items->book_id}" class="blue" onclick="setCookie('{$items->name}')">
							{if $items->learnActual != 0}
							{assign var="learnActual" value=$items->learnActual}
							{assign var="learnPlan" value=$items->learnPlan}
							{assign var="result" value=$learnActual / $learnPlan * 100}							
								<div class="progress" style="background-image: linear-gradient(to bottom, #FFFACD 0px, #FFFACD 100%); background-repeat: repeat-x; padding: 0 0 !important;">
								  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{$result}" aria-valuemin="0" aria-valuemax="100" style="width: {$result}%; overflow: hidden;">								    
								  </div>
								  <font color="black">{$result|number_format:0}%</font>
								</div>
							{else}
								<div class="progress" style="background-image: linear-gradient(to bottom, #FFFACD 0px, #FFFACD 100%); background-repeat: repeat-x;">
								  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
								    <!-- <font color="black">0%</font> -->
								  </div>
								</div>
							{/if}
							</a>
							</div>
						</div>
						<div class="table__cell table__cell--center" style="width:180px;">
							<a href="/learning_progress_examination/index/{$items->book_id}" class="blue" onclick="setCookie('{$items->name}')">
							{if $items->examActual != 0}
							{assign var="examActual" value=$items->examActual}
							{assign var="examPlan" value=$items->examPlan}
							{assign var="result" value=$examActual / $examPlan * 100}
								<div class="progress" style="background-image: linear-gradient(to bottom, #FFFACD 0px, #FFFACD 100%); background-repeat: repeat-x; padding: 0 0 !important;">
								  <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="{$result}" aria-valuemin="0" aria-valuemax="100" style="width: {$result}%; overflow: hidden;">
								  </div>
								  <font color="black">{$result|number_format:0}%</font>
								</div>
							{else}
								<div class="progress" style="background-image: linear-gradient(to bottom, #FFFACD 0px, #FFFACD 100%); background-repeat: repeat-x;">
								  <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;">
								    <!-- <font color="black">0%</font> -->
								  </div>
								</div>
							{/if}
							</a>
						</div>
					</div>
					{/foreach}
					{/if}
				</div>
				<p class="table-description blue">※ 진도율의 숫자부분을 누르면 상세한 진도현황을 알 수 있는 팝업화면이 뜹니다.</p>
				<!-- <div class="paging">
					<a class="paging__first" href="">&#9668;&#9668;</a>
					<a class="paging__prev" href="">&#9668;</a>
					<a class="paging__to blue" href="">1</a>
					<a class="paging__to" href="">2</a>
					<a class="paging__to" href="">3</a>
					<a class="paging__to" href="">4</a>
					<a class="paging__next" href="">&#9658;</a>
					<a class="paging__last" href="">&#9658;&#9658;</a>
				</div> -->
			</section>
		</section>
		<!-- end learning progress all -->

{include file="common/footer.tpl"}