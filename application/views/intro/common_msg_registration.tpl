<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="/assets/css/base.css">
		<link rel="stylesheet" href="/assets/css/module.css">
		<title>Registration Message</title>
	</head>
	<body>
		<!-- message -->
		<section class="message">
			<img class="login__logo" src="/assets/img/logo.png" alt="Logo">
			<form method="GET" action="{$base_url}">
				<h3 class="blue">{if isset({$common_msg_header})}{$common_msg_header}{/if}</h3>
				<br>
				<div class="message__box message__box--border">
					<div class="message__box__middle message__box__middle--expand">
						<p>{if isset({$common_msg_content})}{$common_msg_content}{/if}</p>
					</div>
				</div>
				<button class="message__confirm confirm" type="submit">홈으로</button>
			</form>
		</section>
		<!-- end message -->
	</body>
</html>