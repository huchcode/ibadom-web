<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="{$base_url}assets/css/base.css">
		<link rel="stylesheet" href="{$base_url}assets/css/intro.css">
		<link rel="stylesheet" href="{$base_url}assets/css/layout.css">
		<link rel="stylesheet" href="{$base_url}assets/css/form.css">
		<title>Forgot Password</title>
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
	</head>
	<body>

		<!-- forgot passsword -->
		<section id="content">
			<!-- back -->
			<a id="back" href="#" onclick="location.href='/login'">
				<img src="{$base_url}assets/img/back.png" alt="Back">
			</a>
			<!-- end back -->
			<section id="forgot-password" class="login-container">
				<img class="login__logo" src="{$base_url}assets/img/logo.png" alt="Logo">
				{if !empty($errormessage)}
				<!-- error message -->
				<p class="error-message red">{$errormessage}</p>
				<!-- end error message -->
				{/if}
				<br>
				<form id="forgot-password-form" method="POST" action="">
					<input id="forgot-passsword__username" type="text" name="username" placeholder="아이디" value="{if $username}{$username}{/if}">
					<br>
					<input id="forgot-passsword__email" type="text" name="email" placeholder="이메일" value="{if $email}{$email}{/if}">
					<br>
					<input id="forgot-passsword__name" type="text" name="name" placeholder="성명" value="{if $name}{$name}{/if}">
					<div class="intro__options">
						<p class="blue">마스터 계정은 회사명을 입력하세요</p>
					</div>
					<button id="forgot-passsword__submit" class="confirm" type="submit">비밀번호 리셋</button>
				</form>
			</section>
		</section>
		<!-- end forgot passsword -->
	</body>
</html>