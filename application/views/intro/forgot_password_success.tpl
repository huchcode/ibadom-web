<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="{$base_url}assets/css/base.css">
		<link rel="stylesheet" href="{$base_url}assets/css/intro.css">
		<link rel="stylesheet" href="{$base_url}assets/css/layout.css">
		<link rel="stylesheet" href="{$base_url}assets/css/form.css">
		<title>Forgot Password</title>
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
	</head>
	<body>

		<!-- forgot passsword -->
		<section id="content">
			<!-- back -->
			<a id="back" href="#" onclick="location.href='/login'">
				<img src="{$base_url}assets/img/back.png" alt="Back">
			</a>
			<!-- end back -->
			<section id="forgot-password" class="login-container">
				<img class="login__logo" src="{$base_url}assets/img/logo.png" alt="Logo">
				<p class="red">{$common_msg_header}</p>
				<p class="blue">{$common_msg_content}</p>
				<br>
			</section>
		</section>
		<!-- end forgot passsword -->
	</body>
</html>