<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="{$base_url}assets/css/base.css">
		<link rel="stylesheet" href="{$base_url}assets/css/layout.css">
		<link rel="stylesheet" href="{$base_url}assets/css/intro.css">
		<link rel="stylesheet" href="{$base_url}assets/css/module.css">
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery-1.11.2.min.js">
		</script>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery.validate.js">
		</script>
		<title>Registration Terms</title>
	</head>
	<body>
				<!-- registration terms -->
		<section id="content">
			<!-- back -->
			<a id="back" href="/" onclick="javascript: history.go(-1)">
				<img src="{$base_url}assets/img/back.png" alt="Back">
			</a>
			<!-- end back -->
			<section id="registration-terms" class="container">
				<div id="register-header">
					<img src="{$base_url}assets/img/register_header.png" alt="Registration">
					<h1 id="register-header__heading" class="blue">마스터회원 가입 신청</h1>
					<p>지팩는 회원님의 소중한 개인정보를 신중하게 취급합니다.<p>
					<p>입력하신 정보는 회원님의 동의 없이 공개되지 않으며, 개인정보 취급방침에 의해 보호 받습니다.</p>
				</div>
				<form id="register-terms-form" class="form-container" method="" action="/registration/registration_form">
					<div class="form__table__container">
						<div class="register__heading inline-middle form__table__heading">
							<img src="{$base_url}assets/img/terms_icon.png" alt="Terms of services">
							<p>이용약관</p>
						</div>
						<div class="register-terms__box">
							<p>
								본 약관(이하 ‘본 약관’이라 함)은 LINE주식회사(이하 ‘당사’라 함)가 제공하는 네이버 라인에 관한 모든 제품 및 서비스
								(이하 ‘본 서비스’라 함)의 이용에 관한 조건에 대해 본 서비스를 이용하는 고객(이하 ‘고객’이라 함)과 당사간에 정하는 것입니다.
							</p>
							<p>
								1. 정의
							</p>
							<p>
								본 약관에서는 다음 용어를 사용합니다.
								1.1. ‘콘텐츠’란 문장, 음성, 음악, 이미지, 동영상, 소프트웨어, 프로그램, 코드 기타 정보를 말합니다.
								1.2. ‘본 콘텐츠’란 본 서비스를 통해 접속할 수 있는 콘텐츠를 말합니다.
							</p>
							<p>
								본 약관(이하 ‘본 약관’이라 함)은 LINE주식회사(이하 ‘당사’라 함)가 제공하는 네이버 라인에 관한 모든 제품 및 서비스
								(이하 ‘본 서비스’라 함)의 이용에 관한 조건에 대해 본 서비스를 이용하는 고객(이하 ‘고객’이라 함)과 당사간에 정하는 것입니다.
							</p>
							<p>
								1. 정의
							</p>
							<p>
								본 약관에서는 다음 용어를 사용합니다.
								1.1. ‘콘텐츠’란 문장, 음성, 음악, 이미지, 동영상, 소프트웨어, 프로그램, 코드 기타 정보를 말합니다.
								1.2. ‘본 콘텐츠’란 본 서비스를 통해 접속할 수 있는 콘텐츠를 말합니다.
							</p>
						</div>
						<div class="register-terms__check inline-middle">
							<input type="checkbox" name="terms_check" required>
							<p class="blue">이용약관에 동의합니다.</p>
						</div>
					</div>
					<div class="form__table__container">
						<div class="register__heading form__table__heading inline-middle">
							<img src="{$base_url}assets/img/terms_icon.png" alt="Privacy policy">
							<p>개인정보 보호정책</p>
						</div>
						<div class="register-terms__box">
							<p>
								본 약관(이하 ‘본 약관’이라 함)은 LINE주식회사(이하 ‘당사’라 함)가 제공하는 네이버 라인에 관한 모든 제품 및 서비스
								(이하 ‘본 서비스’라 함)의 이용에 관한 조건에 대해 본 서비스를 이용하는 고객(이하 ‘고객’이라 함)과 당사간에 정하는 것입니다.
							</p>
							<p>
								1. 정의
							</p>
							<p>
								본 약관에서는 다음 용어를 사용합니다.
								1.1. ‘콘텐츠’란 문장, 음성, 음악, 이미지, 동영상, 소프트웨어, 프로그램, 코드 기타 정보를 말합니다.
								1.2. ‘본 콘텐츠’란 본 서비스를 통해 접속할 수 있는 콘텐츠를 말합니다.
							</p>
							<p>
								본 약관(이하 ‘본 약관’이라 함)은 LINE주식회사(이하 ‘당사’라 함)가 제공하는 네이버 라인에 관한 모든 제품 및 서비스
								(이하 ‘본 서비스’라 함)의 이용에 관한 조건에 대해 본 서비스를 이용하는 고객(이하 ‘고객’이라 함)과 당사간에 정하는 것입니다.
							</p>
							<p>
								1. 정의
							</p>
							<p>
								본 약관에서는 다음 용어를 사용합니다.
								1.1. ‘콘텐츠’란 문장, 음성, 음악, 이미지, 동영상, 소프트웨어, 프로그램, 코드 기타 정보를 말합니다.
								1.2. ‘본 콘텐츠’란 본 서비스를 통해 접속할 수 있는 콘텐츠를 말합니다.
							</p>
						</div>
						<div class="register-terms__check inline-middle">
							<input type="checkbox" name="terms_check" required>
							<p class="blue">개인정보 보호정책에 동의합니다.</p>
						</div>
					</div>
					<div class="register__confirm">
						<button class="confirm" type="submit">동의합니다</button>
					</div>
				</form>
			</section>
		</section>
		<!-- end registration terms -->
	</body>
</html>