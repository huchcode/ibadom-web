<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="/assets/css/base.css">
		<link rel="stylesheet" href="/assets/css/intro.css">
		<link rel="stylesheet" href="/assets/css/form.css">
		<link rel="stylesheet" href="/assets/css/login.css">
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
		<link href="/assets/css/custom/custom.css?v=1.0.2" rel="stylesheet">
		<link href="/assets/skins/all.css?v=1.0.2" rel="stylesheet">
		<script type="text/javascript" charset="utf8" src="/assets/js/jquery-1.11.2.min.js"></script>
		<script src="/assets/js/custom/icheck.js?v=1.0.2"></script>
		<script src="/assets/js/custom/custom.min.js?v=1.0.2"></script>
		<title>Login</title>
		<script>
			$(document).ready(function(){
			var callbacks_list = $('.demo-callbacks ul');
			$('.demo-list input').on('ifChecked ifUnchecked', function(event){
			  //alert('checked');
			}).iCheck({
			  checkboxClass: 'icheckbox_square-blue',
			  radioClass: 'iradio_square-blue',
			  increaseArea: '20%'
			});
			});
		</script>
	</head>
	<body>
		<!-- login -->
		<section id="content">
			<!-- back -->
			<a id="back" href="{$base_url}">
				<img src="{$base_url}assets/img/back.png" alt="Back">
			</a>
			<!-- end back -->
			<section id="login" class="login-container" style="position:absolute;left:50%;top:50%;margin-left:-175px;	margin-top:-350px;text-align: center;">				
				<img class="login__logo" src="{$base_url}assets/img/logo.png" alt="Logo">
				<p class="error-message red">{if isset($errormsg)}{$errormsg}{/if}</p>
				<br>
				<form id="login-form" method="POST" action="">
					<input id="login__username" type="text" name="username" placeholder="아이디" value="{if isset({$username})}{$username}{/if}" autocomplete="off">
					<br>
					<input id="login__password" type="password" name="password" placeholder="비밀번호" value="{if isset({$password})}{$password}{/if}" autocomplete="off">
					<div class="intro__options">
						{if empty($errormsg)}
						<div class="demo-list clear">
					      <ul>
					        <li>
					          <input tabindex="1" type="checkbox" id="remember_me" name="remember_me" {if {$remember_me}}checked{/if}>
					          <label for="remember_me">로그인 정보 저장</label>
					        </li>
					      </ul>
					    </div>
						<!-- <a id="login__save" class="login__link inline-middle" href="#">
							<img src="{$base_url}assets/img/intro_login_save.png" alt="Save login info">
							<p>로그인 정보 저장</p>
						</a> -->
						{else}
						<a id="login__forgot-password" class="login__link inline-middle" href="/forgot_password">
							<p class="blue">비밀번호를 잊으셨나요?</p>
						</a>
						{/if}
						<a id="login__register" class="login__link inline-middle" href="/registration">
							<img src="{$base_url}assets/img/intro_create_account.png" alt="Create account">
							<p class="blue">계정 만들기</p>
						</a>
					</div>
					<button id="login__submit" class="confirm" type="submit">Login</button>
				</form>
			</section>
		</section>
		<!-- end login -->
	</body>
</html>