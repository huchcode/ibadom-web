<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
		<link rel="stylesheet" href="{$base_url}assets/css/base.css">
		<link rel="stylesheet" href="{$base_url}assets/css/form.css">
		<link rel="stylesheet" href="{$base_url}assets/css/register.css">
		<link rel="stylesheet" href="{$base_url}assets/css/register_form.css">
		<link rel="stylesheet" href="{$base_url}assets/css/popup.css">
		<link rel="icon" type="image/ico" href="{$base_url}assets/img/favicon.ico"/>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery-1.11.3.min.js">
		</script>
		<script type="text/javascript" charset="utf8" src="{$base_url}assets/js/jquery.validate.js">
		</script>

		<title>Registration</title>

		<style type="text/css">
		.fileUpload {
			position: relative;
			overflow: hidden;
			margin: 10px;
		}
		.fileUpload input.upload {
			position: absolute;
			top: 0;
			right: 0;
			margin: 0;
			padding: 0;
			font-size: 20px;
			cursor: pointer;
			opacity: 0;
			filter: alpha(opacity=0);
		}
		.btn {
			display: inline-block;
			padding: 6px 12px;
			margin-bottom: 0;
			font-size: 14px;
			font-weight: 400;
			line-height: 1.42857143;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			-ms-touch-action: manipulation;
			touch-action: manipulation;
			cursor: pointer;
			-webkit-user-select: none;
			-moz-user-select: none;
			-ms-user-select: none;
			user-select: none;
			background-image: none;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		</style>	

		<script>
			$().ready(function() {
				// validate register-info-form on keyup and submit
				$("#register-info-form").validate({
					rules: {
						username: "required",
						password: {
							required: true
						},
						confirm_password: {
							required: true,
							equalTo: "#password"
						},
						name: "required",
						phone: "required",
						user_email: "required",
						host_email: "required",
						company_number: "required",
						company_name: "required",
						representative_name: "required",
						initials: "required",
						company_image_copy: "required"
						// email: {
						// 	required: true,
						// 	email: true
						// }
					},
					messages: {
						username: {
							required: "아이디를 입력하세요"
						},
						password: {
							required: "비밀번호를 입력하세요"
						},
						confirm_password: {
							required: "비밀번호 확인을 입력하세요",
							equalTo: "비밀번호와 비밀번호 확인이 다릅니다."
						},
						name: "이름을 입력하세요",
						phone: "휴대폰번호를 입력하세요",
						user_email: "이메일을 입력하세요",
						host_email: "이메일을 입력하십시오",
						company_number: "사업자등록번호를 입력하세요",
						company_name: "회사명을 입력하세요",
						representative_name: "대표자명을 입력하세요",
						initials: "이니셜을 입력하세요",
						company_image_copy: {
							required: "사업자 등록증 사본이 필요"
						}
					},
					errorElement : 'div',
    				errorLabelContainer: '.errorTxt'
				});

				$("#checkavailability").validate({
					rules: {
						check_username: "required"
					},
					messages: {
						check_username: {
							required: "아이디를 입력하세요"
						}
					},
					errorElement : 'div',
    				errorLabelContainer: '#username_required'
				});

			});
		</script>


		<script type="text/javascript">
		$(document).ready(function() {
			$('#register-info-form').on('click', '#check', function(){
					$('#register-popup').show();
					$('#register__username-check').val($('#username').val());
				});

			$('#register-info-form').on('click', '#checkinitials', function(){
					$('#initial-popup').show();
					$('#register__initial-check').val($('#initials').val());
				});

			$('#register-popup').on('click', '#btnuserreg', function(){
					$('#register-popup').hide();
					$('#username').val($('#register__username-check').val());
					$('#username_response').text('');
					$('#username_required').text('');
				});

			$('#initial-popup').on('click', '#btncheckinitial', function(){
					$('#initial-popup').hide();
					$('#initials').val($('#register__initial-check').val());
					$('#initial_response').text('');
					$('#initial_required').text('');
				});

			if('{if isset($popup)}{$popup}{/if}' == true)
			{
				$('#register-popup').show();
				$('#register__username-check').val('{if isset($smarty.post.check_username)}{$smarty.post.check_username}{/if}');
			}

			if('{if isset($popupinitial)}{$popupinitial}{/if}' == true)
			{
				$('#initial-popup').show();
				$('#register__initial-check').val('{if isset($smarty.post.check_initial)}{$smarty.post.check_initial}{/if}');
			}

			$('select#mails').on('change', function() {
		    	$('input#host_email').val($('select#mails').val());
		    });

		    $('input#profile_image').on('change', function() {
		    	$('label#profile_image').text($('input#profile_image').val());
		    });

		    $('input#company_image').on('change', function() {
		    	$('label#company_image').text($('input#company_image').val());
		    	$('input#company_image_copy').val($('input#company_image').val());
		    });

			$('form#checkavailability').on('submit', function(e) {
				$('#username_response').text('');
		        var username = $("input#register__username-check").val();
		        if(username.length > 0)
		        {
			        $.ajax({
			            url : '/registration/check_username',
			            type: "POST",
			            data: { check_username: username },
			            success: function (data) {
			                $("p#username_response").text(data);
			            },
			            error: function (jXHR, textStatus, errorThrown) {
			                alert(jXHR.responseText);
			            }
			        });
			        e.preventDefault();
		    	}
		    });

		    $('form#checkavailability_initial').on('submit', function(e) {
		    	$('#initial_response').text('');
		        var initial = $("input#register__initial-check").val();
		        if(initial.length > 0)
		        {
			        $.ajax({
			            url : '/registration/check_initial',
			            type: "POST",
			            data: { check_initial: initial },
			            success: function (data) {
			                $("p#initial_response").text(data);
			            },
			            error: function (jXHR, textStatus, errorThrown) {
			                alert(jXHR.responseText);
			            }
			        });
			        e.preventDefault();
		    	}
		    });
		});
		</script>
	</head>
	<body>
		<!-- registration terms -->
		<a id="back" href="/registration" onclick="javascript: history.go(-1)">
			<img src="{$base_url}assets/img/back.png">
		</a>
		<section id="registration-terms" class="container form-container">
			<div id="register-header">
				<img src="{$base_url}assets/img/register_header.png">
				<h1 id="register-header__heading" class="blue">마스터회원 가입 신청</h1>
				<p>입력하신 정보는 개인정보 보호정책에 따라 보호됩니다.</p>

				<div class="errorTxt red"></div>
				<!--Error Msg-->
				<p class="error-message red">{if isset($error_msg)}{$error_msg}{/if}</p>
				<!--Error Msg-->
				
			</div>
			<form id="register-info-form" method="POST" enctype="multipart/form-data" action="">
				<div id="register-info__basic" class="form__table__container">
					<div class="form__table__heading inline-middle">
						<img src="{$base_url}assets/img/register_info_icon.png">
						<p>기본정보</p>
						<span class="register__heading-sub form__table__list__heading__necessary blue">표시는 필수 항목</span>
					</div>
					<div class="register-info__table form__table">
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading__necessary">
								<p>아이디</p>
							</div>
							<div class="form__table__list__value">
								<input id="username" type="text" name="username" value="{if isset($smarty.post.username)}{$smarty.post.username}{/if}" required>
								<button id="check" type="button" class="activate activate--blue">중복확인</button>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading__necessary">
								<p>비밀번호</p>
							</div>
							<div class="form__table__list__value">
								<input type="password" id="password" name="password" value="{if isset($smarty.post.password)}{$smarty.post.password}{/if}" required>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading__necessary">
								<p>비밀번호 확인</p>
							</div>
							<div class="form__table__list__value">
								<input type="password" name="confirm_password" value="{if isset($smarty.post.confirm_password)}{$smarty.post.confirm_password}{/if}" required>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading__necessary">
								<p>이름</p>
							</div>
							<div class="form__table__list__value">
								<input type="text" name="name" value="{if isset($smarty.post.name)}{$smarty.post.name}{/if}" required>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading__necessary">
								<p>휴대폰 번호</p>
							</div>
							<div class="form__table__list__value">
								<input type="text" name="phone" value="{if isset($smarty.post.phone)}{$smarty.post.phone}{/if}" required>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading__necessary">
								<p>이메일</p>
							</div>
							<div class="form__table__list__value">
								<input id="user_email" type="text" name="user_email" value="{if isset($smarty.post.user_email)}{$smarty.post.user_email}{/if}" required><span> @ </span><input type="text" id="host_email" name="host_email" value="{if isset($smarty.post.host_email)} {$smarty.post.host_email} {/if}" required>
								<select id="mails" name="mails" value="{if isset($smarty.post.mails)}{$smarty.post.mails}{/if}">
									<option value="" selected>직접입력</option>
									<option value="gmail.com">지메일</option>
									<option value="hanmail.net">한메일</option>
									<option value="naver.com">네이버</option>
								</select>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>부서</p>
							</div>
							<div class="form__table__list__value">
								<input type="text" name="department" value="{if isset($smarty.post.department)}{$smarty.post.department}{/if}" maxlength="20">
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>직위</p>
							</div>
							<div class="form__table__list__value">
								<input type="text" name="position" value="{if isset($smarty.post.position)}{$smarty.post.position}{/if}" maxlength="20">
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading">
								<p>프로필사진</p>
							</div>
							<div class="form__table__list__value" style="color:#000000">
								<div class="fileUpload btn" style="background-color: #ADDCF0;">
								    <!-- <span>사진 올리기</span> -->
								    <label for="profile_image">사진 올리기</label>
								    <input type="file" id="profile_image" class="upload" accept="image/x-png, image/gif, image/jpeg" name="profile_image" value="{if isset($smarty.post.profile_image)}{$smarty.post.profile_image}{/if}"/>
								</div>
								<label id="profile_image"></label>
							</div>
						</div>
					</div>
				</div>
				<div id="register-info__company" class="form__table__container">
					<div class="form__table__heading inline-middle">
						<img src="{$base_url}assets/img/register_info_icon.png">
						<p>업체정보</p>
						<span class="register__heading-sub form__table__list__heading__necessary blue">표시는 필수 항목</span>
					</div>
					<div class="register-info__table form__table">
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend form__table__list__heading__necessary">
								<p>사업자등록번호</p>
							</div>
							<div class="form__table__list__value">
								<input type="text" name="company_number" value="{if isset($smarty.post.company_number)}{$smarty.post.company_number}{/if}" required>
								<span>하이픈(-) 없이 입력해주세요</span>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend form__table__list__heading__necessary">
								<p>회사명</p>
							</div>
							<div class="form__table__list__value">
								<input type="text" name="company_name" value="{if isset($smarty.post.company_name)}{$smarty.post.company_name}{/if}" required>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend form__table__list__heading__necessary">
								<p>대표자명</p>
							</div>
							<div class="form__table__list__value">
								<input type="text" name="representative_name" value="{if isset($smarty.post.representative_name)}{$smarty.post.representative_name}{/if}" required>
							</div>
						</div>
						<div class="form__table__list">
							<div class="form__table__list__heading form__table__list__heading--extend form__table__list__heading__necessary">
								<p>이니셜 (영문 3자)</p>
							</div>
							<div class="form__table__list__value">
								<input type="text" id="initials" name="initials" value="{if isset($smarty.post.initials)}{$smarty.post.initials}{/if}" maxlength="3" required>
								<button id="checkinitials" type="button" class="activate activate--blue">중복확인</button>
							</div>
						</div>
					</div>
					<p class="form__table__more">
						※이니셜은 회사 영문 이름에서 대문자로 3글자를 추출하여 사용할 것을 권장합니다. 아이디에 대해 부연설명하면<br>
						이메일 주소가 <span class="blue">hgd@gmail.com</span> 이고 회사 영문 이름에서 3글자를 추출한 이니셜이 <span class="blue">skh</span>라면 아이디는 <span class="blue">skhhgd</span>가 됩니다.<br>
						이니셜과 이메일 주소 앞단을 조합하면 고유한 아이디를 효율적이고 신속하게 만들 수 있습니다.
					</p>
				</div>
				<div id="register-info__company-image" class="form__table__container inline-middle">
					<p class="form__table__list__heading__necessary">사업자등록사본 업로드</p>
					<div class="fileUpload btn" style="background-color: #ADDCF0;">
					    <!-- <span></span> -->
					    <label for="company_image">사진 올리기</label>
					    <input type="file" id="company_image" class="upload" accept="image/x-png, image/gif, image/jpeg" name="company_image" required/>
					</div>
					<label id="company_image" required></label>					
				</div>
				<div class="register__confirm">
					<button class="confirm" type="submit">회원가입</button>
					<input type="text" id="company_image_copy" name="company_image_copy" style="color: #ffffff; background-color:transparent; border-color: #ffffff;" required/>
				</div>
			</form>
		</section>
		<!-- end registration terms -->
		<!-- registration popup -->
		<section id="register-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle">
					<img src="{$base_url}assets/img/register_magnifier_icon.png">
					<h3>사용하실 아이디를 입력하세요</h3>
				</div>
				<div class="popup__content">
					<form id="checkavailability" method="POST" action="">
						<div class="popup__content__section">
							<input id="register__username-check" type="text" name="check_username">
							<button id="check_availability" type="submit" class="activate activate--blue">중복확인</button>
						</div>
						<div id="msg">
							<p id="username_response" class="error-message red"></p>
							<p id="username_required" class="error-message red"></p>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<!-- <p>
								입력하신 아이디 skhhgd0234234는 사용할 수 있는 아이디입니다.</br>
								아이디 등록을 클릭하여 주세요.
							</p> -->
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="btnuserreg" type="button" class="activate activate--orange">아이디 등록</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end registration popup -->
		<!-- check initials popup -->
		<section id="initial-popup" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__heading inline-middle">
					<img src="{$base_url}assets/img/register_magnifier_icon.png">
					<h3>기업의 초기 입력</h3>
				</div>
				<div class="popup__content">
					<form id="checkavailability_initial" method="POST" action="">
						<div class="popup__content__section">
							<input id="register__initial-check" type="text" name="check_initial" style="display: inline-block; width: 235px;" maxlength="3" required>
							<button id="check_initial" type="submit" class="activate activate--blue">중복확인</button>
						</div>
						<div id="initial_msg">
							<p id="initial_response" class="error-message red"></p>
							<p id="initial_required" class="error-message red"></p>
						</div>
						<div class="popup__content__section popup__content__section--center">
							<!-- <p>
								이니셜은 HCC 가능한 입력 이니셜이다.<br/>
								이니셜 등록을 클릭하십시오.
							</p> -->
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="btncheckinitial" type="button" class="activate activate--orange">이니셜 등록</button>
						</div>
					</form>
				</div>
			</div>
		</section>
		<!-- end check initials popup -->
	</body>
</html>