{include file="common/header.tpl"}

		<!-- top section -->
		<section id="intro-top" class="container intro-container">
			<div class="inner-fix">
				<h1 class="blue">연결된 지식을 쉽게! 누구나 만들 수 있고! 누구나 배울 수 있게!</h1>
				<img id="intro__innovative-img" src="{$base_url}assets/img/intro_innovative.png" alt="Innovative Connected Learning">
				<a href="#">
					<img src="{$base_url}assets/img/intro_demo.png" alt="Demo version 무료로 사용해 보기">
				</a>
				<br>
				<div id="intro__sentence-wrap">
					<p class="intro__sentence grey inline-middle">
						또는 <a id="intro__register" href="#" class="blue" style="border-bottom: 1px solid #cdcdcd;">회원 사용하기</a>
					</p>
					<p class="intro__sentence second grey">
						<span id="intro__gpac-enterprise" class="blue">GPAC ENTERPRISE</span>로 할 수 있는 것!
					</p>
					<p class="intro__sentence third" style="width:590px">
						지팩 엔터프라이즈는 귀사의 교육에 들어가는 리소스와 비용을<br>
						현저하게 낮춰주며, 기존의 교육시스템과는 전혀 차별화된 저비용, 고효율,<br>
						탁월한 개인맞춤 학습시스템을 제공합니다.
					</p>
				</div>
			</div>
		</section>
		<!-- end top section -->
		<!-- middle section -->
		<section id="intro-middle" class="container intro-container">
			<div class="inner-fix">
				<img src="{$base_url}assets/img/intro_message.png" alt="Easy to create, fast to deliver, effective to manage">
				<br>
				<br>
				<a href="#">
					<img src="{$base_url}assets/img/intro_more.png" alt="GPAC Enterprise 자세히 알아보기">
				</a>
			</div>
		</section>
		<!-- end middle section -->
		<!-- bottom section -->
		<section id="intro-bottom" class="container intro-container">
			<div class="inner-fix">
				<img src="{$base_url}assets/img/intro_service_concept.png" alt="서비스 컨셉">
				<div id="intro__service-concept">
					<div class="intro__service-concept__descriptions first">
						<p>
							웹에서 회원가입을 하시고<br>
							제작가이드에 맞춰<br>
							콘텐츠를 생성하고 앱을<br>
							만듭니다.
						</p>
					</div>
					<div class="intro__service-concept__descriptions second" style="text-align: left;width:130px">
						<p>
							원하는 콘텐츠를 담은<br>
							마인드맵 교육용 앱을<br>
							출시합니다.
						</p>
					</div>
					<div class="intro__service-concept__descriptions third">
						<p>
							교육학습상황을 모니터링<br>
							하고 관리할 수 있습니다.
						</p>
					</div>
				</div>
			</div>
		</section>
		<!-- end bottom section -->

{include file="common/footer.tpl"}