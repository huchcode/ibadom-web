{include file="common/header.tpl"}

		<!-- content assign people -->
		<section id="content">
			<nav id="content__nav">
				home > 교육배정 > 개인별교육배정
			</nav>
			<section id="content-assign-people" class="content-container">
				<h1 id="content__heading" class="blue">개인별 교육배정</h1>
				{if {$smarty.session.role} != 'admin' and {$smarty.session.role} != 'author'}
					{include file="common/permission.tpl"}

					<script type="text/javascript">
					$(document).ready(function() {
						$('div#content-assign-people__options').empty();
						$('div.table-container').empty();
					});
					</script>
				{/if}
				<a id="content__sub-heading" class="grey inline-middle" href="/content_assign">
					<img src="{$base_url}assets/img/content_assign_content_icon.png" alt="Content assign by content">
					<span class="no-padding">컨텐츠별 교육배정</span>
				</a>
				<div id="content-assign-people__options" class="content-options clearfix">
					<div class="content-options--left">
						<form id="search_assignee" method="POST" action="/content_assign/people" class="inline-middle">
							<p class="grey">부서(성명)</p>
							<div class="search-bar">
								<input id="emp_name" type="text" name="emp_name" placeholder="부서(성명)을 입력하세요" autocomplete="off" value="{if isset($smarty.post.emp_name)}{$smarty.post.emp_name}{else}{if isset($emp_name)}{$emp_name}{/if}{/if}">
								<input id="id" type="hidden" name="id">
								<input id="usertype" type="hidden" name="usertype">
								<img src="{$base_url}assets/img/magnifier_grey_icon.png" alt="Search">
								<!-- search results go here -->
								<!-- search results -->
								<div id="emp_result" class="search__results" style="max-height:290px; overflow-y: scroll">
								</div>
								<!-- end search results -->
							</div>

						</form>
					</div>
					<div class="content-options--right">
						<button id="content-assign__add" class="activate activate--dark-blue activate--white activate--icon" {if ! {$enable_add}}disabled{/if}>
							<img src="{$base_url}assets/img/stack_white_icon.png" alt="Add">
							추가
						</button>
						<form id="content-assign__add-search" method="POST" action="/content_assign/people" class="inline-middle">
							<div class="search-bar">
								<input id="search" type="text" name="search" placeholder="검색어를 입력하세요" autocomplete="off">
								<input id="emp_book_id" type="hidden" name="emp_book_id">
								<input id="book_id" type="hidden" name="book_id">
								<input id="target_type" type="hidden" name="target_type">
								<input id="target_id" type="hidden" name="target_id">
								<input id="emp_book_name" type="hidden" name="emp_book_name">
								<img id="content-assign__add-search__exit" src="{$base_url}assets/img/search_exit.png" alt="Search">
								<!-- search results go here -->
								<!-- search results -->
								<div id="content_results" class="search__results" style="max-height:290px; overflow-y: scroll">
								</div>
								<!-- end search results -->
							</div>
						</form>
					</div>
				</div>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<div id="content-assign-people__num" class="table__cell">
							<p>No.</p>
						</div>
						<div id="content-assign-people__name" class="table__cell">
							<p>컨텐츠명</p>
						</div>
						<div id="content-assign-people__author" class="table__cell">
							<p>저자</p>
						</div>
						<div id="content-assign-people__method" class="table__cell">
							<p>배정방법</p>
						</div>
						<div id="content-assign-people__delete" class="table__cell">
							<p>삭제</p>
						</div>
					</div>
					{if {$assignees|@count} gt 0}
					{foreach from=$assignees item=items}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>{counter}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p id="{$items->learningassign_id}">{$items->book_name}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p>{$items->target_name}</p>							
						</div>
						<div class="table__cell table__cell--center"> 
							{if {$items->target_type} == 'all'}
								전직원
							{else} 	{if {$items->target_type} == 'department'}
								부서
									{else}
								개인
									{/if}
							{/if}
						</div>
						<div class="table__cell table__cell--center">
							{if {$target_type} == {$items->target_type}}
							<p id="delete" class="blue" data-id="{$items->learningassign_id}"><a href="javascript:void(0);">삭제</a></p>
							{/if}
						</div>
					</div>
					{/foreach}
					{else}
					<div class="table__row">
					<div class="table__cell table__cell--center"></div>
					<div class="table__cell table__cell--center">
						<center>
							No Assignee
							{if isset($smarty.post.search)}
								for <strong class="blue">{$smarty.post.search}</strong>
							{else}
								for 
								<strong class="blue">
								<script type="text/javascript">
									document.write($("input#emp_name").val());
								</script>
								</strong>
							{/if}
						</center>
					</div>
					<div class="table__cell table__cell--center"></div>
					<div class="table__cell table__cell--center"></div>
					</div>
					{/if}
				</div>
			</section>
		</section>
		<!-- end content assign people -->
		<!-- confirmation -->
		<section id="confirmation" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-evaluation-list__add">
					<img src="{$base_url}assets/img/popup_exit.png">
				</div>
				<form id="delete_assignee" method="POST" action="/content_assign/delete_assignees/">
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div id="confirm_message" class="popup__content__section--center">
							
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" id="emp_name" name="emp_name">
							<input id="target_type" type="hidden" name="target_type" value="{$target_type}">
							<button class="activate activate--dark-blue activate--white" type="submit">확인</button>
							<!-- <button type="button" id="cancel" class="activate activate--grey activate--white">아니</button> -->
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end confirmation -->
		<!-- confirmation add all -->
		<section id="confirmation_add_all" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-evaluation-list__add_all">
					<img src="{$base_url}/assets/img/popup_exit.png">
				</div>
				<!-- <form id="delete_assignee" method="POST" action=""> -->
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div ="confirmation_add_all" class="popup__content__section--center">
							전직원을 배정하시면 이미 배정된 부서(사람)는 삭제됩니다. 계속 하시겠습니까?
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="confirmation_add_all" class="activate activate--dark-blue activate--white" type="submit">확인</button>
							<!-- <button type="button" id="cancel" class="activate activate--grey activate--white">아니</button> -->
						</div>
					</div>
				<!-- </form> -->
			</div>
		</section>
		<!-- end confirmation add all -->

		<script type="text/javascript">
				$(document).ready(function() {

				$('body').click(function() {
					$("#emp_result").empty();
					$("#content_results").empty();
				});

				$(document).on('click', '#close-evaluation-list__add', function(){
					$('section#confirmation').hide();
				});

				$('form#content-assign__add-search').on("keyup keypress", function(e) {
					var code = e.keyCode || e.which; 
					if (code  == 13) {
						e.preventDefault();
						return false;
					}
				});

				$('form#search_assignee').on("keyup keypress", function(e) {
					var code = e.keyCode || e.which; 
					if (code  == 13) {
						e.preventDefault();
						return false;
					}
				});

				$(document).on('click', 'p#delete', function(){				
			    	var id = $(this).data("id");
			    	var text = $('p#'+id).text();
			    	$('#delete_assignee').attr('action', '/content_assign/people/'+id+'/'+{$userid});
			    	$('input#emp_name').val($("input#emp_name").val());
			    	$('div#confirm_message').text(text+' 을(를) 삭제하시겠습니까?');
					$('section#confirmation').show();
				    //alert(id);
				});

				$(document).on('click', '#emp_result_list', function(){ 
			    	var text = $(this).text().trim();
			    	var id = $(this).data("id");
			    	var type = $(this).data("type");
			    	$('input#id').val(id);
					$('input#emp_name').val(text);
					$('input#usertype').val(type);
					$('.search__results').hide();

					$("#search_assignee").submit();
				 });

				$(document).on('click', '#book_results', function(){
			    	var text = $(this).text().trim();
			    	var id = $(this).data("id");
			    	var type = $(this).data("type");
			    	$('input#emp_book_name').val($("input#emp_name").val());
			    	$('input#emp_book_id').val(id);
			    	$('input#target_id').val({$userid});
			    	$('input#target_type').val("{$target_type}");
					$('.search__results').hide();
					$("#content-assign__add-search").submit();
				 });

				$(document).on('click', 'input#emp_name', function(){
					if (!$('div#emp_result_list').is(':visible')) {
					var name = $("#emp_name").val();
			    	$.ajax({
					      url: "/content_assign/search_employee_people",
					      type: "POST",
					      data: { name: name },
					      dataType: "json",
					      success: function(response) {
					      	console.log(response);
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#emp_result").append('<div id="emp_result_list" class="search__results__item" data-id="' + response[i].id + '" data-type="' + response[i].usertype + '" data-status="' + response[i].target_status + '">' + response[i].name + '</div>')
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(error);
							  alert(xhr.responseText);
							}
					    })
			    	}
				 });

				$("input#emp_name").keyup(function() {
					$("#emp_result").empty();
					var name = $("#emp_name").val();
					if(name.length > 0)
					{
						$("#emp_result").empty();
					    $.ajax({
					      url: "/content_assign/search_employee_people",
					      type: "POST",
					      data: { name: name },
					      dataType: "json",
					      success: function(response) {
					      	$("#emp_result").empty();
					      	console.log(response);
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#emp_result").append('<div id="emp_result_list" class="search__results__item" data-id="' + response[i].id + '" data-type="' + response[i].usertype + '">' + response[i].name + '</div>')
								});
					      	}
					      },
					      error: function(xhr, status, error) {
					      	console.log(xhr);
							  alert(xhr.responseText);
							}
					    })
					}
				});

				$("input#search").keyup(function() {
					$("#content_results").empty();
					var keyword = $("#search").val();
					if(keyword.length > 0)
					{
						$("#content_results").empty();
					    $.ajax({
					      url: "/content_assign/search_book_people",
					      type: "POST",
					      data: { search: keyword, targetid: {$userid} },
					      dataType: "json",
					      success: function(response) {
					      	$("#content_results").empty();
					      	var total = response.length;
					      	if(total > 0)
					      	{
						      	$.each(response, function(i, item) {
								    $("#content_results").append('<div id="book_results" class="search__results__item" data-id="' + response[i].id + '">' + response[i].title + '</div>')
								});
					      	}
					      },
					      error: function(xhr, status, error) {
							  alert(xhr.responseText);
							}
					    })
					}
				});
			});
		</script>

{include file="common/footer.tpl"}