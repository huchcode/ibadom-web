{include file="common/header.tpl"}

		<script type="text/javascript">
		$(document).ready(function() {
			// $('.search__results__item').click(function(){
			// 	var text = $(this).text().trim();
			// 	$('input#search').val(text);
			// 	$('.search__results').hide();			     
			//  });

			$('body').click(function() {
				$("#emp_result").empty();
				$("#content_results").empty();
			});

			$('form#content-assign__add-search').on("keyup keypress", function(e) {
				var code = e.keyCode || e.which; 
				if (code  == 13) {
					e.preventDefault();
					return false;
				}
			});

			$('form#search_assignee').on("keyup keypress", function(e) {
				var code = e.keyCode || e.which; 
				if (code  == 13) {
					e.preventDefault();
					return false;
				}
			});

			$(document).on('click', '#emp_name', function(){
				if (!$('div#emp_result_list').is(':visible')) {
				var name = $("#emp_name").val();
		    	$.ajax({
				      url: "/content_assign/search_employee",
				      type: "POST",
				      data: { name: name, book_id: {$book_id} },
				      dataType: "json",
				      success: function(response) {
				      	console.log(response);
				      	var total = response.length;
				      	if(total > 0)
				      	{
					      	$.each(response, function(i, item) {
							    $("#emp_result").append('<div id="emp_result_list" class="search__results__item" data-bookid="' + {$book_id} + '" data-id="' + response[i].target_id + '" data-type="' + response[i].target_type + '" data-status="' + response[i].target_status + '">' + response[i].target_name + '</div>')
							});
				      	}
				      },
				      error: function(xhr, status, error) {
				      	console.log(xhr);
						  alert(xhr.responseText);
						}
				    })
		    	}
			 });

			$(document).on('click', '#book_result_list', function(){ 
		    	var text = $(this).text().trim();
		    	var id = $(this).data("id");
		    	$('input#book_id').val(id);
				$('input#search').val(text);
				//saveValue($("input#search"));
				$('.search__results').hide();
				$("#search_assignee").submit();
			 });

			$(document).on('click', '#emp_result_list', function(){ 
		    	var text = $(this).text().trim();
		    	var bookid = $(this).data("bookid");
		    	var id = $(this).data("id");
		    	var type = $(this).data("type");
		    	var status = $(this).data("status");
		    	$('input#emp_book_name').val($("input#search").val());
		    	$('input#emp_book_id').val(bookid);
		    	$('input#target_id').val(id);
		    	$('input#target_type').val(type);
				$('input#emp_name').val(text);
				//saveValue($("input#emp_name"));
				$('.search__results').hide();

				if($("div#target_type_all").text() == "all") {
					$("div#confirm_message_all").text("전직원이 배정되어 있습니다. 전직원을 삭제하시고 추가 하시겠습니까?");
					$('section#confirmation_add_all').show();
					return false;
				}

				if(type == 'all') {
					$("div#confirm_message_all").text("전직원을 배정하시면 이미 배정된 부서(사람)는 삭제됩니다. 계속 하시겠습니까?");
					$('section#confirmation_add_all').show();
					return false;
				}

				$("#content-assign__add-search").submit();
			 });

			$(document).on('click', 'button#confirmation_add_all', function(){				
		    	$("#content-assign__add-search").submit();
			});

			$(document).on('click', 'div#delete', function(){				
		    	var id = $(this).data("id");
		    	var text = $('p#'+id).text();
		    	$('#delete_assignee').attr('action', '/content_assign/index/'+id+'/'+{$book_id});
		    	$('input#book_name').val($("input#search").val());
		    	$('div#confirm_message').text(text+' 을(를) 삭제하시겠습니까?')
				$('section#confirmation').show();
			    //alert(id);
			});

			$(document).on('click', '#close-evaluation-list__add', function(){
				$('section#confirmation').hide();
			});

			$(document).on('click', '#close-evaluation-list__add_all', function(){
				$('section#confirmation_add_all').hide();
			});

			$("input#search").keyup(function() {
				$("#content_results").empty();
				var keyword = $("#search").val();
				if(keyword.length > 0)
				{
				    $.ajax({
				      url: "/content_assign/search_book",
				      type: "POST",
				      data: { search: keyword },
				      dataType: "json",
				      success: function(response) {
				      	$("#content_results").empty();
				      	var total = response.length;
				      	if(total > 0)
				      	{
					      	$.each(response, function(i, item) {
							    $("#content_results").append('<div id="book_result_list" class="search__results__item" data-id="' + response[i].id + '">' + response[i].title + '</div>');

							// $('<div id="book_result_list" class="search__results__item" data-id="' + response[i].id + '">' + response[i].title + '</div>').hide().appendTo("#content_results").slideDown("fast");
							});
				      	}
				      },
				      error: function(xhr, status, error) {
						  alert(xhr.responseText);
						}
				    })
				}
			});

			$("input#emp_name").keyup(function() {
				$("#emp_result").empty();
				var name = $("#emp_name").val();
				if(name.length > 0)
				{
				    $.ajax({
				      url: "/content_assign/search_employee",
				      type: "POST",
				      data: { name: name, book_id: {$book_id} },
				      dataType: "json",
				      success: function(response) {
				      	$("#emp_result").empty();
				      	console.log(response);
				      	var total = response.length;
				      	if(total > 0)
				      	{
					      	$.each(response, function(i, item) {
							    $("#emp_result").append('<div id="emp_result_list" class="search__results__item" data-bookid="' + {$book_id} + '" data-id="' + response[i].target_id + '" data-type="' + response[i].target_type + '" data-status="' + response[i].target_status + '">' + response[i].target_name + '</div>')
							});
				      	}
				      },
				      error: function(xhr, status, error) {
				      	console.log(xhr);
						  alert(xhr.responseText);
						}
				    })
				}
			});

		});
		</script>

		<div style="display:none">{counter start=0 skip=1}</div>

		<!-- content assign content -->
		<section id="content">
			<nav id="content__nav">
				home > 교육배정 > 컨텐츠별 교육배정
			</nav>
			<section id="content-assign-content" class="content-container">
				<h1 id="content__heading" class="blue">컨텐츠별 교육배정</h1>
				{if {$smarty.session.role} != 'admin' and {$smarty.session.role} != 'author'}
					{include file="common/permission.tpl"}

					<script type="text/javascript">
					$(document).ready(function() {
						$('div#content-assign-content__options').empty();
						$('div.table-container').empty();
					});
					</script>
				{/if}
				<a id="content__sub-heading" class="grey inline-middle" href="/content_assign/people">
					<img src="{$base_url}assets/img/content_assign_people_icon.png" alt="Content assign by people">
					<span class="no-padding">개인별 교육배정</span>
				</a>
				<div id="content-assign-content__options" class="content-options clearfix">
					<div class="content-options--left">
						<form id="search_assignee" method="POST" action="/content_assign" class="inline-middle">
							<p class="grey">컨텐츠명</p>
							<div class="search-bar">
								<input id="search" type="text" name="search" placeholder="컨텐츠명을 입력하세요" autocomplete="off" value="{if isset($smarty.post.search)}{$smarty.post.search}{else}{if isset($book_name)}{$book_name}{/if}{/if}">
								<input id="book_id" type="hidden" name="book_id">
								<img src="{$base_url}assets/img/magnifier_grey_icon.png" alt="Search">
								<!-- search results -->
								<div id="content_results" class="search__results" style="max-height:290px; overflow-y: scroll">
								</div>
								<!-- end search results -->
							</div>
						</form>
					</div>
					<div class="content-options--right">
						<button id="content-assign__add" class="activate activate--dark-blue activate--white activate--icon" {if ! {$enable_add}}disabled{/if}>
							<img src="{$base_url}assets/img/stack_white_icon.png" alt="Add">
							추가
						</button>
						<form id="content-assign__add-search" method="POST" action="/content_assign" class="inline-middle">
							<div class="search-bar">
								<input id="emp_name" type="text" name="emp_name" placeholder="부서(성명)을 입력하세요" autocomplete="off" value="{if isset($smarty.post.emp_name)}{$smarty.post.emp_name}{/if}">
								<input id="emp_book_name" type="hidden" name="emp_book_name">
								<input id="emp_book_id" type="hidden" name="emp_book_id">
								<input id="target_type" type="hidden" name="target_type">
								<input id="target_id" type="hidden" name="target_id">
								<input id="target_status" type="hidden" name="target_status">
								<img id="content-assign__add-search__exit" src="{$base_url}assets/img/search_exit.png" alt="Search">
								<!-- search results go here -->
								<div id="emp_result" class="search__results" style="max-height:290px; overflow-y: scroll">									
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="table-container">
					<div class="table__row table__row--heading">
						<div id="content-assign-content__num" class="table__cell">
							<p>No.</p>
						</div>
						<div id="content-assign-content__name" class="table__cell">
							<p>부서명 / 이름</p>
						</div>
						<div id="content-assign-content__delete" class="table__cell">
							<p>삭제</p>
						</div>
					</div>
					{if {$assignees|@count} gt 0}
					{foreach from=$assignees item=items}
					<div class="table__row">
						<div class="table__cell table__cell--center">
							<p>{counter}</p>
						</div>
						<div class="table__cell table__cell--center">
							<p id="{$items->learningassign_id}">{$items->target_name}</p>
							<div id="target_type_all" style="display:none">{$items->target_type}</div>
						</div>
						<div id="delete" class="table__cell table__cell--center" data-id="{$items->learningassign_id}">
							<p class="blue" ><a href="javascript:void(0);">삭제</a></p>
						</div>
					</div>
					{/foreach}
					{else}
					<div class="table__row">
					<div class="table__cell table__cell--center"></div>
					<div class="table__cell table__cell--center">
						<center>
							No Assignee
							{if isset($smarty.post.search)}
								for <strong class="blue">{$smarty.post.search}</strong>
							{else}
								for 
								<strong class="blue">
								<script type="text/javascript">
									document.write($("input#search").val());
								</script>
								</strong>
							{/if}
						</center>
					</div>
					<div class="table__cell table__cell--center"></div>
					</div>
					{/if}
				</div>
				{if !empty({$assignees})}
				<!-- <div class="paging">
					<a class="paging__first" href="">&#9668;&#9668;</a>
					<a class="paging__prev" href="">&#9668;</a>
					<a class="paging__to blue" href="">1</a>
					<a class="paging__to" href="">2</a>
					<a class="paging__to" href="">3</a>
					<a class="paging__to" href="">4</a>
					<a class="paging__next" href="">&#9658;</a>
					<a class="paging__last" href="">&#9658;&#9658;</a>
				</div> -->
				{/if}
			</section>
		</section>
		<!-- end content assign content -->
		<!-- confirmation -->
		<section id="confirmation" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-evaluation-list__add">
					<img src="{$base_url}/assets/img/popup_exit.png">
				</div>
				<form id="delete_assignee" method="POST" action="/content_assign/delete_assignees/">
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div id="confirm_message" class="popup__content__section--center">
							
						</div>
						<div class="popup__content__section popup__content__section--center">
							<input type="hidden" id="book_name" name="book_name">
							<button class="activate activate--dark-blue activate--white" type="submit">확인</button>
							<!-- <button type="button" id="cancel" class="activate activate--grey activate--white">아니</button> -->
						</div>
					</div>
				</form>
			</div>
		</section>
		<!-- end confirmation -->
		<!-- confirmation add all -->
		<section id="confirmation_add_all" class="popup" style="display:none">
			<div class="popup__bg"></div>
			<div class="popup__container">
				<div class="popup__exit" id="close-evaluation-list__add_all">
					<img src="{$base_url}/assets/img/popup_exit.png">
				</div>
				<!-- <form id="delete_assignee" method="POST" action=""> -->
					<div class="popup__heading inline-middle">
						<h3>확인</h3>
					</div>
					<div class="popup__content">
						<div id="confirm_message_all" class="popup__content__section--center">
							
						</div>
						<div class="popup__content__section popup__content__section--center">
							<button id="confirmation_add_all" class="activate activate--dark-blue activate--white" type="submit">확인</button>
							<!-- <button type="button" id="cancel" class="activate activate--grey activate--white">아니</button> -->
						</div>
					</div>
				<!-- </form> -->
			</div>
		</section>
		<!-- end confirmation add all -->

		<script type="text/javascript">
		// 	function setCookie(c_name, value, exdays) {
		// 	    var exdate = new Date();
		// 	    exdate.setDate(exdate.getDate() + exdays);
		// 	    var c_value = escape(value) + ((exdays == null) ? "" : "; expires=" + exdate.toUTCString());
		// 	    document.cookie = c_name + "=" + c_value;
		// 	}

		// 	function getCookie(c_name) {
		// 	    var c_value = document.cookie;
		// 	    var c_start = c_value.indexOf(" " + c_name + "=");
		// 	    if (c_start == -1) {
		// 	        c_start = c_value.indexOf(c_name + "=");
		// 	    }
		// 	    if (c_start == -1) {
		// 	        c_value = null;
		// 	    } else {
		// 	        c_start = c_value.indexOf("=", c_start) + 1;
		// 	        var c_end = c_value.indexOf(";", c_start);
		// 	        if (c_end == -1) {
		// 	            c_end = c_value.length;
		// 	        }
		// 	        c_value = unescape(c_value.substring(c_start, c_end));
		// 	    }
		// 	    return c_value;
		// 	}
		// 	function saveValue(input) {
		// 	    var name = input.attr('name');
		// 	    var value = input.val();
		// 	    setCookie(name,value);
		// 	}

		// 	function getValue(input) {
		// 	    var name = input.attr('name');
		// 	    //alert(name);
		// 	    var value = getCookie(name);
		// 	    if(value != null && value != "" ) {
		// 	        return value;
		// 	    }
		// 	}
		// 	$(document).ready(function() {
		// 		$('input[type=text]').each(function(){
		// 		    var value = getValue($(this));
		// 		    $(this).val(value);
		// 		}).on('change', function(){
		// 		    if($(this).val() != '' ) {
		// 		        //saveValue($(this));
		// 		    }
		// 		});
		// 	});
		// </script>

{include file="common/footer.tpl"}

