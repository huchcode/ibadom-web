<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
        $this->load->helper(array('form', 'url'));
    }

    function register($username, $password, $company_name, $profile_image, $admin_name, $admin_phone_number, $admin_email, $admin_dept, $admin_position, $company_number, $representative_name, $initials, $company_image)
    {
    	$query_user = $this->db->get_where('user', array('username' => $username));
    	if($query_user->num_rows() > 0) {
            $data['error'] = '사용할 수 없는 아이디 입니다.';
    		return $data;
    	}

    	$query_companyinfo = $this->db->get_where('companyInformation', array('company_initial' => $initials));
    	if($query_companyinfo->num_rows() > 0) {
    		$data['error'] = '사용할 수 없는 이니셜 입니다.';
            return $data;
    	}

        $user = array(
		   'username' => $username,
		   'password' => md5($password),
		   'name' => $company_name,
		   'profile_picture' => '',
		   'status' => 'temporary',
		   'role' => 'master',
		   'type' => 'master',
           'master_user_id' => 0
		);

		// $this->db->trans_start();
        $this->db->trans_begin();
        $query_user = $this->db->insert('user', $user);
        $insert_id = $this->db->insert_id();
        $data = array(
               'master_user_id' => $insert_id,
               'modified' => date("Y-m-d H:i:s")
               );

        $this->db->where('id', $insert_id);
        $this->db->update('user', $data);

        $companyinfo = array(
        	'user_id' => $insert_id,
			'admin_name' => $admin_name,
			'admin_phone_number' => $admin_phone_number,
			'admin_email' => $admin_email,
			'admin_dept' => $admin_dept,
			'admin_position' => $admin_position,
			'business_registration_number' => $company_number,
			'owner_name' => $representative_name,
			'company_initial' => strtoupper($initials),
			'location_business_registration' => ''
            );

        $this->db->insert('companyInformation', $companyinfo);

        $ip_address = $this->input->ip_address();

        $userstatushistory = array(
        	'user_id' => $insert_id,
        	'comment' => 'registration',
        	'ip_address' => $ip_address
            );

        $this->db->insert('userStatusHistory', $userstatushistory);

        $data = null;
        $reset = true;

        if(!empty($profile_image)) {
            $data = $this->upload($insert_id, '/assets/uploads/profilePics/', 'profile_image', 'profile', 'gif|jpg|png', '5120');
            $profile_picture_url = '/assets/uploads/profilePics/'.$insert_id.'/'.$data['upload_data']['file_name'];
            $this->update_user_per_field('profile_picture',$profile_picture_url,$insert_id,'user');
        } else {
            $reset = false;
        }

        if(isset($data['error'])) {
            $this->db->trans_rollback();
        } else {
            $data = $this->upload($insert_id, '/assets/uploads/businessRegistration/', 'company_image', 'businessReg', 'gif|jpg|png', '5120', $reset);
            if(isset($data['error'])) {
                $this->db->trans_rollback();
            } else {
                $company_picture_url = '/assets/uploads/businessRegistration/'.$insert_id.'/'.$data['upload_data']['file_name'];
                $this->update_user_per_field('location_business_registration',$company_picture_url,$insert_id,'companyInformation');

                $this->db->trans_commit();
            }
        }

   		return $data;
    }

    function login($username, $password)
    {
        $password = md5($password);
        $query_user = $this->db->get_where('user', array('username' => $username, 'password' => $password));
        if($query_user->num_rows() <= 0) {
            return '로그인 정보가 일치하지 않습니다.';
        }

        foreach ($query_user->result() as $row) {
            $this->db->select('company_initial');
            $this->db->from('companyInformation');
            // $this->db->where('user_id',$row->master_user_id);
            // $initial = $this->db->get()->row()->company_initial;

            $newdata = array(
                'user_id' => $row->id,
                'username' => $row->username,
                'name' => $row->name,
                'type' => $row->type,
                'role' => $row->role,
                'profile_picture' => $row->profile_picture,
                'logged_in' => TRUE,
                // 'master_user_id' => $row->master_user_id,
                // 'company_initial' => $initial,
                'content_id' => 0
                );

            $this->session->set_userdata($newdata);
        }
        return $query_user->result();
    }

    function validate_user_password($user_id, $password)
    {
        $password = md5($password);
        $query_user = $this->db->get_where('user', array('id' => $user_id, 'password' => $password));
        if($query_user->num_rows() <= 0) {
            return '로그인 정보가 일치하지 않습니다.';
        }

        foreach ($query_user->result() as $row) {

            $newdata = array(
                'user_id' => $row->id,
                'username' => $row->username,
                'name' => $row->name,
                'type' => $row->type,
                'role' => $row->role,
                'profile_picture' => $row->profile_picture,
                'logged_in' => TRUE,
                'master_user_id' => $row->master_user_id,
                'content_id' => 0
                );

            $this->session->set_userdata($newdata);
        }
        return $query_user->result();
    }

    function check_availability($username)
    {
    	$query_user = $this->db->get_where('user', array('username' => $username));
    	if($query_user->num_rows() > 0) {
    		return '사용할 수 없는 계정 입니다.';
    	} else {
    		return $username.' 사용할 수 있습니다';
    	}
    }

    function check_initial($initial)
    {
        $query_initial = $this->db->get_where('companyInformation', array('company_initial' => $initial));
        if($query_initial->num_rows() > 0) {
            return '회사 초기 사용할 수 없다.';
        } else {
            return $initial.' 사용할 수 있습니다';
        }
    }

    function get_user_info($user_id)
    {
        $this->db->select('id,username,password,name,status,role,type,master_user_id,profile_picture');
        $this->db->from('user');
        $this->db->where('id',$user_id);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    function get_user_info_using_username($username)
    {
        $this->db->select('id,username,name,status,role,type,master_user_id,profile_picture');
        $this->db->from('user');
        $this->db->where('username',$username);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    function get_user_employee_via_username($username)
    {
        $this->db->select('employee.user_id,user.username,user.name as name_from_user,employee.name as name_from_employee,employee.email');
        $this->db->from('user');
        $this->db->join('employee', 'employee.user_id = user.id', 'left');
        $this->db->where('username',$username);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    function get_user_detail($user_id,$type)
    {

        if ($type == 'master') {
            $this->db->select('*');
            $this->db->from('companyInformation');
        } else {
            $this->db->select('employee.*,department.name as department_name');
            $this->db->from('employee');
            $this->db->join('department', 'department.id = employee.department_id', 'left');
        }
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    function get_random_password($length = 8) {

        $base = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $temp_pw = "";
        for($i=0;$i<$length;$i++) {
            $temp_pw .= $base[rand(0,61)];
        }

        return $temp_pw;
    }

    function reset_user_password($user_id,$new_password) {

        $this->db->where('id', $user_id);
        $this->db->update('user', array('password' => md5($new_password),'modified' => date("Y-m-d H:i:s")));

        return $new_password;
    }

    function update_user_photo($user_id,$photo_url) {

        $this->db->where('id', $user_id);
        $this->db->update('user', array('profile_picture' => $photo_url,'modified' => date("Y-m-d H:i:s")));

        return $photo_url;
    }

    function update_user_per_field($field,$value,$user_id,$table) {


        $this->db->trans_start();

        if ($field=='name') {

            $this->db->where('id', $user_id);
            $this->db->update('user', array('name' => $value,'modified' => date("Y-m-d H:i:s")));

            $this->db->where('user_id', $user_id);
            $this->db->update('employee', array('name' => $value,'modified' => date("Y-m-d H:i:s")));

        } else {

            if ($table=='user') {
                $this->db->where('id', $user_id);
            } else {
                $this->db->where('user_id', $user_id);
            }

            $this->db->update($table, array($field => $value, 'modified' => date("Y-m-d H:i:s")));
        }

        $this->db->trans_complete();

        return $this->db->last_query();
    }

    function upload_profile_pic($file,$user_id) {

        $data = $this->upload($user_id, '/assets/uploads/profilePics/','profile_image', 'profile', 'gif|jpg|png', '5120');

        if(isset($data['error'])) {
            return 'Error';
        } else {
            $picture_url = '/assets/uploads/profilePics/'.$user_id.'/'.$data['upload_data']['file_name'];
            $this->update_user_per_field('profile_picture',$picture_url,$user_id,'user');
            return $picture_url;
        }
    }

    function upload_companycert_pic($file,$user_id) {

        $data = $this->upload($user_id, '/assets/uploads/businessRegistration/','company_image', 'businessReg', 'gif|jpg|png', '5120');

        if(isset($data['error'])) {
            return 'Error';
        } else {

            $picture_url = '/assets/uploads/businessRegistration/'.$user_id.'/'.$data['upload_data']['file_name'];
            $this->update_user_per_field('location_business_registration',$picture_url,$user_id,'companyInformation');
            return $picture_url;
        }
    }

    function update_user_status($user_id,$status,$reason) {

        $this->db->trans_start();

        $this->db->where('id', $user_id);
        $this->db->update('user', array('status' => $status,'modified' => date("Y-m-d H:i:s")));

        $this->db->insert('userStatusHistory', array('user_id'=>$user_id,'status' => $status,'comment' => $reason, 'ip_address' => $this->input->ip_address()));

        $this->db->trans_complete();

        return true;
    }

    function search_user_name($master_user_id, $name)
    {
        $this->db->select('user_id, name');
        $this->db->from('employee');
        $this->db->where('master_user_id', $master_user_id);
        $this->db->where('status !=','inactive');
        $this->db->like('name', $name);
        $query = $this->db->get();

        $data = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $key) {
                $data[] = $key;
            }
        }
        return print_r(json_encode($data));

    }

    function upload_exam_attach_image($file,$exam_id) {

        $data = $this->upload($exam_id, '/assets/uploads/examination/','attached_image', time(), 'gif|jpg|png', '5120');

        if(isset($data['error'])) {
            return 'Error: function User_model->upload_exam_attach_image';
        } else {
            $picture_url = '/assets/uploads/examination/'.$exam_id.'/'.$data['upload_data']['file_name'];
            return $picture_url;
        }
    }

    function upload($id, $path, $field, $filename, $allowed_types = 'gif|jpg|png', $max_size, $reset = false)
    {
        if (!is_dir(FCPATH.$path.$id)) {
            mkdir(FCPATH.$path.$id, 0777, true);
        }

        if(!$reset) {
            $config['upload_path'] = FCPATH.$path.$id;
            $config['allowed_types'] = $allowed_types;
            $config['max_size'] = $max_size;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
        } else {
            $config['upload_path'] = FCPATH.$path.$id;
            $config['allowed_types'] = $allowed_types;
            $config['max_size'] = $max_size;
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);
        }

        if ( ! $this->upload->do_upload($field, $filename)) {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }

    function get_employee_list($master_user_id,$search_name = null,$search_dept_id = null) {

        $this->db->select('employee.user_id, user.name, employee.email, employee.employee_number, department.name as dept_name, user.role, department.dept_code');
        $this->db->from('user');
        $this->db->join('employee', 'employee.user_id = user.id', 'inner');
        $this->db->join('department', 'department.id = employee.department_id and department.status = "active" ', 'left');
        $this->db->where('user.master_user_id',$master_user_id);
        $this->db->where('user.role !=','master');
        $this->db->where('user.status !=','closed');

        if (!empty($search_name)) {
            $this->db->like('user.name',$search_name);
        }

        if (!empty($search_dept_id)) {
            $this->db->where('employee.department_id',$search_dept_id);
        }

        $query = $this->db->get();

        $array = array();
        if($query->num_rows() > 0) {

            foreach ($query->result() as $row)
            {
               $array[] = $row;
            }
            return $array;

        } else {
            return null;
        }
    }

    function get_department_list($master_user_id) {

        $this->db->select('*');
        $this->db->from('department');
        $this->db->where('master_user_id',$master_user_id);
        $this->db->where('status','active');
        $query = $this->db->get();

        $array = array();
        if($query->num_rows() > 0) {

            foreach ($query->result() as $row)
            {
               $array[] = $row;
            }
            return $array;

        } else {
            return null;
        }

    }

    function get_department($dept_id) {
        $this->db->select('id as dept_id,name as dept_name,dept_code');
        $this->db->from('department');
        $this->db->where('id',$dept_id);
        $this->db->where('status','active');
        $query = $this->db->get();

        return $query->row();

    }

    function add_department($master_user_id, $post_value){

        if (strlen(trim($post_value['dept_name'])) == 0 || strlen(trim($post_value['dept_code'])) == 0) {
            return false;

        } else {

            $dept = array(
                'master_user_id' => $master_user_id,
                'name' => $post_value['dept_name'],
                'dept_code' => $post_value['dept_code'],
                );

            return $this->db->insert('department', $dept);
        }

    }

    function update_department($post_value) {
        $data = array(
            'name' => $post_value['dept_name'],
            'dept_code' => $post_value['dept_code'],
            'modified' => date("Y-m-d H:i:s")
            );

        return $this->db->update('department', $data, array('id' => $post_value['dept_id']));

    }

    function delete_department($dept_id) {

        $this->db->update('department', array('status' => 'inactive','modified' => date("Y-m-d H:i:s")), array('id' => $dept_id));
        //$this->db->delete('department', array('id' => $dept_id ));

    }

    function add_department_batch($master_user_id, $post_values){

        $dept = array();
        foreach ($post_values['dept_name'] as $key => $dept_name) {
            if (strlen(trim($dept_name)) == 0 || strlen(trim($post_values['dept_code'][$key])) == 0) continue;

            $dept[] = array(
                'master_user_id' => $master_user_id,
                'name' => $dept_name,
                'dept_code' => $post_values['dept_code'][$key],
                );
        }

        return $this->db->insert_batch('department', $dept);

    }

    function check_duplicate_user($field,$value,$master_user_id=null) {

        if ($field == 'email') {

            $emailname = $value;

            $this->db->select('company_initial');
            $this->db->from('companyInformation');
            $this->db->where('user_id',$master_user_id);
            $query = $this->db->get();

            if($query->num_rows() > 0) {
                $ci = $query->row()->company_initial;
            }

            $field = 'username';
            $value = $ci.$emailname;
        }

        $this->db->select('*');
        $this->db->from('user');
        $this->db->where_in($field,$value);
        $query = $this->db->get();

        $array = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row)
            {
               $array[] = $row;
            }
        }

        return $array;
    }

    function add_employee($master_user_id, $post_value){
        $this->db->db_debug = false;
        $password = $this->get_random_password();
        $company_info = $this->get_user_detail($master_user_id,'master');
        $email_exploded = explode("@", $post_value['user_email']);

        $this->db->trans_begin();

        $uservars = array(
            'username' => $company_info->company_initial.$email_exploded[0],
            'name' => $post_value['user_name'],
            'password' => md5('gpace7'),
//            'password' => md5($password),
            'role' => $post_value['user_role'],
            'master_user_id' => $master_user_id
            );

        $this->db->insert('user', $uservars);
        $insert_user_id = $this->db->insert_id();

        if(empty($post_value['department_id'])) {
            $department_id = null;
        } else {
            $department_id = $post_value['department_id'];
        }


        $employeevars = array(
            'user_id' => $insert_user_id,
            'name' => $post_value['user_name'],
            'email' => $post_value['user_email'],
            'employee_number' => $post_value['user_employee_number'],
            'department_id' => $department_id,
            'master_user_id' => $master_user_id
            );
        $this->db->insert('employee', $employeevars);

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }

    }

    function add_employee_batch($master_user_id, $post_values){
        $this->db->db_debug = false;
        $this->db->trans_begin();

        foreach ($post_values['user_name'] as $key => $user_name) {

            $user_role = $post_values['user_role'][$key];
            if ($user_role!='admin' && $user_role!='author') {
                $user_role = 'trainee';
            }

            $user_email = $post_values['user_email'][$key];
            $user_employee_number = $post_values['user_employee_number'][$key];


            $this->db->select('id as dept_id,name as dept_name,dept_code');
            $this->db->from('department');
            $this->db->where('dept_code',$post_values['department_code'][$key]);
            $this->db->where('status','active');
            $query = $this->db->get();

            if($query->num_rows() > 0) {
                $department_id = $query->row()->dept_id;
            } else {
                $department_id = null;
            }


            $password = $this->get_random_password();
            $company_info = $this->get_user_detail($master_user_id,'master');
            $email_exploded = explode("@", $user_email);

            $uservars = array(
                'username' => $company_info->company_initial.$email_exploded[0],
                'name' => $user_name,
                'password' => md5($password),
                'role' => $user_role,
                'master_user_id' => $master_user_id
                );

            $this->db->insert('user', $uservars);
            $insert_user_id = $this->db->insert_id();

            $employeevars = array(
                'user_id' => $insert_user_id,
                'name' => $user_name,
                'email' => $user_email,
                'employee_number' => $user_employee_number,
                'department_id' => $department_id,
                'master_user_id' => $master_user_id
                );
            $this->db->insert('employee', $employeevars);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    function delete_employee($user_id) {

        $this->db->trans_start();

        $this->db->update('user', array('status' => 'closed','modified' => date("Y-m-d H:i:s")), array('id' => $user_id));
        $this->db->update('employee', array('status' => 'inactive','modified' => date("Y-m-d H:i:s")), array('user_id' => $user_id));

        //$this->db->delete('user', array('id' => $user_id ));
        //$this->db->delete('employee', array('user_id' => $user_id ));

        $this->db->trans_complete();

        return true;

    }

    function update_employee($post_value) {

        $this->db->trans_start();

        if (empty($post_value['department_id'])) {
            $dept_id = null;
        } else {
            $dept_id = $post_value['department_id'];
        }

        $data = array(
            'name' => $post_value['user_name'],
            'email' => $post_value['user_email'],
            'employee_number' => $post_value['user_employee_number'],
            'department_id' => $dept_id,
            'modified' => date("Y-m-d H:i:s")
            );

        $this->db->update('employee', $data, array('user_id' => $post_value['user_id']));

        $data = array(
            'name' => $post_value['user_name'],
            'role' => $post_value['user_role'],
            'modified' => date("Y-m-d H:i:s")
            );

        $this->db->update('user', $data, array('id' => $post_value['user_id']));

        $this->db->trans_complete();
    }

    function get_pending_users() {

        $this->db->select('employee.user_id, employee.name, employee.email, employee.employee_number, user.username');
        $this->db->from('employee');
        $this->db->join('user', 'user.id = employee.user_id', 'inner');

        $this->db->where('employee.sent_email');
        $this->db->where('employee.status','active');
        $query = $this->db->get();

        $array = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row)
            {
               $array[] = $row;
            }
        }

        return $array;

    }

    function update_employee_email_sent($user_id) {

        $this->db->where('user_id',$user_id);
        $this->db->set('sent_email','NOW()',FALSE);
        $this->db->set('modified','NOW()',FALSE);
        $this->db->update('employee');
    }
}