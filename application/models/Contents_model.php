<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contents_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
        $this->load->helper(array('form', 'url'));
    }

    function get_contents($book_id)
    {
        // $this->get_children_id(3);

        $array = array('book_id' => $book_id, 'connection_type' => 'root');

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where($array);
        // $this->db->join('iconContents', 'iconContents.creator = book.author', 'left');
        // $this->db->join('backGroundMindMap', 'backGroundMindMap.creator = book.author', 'left');
        // $this->db->group_by('book.id');
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    function child_contents($book_id, $children = array())
    {
        //$query = $this->db->get_where("contents",array("book_id"=>$book_id,"row_flag !="=>"deleted"));

        $array = array('book_id' => $book_id, "row_flag !="=>"deleted");

        // $this->db->select('contents.id, contents.book_id, contents.title, contents.subtitle, contents.description, contents.connection_type, contents.pictogram_id, contents.pictogramBackground, contents.parent_content_id, contents.sort, contents.mindmap_angle, contents.distance, contents.row_flag, contents.created, contents.modified, pictogram.picture as pictogram_icon, pictogramBackground.picture as pictogram_Background');
        $this->db->select('contents.id, contents.book_id, contents.title, contents.subtitle, contents.description, contents.connection_type, contents.pictogram_id, contents.pictogramBackground, contents.parent_content_id, contents.sort, contents.mindmap_angle, contents.row_flag, contents.created, contents.modified, pictogram.picture as pictogram_icon, pictogramBackground.picture as pictogram_Background');
        $this->db->from('contents');
        $this->db->where($array);
        $this->db->join('pictogram','pictogram.id = contents.pictogram_id','left outer');
        $this->db->join('pictogramBackground','pictogramBackground.id = contents.pictogramBackground','left outer');
        $this->db->order_by('contents.sort', 'asc');
        $query = $this->db->get();

        // $this->db->select('*');
        // $this->db->from('contents');
        // $this->db->where($array);
        // $query = $this->db->get();

        foreach ($query->result_array() as $child) {
            array_push($children, $child);
        }

        //print_r($children); exit;

        return $children;
    }

    function parseTree($list, $pid = 0)
    {
        $lookup = array();
         foreach( $list as $item )
         {
            //print_r($list); exit;
            $item['children'] = array();
            $lookup[$item['id']] = $item;
         }

         //print_r($lookup); exit;

         $tree = array();
         foreach( $lookup as $id => $hcc )
         {
             $item = &$lookup[$id];
             if( $item['parent_content_id'] == $pid)
             {
                 $tree[$id] = &$item;
             }
             else {
                 if( isset( $lookup[$item['parent_content_id']] ) )
                 {
                     $lookup[$item['parent_content_id']]['children'][$id] = &$item;
                 }
                 else
                 {
                     $tree['Books'][$id] = &$item;
                 }
            }
         }

        //print_r($tree); exit;

         return empty($tree) ? null : $tree;
    }

    function parseTree_chejae($list, $pid = 0)
    {
        $lookup = array();
         foreach( $list as $item )
         {
            //print_r($list); exit;
            $item['children'] = array();
            $lookup[$item['id']] = $item;
         }

         //print_r($lookup); exit;

         $tree = array();
         foreach( $lookup as $id => $hcc )
         {
             $item = &$lookup[$id];
             if( $item['parent_content_id'] == $pid)
             {
                 $tree[$id] = &$item;
                 // array_push($tree[$id], $item);
             }
             else {
                 if( isset( $lookup[$item['parent_content_id']] ) )
                 {
                     //$lookup[$item['parent_content_id']]['children'] = &$item;
                     array_push($lookup[$item['parent_content_id']]['children'], $item);
                 }
                 else
                 {
                     $tree['Books'][$id] = &$item;
                     // array_push($tree['Books'][$id], $item);
                 }
            }
         }

        //print_r($tree); exit;

         return empty($tree) ? null : $tree;
    }

    function insert_contents($title, $subtitle, $book_id, $id)
    {
        $connection_type  = '';
        $angle = 0;
        $prev_sort = 0;
        $next_sort = 0;
        $new_sort = 0;

        $type = $this->get_connection_type($book_id);
        $parent_content_id =  $this->get_parent_id($id);

        $isroot = $this->get_connection_type_by_parent($parent_content_id);

        $this->db->select('pictogramBackground');
        $this->db->from('book');
        $this->db->where('id', $book_id);
        $query = $this->db->get();

        if($isroot == 'root') {
            $connection_type = 'connection';

            $this->db->where('parent_content_id', $parent_content_id);
            $this->db->where('connection_type !=', 'root');
            $this->db->from('contents');
            $x = $this->db->count_all_results();
            $x += 1;

            // $query3 =  $this->db->last_query();
            // print_r($query3); exit;

            if($x > 16) {
                return false;
            }
            //print_r($x); exit;

            $array = array('1' => '0',
                '2' => '180',
                '3' => '90',
                '4' => '270',
                '5' => '45',
                '6' => '225',
                '7' => '135',
                '8' => '315',
                '9' => '67.5',
                '10' => '247.5',
                '11' => '157.5',
                '12' => '337.5',
                '13' => '22.5',
                '14' => '202.5',
                '15' => '112.5',
                '16' => '292.5'
                );

            $angle = $array[''.$x.''];

            //print_r($angle); exit;
        }

        if($isroot == 'connection') {
            $connection_type = 'leaf';
        }

        if($isroot == 'leaf') {
            $connection_type = 'connection';
        }

        $pictogramBackground = $query->row()->pictogramBackground;

        //Angle
        if($isroot != 'root') {
            $this->db->where('parent_content_id', $parent_content_id);
            $this->db->where('connection_type !=', 'root');
            $this->db->from('contents');
            $x = $this->db->count_all_results();

            if($x == 0) {
                $this->db->select('mindmap_angle');
                $this->db->from('contents');
                $this->db->where('id', $parent_content_id);
                $query = $this->db->get();
                $angle = $query->row()->mindmap_angle;
            } else {

                $x += 1;

                if ($x % 2 == 0) {
                    $query = "SELECT id
                    FROM contents
                    WHERE parent_content_id = $parent_content_id
                    AND id < (SELECT MAX(id) FROM `contents` WHERE parent_content_id = $parent_content_id)
                    ORDER BY id DESC LIMIT 1";

                    $query = $this->db->query($query);

                    if($query->num_rows() == 0) {
                        $query = "SELECT id
                        FROM contents
                        WHERE parent_content_id = $parent_content_id
                        AND id = (SELECT MAX(id) FROM `contents` WHERE parent_content_id = $parent_content_id)
                        ORDER BY id DESC LIMIT 1";
                        $query = $this->db->query($query);
                    }

                    $id = $query->row()->id;

                    $this->db->select('mindmap_angle');
                    $this->db->from('contents');
                    $this->db->where('id', $id);
                    $query = $this->db->get();
                    $angle = $query->row()->mindmap_angle + 26;
                    if ($angle < 0) {// negative
                        $angle += 360;
                    }
                } else {
                    $query = "SELECT id
                    FROM contents
                    WHERE parent_content_id = $parent_content_id
                    AND id < (SELECT MAX(id) FROM `contents` WHERE parent_content_id = $parent_content_id)
                    ORDER BY id DESC LIMIT 1";

                    $query = $this->db->query($query);

                    $id = $query->row()->id;

                    $this->db->select('mindmap_angle');
                    $this->db->from('contents');
                    $this->db->where('id', $id);
                    $query = $this->db->get();
                    $angle = $query->row()->mindmap_angle - 26;
                    if ($angle < 0) {// negative
                        $angle += 360;
                    }
                }
            }
        }
        //End Angle

        $this->db->select('sort');
        $this->db->from('contents');
        $this->db->where('id', $id);
        $query = $this->db->get();
        $prev_sort = $query->row()->sort;

        $array = array('parent_content_id' => $parent_content_id, 'sort >' => $prev_sort);

        $this->db->select('sort');
        $this->db->from('contents');
        $this->db->where($array);
        $this->db->order_by("sort", "asc");
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            $next_sort = $query->row()->sort;
        }

        //print_r($next_sort); exit;

        if($prev_sort == 0 && $next_sort == 0) {
            $new_sort = 100;
        } elseif ($next_sort < 900 && $next_sort != 0) {
            $new_sort = $prev_sort + (($next_sort - $prev_sort) / 2);
            //$new_sort = $prev_sort + 100;
        } elseif ($next_sort < 900 && $next_sort == 0) {
            $new_sort = $prev_sort + 100;
        } elseif (($next_sort - $prev_sort) == 0) {
            return false;
        } else {
            $new_sort = $prev_sort + (($next_sort - $prev_sort) / 2);
        }

        $contents = array(
           'book_id' => $book_id,
           'title' => $title,
           'subtitle' => $subtitle,
           'connection_type' => $connection_type,
           'parent_content_id' => $parent_content_id,
           'pictogramBackground' => $pictogramBackground,
           'mindmap_angle' => $angle,
           'sort' => $new_sort
           );

        $query = $this->db->insert('contents', $contents);
    }

    function delete_book($id="")
    {
        $this->db->trans_start();
        $this->db->select('id');
        $this->db->from('contents');
        $this->db->where('book_id', $id);
        $query = $this->db->get();
        $ids = array();
        foreach ($query->result() as $row) {
            $ids[] = $row->id;
        }
        $this->db->where_in('content_id', $ids);
        $this->db->delete('contentsAlbum');
        $this->db->where_in('content_id', $ids);
        $this->db->delete('contentsSound');
        $this->db->where_in('content_id', $ids);
        $this->db->delete('contentsVideo');
        $this->db->delete('contents', array('book_id' => $id));
        $this->db->delete('bookTemplate', array('book_id' => $id));
        $this->db->delete('bookHistory', array('book_id' => $id));
        $this->db->delete('book', array('id' => $id));
        $this->db->trans_complete();
    }

    function delete_content($id="")
    {
        $children = $this->get_children_id($id);

        $data = array('row_flag' => 'deleted','modified' => date("Y-m-d H:i:s"));

        $this->db->update('contents', $data, array('id' => $id));

        foreach ($children as $key => $parent_id) {
            $this->db->update('contents', $data, array('parent_content_id' => $parent_id));
        }
    }

    function delete_child($id="")
    {
        $children = $this->get_children_id($id);

        $data = array('row_flag' => 'deleted','modified' => date("Y-m-d H:i:s"));

        foreach ($children as $key => $parent_id) {
            $this->db->update('contents', $data, array('parent_content_id' => $parent_id));
        }
    }

    function update_content($id="", $title_attribute)
    {
        $data = array('title' => $title_attribute,'modified' => date("Y-m-d H:i:s"));

        $this->db->update('contents', $data, array('id' => $id));
    }

    function insert_child_contents($title, $subtitle, $book_id, $parent_id)
    {
        $connection_type  = '';
        $angle = 0;
        $prev_sort = 0;
        $next_sort = 0;
        $new_sort = 0;

        $type = $this->get_connection_type_by_parent($parent_id);
        print_r($type); exit;

        $this->db->select('pictogramBackground');
        $this->db->from('book');
        $this->db->where('id', $book_id);
        $query = $this->db->get();

        if($type == 'root') {
            $connection_type = 'connection';

            $this->db->where(array('parent_content_id' => $parent_id, 'connection_type !=' => 'root'));
            $this->db->from('contents');
            $x = $this->db->count_all_results();
            $x += 1;

            if($x > 16) {
                return false;
            }
            //print_r($x); exit;

            $array = array('1' => '0',
                '2' => '180',
                '3' => '90',
                '4' => '270',
                '5' => '45',
                '6' => '225',
                '7' => '135',
                '8' => '315',
                '9' => '67.5',
                '10' => '247.5',
                '11' => '157.5',
                '12' => '337.5',
                '13' => '22.5',
                '14' => '202.5',
                '15' => '112.5',
                '16' => '292.5'
                );

            $angle = $array[''.$x.''];
        }

        if($type == 'connection') {
            $connection_type = 'leaf';
        }

        if($type == 'leaf') {
            $connection_type = 'leaf';

            $data = array(
               'connection_type' => 'connection',
               'modified' => date("Y-m-d H:i:s")
               );

            $this->db->where('id', $parent_id);
            $this->db->update('contents', $data);
        }

        $pictogramBackground = $query->row()->pictogramBackground;

        //Angle
        if($type != 'root') {
            $this->db->where('parent_content_id', $parent_id);
            $this->db->where('connection_type !=', 'root');
            $this->db->from('contents');
            $x = $this->db->count_all_results();

            if($x == 0) {
                $this->db->select('mindmap_angle');
                $this->db->from('contents');
                $this->db->where('id', $parent_id);
                $query = $this->db->get();
                $angle = $query->row()->mindmap_angle;
            } else {

                $x += 1;

                if ($x % 2 == 0) {
                    $query = "SELECT id
                    FROM contents
                    WHERE parent_content_id = $parent_id
                    AND id < (SELECT MAX(id) FROM `contents` WHERE parent_content_id = $parent_id)
                    ORDER BY id DESC LIMIT 1";

                    $query = $this->db->query($query);

                    if($query->num_rows() == 0) {
                        $query = "SELECT id
                        FROM contents
                        WHERE parent_content_id = $parent_id
                        AND id = (SELECT MAX(id) FROM `contents` WHERE parent_content_id = $parent_id)
                        ORDER BY id DESC LIMIT 1";
                        $query = $this->db->query($query);
                    }

                    $id = $query->row()->id;

                    $this->db->select('mindmap_angle');
                    $this->db->from('contents');
                    $this->db->where('id', $id);
                    $query = $this->db->get();
                    $angle = $query->row()->mindmap_angle + 26;
                    if ($angle < 0) {// negative
                        $angle += 360;
                    }
                } else {
                    $query = "SELECT id
                    FROM contents
                    WHERE parent_content_id = $parent_id
                    AND id < (SELECT MAX(id) FROM `contents` WHERE parent_content_id = $parent_id)
                    ORDER BY id DESC LIMIT 1";

                    $query = $this->db->query($query);

                    $id = $query->row()->id;

                    $this->db->select('mindmap_angle');
                    $this->db->from('contents');
                    $this->db->where('id', $id);
                    $query = $this->db->get();
                    $angle = $query->row()->mindmap_angle - 26;
                    if ($angle < 0) {// negative
                        $angle += 360;
                    }
                }
            }
        }
        //End Angle

        //Sorting
        $this->db->select('sort');
        $this->db->from('contents');
        $this->db->where('id', $parent_id);
        $query = $this->db->get();
        $prev_sort = $query->row()->sort;

        //print_r($prev_sort); exit;

        $array = array('parent_content_id' => $parent_id, 'sort >' => $prev_sort);

        $this->db->select('sort');
        $this->db->from('contents');
        $this->db->where($array);
        $this->db->order_by("sort", "asc");
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            $next_sort = $query->row()->sort;
        }

        if($prev_sort == 0 && $next_sort == 0) {
            $new_sort = 100;
        } elseif ($next_sort < 900 && $next_sort != 0) {
            //$new_sort = $prev_sort + (($next_sort - $prev_sort) / 2);
            $new_sort = $prev_sort + 100;
        } elseif ($next_sort < 900 && $next_sort == 0) {
            $new_sort = $prev_sort + 100;
        } elseif (($next_sort - $prev_sort) == 0) {
            return false;
        } else {
            $new_sort = $prev_sort + (($next_sort - $prev_sort) / 2);
        }
        //End Sorting

        $contents = array(
           'book_id' => $book_id,
           'title' => $title,
           'subtitle' => $subtitle,
           'connection_type' => $connection_type,
           'parent_content_id' => $parent_id,
           'pictogramBackground' => $pictogramBackground,
           'mindmap_angle' => $angle,
           'sort' => $new_sort
        );

        $query = $this->db->insert('contents', $contents);
    }

    function get_connection_type($book_id)
    {
        $this->db->select('connection_type');
        $this->db->from('contents');
        $this->db->where('book_id',$book_id);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row()->connection_type;
        } else {
            return false;
        }
    }

    function get_connection_type_by_parent($parent_id)
    {
        $this->db->select('connection_type');
        $this->db->from('contents');
        $this->db->where('id',$parent_id);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row()->connection_type;
        } else {
            return false;
        }
    }

    function get_content_id($book_id)
    {
        $this->db->select('id');
        $this->db->from('contents');
        $this->db->where('book_id',$book_id);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row()->id;
        } else {
            return false;
        }
    }

    function get_parent_id($id)
    {
        $this->db->select('parent_content_id');
        $this->db->from('contents');
        $this->db->where('id',$id);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row()->parent_content_id;
        } else {
            return false;
        }

    }

    function get_parent_content_id($book_id)
    {
        $query = $this->db->get_where("contents",array("book_id"=>$book_id,"connection_type"=>"root"));

        if($query->num_rows() > 0) {
            return $query->row()->parent_content_id;
        } else {
            return false;
        }
    }

    function get_children_id($parent_id)
    {
        $query = $this->db->get_where("contents",array("parent_content_id"=>$parent_id));

        $lookup = array($parent_id => $parent_id);
         foreach( $query->result_array() as $item ) {
             $lookup[$item['id']] = $item['id'];
         }

         //print_r($lookup); exit;

        return $lookup;
    }

    function get_book_contents($book_id)
    {
        $array = array('book_id' => $book_id);

        $this->db->select('*');
        $this->db->from('contents');
        $this->db->where($array);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        return $contents;
    }

    function get_examination_list($book_id,$listsort='asc')
    {

        $array = array('contents.book_id' => $book_id);

        $this->db->select('examination.id, examination.question, contents.title, examination.type');
        $this->db->from('examination');
        $this->db->join('contents', 'contents.id = examination.content_id', 'left');
        $this->db->where($array);
        $this->db->order_by("examination.sort", $listsort);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        return $contents;
    }

    function add_examination_list($post,$sort=0){

        $contents = array(
           'book_id' => $post['book_id'],
           'content_id' => $post['content_id'],
           'type' => $post['input_type'],
           'question' => $post['question'],
           'sort' => $sort,
           'picture' => $post['picture'],
           'movie' => null,
           'music' => null
        );

        $this->db->trans_begin();
        $query = $this->db->insert('examination', $contents);
        $examination_id = $this->db->insert_id();

        foreach ($post['examination_answer_text'] as $key => $value) {
            if (empty($value) && empty($post['examination_answer_picture'][$key])) {
                continue;
            } else {

                $examinationAnswer = array(
                   //'id' => $post['content_id'],
                   'examination_id' => $examination_id,
                   'answer_text' => $value,
                   'answer_picture' => @$post['examination_answer_picture'][$key],
                   'correct_flag' => @$post['examination_answer_correct_flag'][$key]
                );
                $query = $this->db->insert('examinationAnswer', $examinationAnswer);
            }
        }

        $this->db->trans_commit();
    }

    function update_examination_list($post){

        $contents = array(
           'book_id' => $post['book_id'],
           'content_id' => $post['content_id'],
           'type' => $post['input_type'],
           'question' => $post['question'],
           'picture' => $post['picture'],
           'movie' => null,
           'music' => null,
           'modified' => date("Y-m-d H:i:s")
        );

        $this->db->trans_begin();
        $this->db->where('id', $post['examination_id']);
        $query = $this->db->update('examination', $contents);
        $examination_id = $this->db->insert_id();

        $this->db->delete('examinationAnswer', array('examination_id' => $post['examination_id']));

        foreach ($post['examination_answer_text'] as $key => $value) {
            if (empty($value) && empty($post['examination_answer_picture'][$key])) {
                continue;
            } else {

                $examinationAnswer = array(
                   //'id' => $post['content_id'],
                   'examination_id' => $post['examination_id'],
                   'answer_text' => $value,
                   'answer_picture' => $post['examination_answer_picture'][$key],
                   'correct_flag' => $post['examination_answer_correct_flag'][$key]
                );
                $query = $this->db->insert('examinationAnswer', $examinationAnswer);
            }
        }

        $this->db->trans_commit();
    }

    function delete_examination($examination_id) {
        $this->db->delete('examinationAnswer', array('examination_id' => $examination_id));
        return $this->db->delete('examination', array('id' => $examination_id));
    }

    function get_exam_questions($examination_id) {

        $this->db->select('*');
        $this->db->from('examination');
        $this->db->where(array('id' => $examination_id));
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            $exam = $query->row();
        }

        $this->db->select('*');
        $this->db->from('examinationAnswer');
        $this->db->where(array('examination_id' => $examination_id));
        $query = $this->db->get();

        $answer = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $answer[] = $row;
            }
        }
        $contents = array('exam'=>$exam,'answer'=>$answer);

        return $contents;
    }

    function get_contentsTitle($content_id)
    {
        $this->db->select('title');
        $this->db->from('contents');
        $this->db->where('id',$content_id);
        $query = $this->db->get();
        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }
        print_r(json_encode($contents));
    }

    function get_contentsDescription($content_id)
    {
        $this->db->select('description');
        $this->db->from('contents');
        $this->db->where('id',$content_id);
        $query = $this->db->get();
        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }
        print_r(json_encode($contents));
    }

    function get_contentsSubtitle($content_id)
    {
        $this->db->select('subtitle');
        $this->db->from('contents');
        $this->db->where('id',$content_id);
        $query = $this->db->get();
        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }
        print_r(json_encode($contents));
    }

    function get_contentsPictogram($content_id)
    {
        $this->db->select('pictogram_id');
        $this->db->from('contents');
        $this->db->where('id',$content_id);
        $query = $this->db->get();
        $contents = array();
        if($query->num_rows() > 0) {
            $this->db->select('picture');
            $this->db->from('pictogram');
            $this->db->where('id',$query->row()->pictogram_id);
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }
        print_r(json_encode($contents));
    }

    function get_contentsPictogramBackground($content_id)
    {
        $this->db->select('pictogramBackground');
        $this->db->from('contents');
        $this->db->where('id',$content_id);
        $query = $this->db->get();
        $contents = array();
        if($query->num_rows() > 0) {
            $this->db->select('picture');
            $this->db->from('pictogramBackground');
            $this->db->where('id',$query->row()->pictogramBackground);
            $query = $this->db->get();
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }
        print_r(json_encode($contents));
    }

    function get_contentsList($book_id)
    {
        $user_id = $this->session->userdata('user_id');

        $this->db->select('a.title as title, b.created as actual_date, a.connection_type');
        $this->db->from('contents a');
        $this->db->where('a.book_id',$book_id);
        $this->db->join('learningActual b','a.id = b.content_examination_id and b.type="learning" and b.user_id = '.$user_id,'left');
        $this->db->order_by('a.sort');
        $query = $this->db->get();

        return $query->result();
    }

    function change_contents_sort($position, $content_id)
    {
        $this->db->trans_begin();

        if($position == "up") {
            $query = "SELECT id, sort FROM contents
                    WHERE parent_content_id = (select parent_content_id from contents WHERE id = $content_id)
                    AND sort < (select sort from contents WHERE id = $content_id)
                    ORDER BY sort DESC LIMIT 1";
            $query = $this->db->query($query);
            
            if($query->num_rows() == 0) {
                return print_r(false);
            }
            
            $id = $query->row()->id;
            $next_sort = $query->row()->sort;

            $this->db->select('sort');
            $this->db->from('contents');
            $this->db->where('id',$content_id);
            $query = $this->db->get();
            $prev_sort =  $query->row()->sort;

            $data = array('sort' => $next_sort,'modified' => date("Y-m-d H:i:s"));
            $this->db->update('contents', $data, array('id' => $content_id));            

            $data = array('sort' => $prev_sort,'modified' => date("Y-m-d H:i:s"));
            $this->db->update('contents', $data, array('id' => $id));

            //print_r($id." - ".$sort); exit;
        }

        if($position == "down") {
            $query = "SELECT id, sort FROM contents
                    WHERE parent_content_id = (select parent_content_id from contents WHERE id = $content_id)
                    AND sort > (select sort from contents WHERE id = $content_id)
                    ORDER BY sort ASC LIMIT 1";
            $query = $this->db->query($query);
            
            if($query->num_rows() == 0) {
                return print_r(false);
            }

            $id = $query->row()->id;
            $next_sort = $query->row()->sort;

            $this->db->select('sort');
            $this->db->from('contents');
            $this->db->where('id',$content_id);
            $query = $this->db->get();
            $prev_sort =  $query->row()->sort;

            $data = array('sort' => $next_sort,'modified' => date("Y-m-d H:i:s"));
            $this->db->update('contents', $data, array('id' => $content_id));            

            $data = array('sort' => $prev_sort,'modified' => date("Y-m-d H:i:s"));
            $this->db->update('contents', $data, array('id' => $id));

            //print_r($id." - ".$sort); exit;
        }

        if($position == "left") {
            $query = "SELECT connection_type, parent_content_id FROM contents WHERE parent_content_id = (select parent_content_id from contents WHERE id = $content_id)";
            $query = $this->db->query($query);
            $parent_content_id = $query->row()->parent_content_id;
            $connection_type = $query->row()->connection_type;
            
            if($connection_type == 'root') {
                return print_r(false);
            }

            $query = "SELECT parent_content_id FROM contents WHERE id = $parent_content_id";
            $query = $this->db->query($query);
            $parent_content_id = $query->row()->parent_content_id;

            $query = "UPDATE contents
                    SET connection_type = 'connection', parent_content_id = $parent_content_id
                    WHERE id = $content_id";

            $query = $this->db->query($query);
        }

        return print_r($this->db->trans_commit());
    }

    function get_attribute_preview($position, $content_id)
    {
        //$this->db->trans_begin();

        if($position == "previous") {
            $query = "SELECT * FROM contents
                    WHERE parent_content_id = (select parent_content_id from contents WHERE id = $content_id)
                    AND sort < (select sort from contents WHERE id = $content_id)
                    AND row_flag <> 'deleted'
                    ORDER BY sort DESC LIMIT 1";
            $query = $this->db->query($query);

            if($query->num_rows() == 0) {
                $query = "SELECT * FROM contents 
                        WHERE parent_content_id = (select parent_content_id from contents WHERE id = $content_id)
                        AND sort < (select sort from contents WHERE id = $content_id)
                        AND row_flag <> 'deleted'
                        ORDER BY sort DESC LIMIT 1";
                $query = $this->db->query($query);

                if($query->num_rows() == 0) {
                    $query = "SELECT * FROM contents
                            WHERE id = (select parent_content_id from contents WHERE id = $content_id)
                            AND sort < (select sort from contents WHERE id = $content_id)
                            AND row_flag <> 'deleted'
                            ORDER BY sort DESC LIMIT 1";
                    $query = $this->db->query($query);

                    if($query->num_rows() == 0) {                    
                        $query = "SELECT id, parent_content_id FROM contents
                                WHERE parent_content_id = (select parent_content_id from contents WHERE id = $content_id)
                                AND row_flag <> 'deleted' LIMIT 1";
                        $query = $this->db->query($query);
                        $parent_content_id = $query->row()->parent_content_id;

                        $query = "SELECT * FROM contents
                            WHERE parent_content_id = (select parent_content_id from contents WHERE id = $parent_content_id)
                            AND sort < (select sort from contents WHERE id = $parent_content_id)
                            AND row_flag <> 'deleted'
                            ORDER BY sort DESC LIMIT 1";
                        $query = $this->db->query($query);

                        // $query3 =  $this->db->last_query();
                        // print_r($query3); exit;

                        //$query = $this->db->query($query);
                        return $query->result();
                    }

                    return $query->result();
                }

                return $query->result();
            }

            return $query->result();
        }

        if($position == "next") {
            $query = "SELECT * FROM contents
                WHERE parent_content_id = $content_id
                AND connection_type <> 'root'
                AND row_flag <> 'deleted'
                ORDER BY sort ASC LIMIT 1";
            $query = $this->db->query($query);
            
            if($query->num_rows() == 0) {
                //print_r("first");
                $query = "SELECT * FROM contents
                        WHERE parent_content_id = (select parent_content_id from contents WHERE id = $content_id)
                        AND sort > (select sort from contents WHERE id = $content_id)
                        AND row_flag <> 'deleted'
                        ORDER BY sort ASC LIMIT 1";
                $query = $this->db->query($query);

                if($query->num_rows() == 0) {
                    //print_r("second");
                    $query = "SELECT id, parent_content_id FROM contents
                            WHERE parent_content_id = (select parent_content_id from contents WHERE id = $content_id)
                            AND row_flag <> 'deleted' LIMIT 1";
                    $query = $this->db->query($query);
                    $parent_content_id = $query->row()->parent_content_id;

                    $query = "SELECT * FROM contents
                        WHERE parent_content_id = (select parent_content_id from contents WHERE id = $parent_content_id)
                        AND sort > (select sort from contents WHERE id = $parent_content_id)
                        AND row_flag <> 'deleted'
                        ORDER BY sort ASC LIMIT 1";
                    $query = $this->db->query($query);

                    // $query3 =  $this->db->last_query();
                    // print_r($query3); exit;

                    //$query = $this->db->query($query);
                    return $query->result();
                }

                // $query3 =  $this->db->last_query();
                //     print_r($query3); exit;

                return $query->result();
            }

            return $query->result();
        }

        //$this->db->trans_commit();
    }
}