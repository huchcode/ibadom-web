<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function update_position($array)
    {
        return $this->db->update_batch('contents', $array, 'id');
    }

    function insert_sync_device($user_id,$device_info) {

        $synced = '2015-01-01 00:00:00';

        /*$this->db->select('*');
        $this->db->from('syncDevice');
        $this->db->where(array('user_id'=>$user_id));
        $query = $this->db->get();

        if($query->num_rows() > 0) {

            $sync_id = $query->row()->id;

            $this->db->where('id', $sync_id);
            $this->db->update('syncDevice', array('synced' => $synced));

            return $sync_id;

        } else {*/

            $data = array(
                'user_id' => $user_id,
                'device_information' => $device_info,
                'synced' => $synced
                );
            $query = $this->db->insert('syncDevice', $data);

            return $this->db->insert_id();

        //}

    }

    function update_sync_device($sync_id) {

        $data = array('synced' => date('Y-m-d H:i:s'));

        $this->db->where('id', $sync_id);
        $this->db->update('syncDevice', $data);
    }

    function reset_sync_device($sync_id) {

        $data = array('synced' => '2015-01-01 00:00:00');

        $this->db->where('id', $sync_id);
        $this->db->update('syncDevice', $data);

        return $sync_id;
    }

    function get_sync_device($sync_id) {

        $this->db->select('*');
        $this->db->from('syncDevice');
        $this->db->where(array('id'=>$sync_id));
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return $query->row();
        } else {
            return null;
        }
    }

    function get_book_id_for_mobile($employee_info) {

        $sql = "
            SELECT book_id FROM learningAssign a
            JOIN book b ON a.book_id=b.id and release_status='ongoing'
            WHERE a.target = 0 and a.master_user_id = '".$employee_info['master_user_id']."' ";

        if (!empty($employee_info['department_id'])) {
            $sql .= "UNION
                SELECT book_id FROM learningAssign a
                JOIN book b ON a.book_id=b.id and release_status='ongoing'
                WHERE target = '".$employee_info['department_id']."' and type ='department' ";
            }

        $sql .= "UNION
            SELECT book_id FROM learningAssign a
            JOIN book b ON a.book_id=b.id and release_status='ongoing'
            WHERE target = '".$employee_info['employee_id']."' and type ='employee' ";

        $sql .= "UNION
            SELECT id AS book_id FROM book
            WHERE author = '".$employee_info['user_id']."' and release_status <> 'closed'";

        $query = $this->db->query($sql);

        $contents = array();
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row) {
                $contents[] = $row->book_id;
            }
        }
        return $contents;
    }

    function count_newly_added_books($employee_info,$synced) {

        $sql = "
            SELECT book_id FROM learningAssign a
            JOIN book b ON a.book_id=b.id and release_status='ongoing'
            WHERE a.target = 0
            AND a.modified > '".$synced."'
            AND a.master_user_id = '".$employee_info['master_user_id']."' ";

        if (!empty($employee_info['department_id'])) {
            $sql .= "UNION
                SELECT book_id FROM learningAssign a
                JOIN book b ON a.book_id=b.id AND release_status='ongoing'
                WHERE target = '".$employee_info['department_id']."'
                AND a.modified > '".$synced."'
                AND type ='department' ";
            }

        $sql .= "UNION
            SELECT book_id FROM learningAssign a
            JOIN book b ON a.book_id=b.id and release_status='ongoing'
            WHERE target = '".$employee_info['employee_id']."'
            AND a.modified > '".$synced."'
            AND type ='employee' ";

        $sql .= "UNION
            SELECT id AS book_id FROM book
            WHERE author = '".$employee_info['user_id']."'
            AND modified > '".$synced."'
            AND release_status <> 'closed'";

        $query = $this->db->query($sql);

        $contents = array();
        if ($query->num_rows() > 0)
        {
            foreach ($query->result() as $row) {
                $contents[] = $row->book_id;
            }
        }
        return count($contents);
    }

    function get_book_for_mobile($book_ids,$synced) {

        $this->db->select('book.*,UNIX_TIMESTAMP(book.created) AS created_timestamp,UNIX_TIMESTAMP(book.modified) AS modified_timestamp');
        $this->db->select('backGroundMindMap.picture AS background_mindmap_image,pictogramBackground.picture AS pictogramBackground_image,iconContents.picture AS book_icon_image');
        $this->db->from('book');
        $this->db->join('backGroundMindMap', 'book.background_mindmap = backGroundMindMap.id', 'left');
        $this->db->join('pictogramBackground', 'book.pictogramBackground = pictogramBackground.id', 'left');
        $this->db->join('iconContents', 'book.book_icon = iconContents.id', 'left');
        $this->db->where(array('book.modified >'=>$synced));
        $this->db->where_in('book.id',$book_ids);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $row_new = $row->background_mindmap_image;
                $row_temp = explode("/", $row_new);
                $row->background_mindmap_image = $row_temp[count($row_temp) - 1];

                $row_new2 = $row->pictogramBackground_image;
                $row_temp2 = explode("/", $row_new2);
                $row->pictogramBackground_image = $row_temp2[count($row_temp2) - 1];

                $row_new3 = $row->book_icon_image;
                $row_temp3 = explode("/", $row_new3);
                $row->book_icon_image = $row_temp3[count($row_temp3) - 1];

                $contents[] = $row;
            }
        }

        return $contents;

    }

    function get_contents_for_mobile($book_ids,$synced) {

        $this->db->select('contents.*,UNIX_TIMESTAMP(contents.created) AS created_timestamp,UNIX_TIMESTAMP(contents.modified) AS modified_timestamp');
        $this->db->select('pictogram.picture AS pictogram_image,pictogramBackground.picture AS pictogramBackground_image');
        $this->db->from('contents');
        $this->db->join('pictogram', 'contents.pictogram_id = pictogram.id', 'left');
        $this->db->join('pictogramBackground', 'contents.pictogramBackground = pictogramBackground.id', 'left');
        $this->db->where(array('contents.modified >'=>$synced));
        $this->db->where_in('contents.book_id',$book_ids);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $key=>$row) {
                $row_new = $row->pictogram_image;
                $row_temp = explode("/", $row_new);
                $row->pictogram_image = $row_temp[count($row_temp) - 1];

                $row_new2 = $row->pictogramBackground_image;
                $row_temp2 = explode("/", $row_new2);
                $row->pictogramBackground_image = $row_temp2[count($row_temp2) - 1];

                $row->contentsAlbum =  $this->get_content_album($row->id);
                $row->contentsSound =  $this->get_content_sound($row->id);
                $row->contentsVideo =  $this->get_content_video($row->id);

                $contents[] = $row;
            }
        }

        return $contents;

    }

    function get_content_album($content_id) {

        $this->db->select('*,UNIX_TIMESTAMP(contentsAlbum.created) AS created_timestamp,UNIX_TIMESTAMP(contentsAlbum.modified) AS modified_timestamp');
        $this->db->select('contentsAlbum.picture AS picture_image');
        $this->db->from('contentsAlbum');
        $this->db->where_in('contentsAlbum.content_id',$content_id);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $row_new = $row->picture_image;
                $row_temp = explode("/", $row_new);
                $row->picture_image = $row_temp[count($row_temp) - 1];

                $contents[] = $row;
            }
        }

        return $contents;
    }

    function get_content_sound($content_id) {

        $this->db->select('*,UNIX_TIMESTAMP(contentsSound.created) AS created_timestamp,UNIX_TIMESTAMP(contentsSound.modified) AS modified_timestamp');
        $this->db->from('contentsSound');
        $this->db->where_in('contentsSound.content_id',$content_id);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }
        return $contents;
    }

    function get_content_video($content_id) {

        $this->db->select('*,UNIX_TIMESTAMP(contentsVideo.created) AS created_timestamp,UNIX_TIMESTAMP(contentsVideo.modified) AS modified_timestamp');
        $this->db->from('contentsVideo');
        $this->db->where_in('contentsVideo.content_id',$content_id);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }
        return $contents;
    }

    function get_examination_for_mobile($book_ids,$synced) {

        $this->db->select('*,UNIX_TIMESTAMP(examination.created) AS created_timestamp,UNIX_TIMESTAMP(examination.modified) AS modified_timestamp');
        $this->db->select('examination.picture AS picture_image');
        $this->db->from('examination');
        $this->db->where(array('examination.modified >'=>$synced));
        $this->db->where_in('examination.book_id',$book_ids);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $row_new = $row->picture_image;
                $row_temp = explode("/", $row_new);
                $row->picture_image = $row_temp[count($row_temp) - 1];

                $row->examinationAnswer =  $this->get_exam_answer($row->id);

                $contents[] = $row;
            }
        }

        return $contents;

    }

    function get_exam_answer($exam_id) {

        $this->db->select('*,UNIX_TIMESTAMP(examinationAnswer.created) AS created_timestamp,UNIX_TIMESTAMP(examinationAnswer.modified) AS modified_timestamp');
        $this->db->select('examinationAnswer.answer_picture AS answer_picture_image');
        $this->db->from('examinationAnswer');
        $this->db->where_in('examinationAnswer.examination_id',$exam_id);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $row_new = $row->answer_picture_image;
                $row_temp = explode("/", $row_new);
                $row->answer_picture_image = $row_temp[count($row_temp) - 1];

                $contents[] = $row;
            }
        }

        return $contents;

    }

    function get_file_list($master_user_id,$synced) {

        $contents = array();

        $this->db->select('picture');
        $this->db->from('pictogram');
        $this->db->where(array('created >'=>$synced));
        $this->db->where_in('master_user_id',array('0',$master_user_id));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row->picture;
            }
        }

        $this->db->select('picture');
        $this->db->from('pictogramBackground');
        $this->db->where(array('created >'=>$synced));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row->picture;
            }
        }

        $this->db->select('picture');
        $this->db->from('iconContents');
        $this->db->where(array('created >'=>$synced));
        $this->db->where_in('master_user_id',array('0',$master_user_id));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row->picture;
            }
        }

        $this->db->select('picture');
        $this->db->from('backGroundMindMap');
        $this->db->where(array('created >'=>$synced));
        $this->db->where_in('master_user_id',array('0',$master_user_id));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row->picture;
            }
        }

        $this->db->select('examination.picture,examinationAnswer.answer_picture');
        $this->db->from('book');
        $this->db->join('examination', 'examination.book_id = book.id');
        $this->db->join('examinationAnswer', 'examinationAnswer.examination_id = examination.id');
        $this->db->where(array('examination.created >'=>$synced));
        $this->db->where_in('book.master_user_id',array('0',$master_user_id));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                if (!empty($row->picture)) {
                    $contents[] = $row->picture;
                }
                if (!empty($row->answer_picture)) {
                    $contents[] = $row->answer_picture;
                }
            }
        }

        $this->db->select('contentsAlbum.picture');
        $this->db->from('book');
        $this->db->join('contents', 'contents.book_id = book.id');
        $this->db->join('contentsAlbum', 'contentsAlbum.content_id = contents.id');
        $this->db->where(array('contents.modified >'=>$synced));
        $this->db->where_in('book.master_user_id',array('0',$master_user_id));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row->picture;
            }
        }

        return $contents;

    }

    function sync_learning_actual($user_id,$sync_id,$data) {

        $leaningActual = json_decode(str_replace('\"', '"', $data), true);

        $this->db->trans_start();

        $learningActualArray = array();
        $syncLeaningActualArray = array();
        foreach ($leaningActual as $key => $value) {
            $learningActualArray[] = array(
                'user_id' => $user_id,
                'type' => $value['type'],
                'content_examination_id' => $value['content_examination_id'],
                'book_id' => $value['book_id']
                );
            $syncLeaningActualArray[] = array(
                'learning_actual_id' => $value['id'],
                'sync_device_id' => $sync_id
                );
        }
        if (count($learningActualArray) > 0) {
            $this->db->insert_batch('learningActual', $learningActualArray);
            $this->db->insert_batch('syncLearningActual', $syncLeaningActualArray);
        }


        $contents = array();
        $this->db->select('learningActual.type, learningActual.content_examination_id, learningActual.book_id');
        $this->db->from('learningActual');
        $this->db->join('syncLearningActual', 'learningActual.id = syncLearningActual.learning_actual_id AND syncLearningActual.sync_device_id = "'.$sync_id.'" ', 'left');
        $this->db->where(array(
            'learningActual.user_id'=>$user_id,
            'syncLearningActual.id IS NULL'=>null
            ));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        $insertcontents = array();
        $this->db->select('learningActual.id');
        $this->db->from('learningActual');
        $this->db->join('syncLearningActual', 'learningActual.id = syncLearningActual.learning_actual_id AND syncLearningActual.sync_device_id = "'.$sync_id.'" ', 'left');
        $this->db->where(array(
            'learningActual.user_id'=>$user_id,
            'syncLearningActual.id IS NULL'=>null
            ));
        $query = $this->db->get();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $insertcontents[] = array(
                    'learning_actual_id' => $row->id,
                    'sync_device_id' => $sync_id
                );

            }
        }
        if (count($insertcontents) > 0) $this->db->insert_batch('syncLearningActual', $insertcontents);

        $this->db->trans_complete();

        if ($this->db->trans_status() === FALSE) {
            $response = array("errorCode"=>"30001","errorMessage"=>"Database transaction error. Please contact you administrator.","data"=>array());
        } else {
            $response = array("errorCode"=>"0","errorMessage"=>"Success","data"=>$contents);
        }

        return $response;
    }

    function update_user_photo($uid,$image) {
        return $image;
    }
}