<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ContentsAlbum_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->model('User_model');
        $this->load->model('Upload_model');
    }

    function insert_contentsAlbum($content_id)
    {        
      $i=1;
      $x=1;
      $reset = true;
      $albumPath = '/assets/uploads/contentsAlbum/';

      if(!empty($_POST['img_delete1']) || !empty($_POST['img_delete2']) || !empty($_POST['img_delete3'])) {
        //print_r($_POST); exit;
        $this->db->trans_begin();
        foreach($_POST as $key =>$value){
          if(!empty($key))
          {
            if($key == 'img_delete'.$x) {
              $this->db->delete('contentsAlbum', array('picture' => $value));
              if(is_file(FCPATH.$value)) {
                unlink(FCPATH.$value);
              }
              $x++;
            }
          }
        }

        if(empty($_FILES['img_content1']['name']) && empty($_FILES['img_content2']['name']) && empty($_FILES['img_content3']['name'])) {
          return $this->db->trans_complete();
        } else {
          //$this->db->delete('contentsAlbum', array('content_id' => $content_id));
        }
      }

      // $files = glob(FCPATH.$albumPath.$content_id.'/*');
      // foreach($files as $file){
      //   if(is_file($file))
      //     unlink($file);
      // }

      //$this->db->trans_begin();
      //$this->db->delete('contentsAlbum', array('content_id' => $content_id));

      foreach ($_FILES as $file) {
        //print_r($_FILES); exit;
        if(!empty($file['name'])) {
          $data = $this->Upload_model->upload($content_id, '/assets/uploads/contentsAlbum/', 'img_content'.$i, 'contentsAlbum'.$i, 'gif|jpg|png', '5120');

          //print_r($data); exit;
          if(isset($data['upload_data'])) {
            $contents = array(
            'content_id' => $content_id,
            'picture' => $albumPath.$data['upload_data']['file_name'],
            'sort' => 0,
            'modified' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('contentsAlbum', $contents);
          } else {
            $this->db->trans_rollback();
            return $data;
          }
        }
        $i++;
      }

      return $this->db->trans_complete();
    }

    function get_contentsAlbum($content_id)
    {
        $this->db->select('id, picture');
        $this->db->from('contentsAlbum');
        $this->db->where('content_id',$content_id);
        $query = $this->db->get();

        $contents = array();        
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        echo json_encode($contents);
    }
}