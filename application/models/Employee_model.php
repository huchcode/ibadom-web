<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Employee_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function search_employee($master_user_id, $name)
    {
        $this->db->select('a.user_id as input_user_id, if (b.name is null, a.name, CONCAT(a.name, " (", b.name,")")) as name, "employee" as type');
        $this->db->from('employee a');
        $this->db->where('a.master_user_id', $master_user_id);
        $this->db->like('a.name', $name);
        $this->db->join('department b', 'a.department_id = b.id', 'left');
        $query = $this->db->get();

        $data = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $key) {
                $data[] = $key;
            }
        }
        return print_r(json_encode($data));
    }
}