<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ContentsText_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    function insert_contentsSubtitle($content_id, $subtitle)
    {
        $isExists = $this->isContentsSubtitleExists($content_id);

        if($isExists) {
          $contents = array(
            'subtitle' => $subtitle,
            'modified' => date("Y-m-d H:i:s")
            );
          $this->db->where('id', $content_id);
          $this->db->update('contents', $contents);
        } else {
          $contents = array(
           'subtitle' => $subtitle
           );
          $query = $this->db->insert('contents', $contents);
        }
    }

    function insert_contentsDetail($content_id, $description)
    {
        $isExists = $this->isContentsDetailExists($content_id);

        if($isExists) {
          $contents = array(
            'description' => $description,
            'modified' => date("Y-m-d H:i:s")
            );
          $this->db->where('id', $content_id);
          $this->db->update('contents', $contents);
        } else {
          $contents = array(
           'description' => $description
           );
          $query = $this->db->insert('contents', $contents);
        }
    }

    function isContentsDetailExists($content_id)
    {
        $array = array('id' => $content_id);

        $this->db->select('id');
        $this->db->from('contents');
        $this->db->where($array);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function isContentsSubtitleExists($content_id)
    {
        $array = array('id' => $content_id);

        $this->db->select('id');
        $this->db->from('contents');
        $this->db->where($array);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }
}