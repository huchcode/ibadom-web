<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Department_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function search_department($master_user_id, $name)
    {
        $this->db->select('id as input_department_id, name as department_name, "department" as type');
        $this->db->from('department');
        $this->db->where('master_user_id', $master_user_id);
        $this->db->like('name', $name);
        $query = $this->db->get();

        $data = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $key) {
                $data[] = $key;
            }
        }
        return print_r(json_encode($data));
    }
}