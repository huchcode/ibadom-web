<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Examination_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function examination_list($book_id)
    {
        $this->db->select('a.id, a.question, a.type, b.created as actual_date, c.title as name');
        $this->db->from('examination a');
        $this->db->where('a.book_id',$book_id);
        $this->db->join('learningActual b','a.id = b.content_examination_id and b.type = "examination"','left');
        $this->db->join('book c','c.id = '.$book_id,'left');
        $this->db->order_by('a.sort');
        $query = $this->db->get();

        return $query->result();
    }
}