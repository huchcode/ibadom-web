<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class LearningAssign_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
    }

    function assignees_list_contents($book_id)
    {
    	$this->db->limit(1);
		$this->db->select("learningAssign.id as learningassign_id, '전직원' as target_name, 'all' as target_type");
		$this->db->from('learningAssign');
		$this->db->where('learningAssign.book_id',$book_id);
		$this->db->where('learningAssign.type','all');
		$query = $this->db->get();
		$query1 = $this->db->last_query();
		// print_r($query1); exit;

		if($query->num_rows() > 0) {
            return $query->result();
        }
		
		$this->db->select('learningAssign.id as learningassign_id, department.name as target_name, learningAssign.type as target_type');
		$this->db->from('learningAssign');
		$this->db->where('learningAssign.book_id',$book_id);
		$this->db->where('learningAssign.type','department');
		$this->db->join('department', 'department.id = learningAssign.target'); 
		$this->db->get();
		$query2 =  $this->db->last_query();

		// print_r($query2); exit;

		$where_array = array('a.book_id' => $book_id);
		$this->db->select("a.id as learningassign_id, if (c.name is null, CONCAT(b.name, ' ( )'), CONCAT(b.name , ' (' , c.name, ')')) as target_name, a.type as target_type");
		$this->db->from('learningAssign a');
		$this->db->where($where_array);
		$this->db->where('a.type','employee');
		$this->db->join('employee b', 'a.target = b.id');
		$this->db->join('department c', 'b.department_id = c.id', 'left');
		$this->db->get();
		$query3 =  $this->db->last_query();

		// print_r($query3); exit;

		$query = $this->db->query($query1." UNION ".$query2." UNION ".$query3);

		return $query->result();
    }

    function search_employee_contents($login_master_user_id, $name, $book_id)
    {
    	//All
    	$this->db->limit(1);
    	$where_array = array('a.master_user_id' => $login_master_user_id, 'b.id' => null, 'a.status' => 'active');
    	$this->db->select("a.id as target_id, '전직원' as target_name, 'all' as target_type, a.status as target_status, b.id as null_id");
		$this->db->from('employee a');
		$this->db->where($where_array);
		if(!empty($name)) {
			$this->db->like('a.name', $name);
			$this->db->join('learningAssign b', 'b.book_id = '.$book_id.' AND b.type = "all"');
		} else {
			$this->db->join('learningAssign b', 'b.book_id = '.$book_id.' AND b.type = "all"', 'left');
		}
		$this->db->get();
		$query1 =  $this->db->last_query();

		if(empty($name)) {
			$query = $this->db->query($query1);
			$contents = array();        
	        if($query->num_rows() > 0) {
	            foreach ($query->result() as $row) {
	                $contents[] = $row;
	            }
	        }
	        return print_r(json_encode($contents));
	    }

    	//Employee
    	$where_array = array('a.master_user_id' => $login_master_user_id, 'b.id' => null, 'a.status' => 'active');
		$this->db->select("a.id as target_id, a.name as target_name, 'employee' as target_type, a.status as target_status, b.id as null_id");
		$this->db->from('employee a');
		$this->db->where($where_array);
		if(!empty($name)) {
			$this->db->like('a.name', $name);
		}
		$this->db->join('learningAssign b', 'b.target = a.id AND b.book_id = '.$book_id.' AND b.type = "employee"', 'left');
		$this->db->get();
		$query2 =  $this->db->last_query();

		//Department
		$like_array = array('a.master_user_id' => $login_master_user_id, 'b.id' => null);
		$this->db->select("a.id as target_id, a.name as target_name, 'department' as target_type, a.status as target_status, b.id as null_id");
		$this->db->from('department a');
		$this->db->where($like_array);
		if(!empty($name)) {
			$this->db->like('a.name', $name);
		}
		$this->db->join('learningAssign b', 'a.id = b.target AND b.book_id = '.$book_id.' AND b.type = "department"', 'left');
		$this->db->get();
		$query3 =  $this->db->last_query();

		$query = $this->db->query($query1." UNION ".$query2." UNION ".$query3);

		$contents = array();        
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        return print_r(json_encode($contents));
    }

    function search_employee_people($login_master_user_id, $name)
    {
    	//All
    	$this->db->limit(1);
    	$where_array = array('a.master_user_id' => $login_master_user_id, 'a.status' => 'active');
    	$this->db->select("a.id, '전직원' as name, 'all' as usertype, a.status");
		$this->db->from('employee a');
		$this->db->where($where_array);
		$this->db->get();
		$query1 =  $this->db->last_query();

		if(empty($name)) {
			$query = $this->db->query($query1);
			$contents = array();        
	        if($query->num_rows() > 0) {
	            foreach ($query->result() as $row) {
	                $contents[] = $row;
	            }
	        }	        
	        return print_r(json_encode($contents));
	    }

    	$master_user_id = $this->session->userdata('master_user_id');

        $array = array('master_user_id' => $master_user_id, 'status' => 'active');

        $this->db->select('id, name, "employee" as usertype, status');
        $this->db->from('employee');
        $this->db->where($array);
        $this->db->like('name', $name);
        $this->db->get();
        $query2 =  $this->db->last_query();

        $array = array('master_user_id' => $master_user_id, 'status' => 'active');

        $this->db->select('id, name, "department" as usertype, status');
        $this->db->from('department');
        $this->db->where($array);
        $this->db->like('name', $name);
        $this->db->get();
        $query3 =  $this->db->last_query();

        $query = $this->db->query($query1." UNION ".$query2." UNION ".$query3);

		$contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        return print_r(json_encode($contents));
    }

    function assignees_list_people($master_user_id, $id, $usertype)
    {
		$where_array = array('learningAssign.master_user_id' => $master_user_id, 'learningAssign.type' => 'all');
		$this->db->select("learningAssign.id as learningassign_id, book.title as book_name, if (department.name is null, employee.name, CONCAT(employee.name , ' (' , department.name, ')')) as target_name, learningAssign.type as target_type");
		$this->db->from('learningAssign');
		$this->db->where($where_array);
		$this->db->join('book', 'book.id = learningAssign.book_id');
		$this->db->join('employee', 'employee.user_id = book.author'); 
		$this->db->join('department', 'department.id = employee.department_id', 'left');
		$this->db->get();
		$query1 =  $this->db->last_query();
		// print_r($query1); exit;
		if($usertype == "all") {
			$query1 = $this->db->query($query1);
			return $query1->result();
		}

		if($usertype != "department") {
			$this->db->select('department_id');
			$this->db->from('employee');
			$this->db->where('id', $id);
			$query = $this->db->get();
			if($query->num_rows() > 0) {
            	$dept_id = $query->row()->department_id;
            } else {
            	$dept_id = 0;
            }
			$where_array = array('learningAssign.target' => $dept_id, 'learningAssign.master_user_id' => $master_user_id, 'learningAssign.type' => 'department'); 
		} else {
			$where_array = array('learningAssign.target' => $id, 'learningAssign.master_user_id' => $master_user_id, 'learningAssign.type' => 'department'); 
		}
		$this->db->select("learningAssign.id as learningassign_id, book.title as book_name, if (department.name is null, employee.name, CONCAT(employee.name , ' (' , department.name, ')')) as target_name, learningAssign.type as target_type");
		$this->db->from('learningAssign');
		$this->db->where($where_array);
		$this->db->join('book', 'book.id = learningAssign.book_id');
		$this->db->join('employee', 'employee.user_id = book.author'); 
		$this->db->join('department', 'department.id = employee.department_id', 'left');
		$this->db->get();
		$query2 =  $this->db->last_query();
		// print_r($query2); exit;
		if($usertype == "department") {
			$query2 = $this->db->query($query1." UNION ".$query2);
			return $query2->result();
		}

		$where_array = array('learningAssign.target' => $id, 'learningAssign.master_user_id' => $master_user_id, 'learningAssign.type' => 'employee');
		$this->db->select("learningAssign.id as learningassign_id, book.title as book_name, if (department.name is null, employee.name, CONCAT(employee.name , ' (' , department.name, ')')) as target_name, learningAssign.type as target_type");
		$this->db->from('learningAssign');
        $this->db->where($where_array);
		$this->db->join('book', 'book.id = learningAssign.book_id');
		$this->db->join('employee', 'employee.user_id = book.author'); 
		$this->db->join('department', 'department.id = employee.department_id', 'left');
		$this->db->get();
		$query3 =  $this->db->last_query();
		print_r($query3); exit;
		$query = $this->db->query($query1." UNION ".$query2." UNION ".$query3);
		return $query->result();
    }

    function add_assignees($master_user_id, $book_id, $target_type, $target_id)
    {
    	if($target_type == 'all') {
    		$target_id = 0;
    	}

    	$data = array(
		   'book_id' => $book_id,
		   'master_user_id' => $master_user_id,
		   'type' => $target_type,
		   'target' => $target_id
		);

		$this->db->trans_start();

		if($target_type == 'all') {
			$this->db->delete('learningAssign', array('book_id' => $book_id));
		} else {
			$this->db->delete('learningAssign', array('book_id' => $book_id, 'type' => 'all'));
		}

        $query = $this->db->insert('learningAssign', $data);
        $this->db->trans_complete();

   		return 'Success';
    }

    function delete_assignees($id)
    {
		$this->db->trans_start();

		$this->db->select("status");
		$this->db->from('learningAssign');
		$this->db->where('id', $id);
		$query = $this->db->get();

		if($query->num_rows() > 0) {
            if($query->row()->status != 'assign'){
            	$data = array(
				   'status' => 'deleted',
				   'modified' => date("Y-m-d H:i:s")
				);
            	$book = array(
				   'id' => $id
				);
            	$this->db->where($book);
        		$this->db->update('learningAssign', $data);
            } else {
            	$this->db->delete('learningAssign', array('id' => $id)); 
            }
        } else {
            return false;
        }

        $this->db->trans_complete();

   		return true;
    }
}