<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function created_contents_list($author_id)
    {
        $this->db->select('book.id as item_id, book.title as title, book.status as current_status, book.modified, count(examination.id) as count');
        $this->db->from('book');
        $this->db->where('book.author', $author_id);
        $this->db->join('contents', 'contents.book_id = book.id', 'left');
        $this->db->join('examination', 'examination.content_id = contents.id', 'left');
        $this->db->group_by('book.id');
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $key) {
                $contents[] = $key;
            }
        }

        return $contents;
    }

    function create_content_basic($author_id, $title, $description, $master_user_id, $icon_id, $background_mindmap_id)
    {
        $book = array(
           'author' => $author_id,
           'title' => $title,
           'description' => $description,
           // 'master_user_id' => $master_user_id,
           'book_icon' => $icon_id,
           'background_mindmap' => $background_mindmap_id, //Temporary backGroundMindMap ID
           'pictogramBackground' => 1,
           'category_id' => ''
           );

        $this->db->trans_start();
        $query = $this->db->insert('book', $book);
        $insert_id = $this->db->insert_id();

        // $bookTemplate = array(
        //     'book_id' => $insert_id,
        //     'book_icon' => $icon_id, //Temporary iconContents ID
        //     'background_mindmap' => $background_mindmap_id //Temporary backGroundMindMap ID
        //     );
        // $query = $this->db->insert('bookTemplate', $bookTemplate);

        $bookHistory = array(
            'book_id' => $insert_id,
            'user_id' => $author_id,
            'comment' => 'created'
            );
        $query = $this->db->insert('bookHistory', $bookHistory);

        $contents = array(
            'book_id' => $insert_id,
            'title' => $title,
            'connection_type' => 'root',
            'pictogram_id' => 0, //Temporary Pictogram ID
            'parent_content_id' => 0,
            'sort' => 0,
            'mindmap_angle' => 0,
            'pictogramBackground' => 1,
            // 'distance' => 0
            );
        $query = $this->db->insert('contents', $contents);
        $insert_id = $this->db->insert_id();

        $data = array(
               'parent_content_id' => $insert_id,
               'modified' => date("Y-m-d H:i:s")
               );

        $this->db->where('id', $insert_id);
        $this->db->update('contents', $data);
        $this->db->trans_complete();

        return 'Success';
    }

    function get_book_info($book_id)
    {
        if( ! empty($book_id)) {
            $this->db->select('title, description, iconContents.name as icon_name, iconContents.picture as icon_picture, backGroundMindMap.name as bgmindmap_name, backGroundMindMap.picture as bgmindmap_picture, pictogramBackground.picture as pictogramBackground_picture');
            $this->db->from('book');
            $this->db->where('book.id',$book_id);
            $this->db->join('iconContents', 'iconContents.id = book.book_icon', 'left');
            $this->db->join('backGroundMindMap', 'backGroundMindMap.id = book.background_mindmap', 'left');
            $this->db->join('pictogramBackground', 'pictogramBackground.id = book.pictogramBackground', 'left');
            $this->db->group_by('book.id');
            $query = $this->db->get();

            if($query->num_rows() > 0) {
                return $query->row();
            } else {
                return null;
            }
        }
    }

    function get_background_mindmap_id($book_id)
    {
        if( ! empty($book_id)) {
            $this->db->select('background_mindmap');
            $this->db->from('book');
            $this->db->where('id',$book_id);            
            $query = $this->db->get();

            if($query->num_rows() > 0) {
                return $query->row();
            } else {
                return null;
            }
        }
    }

    function get_background_mindmap_picture($background_mindmap)
    {
        if( ! empty($background_mindmap)) {
            $this->db->select('picture');
            $this->db->from('backGroundMindMap');
            $this->db->where('id',$background_mindmap);            
            $query = $this->db->get();

            if($query->num_rows() > 0) {
                return $query->row();
            } else {
                return null;
            }
        }
    }

    function get_book_title($book_id)
    {
        if( ! empty($book_id)) {
            $this->db->select('title');
            $this->db->from('book');
            $this->db->where('book.id',$book_id);
            $query = $this->db->get();

            if($query->num_rows() > 0) {
                return $query->row();
            } else {
                return null;
            }
        }
    }

    function edit_title($edit_title, $book_id)
    {
        if( ! empty($book_id)) {
            $data = array(
               'title' => $edit_title,
               'modified' => date("Y-m-d H:i:s")
               );
            $this->db->where('id', $book_id);
            $this->db->update('book', $data);
        }
    }

    function edit_description($edit_description, $book_id)
    {
        if( ! empty($book_id)) {
            $data = array(
               'description' => $edit_description,
               'modified' => date("Y-m-d H:i:s")
               );
            $this->db->where('id', $book_id);
            $this->db->update('book', $data);
        }
    }

    function change_status_book($book_id)
    {
        if( ! empty($book_id)) {
            $this->db->select('status');
            $this->db->from('book');
            $this->db->where('book.id',$book_id);
            $query = $this->db->get();

            //print_r($query->row()->status);exit;

            $data = array();

            if($query->row()->status == 'compilation') {
                $data['status'] = 'requested';
            }

            if ($query->row()->status == 'requested') {
                $data['status'] = 'compilation';
            }

            if ($query->row()->status == 'rejected') {
                $data['status'] = 'requested';
            }

            if ($query->row()->status == 'closed') {
                $data['status'] = 'requested';
            }

            $data['modified'] = date("Y-m-d H:i:s");

            $this->db->where('id', $book_id);
            $this->db->update('book', $data);
        }
    }

    function change_book_status($book_id,$new_status)
    {
        $data['status'] = $new_status;

        if ($new_status=='released') {
            $data['release_status'] = 'ongoing';
        } elseif ($new_status=='closed') {
            $data['release_status'] = 'closed';
        }

        $data['modified'] = date("Y-m-d H:i:s");

        $this->db->where('id', $book_id);
        return $this->db->update('book', $data);
    }

    function check_valid_book($book_id)
    {
        // $user_id = $this->session->userdata('user_id');

        // $array = array('book.id =' => $book_id, 'author =' => $user_id);

        //$master_user_id = $this->session->userdata('master_user_id');

        $array = array('book.id' => $book_id);

        $this->db->select('*');
        $this->db->from('book');
        $this->db->where($array);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function isDeletable($book_id)
    {
        $array = array('book.id =' => $book_id, 'release_status <>' => 'ongoing');
        // $array = array('book.id =' => $book_id, 'release_status' => 'creating');

        $this->db->select('id');
        $this->db->from('book');
        $this->db->where($array);
        $query = $this->db->get();

        if($query->num_rows() > 0) {
            return true;
        } else {
            return false;
        }
    }

    function get_department_info() {

        $master_user_id = $this->session->userdata('master_user_id');

        $this->db->select('department.name, department.id');
        $this->db->from('department');
        $this->db->where('department.master_user_id',$master_user_id);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        return $contents;
    }

    function get_content_inspection_list($book_status = null,$department_id = null) {

        $master_user_id = $this->session->userdata('master_user_id');

        $this->db->select('book.id as book_id, book.title as title, employee.name as username, department.name as deptname, book.description as description, book.status as current_status');
        $this->db->from('book');
        $this->db->join('employee', 'employee.user_id=book.author ', 'left');
        $this->db->join('department', 'department.id=employee.department_id', 'left');

        $this->db->where('book.master_user_id', $master_user_id);
        if (!empty($book_status)) {
            $this->db->where('book.status', $book_status);
        }
        if (!empty($department_id)) {
            $this->db->where('employee.department_id', $department_id);
        }
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        return $contents;
    }

    function search_book($text)
    {
        $master_user_id = $this->session->userdata('master_user_id');

        // $where_array =array('master_user_id' => $master_user_id, 'release_status' => 'creating');
        $where_array =array('master_user_id' => $master_user_id);
        $array = array('title' => $text);

        $this->db->select('id, title, "book" as type');
        $this->db->from('book');
        $this->db->where($where_array);
        $this->db->like($array);
        $query = $this->db->get();

        //print_r($this->db->last_query());

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        print_r(json_encode($contents));

        // return json_encode($contents);
    }

    function search_book_people($text, $targetid)
    {
        $master_user_id = $this->session->userdata('master_user_id');

        // $where_array =array('book.master_user_id' => $master_user_id, 'release_status' => 'creating', );
        $where_array =array('book.master_user_id' => $master_user_id);
        $array = array('title' => $text);

        $this->db->select('book_id');
        $this->db->from('learningAssign');
        $this->db->where('target', $targetid);
        $query = $this->db->get();

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row->book_id;
            }
        }

        $this->db->select('book.id, book.title');
        $this->db->from('book');
        $this->db->where($where_array);
        if(!empty($contents)) {
            $this->db->where_not_in('book.id', $contents);
        }
        $this->db->like($array);
        $query = $this->db->get();

        //print_r($this->db->last_query()); exit;

        //print_r($this->db->last_query());

        $contents = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $contents[] = $row;
            }
        }

        print_r(json_encode($contents));

        // return json_encode($contents);
    }

    function get_pictogramBackground_contents()
    {
        $this->db->select('id, picture');
        $this->db->from('pictogramBackground');
        $query = $this->db->get();

        $data = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }

        return $data;
    }

    function get_icon_contents($master_user_id, $name = '')
    {
        $array = array(0, $master_user_id);
        $this->db->select('iconContents.id, iconContents.picture, iconContents.name, iconContents.creator, user.name as username');
        $this->db->from('iconContents');
        // $this->db->where_in('iconContents.master_user_id', $array);
        $this->db->join('user', 'user.id = iconContents.creator');
        if($name != ''){
            $this->db->like('name', $name);
        }
        $query = $this->db->get();

        $data = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }

        return $data;
    }

    function get_background_mindmap($master_user_id, $name = '')
    {
        $array = array(0, $master_user_id);
        $this->db->select('backGroundMindMap.id, backGroundMindMap.picture, backGroundMindMap.name, backGroundMindMap.creator, user.name as username');
        $this->db->from('backGroundMindMap');
        // $this->db->where_in('backGroundMindMap.master_user_id', $array);
        $this->db->join('user', 'user.id = backGroundMindMap.creator');
        if($name != ''){
            $this->db->like('name', $name);
        }
        $query = $this->db->get();

        $data = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }

        return $data;
    }

    function get_pictogram_contents($master_user_id, $name = '')
    {
        //$array = array($master_user_id);
        $array = array(0, $master_user_id);
        $this->db->select('pictogram.id, pictogram.picture, pictogram.name, pictogram.creator, user.name as username');
        $this->db->from('pictogram');
        $this->db->where_in('pictogram.master_user_id', $array);
        $this->db->join('user', 'user.id = pictogram.creator');
        if($name != ''){
            $this->db->like('name', $name);
        }
        $query = $this->db->get();

        $data = array();
        if($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $data[] = $row;
            }
        }

        return $data;
    }

    function update_content_pictogram($content_id, $icon_id)
    {
        $data['pictogram_id'] = $icon_id;
        $data['modified'] = date("Y-m-d H:i:s");

        $this->db->where('id', $content_id);
        return $this->db->update('contents', $data);
    }

    function update_content_pictogrambg($content_id, $icon_id)
    {
        $data['pictogramBackground'] = $icon_id;
        $data['modified'] = date("Y-m-d H:i:s");

        $this->db->where('id', $content_id);
        return $this->db->update('contents', $data);
    }

    function update_pictogram_bg($book_id, $icon_id)
    {
        $data['pictogramBackground'] = $icon_id;
        $data['modified'] = date("Y-m-d H:i:s");

        $this->db->where('id', $book_id);
        return $this->db->update('book', $data);
    }

    function update_icon($book_id, $icon_id)
    {
        $data['book_icon'] = $icon_id;
        $data['modified'] = date("Y-m-d H:i:s");

        $this->db->where('id', $book_id);
        return $this->db->update('book', $data);
    }

    function update_bgmindmap($book_id, $bgmindmap_id)
    {
        $data['background_mindmap'] = $bgmindmap_id;
        $data['modified'] = date("Y-m-d H:i:s");

        $this->db->where('id', $book_id);
        return $this->db->update('book', $data);
    }

    function get_book_author_id($book_id)
    {
        $this->db->select('author');
        $this->db->from('book');
        $this->db->where('id', $book_id);
        $query = $this->db->get();

        return $query->row()->author;
    }
}