<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Learningprogress_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function progress_list($master_user_id, $cond, $id)
    {
    	$where = "";

    	if($cond == 'all') {
			$where = "AND e.master_user_id = ".$master_user_id;
		}
		if($cond == 'department') {
			$where = "AND e.department_id = ".$id;
		}
		if($cond == 'employee') {
			$where = "AND e.user_id = ".$id;
		}
		if($cond == 'book') {
			$where = "AND e.master_user_id = ".$master_user_id." AND a.book_id = ".$id;
		}

    	$query = "SELECT a.master_user_id, e.user_id, if(d.name is null,e.name,concat(e.name,' (',d.name,')')) as name, a.book_id, k.title, 
					       count(distinct b.id) as learnPlan, count(distinct bb.id) as learnActual, 
					       count(distinct c.id) as examPlan, count(distinct cc.id) as examActual 
					FROM learningAssign a 
					JOIN book k ON a.book_id = k.id
					JOIN employee e
					LEFT JOIN department d ON e.department_id = d.id
					LEFT JOIN contents b ON a.book_id = b.book_id
					LEFT JOIN learningActual bb ON bb.user_id = e.user_id and bb.type = 'learning' and b.id = bb.content_examination_id
					LEFT JOIN examination c ON a.book_id = c.book_id
					LEFT JOIN learningActual cc ON cc.user_id = e.user_id and cc.type = 'examination' and c.id = cc.content_examination_id 
					WHERE ((a.master_user_id = $master_user_id and a.type = 'all') OR (a.type = 'department' and a.target = e.department_id) OR (a.type = 'employee' and a.target = e.id))
					$where
					GROUP BY d.name, e.name, a.book_id";

		$query = $this->db->query($query);

		//$query = $this->db->get();
		// $query3 =  $this->db->last_query();
		// print_r($query3); exit;

		return $query->result();
    }

    function progress_list_all($master_user_id)
    {
    	$where_array = array('a.master_user_id' => $master_user_id);
		$this->db->select("a.user_id as user_id, if (c.name is null, b.name, CONCAT(b.name, ' (',c.name, ')')) as name,  b.title as title, if (learning_actual_count = 0,0,learning_actual_count / learning_target_count * 100) as learning_rate, if (examination_actual_count = 0,0,examination_actual_count / examination_target_count * 100) as examination_rate");
		$this->db->from('learningProgress a');
		$this->db->where($where_array);
		$this->db->join('book b','a.book_id = b.id');
		$this->db->join('employee c','b.author = c.id');
		$this->db->join('department d','c.department_id = d.id','left');
		$query = $this->db->get();

		return $query->result();
    }
}