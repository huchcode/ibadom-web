<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_status_history_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function get_user_status_history($user_id)
    {
        $this->db->select('*');
        $this->db->from('userStatusHistory');
        $this->db->where('user_id',$user_id);
        $query = $this->db->get();

        $array = array();
        if($query->num_rows() > 0) {

            foreach ($query->result() as $row)
            {
               $array[] = $row;
            }
            return $array;

        } else {
            return null;
        }
    }

}