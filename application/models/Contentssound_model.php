<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ContentsSound_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->model('User_model');
        $this->load->model('Upload_model');
    }

    function insert_contentsSound($content_id) {
      $i=1;
      $x=1;
      $reset = true;
      $soundPath = '/assets/uploads/contentsSound/';

      if(!empty($_POST['sound_delete1'])) {
        //print_r($_POST); exit;
        $this->db->trans_begin();
        foreach($_POST as $key =>$value){
          if(!empty($key))
          {
            if($key == 'sound_delete'.$x) {
              $this->db->delete('contentsSound', array('voice' => $value));
              if(is_file(FCPATH.$value)) {
                unlink(FCPATH.$value);
              }
              $x++;
            }
          }
        }

        if(empty($_FILES['sound_content1']['name'])) {
          return $this->db->trans_complete();
        } else {
          //$this->db->delete('contentsAlbum', array('content_id' => $content_id));
        }
      }

      foreach ($_FILES as $file) {
        //print_r($_FILES); exit;
        if(!empty($file['name'])) {
          $data = $this->Upload_model->upload($content_id, '/assets/uploads/contentsSound/', 'sound_content'.$i, 'contentsSound'.$i, 'mp3', '20480');

          //print_r($data); exit;
          if(isset($data['upload_data'])) {
            $contents = array(
            'content_id' => $content_id,
            'voice' => $soundPath.$data['upload_data']['file_name'],
            'modified' => date("Y-m-d H:i:s")
            );
            $query = $this->db->insert('contentsSound', $contents);
          } else {
            $this->db->trans_rollback();
            return $data;
          }
        }
        $i++;
      }

      return $this->db->trans_complete();
    }

    function get_contentsSound($content_id) {
      $this->db->select('voice');
      $this->db->from('contentsSound');
      $this->db->where('content_id',$content_id);
      $query = $this->db->get();

      $contents = array();        
      if($query->num_rows() > 0) {
          foreach ($query->result() as $row) {
              $contents[] = $row;
          }
      }

      print_r(json_encode($contents));
    }
}