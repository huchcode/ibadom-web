<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pictogram_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->database();
        $this->load->helper('array');
    }

    function upload_icon($name, $creator, $master_user_id)
    {
    	if(empty($name)) {
    		$name = 'pictogram';
    	}

    	$this->db->trans_begin();

    	$data = array(
            'name' => $name,
            'picture' => $_FILES['pictogram_content']['name'],
            'creator' => $creator,
            'master_user_id' => $master_user_id,
            'modified' => date("Y-m-d H:i:s")
            );

        $this->db->insert('pictogram', $data);
        $insert_id = $this->db->insert_id();

        foreach ($_FILES as $file) {
        //print_r($_FILES); exit;
        if(!empty($file['name'])) {
          $data = $this->Upload_model->upload($insert_id, '/assets/pictogram/', 'pictogram_content', $name, 'gif|jpg|png', '200');

          //print_r($data); exit;
          if(isset($data['upload_data'])) {
          	$icon_url = '/assets/pictogram/'.$data['upload_data']['file_name'];
            $data = array(
            	'name' => $data['upload_data']['raw_name'],
              'picture' => $icon_url,
              'modified' => date("Y-m-d H:i:s")
            );

	        $this->db->where('id', $insert_id);
	        $this->db->update('pictogram', $data);
          } else {
            $this->db->trans_rollback();
            return $data;
          }
        }
      }

      return $this->db->trans_complete();
    }

    function delete_pictogram_contents($id, $userid, $role)
    {
        $this->db->trans_start();
        if($role == 'master') {
          $this->db->delete('pictogram', array('id' => $id, 'master_user_id' => $userid)); 
        } else {
          $this->db->delete('pictogram', array('id' => $id, 'creator' => $userid, 'master_user_id !=' => 0)); 
        }        
        return $this->db->trans_complete();
    }

    function edit_pictogram_contents($id, $name, $userid, $role)
    {
        $this->db->trans_start();
        $data = array(
              'name' => $name,
              'modified' => date("Y-m-d H:i:s")
            );
        if($role == 'master') {
          $this->db->where(array('id' => $id, 'master_user_id' => $userid));
        } else {
          $this->db->where(array('id' => $id, 'creator' => $userid, 'master_user_id !=' => 0));
        }        
        $this->db->update('pictogram', $data);
        return $this->db->trans_complete();
    }
}