<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Upload_model extends CI_Model {

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
        $this->load->helper('array');
    }

    function upload($id, $path, $field, $filename, $allowed_types = 'gif|jpg|png', $max_size, $reset = false)
    {
        if (!is_dir(FCPATH.$path)) {
            mkdir(FCPATH.$path, 0777, true);
        }

        if(!$reset) {
            $config['upload_path'] = FCPATH.$path;
            $config['allowed_types'] = $allowed_types;
            $config['max_size'] = $max_size;
            $config['overwrite'] = TRUE;
            $this->load->library('upload', $config);
        } else {
            $config['upload_path'] = FCPATH.$path;
            $config['allowed_types'] = $allowed_types;
            $config['max_size'] = $max_size;
            $config['overwrite'] = TRUE;
            $this->upload->initialize($config);
        }

        if ( ! $this->upload->do_upload($field, $id."_".$filename)) {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }
}