var x0,
y0,
parent_container,
container,
zoom_label,
root_index,
cont_index,
container_x = 0,
container_y = 0,
scale_x0 = 0,
scale_y0 = 0;

function initMindMap() {

  container_x = 0,
  container_y = 0,
  scale_x0 = 0,
  scale_y0 = 0;

  canvas.width = canvas_width;
  canvas.height = canvas_height;
  x0 = canvas.width/2*1/scale;
  y0 = canvas.height/2*1/scale;

  parent_container = new createjs.Container(),
  container = new createjs.Container();
  container.scaleX = scale;
  container.scaleY = scale;
  container.id = 'main';

  var rect = new createjs.Shape();
  var bg = new Image();

  bg.setAttribute('crossOrigin', 'anonymous');
  bg.onload = function(){
    graphics = rect.graphics;
    graphics.beginBitmapFill(bg);
    graphics.drawRect(0, 0, canvas.width, canvas.height);
    rect.id = 'container';
  }

  bg.src = mindmap_bg;

  parent_container.addChild(rect);

  rect.on("mouseover", function (evt) {
    zoom_label.set({ text: 'x:' + evt.stageX + ' y:' + evt.stageY});
  });

  rect.on("mousedown", function (evt) {
    root_x = plots[root_id].coord.x;
    root_y = plots[root_id].coord.y;
    this.parent.addChild(this);
    parent_container.setChildIndex(this, 0);
    var offset = {};
    offset['x'] = this.parent.children[1].x - evt.stageX;
    offset['y'] = this.parent.children[1].y - evt.stageY;
    this.parent.children[1].offset = offset;
  });

  rect.on("pressmove", function (evt) {
    var xx = evt.stageX + this.parent.children[1].offset.x,
    yy = evt.stageY + this.parent.children[1].offset.y;

    container_x = xx-scale_x0, container_y = yy-scale_y0;

    this.parent.children[1].x = xx;
    this.parent.children[1].y = yy;
    parent_container.children[2].visible = false;
    tick();
  });

  rect.on("rollover", function (evt) {
    this.scaleX = this.scaleY = this.scale * 1.2;
    stage.update();
  });

  rect.on("rollout", function (evt) {
    this.scaleX = this.scaleY = this.scale;
    stage.globalToLocal(stage.mouseX, stage.mouseY);
  });

  async.waterfall([
    function(cb){
      $.each(items, function(i, v){
        if( v.connection_type == 'root' ||  v.connection_type == 'connection'){
          if( v.connection_type == 'root' ){
            if( !plots[v.id] ){
              var coordinates = getCoordinates(0, 0, x0, y0);
              plots[v.id] = { 'coord': { x: coordinates.x, y: coordinates.y }, 'mindmap_angle':parseFloat(v.mindmap_angle), 'distance': parseFloat(v.distance), 'parent_content_id': v.parent_content_id};
            }
            root_id = v.id;
          } else {
            if( !plots[v.id] ){
              var c = plots[v.parent_content_id] ? plots[v.parent_content_id].coord : { 'x': x0, 'y':y0 } ;
              var sx = c.x,
              sy = c.y;
              var coordinates = getCoordinates(v.mindmap_angle, v.distance, sx, sy);
              plots[v.id] = { 'coord': { x: coordinates.x, y: coordinates.y }, 'mindmap_angle':parseFloat(v.mindmap_angle), 'distance': parseFloat(v.distance), 'parent_content_id': v.parent_content_id};
            }
          }
          drawCircle(v);
        }

        if( items.length == i+1 ){
          cb(null, 1);
        }
      });
    },
    function(arg, cb){
      var t = setTimeout(function(){
        $.each(lines, function(inx, val){
          container.addChild(val);
        });
        if( lines.length > 0 ){
          clearTimeout(t);
        }
      }, 1000);
      cb(null, 2);
    },
    function(arr, cb){
      var t = setTimeout(function(){
        $.each(maps, function(inx, val){
          container.addChild(val);
          if( parseInt(root_id) == parseInt(val.name) )
            root_index = container.getChildIndex(val);
          if( val.typ == 'c' ){
            if( !childrens[val.parent_content_id] ){
              childrens[val.parent_content_id] = '';
            }
            childrens[val.parent_content_id] += container.getChildIndex(val) + ',';
          }
        });

        if( maps.length > 0 ){
          clearTimeout(t);
        }
      }, 1000);
      cb(null, 3);
    }
  ], function(err, results){
    parent_container.addChild(container);

    zoom_label = new createjs.Text();
    zoom_label.set({ text: Math.round(scale*100) + "%" });
    zoom_label.x = 10;
    zoom_label.y = 10;
    zoom_label.visible = false;

    parent_container.addChild(zoom_label);

    stage.addChild(parent_container);
    var t = setTimeout(function(){
      tick();
      clearTimeout(t);
    }, 1000);
  });

  $('#mindmap_canvas').on('mousewheel', function(event) {
    if( event.deltaY > 0 ){
      scale += 0.1;
      if( scale > 2 )
        scale = 2;
    } else {
      scale -= 0.1;
      if( scale < 0.5 )
        scale = 0.5;
    }

    scale_x0 = (canvas_width-canvas_width*scale)/2, scale_y0 = (canvas_height-canvas_height*scale)/2;

    container.x = container_x + (canvas_width-canvas_width*scale)/2;
    container.y = container_y + (canvas_height-canvas_height*scale)/2;

    container.scaleX = scale;
    container.scaleY = scale;
    parent_container.children[2].visible = true;
    parent_container.children[2].set({ text: Math.round(scale*100) + "%" });
    tick();
  });

  $('.normal').click(function(){
    scale = 1;
    container.scaleX = scale;
    container.scaleY = scale;
    tick();
  });

  $('.zoomin').click(function(e){
    scale += 0.1;
    if( scale > 2 )
      scale = 2;
    container.scaleX = scale;
    container.scaleY = scale;
    tick();
    e.preventDefault();
  });

  $('.zoomout').click(function(e){
    scale -= 0.1;
    if( scale < 0.5 )
      scale = 0.5;
    container.scaleX = scale;
    container.scaleY = scale;
    tick();
    e.preventDefault();
  });
}

var getCoordinates = function(angle, distance, sx, sy){
  var a = angle*Math.PI/180,
  x = distance *  Math.cos(parseFloat(a)) * cr,
  y = distance * Math.sin(a) * cr;
  if( x > 0 && y > 0 ){
    x = sx + x;
    y = sy - y;
  } else if ( x < 0 && y > 0) {
    x = sx + x;
    y = sy - y;
  } else if ( x < 0 && y < 0) {
    x = sx + x;
    y = sy - y;
  } else {
    x = sx + x;
    y = sy - y;
  }

  return {x:x,y:y};
}

var getAngle = function(x1, x2, y1, y2){
  var deltaX = x2 - x1;
  var deltaY = y2 - y1;
  var rad = Math.atan2(deltaY, deltaX);
  var r = rad * (180 / Math.PI);
  var a = 0;
  if( r < 0 )
    a = r*-1
  else
    a = 360-r;
  return a;
};

var getDistance = function(x1, x2, y1, y2){
  var deltaX = x2 - x1;
  var deltaY = y2 - y1;
  return Math.sqrt((Math.pow(deltaX, 2) + Math.pow(deltaY, 2)));
};

var drawLine = function(id, index){
  var line = new createjs.Shape(),
  plot = plots[id];
  line.graphics.setStrokeStyle(2);
  line.graphics.beginStroke('#99ffdd');
  line.graphics.moveTo(plots[plot.parent_content_id].coord.x, plots[plot.parent_content_id].coord.y);
  line.graphics.lineTo(plot.coord.x, plot.coord.y);
  line.graphics.endStroke();
  line.name = 'l'+id;
  line.typ = 'l';
  line.modify = 0;
  if( typeof index != 'undefined' ){
    container.addChild(line);
    container.setChildIndex(line, index);
  } else {
    lines.push(line);
  }
}

var createText = function(tx, ty, id, word, parent_tree, cont){
  var text = new createjs.Text();
  text.set({ text: word });
  var b = text.getBounds();
  text.x = tx-(text._rectangle.width/2);
  text.y = ty+cr+3;
  text.name = id;
  text.label_for = id.substring(1,id.length);
  text.parent_tree = parent_tree;
  text.typ = 't';
  maps.push(text);
}

var reArrangeText = function(text){
  var p = plots[text.label_for];
  text.x = p.coord.x-(text._rectangle.width/2);
  text.y = p.coord.y+cr+3;
}

var removeChild = function(name){
  var child = container.getChildByName(name);
  var child_index = container.getChildIndex(child);
  container.removeChildAt(child_index);
  line_index = child_index;
}

var arrangeChilds = function(id){
  if( childrens[id] ){
    var childs = childrens[id].split(',');
    $.each(childs, function(i, v){
      if( v != "" ){
        var child = container.getChildAt(parseInt(v)),
        plot = plots[child.name];
        var parent_plot = plots[plot.parent_content_id];
        var coordinates = getCoordinates(plot.mindmap_angle, plot.distance, parent_plot.coord.x, parent_plot.coord.y);
        child.x = coordinates.x-cr;
        child.y = coordinates.y-cr;
        plots[child.name].coord.x = coordinates.x;
        plots[child.name].coord.y = coordinates.y;
        removeChild('l'+child.name);
        drawLine(child.name, line_index);
        var mytext = container.getChildByName('t'+child.name);
        reArrangeText(mytext);
        if( child.connection_type == 'connection' ){
          arrangeChilds(child.name);
        }
      }
    });
  }
}

var drawCircle = function(data){
  if( data.connection_type == 'root' ||  data.connection_type == 'connection' ||  data.connection_type == 'leaf'){
    mergeImage(data.pictogram_Background, data.pictogram_icon, function(dataUrl){
      var image = new Image();
      image.onload = function(){
        var connection_type = data.connection_type,
        mindmap_angle = data.mindmap_angle,
        distance = data.distance,
        id = data.id,
        title = data.title;

        var plot = plots[id];

        if( typeof plots[id] == 'undefined' ){
          var c = plots[data.parent_content_id].coord,
          sx = c.x,
          sy = c.y;
          var coordinates = getCoordinates(data.mindmap_angle, data.distance, sx, sy);
          plots[id] = { 'coord': { x: coordinates.x, y: coordinates.y }, 'mindmap_angle':parseFloat(data.mindmap_angle), 'distance': parseFloat(data.distance), 'parent_content_id': data.parent_content_id};
          plot = plots[id];
        }

        var coords = plot.coord;
        var parent_coordinate = plots[data.parent_content_id].coord;

        var circle = new createjs.Bitmap(image);

        circle.x = coords.x-cr;
        circle.y = coords.y-cr;
        circle.name = id;
        circle.typ = 'c';
        circle.parent_content_id = data.parent_content_id;
        circle.connection_type = connection_type;

        if( connection_type == 'root' || connection_type == 'connection' || connection_type == 'leaf'){
          circle.on("mousedown", function (evt) {
            var global = this.parent.localToGlobal(this.x, this.y);
            this.offset = {x: global.x - evt.stageX + (cr*scale), y: global.y - evt.stageY + (cr*scale)};
            if( connection_type == 'root' ){
              this.parent.parent.children[1].offset = {x: this.parent.parent.children[1].x - evt.stageX, y: this.parent.parent.children[1].y - evt.stageY};
            }
          });

          circle.on("pressmove", function (evt) {
            if( connection_type == 'root' ){
              var xx = evt.stageX + this.parent.parent.children[1].offset.x,
              yy = evt.stageY + this.parent.parent.children[1].offset.y;

              container_x = xx-scale_x0, container_y = yy-scale_y0;
              this.parent.parent.children[1].x = xx;
              this.parent.parent.children[1].y = yy;
            } else {
              var local = this.parent.globalToLocal(evt.stageX + this.offset.x, evt.stageY + this.offset.y);
              var selected = this;
              var nx = local.x, ny = local.y;
              selected.x = nx-cr;
              selected.y = ny-cr;
              var selected_id = selected.name;
              plots[selected_id].coord.x = nx;
              plots[selected_id].coord.y = ny;
              plots[selected_id].distance = getDistance(parent_coordinate.x, nx, parent_coordinate.y, ny)/cr;
              plots[selected_id].mindmap_angle = getAngle(parent_coordinate.x, nx, parent_coordinate.y, ny);
              removeChild('l'+selected_id);
              drawLine(selected_id, line_index);
              var mytext = container.getChildByName('t'+selected_id);
              reArrangeText(mytext);

              if( connection_type == 'connection' ){
                arrangeChilds(selected_id)
              }

              if( modified.indexOf(selected_id) == -1 )
                modified.push(selected_id);

              if( $("button#save").hasClass('hidden') ){
                $("button#save").removeClass('hidden');
                $("button#save").show();
              }
            }
            parent_container.children[2].visible = false;
            tick();
          });

          circle.on("rollover", function (evt) {
            this.scaleX = this.scaleY = this.scale * 1.2;
            stage.update();
          });

          circle.on("rollout", function (evt) {
            this.scaleX = this.scaleY = this.scale;
            stage.globalToLocal(stage.mouseX, stage.mouseY);
          });
        }

        if( connection_type != 'root' ){
          drawLine(id);
        }

        maps.push(circle);
        createText(coords.x, coords.y, 't'+id, title, data.parent_content_id);

        if( connection_type == 'connection' ){
          if( data.children ){
            $.each(data.children, function(i, v){
              drawCircle(v);
            });
          }
        }
      }
      image.src = dataUrl;
    });
  }
};

function mergeImage(pictogram_Background, pictogram_icon, callback) {
  var bg = new Image(),
  bw = cr*2,
  bh = cr*2;

  bg.setAttribute('crossOrigin', 'anonymous');
  bg.onload = function(){
    var canvas = document.createElement("canvas");
    canvas.width = bw;
    canvas.height = bh;
    var ctx = canvas.getContext("2d");

    var pictogram = new Image();
    pictogram.setAttribute('crossOrigin', 'anonymous');
    pictogram.onload = function(){
      ctx.drawImage(bg, 0, 0, bg.width * canvas.width/bg.width, bg.height * canvas.height/bg.height);
      ctx.drawImage(pictogram, 0, 0, pictogram.width * canvas.width/pictogram.width, pictogram.height * canvas.height/pictogram.height);

      var dataURL = canvas.toDataURL("image/png");
      callback(dataURL);
    }
    pictogram.src = pictogram_icon;
  }
  bg.src = pictogram_Background;
}

function tick(){
  stage.update();
}

function generate_minimap(callback){
  var canvas = document.createElement("canvas");
  var bounds = container.getBounds();
  var c = new createjs.Container(),
  w = bounds.width,
  h = bounds.height,
  x = 0,
  y = 0;
  
  if( bounds.width < 2000 ){
    w = 2000;
    x = 1000-(bounds.width/2);
  }

  if( bounds.height < 2000 ){
    h = 2000;
    y = 1000-(bounds.height/2);
  }

  canvas.width = w;
  canvas.height = h,
  s = new createjs.Stage(canvas);
  c.scaleX = 1;
  c.scaleY = 1;
  c.x = x-bounds.x;
  c.y = y-bounds.y;

  var rect = new createjs.Shape();
  rect.x = 0;
  rect.y = 0;
  var graphics = rect.graphics;
  graphics.beginFill("#333").drawRect(0, 0, w, h);
  s.addChild(rect);

  $.each(plots, function(i, v){
    var circle = new createjs.Shape();
    circle.graphics.beginFill("#fff").drawCircle(0, 0, cr);
    circle.x = v.coord.x;
    circle.y = v.coord.y;
    c.addChild(circle);

    var line = new createjs.Shape();
    line.graphics.setStrokeStyle(20);
    line.graphics.beginStroke('#fff');
    line.graphics.moveTo(plots[v.parent_content_id].coord.x, plots[v.parent_content_id].coord.y);
    line.graphics.lineTo(v.coord.x, v.coord.y);
    line.graphics.endStroke();
    c.addChild(line);
  });

  s.addChild(c);
  s.update();
  var img = new Image(),
  img_src = canvas.toDataURL("image/png");

  img.onload = function(){
    callback(img_src);
  }
  img.src = img_src;
}