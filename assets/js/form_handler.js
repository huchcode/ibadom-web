$(document).ready(function () {
	$('button.file-upload').click(function () {
		$(this).next('input[type=file]').click();
	});

	$('.custom-select').each(function (index) {
		var width = $(this).data('width') || 150;
		var placeholder = $(this).data('placeholder');
		var options = $(this).find('option');

		var customSelectContainer = $('<div>', {
			class: 'custom-select-container'
		}).css({
			width: width
		});

		$(this).after(customSelectContainer);
		customSelectContainer = $(this).next('.custom-select-container');
		customSelectContainer.append($(this));

		var customSelect = customSelectContainer.find('.custom-select');

		var customSelectPlaceholder = $('<div>', {
			class: 'custome-select__placeholder',
			text: placeholder
		});
		customSelectContainer.append(customSelectPlaceholder);
		customSelectPlaceholder = customSelectContainer.find('.custome-select__placeholder');

		var customSelectOptions = $('<div>', {
			class: 'custom-select__option-wrap'
		});

		customSelectContainer.append(customSelectOptions);
		customSelectOptions = customSelectContainer.find('.custom-select__option-wrap');

		for (var i = 0; i < options.length; i++) {
			var option = $('<div>', {
				class: 'custom-select__option',
				text: options[i].text,
				'data-value': $(options[i]).attr('value')
			});
			customSelectOptions.append(option);
		}

		customSelectPlaceholder.click(function () {
			customSelectOptions.slideToggle(150);
			if (customSelectContainer.is('.selected')) {
				customSelectContainer.delay(150).queue(function () {
					$(this).removeClass('selected').dequeue();
				});
			} else {
				customSelectContainer.addClass('selected');
			}
		});

		customSelectOptions.click(function (event) {
			var option = $(event.target);
			customSelect.val(option.data('value'));
			customSelect.attr('data-value', option.data('value'));
			customSelectPlaceholder.text(option.text());
			customSelectOptions.slideUp(150);
		});
	})
});