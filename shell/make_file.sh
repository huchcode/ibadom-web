#!/bin/sh
# Make archive file with contents data
# usage : make_file.sh 'user0000.data'

#note: after upload of this file please make sure you do the ffg.
#chmod +x make_file.sh
#chown apache.apache make_file.sh

filename=$1
shellpath="/var/www/html/gpace.huchcode.com/shell"
echo "input file :$filename.txt"

if [[ $filename == "" ]]
then
	echo ">>> missing parameter"
	exit 0
fi;

#printf "%s" "$(<$shellpath/$filename.txt)"

rm ../download/$filename.zip
echo ">>> remove folder ../download/$filename done."
mkdir "$shellpath/$filename"
echo ">>> create folder $filename done."

filetype1="pictogram"
filetype2="pictogramBackground"
filetype3="backGroundMindMap"
filetype4="iconContents"
filetype5="examination"
filetype6="contentsAlbum"

mkdir "$shellpath/$filename/$filetype1"
mkdir "$shellpath/$filename/$filetype2"
mkdir "$shellpath/$filename/$filetype3"
mkdir "$shellpath/$filename/$filetype4"
mkdir "$shellpath/$filename/$filetype5"
mkdir "$shellpath/$filename/$filetype6"

#read file list
#
for LIST in `cat $shellpath/$filename.txt`
do
	#echo "./data/$LIST"

	if [[ $LIST == *"$filetype1/"* ]]
	then
		echo "copy to $shellpath/$filename/$filetype1"
	    cp -P $LIST "$shellpath/$filename/$filetype1"
	fi;

	if [[ $LIST == *"$filetype2/"* ]]
	then
		echo "copy to $filename/$filetype2"
	    cp -P $LIST "$shellpath/$filename/$filetype2"
	fi;

	if [[ $LIST == *"$filetype3/"* ]]
	then
		echo "copy to $filename/$filetype3"
	    cp -P $LIST "$shellpath/$filename/$filetype3"
	fi;

	if [[ $LIST == *"$filetype4/"* ]]
	then
		echo "copy to $filename/$filetype4"
	    cp -P $LIST "$shellpath/$filename/$filetype4"
	fi;

	if [[ $LIST == *"$filetype5/"* ]]
	then
		echo "copy to $filename/$filetype5"
	    cp -P $LIST "$shellpath/$filename/$filetype5"
	fi;

	if [[ $LIST == *"$filetype6/"* ]]
	then
		echo "copy to $filename/$filetype6"
	    cp -P $LIST "$shellpath/$filename/$filetype6"
	fi;

done

echo "compress files"
cd $shellpath
zip -r ./$filename.zip ./$filename/*

mv ./$filename.zip ../download/$filename.zip

rm -rf ./$filename
rm -rf ./$filename.txt
echo "end"

exit 0



